var requirejs, require, define;
!function(global) {
    function isFunction(t) {
        return "[object Function]" === ostring.call(t)
    }
    function isArray(t) {
        return "[object Array]" === ostring.call(t)
    }
    function each(t, e) {
        if (t) {
            var n;
            for (n = 0; n < t.length && (!t[n] || !e(t[n], n, t)); n += 1)
                ;
        }
    }
    function eachReverse(t, e) {
        if (t) {
            var n;
            for (n = t.length - 1; n > -1 && (!t[n] || !e(t[n], n, t)); n -= 1)
                ;
        }
    }
    function hasProp(t, e) {
        return hasOwn.call(t, e)
    }
    function getOwn(t, e) {
        return hasProp(t, e) && t[e]
    }
    function eachProp(t, e) {
        var n;
        for (n in t)
            if (hasProp(t, n) && e(t[n], n))
                break
    }
    function mixin(t, e, n, i) {
        return e && eachProp(e, function(e, r) {
            (n || !hasProp(t, r)) && (!i || "object" != typeof e || !e || isArray(e) || isFunction(e) || e instanceof RegExp ? t[r] = e : (t[r] || (t[r] = {}),
            mixin(t[r], e, n, i)))
        }),
        t
    }
    function bind(t, e) {
        return function() {
            return e.apply(t, arguments)
        }
    }
    function scripts() {
        return document.getElementsByTagName("script")
    }
    function defaultOnError(t) {
        throw t
    }
    function getGlobal(t) {
        if (!t)
            return t;
        var e = global;
        return each(t.split("."), function(t) {
            e = e[t]
        }),
        e
    }
    function makeError(t, e, n, i) {
        var r = new Error(e + "\nhttp://requirejs.org/docs/errors.html#" + t);
        return r.requireType = t,
        r.requireModules = i,
        n && (r.originalError = n),
        r
    }
    function newContext(t) {
        function e(t) {
            var e, n;
            for (e = 0; e < t.length; e++)
                if (n = t[e],
                "." === n)
                    t.splice(e, 1),
                    e -= 1;
                else if (".." === n) {
                    if (0 === e || 1 === e && ".." === t[2] || ".." === t[e - 1])
                        continue;
                    e > 0 && (t.splice(e - 1, 2),
                    e -= 2)
                }
        }
        function n(t, n, i) {
            var r, o, s, a, l, u, c, h, p, f, d, m, g = n && n.split("/"), v = T.map, y = v && v["*"];
            if (t && (t = t.split("/"),
            c = t.length - 1,
            T.nodeIdCompat && jsSuffixRegExp.test(t[c]) && (t[c] = t[c].replace(jsSuffixRegExp, "")),
            "." === t[0].charAt(0) && g && (m = g.slice(0, g.length - 1),
            t = m.concat(t)),
            e(t),
            t = t.join("/")),
            i && v && (g || y)) {
                s = t.split("/");
                t: for (a = s.length; a > 0; a -= 1) {
                    if (u = s.slice(0, a).join("/"),
                    g)
                        for (l = g.length; l > 0; l -= 1)
                            if (o = getOwn(v, g.slice(0, l).join("/")),
                            o && (o = getOwn(o, u))) {
                                h = o,
                                p = a;
                                break t
                            }
                    !f && y && getOwn(y, u) && (f = getOwn(y, u),
                    d = a)
                }
                !h && f && (h = f,
                p = d),
                h && (s.splice(0, p, h),
                t = s.join("/"))
            }
            return r = getOwn(T.pkgs, t),
            r ? r : t
        }
        function i(t) {
            isBrowser && each(scripts(), function(e) {
                return e.getAttribute("data-requiremodule") === t && e.getAttribute("data-requirecontext") === x.contextName ? (e.parentNode.removeChild(e),
                !0) : void 0
            })
        }
        function r(t) {
            var e = getOwn(T.paths, t);
            return e && isArray(e) && e.length > 1 ? (e.shift(),
            x.require.undef(t),
            x.makeRequire(null, {
                skipMap: !0
            })([t]),
            !0) : void 0
        }
        function o(t) {
            var e, n = t ? t.indexOf("!") : -1;
            return n > -1 && (e = t.substring(0, n),
            t = t.substring(n + 1, t.length)),
            [e, t]
        }
        function s(t, e, i, r) {
            var s, a, l, u, c = null, h = e ? e.name : null, p = t, f = !0, d = "";
            return t || (f = !1,
            t = "_@r" + (M += 1)),
            u = o(t),
            c = u[0],
            t = u[1],
            c && (c = n(c, h, r),
            a = getOwn(P, c)),
            t && (c ? d = a && a.normalize ? a.normalize(t, function(t) {
                return n(t, h, r)
            }) : -1 === t.indexOf("!") ? n(t, h, r) : t : (d = n(t, h, r),
            u = o(d),
            c = u[0],
            d = u[1],
            i = !0,
            s = x.nameToUrl(d))),
            l = !c || a || i ? "" : "_unnormalized" + (D += 1),
            {
                prefix: c,
                name: d,
                parentMap: e,
                unnormalized: !!l,
                url: s,
                originalName: p,
                isDefine: f,
                id: (c ? c + "!" + d : d) + l
            }
        }
        function a(t) {
            var e = t.id
              , n = getOwn(C, e);
            return n || (n = C[e] = new x.Module(t)),
            n
        }
        function l(t, e, n) {
            var i = t.id
              , r = getOwn(C, i);
            !hasProp(P, i) || r && !r.defineEmitComplete ? (r = a(t),
            r.error && "error" === e ? n(r.error) : r.on(e, n)) : "defined" === e && n(P[i])
        }
        function u(t, e) {
            var n = t.requireModules
              , i = !1;
            e ? e(t) : (each(n, function(e) {
                var n = getOwn(C, e);
                n && (n.error = t,
                n.events.error && (i = !0,
                n.emit("error", t)))
            }),
            i || req.onError(t))
        }
        function c() {
            globalDefQueue.length && (each(globalDefQueue, function(t) {
                var e = t[0];
                "string" == typeof e && (x.defQueueMap[e] = !0),
                j.push(t)
            }),
            globalDefQueue = [])
        }
        function h(t) {
            delete C[t],
            delete S[t]
        }
        function p(t, e, n) {
            var i = t.map.id;
            t.error ? t.emit("error", t.error) : (e[i] = !0,
            each(t.depMaps, function(i, r) {
                var o = i.id
                  , s = getOwn(C, o);
                !s || t.depMatched[r] || n[o] || (getOwn(e, o) ? (t.defineDep(r, P[o]),
                t.check()) : p(s, e, n))
            }),
            n[i] = !0)
        }
        function f() {
            var t, e, n = 1e3 * T.waitSeconds, o = n && x.startTime + n < (new Date).getTime(), s = [], a = [], l = !1, c = !0;
            if (!y) {
                if (y = !0,
                eachProp(S, function(t) {
                    var n = t.map
                      , u = n.id;
                    if (t.enabled && (n.isDefine || a.push(t),
                    !t.error))
                        if (!t.inited && o)
                            r(u) ? (e = !0,
                            l = !0) : (s.push(u),
                            i(u));
                        else if (!t.inited && t.fetched && n.isDefine && (l = !0,
                        !n.prefix))
                            return c = !1
                }),
                o && s.length)
                    return t = makeError("timeout", "Load timeout for modules: " + s, null, s),
                    t.contextName = x.contextName,
                    u(t);
                c && each(a, function(t) {
                    p(t, {}, {})
                }),
                o && !e || !l || !isBrowser && !isWebWorker || b || (b = setTimeout(function() {
                    b = 0,
                    f()
                }, 50)),
                y = !1
            }
        }
        function d(t) {
            hasProp(P, t[0]) || a(s(t[0], null, !0)).init(t[1], t[2])
        }
        function m(t, e, n, i) {
            t.detachEvent && !isOpera ? i && t.detachEvent(i, e) : t.removeEventListener(n, e, !1)
        }
        function g(t) {
            var e = t.currentTarget || t.srcElement;
            return m(e, x.onScriptLoad, "load", "onreadystatechange"),
            m(e, x.onScriptError, "error"),
            {
                node: e,
                id: e && e.getAttribute("data-requiremodule")
            }
        }
        function v() {
            var t;
            for (c(); j.length; ) {
                if (t = j.shift(),
                null === t[0])
                    return u(makeError("mismatch", "Mismatched anonymous define() module: " + t[t.length - 1]));
                d(t)
            }
            x.defQueueMap = {}
        }
        var y, _, x, w, b, T = {
            waitSeconds: 7,
            baseUrl: "./",
            paths: {},
            bundles: {},
            pkgs: {},
            shim: {},
            config: {}
        }, C = {}, S = {}, k = {}, j = [], P = {}, E = {}, O = {}, M = 1, D = 1;
        return w = {
            require: function(t) {
                return t.require ? t.require : t.require = x.makeRequire(t.map)
            },
            exports: function(t) {
                return t.usingExports = !0,
                t.map.isDefine ? t.exports ? P[t.map.id] = t.exports : t.exports = P[t.map.id] = {} : void 0
            },
            module: function(t) {
                return t.module ? t.module : t.module = {
                    id: t.map.id,
                    uri: t.map.url,
                    config: function() {
                        return getOwn(T.config, t.map.id) || {}
                    },
                    exports: t.exports || (t.exports = {})
                }
            }
        },
        _ = function(t) {
            this.events = getOwn(k, t.id) || {},
            this.map = t,
            this.shim = getOwn(T.shim, t.id),
            this.depExports = [],
            this.depMaps = [],
            this.depMatched = [],
            this.pluginMaps = {},
            this.depCount = 0
        }
        ,
        _.prototype = {
            init: function(t, e, n, i) {
                i = i || {},
                this.inited || (this.factory = e,
                n ? this.on("error", n) : this.events.error && (n = bind(this, function(t) {
                    this.emit("error", t)
                })),
                this.depMaps = t && t.slice(0),
                this.errback = n,
                this.inited = !0,
                this.ignore = i.ignore,
                i.enabled || this.enabled ? this.enable() : this.check())
            },
            defineDep: function(t, e) {
                this.depMatched[t] || (this.depMatched[t] = !0,
                this.depCount -= 1,
                this.depExports[t] = e)
            },
            fetch: function() {
                if (!this.fetched) {
                    this.fetched = !0,
                    x.startTime = (new Date).getTime();
                    var t = this.map;
                    return this.shim ? void x.makeRequire(this.map, {
                        enableBuildCallback: !0
                    })(this.shim.deps || [], bind(this, function() {
                        return t.prefix ? this.callPlugin() : this.load()
                    })) : t.prefix ? this.callPlugin() : this.load()
                }
            },
            load: function() {
                var t = this.map.url;
                E[t] || (E[t] = !0,
                x.load(this.map.id, t))
            },
            check: function() {
                if (this.enabled && !this.enabling) {
                    var t, e, n = this.map.id, i = this.depExports, r = this.exports, o = this.factory;
                    if (this.inited) {
                        if (this.error)
                            this.emit("error", this.error);
                        else if (!this.defining) {
                            if (this.defining = !0,
                            this.depCount < 1 && !this.defined) {
                                if (isFunction(o)) {
                                    if (this.events.error && this.map.isDefine || req.onError !== defaultOnError)
                                        try {
                                            r = x.execCb(n, o, i, r)
                                        } catch (s) {
                                            t = s
                                        }
                                    else
                                        r = x.execCb(n, o, i, r);
                                    if (this.map.isDefine && void 0 === r && (e = this.module,
                                    e ? r = e.exports : this.usingExports && (r = this.exports)),
                                    t)
                                        return t.requireMap = this.map,
                                        t.requireModules = this.map.isDefine ? [this.map.id] : null,
                                        t.requireType = this.map.isDefine ? "define" : "require",
                                        u(this.error = t)
                                } else
                                    r = o;
                                this.exports = r,
                                this.map.isDefine && !this.ignore && (P[n] = r,
                                req.onResourceLoad && req.onResourceLoad(x, this.map, this.depMaps)),
                                h(n),
                                this.defined = !0
                            }
                            this.defining = !1,
                            this.defined && !this.defineEmitted && (this.defineEmitted = !0,
                            this.emit("defined", this.exports),
                            this.defineEmitComplete = !0)
                        }
                    } else
                        hasProp(x.defQueueMap, n) || this.fetch()
                }
            },
            callPlugin: function() {
                var t = this.map
                  , e = t.id
                  , i = s(t.prefix);
                this.depMaps.push(i),
                l(i, "defined", bind(this, function(i) {
                    var r, o, c, p = getOwn(O, this.map.id), f = this.map.name, d = this.map.parentMap ? this.map.parentMap.name : null, m = x.makeRequire(t.parentMap, {
                        enableBuildCallback: !0
                    });
                    return this.map.unnormalized ? (i.normalize && (f = i.normalize(f, function(t) {
                        return n(t, d, !0)
                    }) || ""),
                    o = s(t.prefix + "!" + f, this.map.parentMap),
                    l(o, "defined", bind(this, function(t) {
                        this.init([], function() {
                            return t
                        }, null, {
                            enabled: !0,
                            ignore: !0
                        })
                    })),
                    c = getOwn(C, o.id),
                    void (c && (this.depMaps.push(o),
                    this.events.error && c.on("error", bind(this, function(t) {
                        this.emit("error", t)
                    })),
                    c.enable()))) : p ? (this.map.url = x.nameToUrl(p),
                    void this.load()) : (r = bind(this, function(t) {
                        this.init([], function() {
                            return t
                        }, null, {
                            enabled: !0
                        })
                    }),
                    r.error = bind(this, function(t) {
                        this.inited = !0,
                        this.error = t,
                        t.requireModules = [e],
                        eachProp(C, function(t) {
                            0 === t.map.id.indexOf(e + "_unnormalized") && h(t.map.id)
                        }),
                        u(t)
                    }),
                    r.fromText = bind(this, function(n, i) {
                        var o = t.name
                          , l = s(o)
                          , c = useInteractive;
                        i && (n = i),
                        c && (useInteractive = !1),
                        a(l),
                        hasProp(T.config, e) && (T.config[o] = T.config[e]);
                        try {
                            req.exec(n)
                        } catch (h) {
                            return u(makeError("fromtexteval", "fromText eval for " + e + " failed: " + h, h, [e]))
                        }
                        c && (useInteractive = !0),
                        this.depMaps.push(l),
                        x.completeLoad(o),
                        m([o], r)
                    }),
                    void i.load(t.name, m, r, T))
                })),
                x.enable(i, this),
                this.pluginMaps[i.id] = i
            },
            enable: function() {
                S[this.map.id] = this,
                this.enabled = !0,
                this.enabling = !0,
                each(this.depMaps, bind(this, function(t, e) {
                    var n, i, r;
                    if ("string" == typeof t) {
                        if (t = s(t, this.map.isDefine ? this.map : this.map.parentMap, !1, !this.skipMap),
                        this.depMaps[e] = t,
                        r = getOwn(w, t.id))
                            return void (this.depExports[e] = r(this));
                        this.depCount += 1,
                        l(t, "defined", bind(this, function(t) {
                            this.undefed || (this.defineDep(e, t),
                            this.check())
                        })),
                        this.errback ? l(t, "error", bind(this, this.errback)) : this.events.error && l(t, "error", bind(this, function(t) {
                            this.emit("error", t)
                        }))
                    }
                    n = t.id,
                    i = C[n],
                    hasProp(w, n) || !i || i.enabled || x.enable(t, this)
                })),
                eachProp(this.pluginMaps, bind(this, function(t) {
                    var e = getOwn(C, t.id);
                    e && !e.enabled && x.enable(t, this)
                })),
                this.enabling = !1,
                this.check()
            },
            on: function(t, e) {
                var n = this.events[t];
                n || (n = this.events[t] = []),
                n.push(e)
            },
            emit: function(t, e) {
                each(this.events[t], function(t) {
                    t(e)
                }),
                "error" === t && delete this.events[t]
            }
        },
        x = {
            config: T,
            contextName: t,
            registry: C,
            defined: P,
            urlFetched: E,
            defQueue: j,
            defQueueMap: {},
            Module: _,
            makeModuleMap: s,
            nextTick: req.nextTick,
            onError: u,
            configure: function(t) {
                t.baseUrl && "/" !== t.baseUrl.charAt(t.baseUrl.length - 1) && (t.baseUrl += "/");
                var e = T.shim
                  , n = {
                    paths: !0,
                    bundles: !0,
                    config: !0,
                    map: !0
                };
                eachProp(t, function(t, e) {
                    n[e] ? (T[e] || (T[e] = {}),
                    mixin(T[e], t, !0, !0)) : T[e] = t
                }),
                t.bundles && eachProp(t.bundles, function(t, e) {
                    each(t, function(t) {
                        t !== e && (O[t] = e)
                    })
                }),
                t.shim && (eachProp(t.shim, function(t, n) {
                    isArray(t) && (t = {
                        deps: t
                    }),
                    !t.exports && !t.init || t.exportsFn || (t.exportsFn = x.makeShimExports(t)),
                    e[n] = t
                }),
                T.shim = e),
                t.packages && each(t.packages, function(t) {
                    var e, n;
                    t = "string" == typeof t ? {
                        name: t
                    } : t,
                    n = t.name,
                    e = t.location,
                    e && (T.paths[n] = t.location),
                    T.pkgs[n] = t.name + "/" + (t.main || "main").replace(currDirRegExp, "").replace(jsSuffixRegExp, "")
                }),
                eachProp(C, function(t, e) {
                    t.inited || t.map.unnormalized || (t.map = s(e, null, !0))
                }),
                (t.deps || t.callback) && x.require(t.deps || [], t.callback)
            },
            makeShimExports: function(t) {
                function e() {
                    var e;
                    return t.init && (e = t.init.apply(global, arguments)),
                    e || t.exports && getGlobal(t.exports)
                }
                return e
            },
            makeRequire: function(e, r) {
                function o(n, i, l) {
                    var c, h, p;
                    return r.enableBuildCallback && i && isFunction(i) && (i.__requireJsBuild = !0),
                    "string" == typeof n ? isFunction(i) ? u(makeError("requireargs", "Invalid require call"), l) : e && hasProp(w, n) ? w[n](C[e.id]) : req.get ? req.get(x, n, e, o) : (h = s(n, e, !1, !0),
                    c = h.id,
                    hasProp(P, c) ? P[c] : u(makeError("notloaded", 'Module name "' + c + '" has not been loaded yet for context: ' + t + (e ? "" : ". Use require([])")))) : (v(),
                    x.nextTick(function() {
                        v(),
                        p = a(s(null, e)),
                        p.skipMap = r.skipMap,
                        p.init(n, i, l, {
                            enabled: !0
                        }),
                        f()
                    }),
                    o)
                }
                return r = r || {},
                mixin(o, {
                    isBrowser: isBrowser,
                    toUrl: function(t) {
                        var i, r = t.lastIndexOf("."), o = t.split("/")[0], s = "." === o || ".." === o;
                        return -1 !== r && (!s || r > 1) && (i = t.substring(r, t.length),
                        t = t.substring(0, r)),
                        x.nameToUrl(n(t, e && e.id, !0), i, !0)
                    },
                    defined: function(t) {
                        return hasProp(P, s(t, e, !1, !0).id)
                    },
                    specified: function(t) {
                        return t = s(t, e, !1, !0).id,
                        hasProp(P, t) || hasProp(C, t)
                    }
                }),
                e || (o.undef = function(t) {
                    c();
                    var n = s(t, e, !0)
                      , r = getOwn(C, t);
                    r.undefed = !0,
                    i(t),
                    delete P[t],
                    delete E[n.url],
                    delete k[t],
                    eachReverse(j, function(e, n) {
                        e[0] === t && j.splice(n, 1)
                    }),
                    delete x.defQueueMap[t],
                    r && (r.events.defined && (k[t] = r.events),
                    h(t))
                }
                ),
                o
            },
            enable: function(t) {
                var e = getOwn(C, t.id);
                e && a(t).enable()
            },
            completeLoad: function(t) {
                var e, n, i, o = getOwn(T.shim, t) || {}, s = o.exports;
                for (c(); j.length; ) {
                    if (n = j.shift(),
                    null === n[0]) {
                        if (n[0] = t,
                        e)
                            break;
                        e = !0
                    } else
                        n[0] === t && (e = !0);
                    d(n)
                }
                if (x.defQueueMap = {},
                i = getOwn(C, t),
                !e && !hasProp(P, t) && i && !i.inited) {
                    if (!(!T.enforceDefine || s && getGlobal(s)))
                        return r(t) ? void 0 : u(makeError("nodefine", "No define call for " + t, null, [t]));
                    d([t, o.deps || [], o.exportsFn])
                }
                f()
            },
            nameToUrl: function(t, e, n) {
                var i, r, o, s, a, l, u, c = getOwn(T.pkgs, t);
                if (c && (t = c),
                u = getOwn(O, t))
                    return x.nameToUrl(u, e, n);
                if (req.jsExtRegExp.test(t))
                    a = t + (e || "");
                else {
                    for (i = T.paths,
                    r = t.split("/"),
                    o = r.length; o > 0; o -= 1)
                        if (s = r.slice(0, o).join("/"),
                        l = getOwn(i, s)) {
                            isArray(l) && (l = l[0]),
                            r.splice(0, o, l);
                            break
                        }
                    a = r.join("/"),
                    a += e || (/^data\:|\?/.test(a) || n ? "" : ".js"),
                    a = ("/" === a.charAt(0) || a.match(/^[\w\+\.\-]+:/) ? "" : T.baseUrl) + a
                }
                return T.urlArgs ? a + ((-1 === a.indexOf("?") ? "?" : "&") + T.urlArgs) : a
            },
            load: function(t, e) {
                req.load(x, t, e)
            },
            execCb: function(t, e, n, i) {
                return e.apply(i, n)
            },
            onScriptLoad: function(t) {
                if ("load" === t.type || readyRegExp.test((t.currentTarget || t.srcElement).readyState)) {
                    interactiveScript = null;
                    var e = g(t);
                    x.completeLoad(e.id)
                }
            },
            onScriptError: function(t) {
                var e = g(t);
                return r(e.id) ? void 0 : u(makeError("scripterror", "Script error for: " + e.id, t, [e.id]))
            }
        },
        x.require = x.makeRequire(),
        x
    }
    function getInteractiveScript() {
        return interactiveScript && "interactive" === interactiveScript.readyState ? interactiveScript : (eachReverse(scripts(), function(t) {
            return "interactive" === t.readyState ? interactiveScript = t : void 0
        }),
        interactiveScript)
    }
    var req, s, head, baseElement, dataMain, src, interactiveScript, currentlyAddingScript, mainScript, subPath, version = "2.1.20", commentRegExp = /(\/\*([\s\S]*?)\*\/|([^:]|^)\/\/(.*)$)/gm, cjsRequireRegExp = /[^.]\s*require\s*\(\s*["']([^'"\s]+)["']\s*\)/g, jsSuffixRegExp = /\.js$/, currDirRegExp = /^\.\//, op = Object.prototype, ostring = op.toString, hasOwn = op.hasOwnProperty, ap = Array.prototype, isBrowser = !("undefined" == typeof window || "undefined" == typeof navigator || !window.document), isWebWorker = !isBrowser && "undefined" != typeof importScripts, readyRegExp = isBrowser && "PLAYSTATION 3" === navigator.platform ? /^complete$/ : /^(complete|loaded)$/, defContextName = "_", isOpera = "undefined" != typeof opera && "[object Opera]" === opera.toString(), contexts = {}, cfg = {}, globalDefQueue = [], useInteractive = !1;
    if ("undefined" == typeof define) {
        if ("undefined" != typeof requirejs) {
            if (isFunction(requirejs))
                return;
            cfg = requirejs,
            requirejs = void 0
        }
        "undefined" == typeof require || isFunction(require) || (cfg = require,
        require = void 0),
        req = requirejs = function(t, e, n, i) {
            var r, o, s = defContextName;
            return isArray(t) || "string" == typeof t || (o = t,
            isArray(e) ? (t = e,
            e = n,
            n = i) : t = []),
            o && o.context && (s = o.context),
            r = getOwn(contexts, s),
            r || (r = contexts[s] = req.s.newContext(s)),
            o && r.configure(o),
            r.require(t, e, n)
        }
        ,
        req.config = function(t) {
            return req(t)
        }
        ,
        req.nextTick = "undefined" != typeof setTimeout ? function(t) {
            setTimeout(t, 4)
        }
        : function(t) {
            t()
        }
        ,
        require || (require = req),
        req.version = version,
        req.jsExtRegExp = /^\/|:|\?|\.js$/,
        req.isBrowser = isBrowser,
        s = req.s = {
            contexts: contexts,
            newContext: newContext
        },
        req({}),
        each(["toUrl", "undef", "defined", "specified"], function(t) {
            req[t] = function() {
                var e = contexts[defContextName];
                return e.require[t].apply(e, arguments)
            }
        }),
        isBrowser && (head = s.head = document.getElementsByTagName("head")[0],
        baseElement = document.getElementsByTagName("base")[0],
        baseElement && (head = s.head = baseElement.parentNode)),
        req.onError = defaultOnError,
        req.createNode = function(t, e, n) {
            var i = t.xhtml ? document.createElementNS("http://www.w3.org/1999/xhtml", "html:script") : document.createElement("script");
            return i.type = t.scriptType || "text/javascript",
            i.charset = "utf-8",
            i.async = !0,
            i
        }
        ,
        req.load = function(t, e, n) {
            var i, r = t && t.config || {};
            if (isBrowser)
                return i = req.createNode(r, e, n),
                r.onNodeCreated && r.onNodeCreated(i, r, e, n),
                i.setAttribute("data-requirecontext", t.contextName),
                i.setAttribute("data-requiremodule", e),
                !i.attachEvent || i.attachEvent.toString && i.attachEvent.toString().indexOf("[native code") < 0 || isOpera ? (i.addEventListener("load", t.onScriptLoad, !1),
                i.addEventListener("error", t.onScriptError, !1)) : (useInteractive = !0,
                i.attachEvent("onreadystatechange", t.onScriptLoad)),
                i.src = n,
                currentlyAddingScript = i,
                baseElement ? head.insertBefore(i, baseElement) : head.appendChild(i),
                currentlyAddingScript = null,
                i;
            if (isWebWorker)
                try {
                    importScripts(n),
                    t.completeLoad(e)
                } catch (o) {
                    t.onError(makeError("importscripts", "importScripts failed for " + e + " at " + n, o, [e]))
                }
        }
        ,
        isBrowser && !cfg.skipDataMain && eachReverse(scripts(), function(t) {
            return head || (head = t.parentNode),
            dataMain = t.getAttribute("data-main"),
            dataMain ? (mainScript = dataMain,
            cfg.baseUrl || (src = mainScript.split("/"),
            mainScript = src.pop(),
            subPath = src.length ? src.join("/") + "/" : "./",
            cfg.baseUrl = subPath),
            mainScript = mainScript.replace(jsSuffixRegExp, ""),
            req.jsExtRegExp.test(mainScript) && (mainScript = dataMain),
            cfg.deps = cfg.deps ? cfg.deps.concat(mainScript) : [mainScript],
            !0) : void 0
        }),
        define = function(t, e, n) {
            var i, r;
            "string" != typeof t && (n = e,
            e = t,
            t = null),
            isArray(e) || (n = e,
            e = null),
            !e && isFunction(n) && (e = [],
            n.length && (n.toString().replace(commentRegExp, "").replace(cjsRequireRegExp, function(t, n) {
                e.push(n)
            }),
            e = (1 === n.length ? ["require"] : ["require", "exports", "module"]).concat(e))),
            useInteractive && (i = currentlyAddingScript || getInteractiveScript(),
            i && (t || (t = i.getAttribute("data-requiremodule")),
            r = contexts[i.getAttribute("data-requirecontext")])),
            r ? (r.defQueue.push([t, e, n]),
            r.defQueueMap[t] = !0) : globalDefQueue.push([t, e, n])
        }
        ,
        define.amd = {
            jQuery: !0
        },
        req.exec = function(text) {
            return eval(text)
        }
        ,
        req(cfg)
    }
}(this),
define("requireLib", function() {}),
window.Modernizr = function(t, e, n) {
    function i(t) {
        y.cssText = t
    }
    function r(t, e) {
        return typeof t === e
    }
    function o(t, e) {
        return !!~("" + t).indexOf(e)
    }
    function s(t, e) {
        for (var i in t) {
            var r = t[i];
            if (!o(r, "-") && y[r] !== n)
                return "pfx" == e ? r : !0
        }
        return !1
    }
    function a(t, e, i) {
        for (var o in t) {
            var s = e[t[o]];
            if (s !== n)
                return i === !1 ? t[o] : r(s, "function") ? s.bind(i || e) : s
        }
        return !1
    }
    function l(t, e, n) {
        var i = t.charAt(0).toUpperCase() + t.slice(1)
          , o = (t + " " + w.join(i + " ") + i).split(" ");
        return r(e, "string") || r(e, "undefined") ? s(o, e) : (o = (t + " " + b.join(i + " ") + i).split(" "),
        a(o, e, n))
    }
    var u, c, h, p = "2.6.2", f = {}, d = !0, m = e.documentElement, g = "modernizr", v = e.createElement(g), y = v.style, _ = ({}.toString,
    " -webkit- -moz- -o- -ms- ".split(" ")), x = "Webkit Moz O ms", w = x.split(" "), b = x.toLowerCase().split(" "), T = {
        svg: "http://www.w3.org/2000/svg"
    }, C = {}, S = [], k = S.slice, j = function(t, n, i, r) {
        var o, s, a, l, u = e.createElement("div"), c = e.body, h = c || e.createElement("body");
        if (parseInt(i, 10))
            for (; i--; )
                a = e.createElement("div"),
                a.id = r ? r[i] : g + (i + 1),
                u.appendChild(a);
        return o = ["&#173;", '<style id="s', g, '">', t, "</style>"].join(""),
        u.id = g,
        (c ? u : h).innerHTML += o,
        h.appendChild(u),
        c || (h.style.background = "",
        h.style.overflow = "hidden",
        l = m.style.overflow,
        m.style.overflow = "hidden",
        m.appendChild(h)),
        s = n(u, t),
        c ? u.parentNode.removeChild(u) : (h.parentNode.removeChild(h),
        m.style.overflow = l),
        !!s
    }, P = function(e) {
        var n = t.matchMedia || t.msMatchMedia;
        if (n)
            return n(e).matches;
        var i;
        return j("@media " + e + " { #" + g + " { position: absolute; } }", function(e) {
            i = "absolute" == (t.getComputedStyle ? getComputedStyle(e, null) : e.currentStyle).position
        }),
        i
    }, E = {}.hasOwnProperty;
    h = r(E, "undefined") || r(E.call, "undefined") ? function(t, e) {
        return e in t && r(t.constructor.prototype[e], "undefined")
    }
    : function(t, e) {
        return E.call(t, e)
    }
    ,
    Function.prototype.bind || (Function.prototype.bind = function(t) {
        var e = this;
        if ("function" != typeof e)
            throw new TypeError;
        var n = k.call(arguments, 1)
          , i = function() {
            if (this instanceof i) {
                var r = function() {};
                r.prototype = e.prototype;
                var o = new r
                  , s = e.apply(o, n.concat(k.call(arguments)));
                return Object(s) === s ? s : o
            }
            return e.apply(t, n.concat(k.call(arguments)))
        };
        return i
    }
    ),
    C.touch = function() {
        var n;
        return "ontouchstart"in t || t.DocumentTouch && e instanceof DocumentTouch ? n = !0 : j(["@media (", _.join("touch-enabled),("), g, ")", "{#modernizr{top:9px;position:absolute}}"].join(""), function(t) {
            n = 9 === t.offsetTop
        }),
        n
    }
    ,
    C.history = function() {
        return !!t.history && !!history.pushState
    }
    ,
    C.cssanimations = function() {
        return l("animationName")
    }
    ,
    C.csstransforms = function() {
        return !!l("transform")
    }
    ,
    C.csstransforms3d = function() {
        var t = !!l("perspective");
        return t && "webkitPerspective"in m.style && j("@media (transform-3d),(-webkit-transform-3d){#modernizr{left:9px;position:absolute;height:3px;}}", function(e, n) {
            t = 9 === e.offsetLeft && 3 === e.offsetHeight
        }),
        t
    }
    ,
    C.csstransitions = function() {
        return l("transition")
    }
    ,
    C.svg = function() {
        return !!e.createElementNS && !!e.createElementNS(T.svg, "svg").createSVGRect
    }
    ;
    for (var O in C)
        h(C, O) && (c = O.toLowerCase(),
        f[c] = C[O](),
        S.push((f[c] ? "" : "no-") + c));
    return f.addTest = function(t, e) {
        if ("object" == typeof t)
            for (var i in t)
                h(t, i) && f.addTest(i, t[i]);
        else {
            if (t = t.toLowerCase(),
            f[t] !== n)
                return f;
            e = "function" == typeof e ? e() : e,
            "undefined" != typeof d && d && (m.className += " " + (e ? "" : "no-") + t),
            f[t] = e
        }
        return f
    }
    ,
    i(""),
    v = u = null,
    function(t, e) {
        function n(t, e) {
            var n = t.createElement("p")
              , i = t.getElementsByTagName("head")[0] || t.documentElement;
            return n.innerHTML = "x<style>" + e + "</style>",
            i.insertBefore(n.lastChild, i.firstChild)
        }
        function i() {
            var t = v.elements;
            return "string" == typeof t ? t.split(" ") : t
        }
        function r(t) {
            var e = g[t[d]];
            return e || (e = {},
            m++,
            t[d] = m,
            g[m] = e),
            e
        }
        function o(t, n, i) {
            if (n || (n = e),
            c)
                return n.createElement(t);
            i || (i = r(n));
            var o;
            return o = i.cache[t] ? i.cache[t].cloneNode() : f.test(t) ? (i.cache[t] = i.createElem(t)).cloneNode() : i.createElem(t),
            o.canHaveChildren && !p.test(t) ? i.frag.appendChild(o) : o
        }
        function s(t, n) {
            if (t || (t = e),
            c)
                return t.createDocumentFragment();
            n = n || r(t);
            for (var o = n.frag.cloneNode(), s = 0, a = i(), l = a.length; l > s; s++)
                o.createElement(a[s]);
            return o
        }
        function a(t, e) {
            e.cache || (e.cache = {},
            e.createElem = t.createElement,
            e.createFrag = t.createDocumentFragment,
            e.frag = e.createFrag()),
            t.createElement = function(n) {
                return v.shivMethods ? o(n, t, e) : e.createElem(n)
            }
            ,
            t.createDocumentFragment = Function("h,f", "return function(){var n=f.cloneNode(),c=n.createElement;h.shivMethods&&(" + i().join().replace(/\w+/g, function(t) {
                return e.createElem(t),
                e.frag.createElement(t),
                'c("' + t + '")'
            }) + ");return n}")(v, e.frag)
        }
        function l(t) {
            t || (t = e);
            var i = r(t);
            return v.shivCSS && !u && !i.hasCSS && (i.hasCSS = !!n(t, "article,aside,figcaption,figure,footer,header,hgroup,nav,section{display:block}mark{background:#FF0;color:#000}")),
            c || a(t, i),
            t
        }
        var u, c, h = t.html5 || {}, p = /^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i, f = /^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i, d = "_html5shiv", m = 0, g = {};
        !function() {
            try {
                var t = e.createElement("a");
                t.innerHTML = "<xyz></xyz>",
                u = "hidden"in t,
                c = 1 == t.childNodes.length || function() {
                    e.createElement("a");
                    var t = e.createDocumentFragment();
                    return "undefined" == typeof t.cloneNode || "undefined" == typeof t.createDocumentFragment || "undefined" == typeof t.createElement
                }()
            } catch (n) {
                u = !0,
                c = !0
            }
        }();
        var v = {
            elements: h.elements || "abbr article aside audio bdi canvas data datalist details figcaption figure footer header hgroup mark meter nav output progress section summary time video",
            shivCSS: h.shivCSS !== !1,
            supportsUnknownElements: c,
            shivMethods: h.shivMethods !== !1,
            type: "default",
            shivDocument: l,
            createElement: o,
            createDocumentFragment: s
        };
        t.html5 = v,
        l(e)
    }(this, e),
    f._version = p,
    f._prefixes = _,
    f._domPrefixes = b,
    f._cssomPrefixes = w,
    f.mq = P,
    f.testProp = function(t) {
        return s([t])
    }
    ,
    f.testAllProps = l,
    f.testStyles = j,
    f.prefixed = function(t, e, n) {
        return e ? l(t, e, n) : l(t, "pfx")
    }
    ,
    m.className = m.className.replace(/(^|\s)no-js(\s|$)/, "$1$2") + (d ? " js " + S.join(" ") : ""),
    f
}(this, this.document),
function(t, e, n) {
    function i(t) {
        return "[object Function]" == g.call(t)
    }
    function r(t) {
        return "string" == typeof t
    }
    function o() {}
    function s(t) {
        return !t || "loaded" == t || "complete" == t || "uninitialized" == t
    }
    function a() {
        var t = v.shift();
        y = 1,
        t ? t.t ? d(function() {
            ("c" == t.t ? p.injectCss : p.injectJs)(t.s, 0, t.a, t.x, t.e, 1)
        }, 0) : (t(),
        a()) : y = 0
    }
    function l(t, n, i, r, o, l, u) {
        function c(e) {
            if (!f && s(h.readyState) && (_.r = f = 1,
            !y && a(),
            h.onload = h.onreadystatechange = null,
            e)) {
                "img" != t && d(function() {
                    w.removeChild(h)
                }, 50);
                for (var i in k[n])
                    k[n].hasOwnProperty(i) && k[n][i].onload()
            }
        }
        var u = u || p.errorTimeout
          , h = e.createElement(t)
          , f = 0
          , g = 0
          , _ = {
            t: i,
            s: n,
            e: o,
            a: l,
            x: u
        };
        1 === k[n] && (g = 1,
        k[n] = []),
        "object" == t ? h.data = n : (h.src = n,
        h.type = t),
        h.width = h.height = "0",
        h.onerror = h.onload = h.onreadystatechange = function() {
            c.call(this, g)
        }
        ,
        v.splice(r, 0, _),
        "img" != t && (g || 2 === k[n] ? (w.insertBefore(h, x ? null : m),
        d(c, u)) : k[n].push(h))
    }
    function u(t, e, n, i, o) {
        return y = 0,
        e = e || "j",
        r(t) ? l("c" == e ? T : b, t, e, this.i++, n, i, o) : (v.splice(this.i++, 0, t),
        1 == v.length && a()),
        this
    }
    function c() {
        var t = p;
        return t.loader = {
            load: u,
            i: 0
        },
        t
    }
    var h, p, f = e.documentElement, d = t.setTimeout, m = e.getElementsByTagName("script")[0], g = {}.toString, v = [], y = 0, _ = "MozAppearance"in f.style, x = _ && !!e.createRange().compareNode, w = x ? f : m.parentNode, f = t.opera && "[object Opera]" == g.call(t.opera), f = !!e.attachEvent && !f, b = _ ? "object" : f ? "script" : "img", T = f ? "script" : b, C = Array.isArray || function(t) {
        return "[object Array]" == g.call(t)
    }
    , S = [], k = {}, j = {
        timeout: function(t, e) {
            return e.length && (t.timeout = e[0]),
            t
        }
    };
    p = function(t) {
        function e(t) {
            var e, n, i, t = t.split("!"), r = S.length, o = t.pop(), s = t.length, o = {
                url: o,
                origUrl: o,
                prefixes: t
            };
            for (n = 0; s > n; n++)
                i = t[n].split("="),
                (e = j[i.shift()]) && (o = e(o, i));
            for (n = 0; r > n; n++)
                o = S[n](o);
            return o
        }
        function s(t, r, o, s, a) {
            var l = e(t)
              , u = l.autoCallback;
            l.url.split(".").pop().split("?").shift(),
            l.bypass || (r && (r = i(r) ? r : r[t] || r[s] || r[t.split("/").pop().split("?")[0]]),
            l.instead ? l.instead(t, r, o, s, a) : (k[l.url] ? l.noexec = !0 : k[l.url] = 1,
            o.load(l.url, l.forceCSS || !l.forceJS && "css" == l.url.split(".").pop().split("?").shift() ? "c" : n, l.noexec, l.attrs, l.timeout),
            (i(r) || i(u)) && o.load(function() {
                c(),
                r && r(l.origUrl, a, s),
                u && u(l.origUrl, a, s),
                k[l.url] = 2
            })))
        }
        function a(t, e) {
            function n(t, n) {
                if (t) {
                    if (r(t))
                        n || (h = function() {
                            var t = [].slice.call(arguments);
                            p.apply(this, t),
                            f()
                        }
                        ),
                        s(t, h, e, 0, u);
                    else if (Object(t) === t)
                        for (l in a = function() {
                            var e, n = 0;
                            for (e in t)
                                t.hasOwnProperty(e) && n++;
                            return n
                        }(),
                        t)
                            t.hasOwnProperty(l) && (!n && !--a && (i(h) ? h = function() {
                                var t = [].slice.call(arguments);
                                p.apply(this, t),
                                f()
                            }
                            : h[l] = function(t) {
                                return function() {
                                    var e = [].slice.call(arguments);
                                    t && t.apply(this, e),
                                    f()
                                }
                            }(p[l])),
                            s(t[l], h, e, l, u))
                } else
                    !n && f()
            }
            var a, l, u = !!t.test, c = t.load || t.both, h = t.callback || o, p = h, f = t.complete || o;
            n(u ? t.yep : t.nope, !!c),
            c && n(c)
        }
        var l, u, h = this.yepnope.loader;
        if (r(t))
            s(t, 0, h, 0);
        else if (C(t))
            for (l = 0; l < t.length; l++)
                u = t[l],
                r(u) ? s(u, 0, h, 0) : C(u) ? p(u) : Object(u) === u && a(u, h);
        else
            Object(t) === t && a(t, h)
    }
    ,
    p.addPrefix = function(t, e) {
        j[t] = e
    }
    ,
    p.addFilter = function(t) {
        S.push(t)
    }
    ,
    p.errorTimeout = 1e4,
    null == e.readyState && e.addEventListener && (e.readyState = "loading",
    e.addEventListener("DOMContentLoaded", h = function() {
        e.removeEventListener("DOMContentLoaded", h, 0),
        e.readyState = "complete"
    }
    , 0)),
    t.yepnope = c(),
    t.yepnope.executeStack = a,
    t.yepnope.injectJs = function(t, n, i, r, l, u) {
        var c, h, f = e.createElement("script"), r = r || p.errorTimeout;
        f.src = t;
        for (h in i)
            f.setAttribute(h, i[h]);
        n = u ? a : n || o,
        f.onreadystatechange = f.onload = function() {
            !c && s(f.readyState) && (c = 1,
            n(),
            f.onload = f.onreadystatechange = null)
        }
        ,
        d(function() {
            c || (c = 1,
            n(1))
        }, r),
        l ? f.onload() : m.parentNode.insertBefore(f, m)
    }
    ,
    t.yepnope.injectCss = function(t, n, i, r, s, l) {
        var u, r = e.createElement("link"), n = l ? a : n || o;
        r.href = t,
        r.rel = "stylesheet",
        r.type = "text/css";
        for (u in i)
            r.setAttribute(u, i[u]);
        s || (m.parentNode.insertBefore(r, m),
        d(n, 0))
    }
}(this, document),
Modernizr.load = function() {
    yepnope.apply(window, [].slice.call(arguments, 0))
}
,
define("modernizr", function() {}),
!function(t, e) {
    "object" == typeof module && "object" == typeof module.exports ? module.exports = t.document ? e(t, !0) : function(t) {
        if (!t.document)
            throw new Error("jQuery requires a window with a document");
        return e(t)
    }
    : e(t)
}("undefined" != typeof window ? window : this, function(t, e) {
    function n(t) {
        var e = "length"in t && t.length
          , n = K.type(t);
        return "function" === n || K.isWindow(t) ? !1 : 1 === t.nodeType && e ? !0 : "array" === n || 0 === e || "number" == typeof e && e > 0 && e - 1 in t
    }
    function i(t, e, n) {
        if (K.isFunction(e))
            return K.grep(t, function(t, i) {
                return !!e.call(t, i, t) !== n
            });
        if (e.nodeType)
            return K.grep(t, function(t) {
                return t === e !== n
            });
        if ("string" == typeof e) {
            if (at.test(e))
                return K.filter(e, t, n);
            e = K.filter(e, t)
        }
        return K.grep(t, function(t) {
            return V.call(e, t) >= 0 !== n
        })
    }
    function r(t, e) {
        for (; (t = t[e]) && 1 !== t.nodeType; )
            ;
        return t
    }
    function o(t) {
        var e = dt[t] = {};
        return K.each(t.match(ft) || [], function(t, n) {
            e[n] = !0
        }),
        e
    }
    function s() {
        J.removeEventListener("DOMContentLoaded", s, !1),
        t.removeEventListener("load", s, !1),
        K.ready()
    }
    function a() {
        Object.defineProperty(this.cache = {}, 0, {
            get: function() {
                return {}
            }
        }),
        this.expando = K.expando + a.uid++
    }
    function l(t, e, n) {
        var i;
        if (void 0 === n && 1 === t.nodeType)
            if (i = "data-" + e.replace(xt, "-$1").toLowerCase(),
            n = t.getAttribute(i),
            "string" == typeof n) {
                try {
                    n = "true" === n ? !0 : "false" === n ? !1 : "null" === n ? null : +n + "" === n ? +n : _t.test(n) ? K.parseJSON(n) : n
                } catch (r) {}
                yt.set(t, e, n)
            } else
                n = void 0;
        return n
    }
    function u() {
        return !0
    }
    function c() {
        return !1
    }
    function h() {
        try {
            return J.activeElement
        } catch (t) {}
    }
    function p(t, e) {
        return K.nodeName(t, "table") && K.nodeName(11 !== e.nodeType ? e : e.firstChild, "tr") ? t.getElementsByTagName("tbody")[0] || t.appendChild(t.ownerDocument.createElement("tbody")) : t
    }
    function f(t) {
        return t.type = (null !== t.getAttribute("type")) + "/" + t.type,
        t
    }
    function d(t) {
        var e = Rt.exec(t.type);
        return e ? t.type = e[1] : t.removeAttribute("type"),
        t
    }
    function m(t, e) {
        for (var n = 0, i = t.length; i > n; n++)
            vt.set(t[n], "globalEval", !e || vt.get(e[n], "globalEval"))
    }
    function g(t, e) {
        var n, i, r, o, s, a, l, u;
        if (1 === e.nodeType) {
            if (vt.hasData(t) && (o = vt.access(t),
            s = vt.set(e, o),
            u = o.events)) {
                delete s.handle,
                s.events = {};
                for (r in u)
                    for (n = 0,
                    i = u[r].length; i > n; n++)
                        K.event.add(e, r, u[r][n])
            }
            yt.hasData(t) && (a = yt.access(t),
            l = K.extend({}, a),
            yt.set(e, l))
        }
    }
    function v(t, e) {
        var n = t.getElementsByTagName ? t.getElementsByTagName(e || "*") : t.querySelectorAll ? t.querySelectorAll(e || "*") : [];
        return void 0 === e || e && K.nodeName(t, e) ? K.merge([t], n) : n
    }
    function y(t, e) {
        var n = e.nodeName.toLowerCase();
        "input" === n && Ct.test(t.type) ? e.checked = t.checked : ("input" === n || "textarea" === n) && (e.defaultValue = t.defaultValue)
    }
    function _(e, n) {
        var i, r = K(n.createElement(e)).appendTo(n.body), o = t.getDefaultComputedStyle && (i = t.getDefaultComputedStyle(r[0])) ? i.display : K.css(r[0], "display");
        return r.detach(),
        o
    }
    function x(t) {
        var e = J
          , n = Ht[t];
        return n || (n = _(t, e),
        "none" !== n && n || (qt = (qt || K("<iframe frameborder='0' width='0' height='0'/>")).appendTo(e.documentElement),
        e = qt[0].contentDocument,
        e.write(),
        e.close(),
        n = _(t, e),
        qt.detach()),
        Ht[t] = n),
        n
    }
    function w(t, e, n) {
        var i, r, o, s, a = t.style;
        return n = n || zt(t),
        n && (s = n.getPropertyValue(e) || n[e]),
        n && ("" !== s || K.contains(t.ownerDocument, t) || (s = K.style(t, e)),
        Bt.test(s) && Ft.test(e) && (i = a.width,
        r = a.minWidth,
        o = a.maxWidth,
        a.minWidth = a.maxWidth = a.width = s,
        s = n.width,
        a.width = i,
        a.minWidth = r,
        a.maxWidth = o)),
        void 0 !== s ? s + "" : s
    }
    function b(t, e) {
        return {
            get: function() {
                return t() ? void delete this.get : (this.get = e).apply(this, arguments)
            }
        }
    }
    function T(t, e) {
        if (e in t)
            return e;
        for (var n = e[0].toUpperCase() + e.slice(1), i = e, r = Gt.length; r--; )
            if (e = Gt[r] + n,
            e in t)
                return e;
        return i
    }
    function C(t, e, n) {
        var i = Xt.exec(e);
        return i ? Math.max(0, i[1] - (n || 0)) + (i[2] || "px") : e
    }
    function S(t, e, n, i, r) {
        for (var o = n === (i ? "border" : "content") ? 4 : "width" === e ? 1 : 0, s = 0; 4 > o; o += 2)
            "margin" === n && (s += K.css(t, n + bt[o], !0, r)),
            i ? ("content" === n && (s -= K.css(t, "padding" + bt[o], !0, r)),
            "margin" !== n && (s -= K.css(t, "border" + bt[o] + "Width", !0, r))) : (s += K.css(t, "padding" + bt[o], !0, r),
            "padding" !== n && (s += K.css(t, "border" + bt[o] + "Width", !0, r)));
        return s
    }
    function k(t, e, n) {
        var i = !0
          , r = "width" === e ? t.offsetWidth : t.offsetHeight
          , o = zt(t)
          , s = "border-box" === K.css(t, "boxSizing", !1, o);
        if (0 >= r || null == r) {
            if (r = w(t, e, o),
            (0 > r || null == r) && (r = t.style[e]),
            Bt.test(r))
                return r;
            i = s && (Q.boxSizingReliable() || r === t.style[e]),
            r = parseFloat(r) || 0
        }
        return r + S(t, e, n || (s ? "border" : "content"), i, o) + "px"
    }
    function j(t, e) {
        for (var n, i, r, o = [], s = 0, a = t.length; a > s; s++)
            i = t[s],
            i.style && (o[s] = vt.get(i, "olddisplay"),
            n = i.style.display,
            e ? (o[s] || "none" !== n || (i.style.display = ""),
            "" === i.style.display && Tt(i) && (o[s] = vt.access(i, "olddisplay", x(i.nodeName)))) : (r = Tt(i),
            "none" === n && r || vt.set(i, "olddisplay", r ? n : K.css(i, "display"))));
        for (s = 0; a > s; s++)
            i = t[s],
            i.style && (e && "none" !== i.style.display && "" !== i.style.display || (i.style.display = e ? o[s] || "" : "none"));
        return t
    }
    function P(t, e, n, i, r) {
        return new P.prototype.init(t,e,n,i,r)
    }
    function E() {
        return setTimeout(function() {
            Qt = void 0
        }),
        Qt = K.now()
    }
    function O(t, e) {
        var n, i = 0, r = {
            height: t
        };
        for (e = e ? 1 : 0; 4 > i; i += 2 - e)
            n = bt[i],
            r["margin" + n] = r["padding" + n] = t;
        return e && (r.opacity = r.width = t),
        r
    }
    function M(t, e, n) {
        for (var i, r = (ne[e] || []).concat(ne["*"]), o = 0, s = r.length; s > o; o++)
            if (i = r[o].call(n, e, t))
                return i
    }
    function D(t, e, n) {
        var i, r, o, s, a, l, u, c, h = this, p = {}, f = t.style, d = t.nodeType && Tt(t), m = vt.get(t, "fxshow");
        n.queue || (a = K._queueHooks(t, "fx"),
        null == a.unqueued && (a.unqueued = 0,
        l = a.empty.fire,
        a.empty.fire = function() {
            a.unqueued || l()
        }
        ),
        a.unqueued++,
        h.always(function() {
            h.always(function() {
                a.unqueued--,
                K.queue(t, "fx").length || a.empty.fire()
            })
        })),
        1 === t.nodeType && ("height"in e || "width"in e) && (n.overflow = [f.overflow, f.overflowX, f.overflowY],
        u = K.css(t, "display"),
        c = "none" === u ? vt.get(t, "olddisplay") || x(t.nodeName) : u,
        "inline" === c && "none" === K.css(t, "float") && (f.display = "inline-block")),
        n.overflow && (f.overflow = "hidden",
        h.always(function() {
            f.overflow = n.overflow[0],
            f.overflowX = n.overflow[1],
            f.overflowY = n.overflow[2]
        }));
        for (i in e)
            if (r = e[i],
            Zt.exec(r)) {
                if (delete e[i],
                o = o || "toggle" === r,
                r === (d ? "hide" : "show")) {
                    if ("show" !== r || !m || void 0 === m[i])
                        continue;
                    d = !0
                }
                p[i] = m && m[i] || K.style(t, i)
            } else
                u = void 0;
        if (K.isEmptyObject(p))
            "inline" === ("none" === u ? x(t.nodeName) : u) && (f.display = u);
        else {
            m ? "hidden"in m && (d = m.hidden) : m = vt.access(t, "fxshow", {}),
            o && (m.hidden = !d),
            d ? K(t).show() : h.done(function() {
                K(t).hide()
            }),
            h.done(function() {
                var e;
                vt.remove(t, "fxshow");
                for (e in p)
                    K.style(t, e, p[e])
            });
            for (i in p)
                s = M(d ? m[i] : 0, i, h),
                i in m || (m[i] = s.start,
                d && (s.end = s.start,
                s.start = "width" === i || "height" === i ? 1 : 0))
        }
    }
    function A(t, e) {
        var n, i, r, o, s;
        for (n in t)
            if (i = K.camelCase(n),
            r = e[i],
            o = t[n],
            K.isArray(o) && (r = o[1],
            o = t[n] = o[0]),
            n !== i && (t[i] = o,
            delete t[n]),
            s = K.cssHooks[i],
            s && "expand"in s) {
                o = s.expand(o),
                delete t[i];
                for (n in o)
                    n in t || (t[n] = o[n],
                    e[n] = r)
            } else
                e[i] = r
    }
    function N(t, e, n) {
        var i, r, o = 0, s = ee.length, a = K.Deferred().always(function() {
            delete l.elem
        }), l = function() {
            if (r)
                return !1;
            for (var e = Qt || E(), n = Math.max(0, u.startTime + u.duration - e), i = n / u.duration || 0, o = 1 - i, s = 0, l = u.tweens.length; l > s; s++)
                u.tweens[s].run(o);
            return a.notifyWith(t, [u, o, n]),
            1 > o && l ? n : (a.resolveWith(t, [u]),
            !1)
        }, u = a.promise({
            elem: t,
            props: K.extend({}, e),
            opts: K.extend(!0, {
                specialEasing: {}
            }, n),
            originalProperties: e,
            originalOptions: n,
            startTime: Qt || E(),
            duration: n.duration,
            tweens: [],
            createTween: function(e, n) {
                var i = K.Tween(t, u.opts, e, n, u.opts.specialEasing[e] || u.opts.easing);
                return u.tweens.push(i),
                i
            },
            stop: function(e) {
                var n = 0
                  , i = e ? u.tweens.length : 0;
                if (r)
                    return this;
                for (r = !0; i > n; n++)
                    u.tweens[n].run(1);
                return e ? a.resolveWith(t, [u, e]) : a.rejectWith(t, [u, e]),
                this
            }
        }), c = u.props;
        for (A(c, u.opts.specialEasing); s > o; o++)
            if (i = ee[o].call(u, t, c, u.opts))
                return i;
        return K.map(c, M, u),
        K.isFunction(u.opts.start) && u.opts.start.call(t, u),
        K.fx.timer(K.extend(l, {
            elem: t,
            anim: u,
            queue: u.opts.queue
        })),
        u.progress(u.opts.progress).done(u.opts.done, u.opts.complete).fail(u.opts.fail).always(u.opts.always)
    }
    function L(t) {
        return function(e, n) {
            "string" != typeof e && (n = e,
            e = "*");
            var i, r = 0, o = e.toLowerCase().match(ft) || [];
            if (K.isFunction(n))
                for (; i = o[r++]; )
                    "+" === i[0] ? (i = i.slice(1) || "*",
                    (t[i] = t[i] || []).unshift(n)) : (t[i] = t[i] || []).push(n)
        }
    }
    function R(t, e, n, i) {
        function r(a) {
            var l;
            return o[a] = !0,
            K.each(t[a] || [], function(t, a) {
                var u = a(e, n, i);
                return "string" != typeof u || s || o[u] ? s ? !(l = u) : void 0 : (e.dataTypes.unshift(u),
                r(u),
                !1)
            }),
            l
        }
        var o = {}
          , s = t === _e;
        return r(e.dataTypes[0]) || !o["*"] && r("*")
    }
    function $(t, e) {
        var n, i, r = K.ajaxSettings.flatOptions || {};
        for (n in e)
            void 0 !== e[n] && ((r[n] ? t : i || (i = {}))[n] = e[n]);
        return i && K.extend(!0, t, i),
        t
    }
    function I(t, e, n) {
        for (var i, r, o, s, a = t.contents, l = t.dataTypes; "*" === l[0]; )
            l.shift(),
            void 0 === i && (i = t.mimeType || e.getResponseHeader("Content-Type"));
        if (i)
            for (r in a)
                if (a[r] && a[r].test(i)) {
                    l.unshift(r);
                    break
                }
        if (l[0]in n)
            o = l[0];
        else {
            for (r in n) {
                if (!l[0] || t.converters[r + " " + l[0]]) {
                    o = r;
                    break
                }
                s || (s = r)
            }
            o = o || s
        }
        return o ? (o !== l[0] && l.unshift(o),
        n[o]) : void 0
    }
    function q(t, e, n, i) {
        var r, o, s, a, l, u = {}, c = t.dataTypes.slice();
        if (c[1])
            for (s in t.converters)
                u[s.toLowerCase()] = t.converters[s];
        for (o = c.shift(); o; )
            if (t.responseFields[o] && (n[t.responseFields[o]] = e),
            !l && i && t.dataFilter && (e = t.dataFilter(e, t.dataType)),
            l = o,
            o = c.shift())
                if ("*" === o)
                    o = l;
                else if ("*" !== l && l !== o) {
                    if (s = u[l + " " + o] || u["* " + o],
                    !s)
                        for (r in u)
                            if (a = r.split(" "),
                            a[1] === o && (s = u[l + " " + a[0]] || u["* " + a[0]])) {
                                s === !0 ? s = u[r] : u[r] !== !0 && (o = a[0],
                                c.unshift(a[1]));
                                break
                            }
                    if (s !== !0)
                        if (s && t["throws"])
                            e = s(e);
                        else
                            try {
                                e = s(e)
                            } catch (h) {
                                return {
                                    state: "parsererror",
                                    error: s ? h : "No conversion from " + l + " to " + o
                                }
                            }
                }
        return {
            state: "success",
            data: e
        }
    }
    function H(t, e, n, i) {
        var r;
        if (K.isArray(e))
            K.each(e, function(e, r) {
                n || Ce.test(t) ? i(t, r) : H(t + "[" + ("object" == typeof r ? e : "") + "]", r, n, i)
            });
        else if (n || "object" !== K.type(e))
            i(t, e);
        else
            for (r in e)
                H(t + "[" + r + "]", e[r], n, i)
    }
    function F(t) {
        return K.isWindow(t) ? t : 9 === t.nodeType && t.defaultView
    }
    var B = []
      , z = B.slice
      , W = B.concat
      , X = B.push
      , V = B.indexOf
      , Y = {}
      , U = Y.toString
      , G = Y.hasOwnProperty
      , Q = {}
      , J = t.document
      , Z = "2.1.4"
      , K = function(t, e) {
        return new K.fn.init(t,e)
    }
      , tt = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g
      , et = /^-ms-/
      , nt = /-([\da-z])/gi
      , it = function(t, e) {
        return e.toUpperCase()
    };
    K.fn = K.prototype = {
        jquery: Z,
        constructor: K,
        selector: "",
        length: 0,
        toArray: function() {
            return z.call(this)
        },
        get: function(t) {
            return null != t ? 0 > t ? this[t + this.length] : this[t] : z.call(this)
        },
        pushStack: function(t) {
            var e = K.merge(this.constructor(), t);
            return e.prevObject = this,
            e.context = this.context,
            e
        },
        each: function(t, e) {
            return K.each(this, t, e)
        },
        map: function(t) {
            return this.pushStack(K.map(this, function(e, n) {
                return t.call(e, n, e)
            }))
        },
        slice: function() {
            return this.pushStack(z.apply(this, arguments))
        },
        first: function() {
            return this.eq(0)
        },
        last: function() {
            return this.eq(-1)
        },
        eq: function(t) {
            var e = this.length
              , n = +t + (0 > t ? e : 0);
            return this.pushStack(n >= 0 && e > n ? [this[n]] : [])
        },
        end: function() {
            return this.prevObject || this.constructor(null)
        },
        push: X,
        sort: B.sort,
        splice: B.splice
    },
    K.extend = K.fn.extend = function() {
        var t, e, n, i, r, o, s = arguments[0] || {}, a = 1, l = arguments.length, u = !1;
        for ("boolean" == typeof s && (u = s,
        s = arguments[a] || {},
        a++),
        "object" == typeof s || K.isFunction(s) || (s = {}),
        a === l && (s = this,
        a--); l > a; a++)
            if (null != (t = arguments[a]))
                for (e in t)
                    n = s[e],
                    i = t[e],
                    s !== i && (u && i && (K.isPlainObject(i) || (r = K.isArray(i))) ? (r ? (r = !1,
                    o = n && K.isArray(n) ? n : []) : o = n && K.isPlainObject(n) ? n : {},
                    s[e] = K.extend(u, o, i)) : void 0 !== i && (s[e] = i));
        return s
    }
    ,
    K.extend({
        expando: "jQuery" + (Z + Math.random()).replace(/\D/g, ""),
        isReady: !0,
        error: function(t) {
            throw new Error(t)
        },
        noop: function() {},
        isFunction: function(t) {
            return "function" === K.type(t)
        },
        isArray: Array.isArray,
        isWindow: function(t) {
            return null != t && t === t.window
        },
        isNumeric: function(t) {
            return !K.isArray(t) && t - parseFloat(t) + 1 >= 0
        },
        isPlainObject: function(t) {
            return "object" !== K.type(t) || t.nodeType || K.isWindow(t) ? !1 : t.constructor && !G.call(t.constructor.prototype, "isPrototypeOf") ? !1 : !0
        },
        isEmptyObject: function(t) {
            var e;
            for (e in t)
                return !1;
            return !0
        },
        type: function(t) {
            return null == t ? t + "" : "object" == typeof t || "function" == typeof t ? Y[U.call(t)] || "object" : typeof t
        },
        globalEval: function(t) {
            var e, n = eval;
            t = K.trim(t),
            t && (1 === t.indexOf("use strict") ? (e = J.createElement("script"),
            e.text = t,
            J.head.appendChild(e).parentNode.removeChild(e)) : n(t))
        },
        camelCase: function(t) {
            return t.replace(et, "ms-").replace(nt, it)
        },
        nodeName: function(t, e) {
            return t.nodeName && t.nodeName.toLowerCase() === e.toLowerCase()
        },
        each: function(t, e, i) {
            var r, o = 0, s = t.length, a = n(t);
            if (i) {
                if (a)
                    for (; s > o && (r = e.apply(t[o], i),
                    r !== !1); o++)
                        ;
                else
                    for (o in t)
                        if (r = e.apply(t[o], i),
                        r === !1)
                            break
            } else if (a)
                for (; s > o && (r = e.call(t[o], o, t[o]),
                r !== !1); o++)
                    ;
            else
                for (o in t)
                    if (r = e.call(t[o], o, t[o]),
                    r === !1)
                        break;
            return t
        },
        trim: function(t) {
            return null == t ? "" : (t + "").replace(tt, "")
        },
        makeArray: function(t, e) {
            var i = e || [];
            return null != t && (n(Object(t)) ? K.merge(i, "string" == typeof t ? [t] : t) : X.call(i, t)),
            i
        },
        inArray: function(t, e, n) {
            return null == e ? -1 : V.call(e, t, n)
        },
        merge: function(t, e) {
            for (var n = +e.length, i = 0, r = t.length; n > i; i++)
                t[r++] = e[i];
            return t.length = r,
            t
        },
        grep: function(t, e, n) {
            for (var i, r = [], o = 0, s = t.length, a = !n; s > o; o++)
                i = !e(t[o], o),
                i !== a && r.push(t[o]);
            return r
        },
        map: function(t, e, i) {
            var r, o = 0, s = t.length, a = n(t), l = [];
            if (a)
                for (; s > o; o++)
                    r = e(t[o], o, i),
                    null != r && l.push(r);
            else
                for (o in t)
                    r = e(t[o], o, i),
                    null != r && l.push(r);
            return W.apply([], l)
        },
        guid: 1,
        proxy: function(t, e) {
            var n, i, r;
            return "string" == typeof e && (n = t[e],
            e = t,
            t = n),
            K.isFunction(t) ? (i = z.call(arguments, 2),
            r = function() {
                return t.apply(e || this, i.concat(z.call(arguments)))
            }
            ,
            r.guid = t.guid = t.guid || K.guid++,
            r) : void 0
        },
        now: Date.now,
        support: Q
    }),
    K.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function(t, e) {
        Y["[object " + e + "]"] = e.toLowerCase()
    });
    var rt = function(t) {
        function e(t, e, n, i) {
            var r, o, s, a, l, u, h, f, d, m;
            if ((e ? e.ownerDocument || e : H) !== D && M(e),
            e = e || D,
            n = n || [],
            a = e.nodeType,
            "string" != typeof t || !t || 1 !== a && 9 !== a && 11 !== a)
                return n;
            if (!i && N) {
                if (11 !== a && (r = yt.exec(t)))
                    if (s = r[1]) {
                        if (9 === a) {
                            if (o = e.getElementById(s),
                            !o || !o.parentNode)
                                return n;
                            if (o.id === s)
                                return n.push(o),
                                n
                        } else if (e.ownerDocument && (o = e.ownerDocument.getElementById(s)) && I(e, o) && o.id === s)
                            return n.push(o),
                            n
                    } else {
                        if (r[2])
                            return Z.apply(n, e.getElementsByTagName(t)),
                            n;
                        if ((s = r[3]) && w.getElementsByClassName)
                            return Z.apply(n, e.getElementsByClassName(s)),
                            n
                    }
                if (w.qsa && (!L || !L.test(t))) {
                    if (f = h = q,
                    d = e,
                    m = 1 !== a && t,
                    1 === a && "object" !== e.nodeName.toLowerCase()) {
                        for (u = S(t),
                        (h = e.getAttribute("id")) ? f = h.replace(xt, "\\$&") : e.setAttribute("id", f),
                        f = "[id='" + f + "'] ",
                        l = u.length; l--; )
                            u[l] = f + p(u[l]);
                        d = _t.test(t) && c(e.parentNode) || e,
                        m = u.join(",")
                    }
                    if (m)
                        try {
                            return Z.apply(n, d.querySelectorAll(m)),
                            n
                        } catch (g) {} finally {
                            h || e.removeAttribute("id")
                        }
                }
            }
            return j(t.replace(lt, "$1"), e, n, i)
        }
        function n() {
            function t(n, i) {
                return e.push(n + " ") > b.cacheLength && delete t[e.shift()],
                t[n + " "] = i
            }
            var e = [];
            return t
        }
        function i(t) {
            return t[q] = !0,
            t
        }
        function r(t) {
            var e = D.createElement("div");
            try {
                return !!t(e)
            } catch (n) {
                return !1
            } finally {
                e.parentNode && e.parentNode.removeChild(e),
                e = null
            }
        }
        function o(t, e) {
            for (var n = t.split("|"), i = t.length; i--; )
                b.attrHandle[n[i]] = e
        }
        function s(t, e) {
            var n = e && t
              , i = n && 1 === t.nodeType && 1 === e.nodeType && (~e.sourceIndex || Y) - (~t.sourceIndex || Y);
            if (i)
                return i;
            if (n)
                for (; n = n.nextSibling; )
                    if (n === e)
                        return -1;
            return t ? 1 : -1
        }
        function a(t) {
            return function(e) {
                var n = e.nodeName.toLowerCase();
                return "input" === n && e.type === t
            }
        }
        function l(t) {
            return function(e) {
                var n = e.nodeName.toLowerCase();
                return ("input" === n || "button" === n) && e.type === t
            }
        }
        function u(t) {
            return i(function(e) {
                return e = +e,
                i(function(n, i) {
                    for (var r, o = t([], n.length, e), s = o.length; s--; )
                        n[r = o[s]] && (n[r] = !(i[r] = n[r]))
                })
            })
        }
        function c(t) {
            return t && "undefined" != typeof t.getElementsByTagName && t
        }
        function h() {}
        function p(t) {
            for (var e = 0, n = t.length, i = ""; n > e; e++)
                i += t[e].value;
            return i
        }
        function f(t, e, n) {
            var i = e.dir
              , r = n && "parentNode" === i
              , o = B++;
            return e.first ? function(e, n, o) {
                for (; e = e[i]; )
                    if (1 === e.nodeType || r)
                        return t(e, n, o)
            }
            : function(e, n, s) {
                var a, l, u = [F, o];
                if (s) {
                    for (; e = e[i]; )
                        if ((1 === e.nodeType || r) && t(e, n, s))
                            return !0
                } else
                    for (; e = e[i]; )
                        if (1 === e.nodeType || r) {
                            if (l = e[q] || (e[q] = {}),
                            (a = l[i]) && a[0] === F && a[1] === o)
                                return u[2] = a[2];
                            if (l[i] = u,
                            u[2] = t(e, n, s))
                                return !0
                        }
            }
        }
        function d(t) {
            return t.length > 1 ? function(e, n, i) {
                for (var r = t.length; r--; )
                    if (!t[r](e, n, i))
                        return !1;
                return !0
            }
            : t[0]
        }
        function m(t, n, i) {
            for (var r = 0, o = n.length; o > r; r++)
                e(t, n[r], i);
            return i
        }
        function g(t, e, n, i, r) {
            for (var o, s = [], a = 0, l = t.length, u = null != e; l > a; a++)
                (o = t[a]) && (!n || n(o, i, r)) && (s.push(o),
                u && e.push(a));
            return s
        }
        function v(t, e, n, r, o, s) {
            return r && !r[q] && (r = v(r)),
            o && !o[q] && (o = v(o, s)),
            i(function(i, s, a, l) {
                var u, c, h, p = [], f = [], d = s.length, v = i || m(e || "*", a.nodeType ? [a] : a, []), y = !t || !i && e ? v : g(v, p, t, a, l), _ = n ? o || (i ? t : d || r) ? [] : s : y;
                if (n && n(y, _, a, l),
                r)
                    for (u = g(_, f),
                    r(u, [], a, l),
                    c = u.length; c--; )
                        (h = u[c]) && (_[f[c]] = !(y[f[c]] = h));
                if (i) {
                    if (o || t) {
                        if (o) {
                            for (u = [],
                            c = _.length; c--; )
                                (h = _[c]) && u.push(y[c] = h);
                            o(null, _ = [], u, l)
                        }
                        for (c = _.length; c--; )
                            (h = _[c]) && (u = o ? tt(i, h) : p[c]) > -1 && (i[u] = !(s[u] = h))
                    }
                } else
                    _ = g(_ === s ? _.splice(d, _.length) : _),
                    o ? o(null, s, _, l) : Z.apply(s, _)
            })
        }
        function y(t) {
            for (var e, n, i, r = t.length, o = b.relative[t[0].type], s = o || b.relative[" "], a = o ? 1 : 0, l = f(function(t) {
                return t === e
            }, s, !0), u = f(function(t) {
                return tt(e, t) > -1
            }, s, !0), c = [function(t, n, i) {
                var r = !o && (i || n !== P) || ((e = n).nodeType ? l(t, n, i) : u(t, n, i));
                return e = null,
                r
            }
            ]; r > a; a++)
                if (n = b.relative[t[a].type])
                    c = [f(d(c), n)];
                else {
                    if (n = b.filter[t[a].type].apply(null, t[a].matches),
                    n[q]) {
                        for (i = ++a; r > i && !b.relative[t[i].type]; i++)
                            ;
                        return v(a > 1 && d(c), a > 1 && p(t.slice(0, a - 1).concat({
                            value: " " === t[a - 2].type ? "*" : ""
                        })).replace(lt, "$1"), n, i > a && y(t.slice(a, i)), r > i && y(t = t.slice(i)), r > i && p(t))
                    }
                    c.push(n)
                }
            return d(c)
        }
        function _(t, n) {
            var r = n.length > 0
              , o = t.length > 0
              , s = function(i, s, a, l, u) {
                var c, h, p, f = 0, d = "0", m = i && [], v = [], y = P, _ = i || o && b.find.TAG("*", u), x = F += null == y ? 1 : Math.random() || .1, w = _.length;
                for (u && (P = s !== D && s); d !== w && null != (c = _[d]); d++) {
                    if (o && c) {
                        for (h = 0; p = t[h++]; )
                            if (p(c, s, a)) {
                                l.push(c);
                                break
                            }
                        u && (F = x)
                    }
                    r && ((c = !p && c) && f--,
                    i && m.push(c))
                }
                if (f += d,
                r && d !== f) {
                    for (h = 0; p = n[h++]; )
                        p(m, v, s, a);
                    if (i) {
                        if (f > 0)
                            for (; d--; )
                                m[d] || v[d] || (v[d] = Q.call(l));
                        v = g(v)
                    }
                    Z.apply(l, v),
                    u && !i && v.length > 0 && f + n.length > 1 && e.uniqueSort(l)
                }
                return u && (F = x,
                P = y),
                m
            };
            return r ? i(s) : s
        }
        var x, w, b, T, C, S, k, j, P, E, O, M, D, A, N, L, R, $, I, q = "sizzle" + 1 * new Date, H = t.document, F = 0, B = 0, z = n(), W = n(), X = n(), V = function(t, e) {
            return t === e && (O = !0),
            0
        }, Y = 1 << 31, U = {}.hasOwnProperty, G = [], Q = G.pop, J = G.push, Z = G.push, K = G.slice, tt = function(t, e) {
            for (var n = 0, i = t.length; i > n; n++)
                if (t[n] === e)
                    return n;
            return -1
        }, et = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped", nt = "[\\x20\\t\\r\\n\\f]", it = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+", rt = it.replace("w", "w#"), ot = "\\[" + nt + "*(" + it + ")(?:" + nt + "*([*^$|!~]?=)" + nt + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + rt + "))|)" + nt + "*\\]", st = ":(" + it + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + ot + ")*)|.*)\\)|)", at = new RegExp(nt + "+","g"), lt = new RegExp("^" + nt + "+|((?:^|[^\\\\])(?:\\\\.)*)" + nt + "+$","g"), ut = new RegExp("^" + nt + "*," + nt + "*"), ct = new RegExp("^" + nt + "*([>+~]|" + nt + ")" + nt + "*"), ht = new RegExp("=" + nt + "*([^\\]'\"]*?)" + nt + "*\\]","g"), pt = new RegExp(st), ft = new RegExp("^" + rt + "$"), dt = {
            ID: new RegExp("^#(" + it + ")"),
            CLASS: new RegExp("^\\.(" + it + ")"),
            TAG: new RegExp("^(" + it.replace("w", "w*") + ")"),
            ATTR: new RegExp("^" + ot),
            PSEUDO: new RegExp("^" + st),
            CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + nt + "*(even|odd|(([+-]|)(\\d*)n|)" + nt + "*(?:([+-]|)" + nt + "*(\\d+)|))" + nt + "*\\)|)","i"),
            bool: new RegExp("^(?:" + et + ")$","i"),
            needsContext: new RegExp("^" + nt + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + nt + "*((?:-\\d)?\\d*)" + nt + "*\\)|)(?=[^-]|$)","i")
        }, mt = /^(?:input|select|textarea|button)$/i, gt = /^h\d$/i, vt = /^[^{]+\{\s*\[native \w/, yt = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/, _t = /[+~]/, xt = /'|\\/g, wt = new RegExp("\\\\([\\da-f]{1,6}" + nt + "?|(" + nt + ")|.)","ig"), bt = function(t, e, n) {
            var i = "0x" + e - 65536;
            return i !== i || n ? e : 0 > i ? String.fromCharCode(i + 65536) : String.fromCharCode(i >> 10 | 55296, 1023 & i | 56320)
        }, Tt = function() {
            M()
        };
        try {
            Z.apply(G = K.call(H.childNodes), H.childNodes),
            G[H.childNodes.length].nodeType
        } catch (Ct) {
            Z = {
                apply: G.length ? function(t, e) {
                    J.apply(t, K.call(e))
                }
                : function(t, e) {
                    for (var n = t.length, i = 0; t[n++] = e[i++]; )
                        ;
                    t.length = n - 1
                }
            }
        }
        w = e.support = {},
        C = e.isXML = function(t) {
            var e = t && (t.ownerDocument || t).documentElement;
            return e ? "HTML" !== e.nodeName : !1
        }
        ,
        M = e.setDocument = function(t) {
            var e, n, i = t ? t.ownerDocument || t : H;
            return i !== D && 9 === i.nodeType && i.documentElement ? (D = i,
            A = i.documentElement,
            n = i.defaultView,
            n && n !== n.top && (n.addEventListener ? n.addEventListener("unload", Tt, !1) : n.attachEvent && n.attachEvent("onunload", Tt)),
            N = !C(i),
            w.attributes = r(function(t) {
                return t.className = "i",
                !t.getAttribute("className")
            }),
            w.getElementsByTagName = r(function(t) {
                return t.appendChild(i.createComment("")),
                !t.getElementsByTagName("*").length
            }),
            w.getElementsByClassName = vt.test(i.getElementsByClassName),
            w.getById = r(function(t) {
                return A.appendChild(t).id = q,
                !i.getElementsByName || !i.getElementsByName(q).length
            }),
            w.getById ? (b.find.ID = function(t, e) {
                if ("undefined" != typeof e.getElementById && N) {
                    var n = e.getElementById(t);
                    return n && n.parentNode ? [n] : []
                }
            }
            ,
            b.filter.ID = function(t) {
                var e = t.replace(wt, bt);
                return function(t) {
                    return t.getAttribute("id") === e
                }
            }
            ) : (delete b.find.ID,
            b.filter.ID = function(t) {
                var e = t.replace(wt, bt);
                return function(t) {
                    var n = "undefined" != typeof t.getAttributeNode && t.getAttributeNode("id");
                    return n && n.value === e
                }
            }
            ),
            b.find.TAG = w.getElementsByTagName ? function(t, e) {
                return "undefined" != typeof e.getElementsByTagName ? e.getElementsByTagName(t) : w.qsa ? e.querySelectorAll(t) : void 0
            }
            : function(t, e) {
                var n, i = [], r = 0, o = e.getElementsByTagName(t);
                if ("*" === t) {
                    for (; n = o[r++]; )
                        1 === n.nodeType && i.push(n);
                    return i
                }
                return o
            }
            ,
            b.find.CLASS = w.getElementsByClassName && function(t, e) {
                return N ? e.getElementsByClassName(t) : void 0
            }
            ,
            R = [],
            L = [],
            (w.qsa = vt.test(i.querySelectorAll)) && (r(function(t) {
                A.appendChild(t).innerHTML = "<a id='" + q + "'></a><select id='" + q + "-\f]' msallowcapture=''><option selected=''></option></select>",
                t.querySelectorAll("[msallowcapture^='']").length && L.push("[*^$]=" + nt + "*(?:''|\"\")"),
                t.querySelectorAll("[selected]").length || L.push("\\[" + nt + "*(?:value|" + et + ")"),
                t.querySelectorAll("[id~=" + q + "-]").length || L.push("~="),
                t.querySelectorAll(":checked").length || L.push(":checked"),
                t.querySelectorAll("a#" + q + "+*").length || L.push(".#.+[+~]")
            }),
            r(function(t) {
                var e = i.createElement("input");
                e.setAttribute("type", "hidden"),
                t.appendChild(e).setAttribute("name", "D"),
                t.querySelectorAll("[name=d]").length && L.push("name" + nt + "*[*^$|!~]?="),
                t.querySelectorAll(":enabled").length || L.push(":enabled", ":disabled"),
                t.querySelectorAll("*,:x"),
                L.push(",.*:")
            })),
            (w.matchesSelector = vt.test($ = A.matches || A.webkitMatchesSelector || A.mozMatchesSelector || A.oMatchesSelector || A.msMatchesSelector)) && r(function(t) {
                w.disconnectedMatch = $.call(t, "div"),
                $.call(t, "[s!='']:x"),
                R.push("!=", st)
            }),
            L = L.length && new RegExp(L.join("|")),
            R = R.length && new RegExp(R.join("|")),
            e = vt.test(A.compareDocumentPosition),
            I = e || vt.test(A.contains) ? function(t, e) {
                var n = 9 === t.nodeType ? t.documentElement : t
                  , i = e && e.parentNode;
                return t === i || !(!i || 1 !== i.nodeType || !(n.contains ? n.contains(i) : t.compareDocumentPosition && 16 & t.compareDocumentPosition(i)))
            }
            : function(t, e) {
                if (e)
                    for (; e = e.parentNode; )
                        if (e === t)
                            return !0;
                return !1
            }
            ,
            V = e ? function(t, e) {
                if (t === e)
                    return O = !0,
                    0;
                var n = !t.compareDocumentPosition - !e.compareDocumentPosition;
                return n ? n : (n = (t.ownerDocument || t) === (e.ownerDocument || e) ? t.compareDocumentPosition(e) : 1,
                1 & n || !w.sortDetached && e.compareDocumentPosition(t) === n ? t === i || t.ownerDocument === H && I(H, t) ? -1 : e === i || e.ownerDocument === H && I(H, e) ? 1 : E ? tt(E, t) - tt(E, e) : 0 : 4 & n ? -1 : 1)
            }
            : function(t, e) {
                if (t === e)
                    return O = !0,
                    0;
                var n, r = 0, o = t.parentNode, a = e.parentNode, l = [t], u = [e];
                if (!o || !a)
                    return t === i ? -1 : e === i ? 1 : o ? -1 : a ? 1 : E ? tt(E, t) - tt(E, e) : 0;
                if (o === a)
                    return s(t, e);
                for (n = t; n = n.parentNode; )
                    l.unshift(n);
                for (n = e; n = n.parentNode; )
                    u.unshift(n);
                for (; l[r] === u[r]; )
                    r++;
                return r ? s(l[r], u[r]) : l[r] === H ? -1 : u[r] === H ? 1 : 0
            }
            ,
            i) : D
        }
        ,
        e.matches = function(t, n) {
            return e(t, null, null, n)
        }
        ,
        e.matchesSelector = function(t, n) {
            if ((t.ownerDocument || t) !== D && M(t),
            n = n.replace(ht, "='$1']"),
            !(!w.matchesSelector || !N || R && R.test(n) || L && L.test(n)))
                try {
                    var i = $.call(t, n);
                    if (i || w.disconnectedMatch || t.document && 11 !== t.document.nodeType)
                        return i
                } catch (r) {}
            return e(n, D, null, [t]).length > 0
        }
        ,
        e.contains = function(t, e) {
            return (t.ownerDocument || t) !== D && M(t),
            I(t, e)
        }
        ,
        e.attr = function(t, e) {
            (t.ownerDocument || t) !== D && M(t);
            var n = b.attrHandle[e.toLowerCase()]
              , i = n && U.call(b.attrHandle, e.toLowerCase()) ? n(t, e, !N) : void 0;
            return void 0 !== i ? i : w.attributes || !N ? t.getAttribute(e) : (i = t.getAttributeNode(e)) && i.specified ? i.value : null
        }
        ,
        e.error = function(t) {
            throw new Error("Syntax error, unrecognized expression: " + t)
        }
        ,
        e.uniqueSort = function(t) {
            var e, n = [], i = 0, r = 0;
            if (O = !w.detectDuplicates,
            E = !w.sortStable && t.slice(0),
            t.sort(V),
            O) {
                for (; e = t[r++]; )
                    e === t[r] && (i = n.push(r));
                for (; i--; )
                    t.splice(n[i], 1)
            }
            return E = null,
            t
        }
        ,
        T = e.getText = function(t) {
            var e, n = "", i = 0, r = t.nodeType;
            if (r) {
                if (1 === r || 9 === r || 11 === r) {
                    if ("string" == typeof t.textContent)
                        return t.textContent;
                    for (t = t.firstChild; t; t = t.nextSibling)
                        n += T(t)
                } else if (3 === r || 4 === r)
                    return t.nodeValue
            } else
                for (; e = t[i++]; )
                    n += T(e);
            return n
        }
        ,
        b = e.selectors = {
            cacheLength: 50,
            createPseudo: i,
            match: dt,
            attrHandle: {},
            find: {},
            relative: {
                ">": {
                    dir: "parentNode",
                    first: !0
                },
                " ": {
                    dir: "parentNode"
                },
                "+": {
                    dir: "previousSibling",
                    first: !0
                },
                "~": {
                    dir: "previousSibling"
                }
            },
            preFilter: {
                ATTR: function(t) {
                    return t[1] = t[1].replace(wt, bt),
                    t[3] = (t[3] || t[4] || t[5] || "").replace(wt, bt),
                    "~=" === t[2] && (t[3] = " " + t[3] + " "),
                    t.slice(0, 4)
                },
                CHILD: function(t) {
                    return t[1] = t[1].toLowerCase(),
                    "nth" === t[1].slice(0, 3) ? (t[3] || e.error(t[0]),
                    t[4] = +(t[4] ? t[5] + (t[6] || 1) : 2 * ("even" === t[3] || "odd" === t[3])),
                    t[5] = +(t[7] + t[8] || "odd" === t[3])) : t[3] && e.error(t[0]),
                    t
                },
                PSEUDO: function(t) {
                    var e, n = !t[6] && t[2];
                    return dt.CHILD.test(t[0]) ? null : (t[3] ? t[2] = t[4] || t[5] || "" : n && pt.test(n) && (e = S(n, !0)) && (e = n.indexOf(")", n.length - e) - n.length) && (t[0] = t[0].slice(0, e),
                    t[2] = n.slice(0, e)),
                    t.slice(0, 3))
                }
            },
            filter: {
                TAG: function(t) {
                    var e = t.replace(wt, bt).toLowerCase();
                    return "*" === t ? function() {
                        return !0
                    }
                    : function(t) {
                        return t.nodeName && t.nodeName.toLowerCase() === e
                    }
                },
                CLASS: function(t) {
                    var e = z[t + " "];
                    return e || (e = new RegExp("(^|" + nt + ")" + t + "(" + nt + "|$)")) && z(t, function(t) {
                        return e.test("string" == typeof t.className && t.className || "undefined" != typeof t.getAttribute && t.getAttribute("class") || "")
                    })
                },
                ATTR: function(t, n, i) {
                    return function(r) {
                        var o = e.attr(r, t);
                        return null == o ? "!=" === n : n ? (o += "",
                        "=" === n ? o === i : "!=" === n ? o !== i : "^=" === n ? i && 0 === o.indexOf(i) : "*=" === n ? i && o.indexOf(i) > -1 : "$=" === n ? i && o.slice(-i.length) === i : "~=" === n ? (" " + o.replace(at, " ") + " ").indexOf(i) > -1 : "|=" === n ? o === i || o.slice(0, i.length + 1) === i + "-" : !1) : !0
                    }
                },
                CHILD: function(t, e, n, i, r) {
                    var o = "nth" !== t.slice(0, 3)
                      , s = "last" !== t.slice(-4)
                      , a = "of-type" === e;
                    return 1 === i && 0 === r ? function(t) {
                        return !!t.parentNode
                    }
                    : function(e, n, l) {
                        var u, c, h, p, f, d, m = o !== s ? "nextSibling" : "previousSibling", g = e.parentNode, v = a && e.nodeName.toLowerCase(), y = !l && !a;
                        if (g) {
                            if (o) {
                                for (; m; ) {
                                    for (h = e; h = h[m]; )
                                        if (a ? h.nodeName.toLowerCase() === v : 1 === h.nodeType)
                                            return !1;
                                    d = m = "only" === t && !d && "nextSibling"
                                }
                                return !0
                            }
                            if (d = [s ? g.firstChild : g.lastChild],
                            s && y) {
                                for (c = g[q] || (g[q] = {}),
                                u = c[t] || [],
                                f = u[0] === F && u[1],
                                p = u[0] === F && u[2],
                                h = f && g.childNodes[f]; h = ++f && h && h[m] || (p = f = 0) || d.pop(); )
                                    if (1 === h.nodeType && ++p && h === e) {
                                        c[t] = [F, f, p];
                                        break
                                    }
                            } else if (y && (u = (e[q] || (e[q] = {}))[t]) && u[0] === F)
                                p = u[1];
                            else
                                for (; (h = ++f && h && h[m] || (p = f = 0) || d.pop()) && ((a ? h.nodeName.toLowerCase() !== v : 1 !== h.nodeType) || !++p || (y && ((h[q] || (h[q] = {}))[t] = [F, p]),
                                h !== e)); )
                                    ;
                            return p -= r,
                            p === i || p % i === 0 && p / i >= 0
                        }
                    }
                },
                PSEUDO: function(t, n) {
                    var r, o = b.pseudos[t] || b.setFilters[t.toLowerCase()] || e.error("unsupported pseudo: " + t);
                    return o[q] ? o(n) : o.length > 1 ? (r = [t, t, "", n],
                    b.setFilters.hasOwnProperty(t.toLowerCase()) ? i(function(t, e) {
                        for (var i, r = o(t, n), s = r.length; s--; )
                            i = tt(t, r[s]),
                            t[i] = !(e[i] = r[s])
                    }) : function(t) {
                        return o(t, 0, r)
                    }
                    ) : o
                }
            },
            pseudos: {
                not: i(function(t) {
                    var e = []
                      , n = []
                      , r = k(t.replace(lt, "$1"));
                    return r[q] ? i(function(t, e, n, i) {
                        for (var o, s = r(t, null, i, []), a = t.length; a--; )
                            (o = s[a]) && (t[a] = !(e[a] = o))
                    }) : function(t, i, o) {
                        return e[0] = t,
                        r(e, null, o, n),
                        e[0] = null,
                        !n.pop()
                    }
                }),
                has: i(function(t) {
                    return function(n) {
                        return e(t, n).length > 0
                    }
                }),
                contains: i(function(t) {
                    return t = t.replace(wt, bt),
                    function(e) {
                        return (e.textContent || e.innerText || T(e)).indexOf(t) > -1
                    }
                }),
                lang: i(function(t) {
                    return ft.test(t || "") || e.error("unsupported lang: " + t),
                    t = t.replace(wt, bt).toLowerCase(),
                    function(e) {
                        var n;
                        do
                            if (n = N ? e.lang : e.getAttribute("xml:lang") || e.getAttribute("lang"))
                                return n = n.toLowerCase(),
                                n === t || 0 === n.indexOf(t + "-");
                        while ((e = e.parentNode) && 1 === e.nodeType);return !1
                    }
                }),
                target: function(e) {
                    var n = t.location && t.location.hash;
                    return n && n.slice(1) === e.id
                },
                root: function(t) {
                    return t === A
                },
                focus: function(t) {
                    return t === D.activeElement && (!D.hasFocus || D.hasFocus()) && !!(t.type || t.href || ~t.tabIndex)
                },
                enabled: function(t) {
                    return t.disabled === !1
                },
                disabled: function(t) {
                    return t.disabled === !0
                },
                checked: function(t) {
                    var e = t.nodeName.toLowerCase();
                    return "input" === e && !!t.checked || "option" === e && !!t.selected
                },
                selected: function(t) {
                    return t.parentNode && t.parentNode.selectedIndex,
                    t.selected === !0
                },
                empty: function(t) {
                    for (t = t.firstChild; t; t = t.nextSibling)
                        if (t.nodeType < 6)
                            return !1;
                    return !0
                },
                parent: function(t) {
                    return !b.pseudos.empty(t)
                },
                header: function(t) {
                    return gt.test(t.nodeName)
                },
                input: function(t) {
                    return mt.test(t.nodeName)
                },
                button: function(t) {
                    var e = t.nodeName.toLowerCase();
                    return "input" === e && "button" === t.type || "button" === e
                },
                text: function(t) {
                    var e;
                    return "input" === t.nodeName.toLowerCase() && "text" === t.type && (null == (e = t.getAttribute("type")) || "text" === e.toLowerCase())
                },
                first: u(function() {
                    return [0]
                }),
                last: u(function(t, e) {
                    return [e - 1]
                }),
                eq: u(function(t, e, n) {
                    return [0 > n ? n + e : n]
                }),
                even: u(function(t, e) {
                    for (var n = 0; e > n; n += 2)
                        t.push(n);
                    return t
                }),
                odd: u(function(t, e) {
                    for (var n = 1; e > n; n += 2)
                        t.push(n);
                    return t
                }),
                lt: u(function(t, e, n) {
                    for (var i = 0 > n ? n + e : n; --i >= 0; )
                        t.push(i);
                    return t
                }),
                gt: u(function(t, e, n) {
                    for (var i = 0 > n ? n + e : n; ++i < e; )
                        t.push(i);
                    return t
                })
            }
        },
        b.pseudos.nth = b.pseudos.eq;
        for (x in {
            radio: !0,
            checkbox: !0,
            file: !0,
            password: !0,
            image: !0
        })
            b.pseudos[x] = a(x);
        for (x in {
            submit: !0,
            reset: !0
        })
            b.pseudos[x] = l(x);
        return h.prototype = b.filters = b.pseudos,
        b.setFilters = new h,
        S = e.tokenize = function(t, n) {
            var i, r, o, s, a, l, u, c = W[t + " "];
            if (c)
                return n ? 0 : c.slice(0);
            for (a = t,
            l = [],
            u = b.preFilter; a; ) {
                (!i || (r = ut.exec(a))) && (r && (a = a.slice(r[0].length) || a),
                l.push(o = [])),
                i = !1,
                (r = ct.exec(a)) && (i = r.shift(),
                o.push({
                    value: i,
                    type: r[0].replace(lt, " ")
                }),
                a = a.slice(i.length));
                for (s in b.filter)
                    !(r = dt[s].exec(a)) || u[s] && !(r = u[s](r)) || (i = r.shift(),
                    o.push({
                        value: i,
                        type: s,
                        matches: r
                    }),
                    a = a.slice(i.length));
                if (!i)
                    break
            }
            return n ? a.length : a ? e.error(t) : W(t, l).slice(0)
        }
        ,
        k = e.compile = function(t, e) {
            var n, i = [], r = [], o = X[t + " "];
            if (!o) {
                for (e || (e = S(t)),
                n = e.length; n--; )
                    o = y(e[n]),
                    o[q] ? i.push(o) : r.push(o);
                o = X(t, _(r, i)),
                o.selector = t
            }
            return o
        }
        ,
        j = e.select = function(t, e, n, i) {
            var r, o, s, a, l, u = "function" == typeof t && t, h = !i && S(t = u.selector || t);
            if (n = n || [],
            1 === h.length) {
                if (o = h[0] = h[0].slice(0),
                o.length > 2 && "ID" === (s = o[0]).type && w.getById && 9 === e.nodeType && N && b.relative[o[1].type]) {
                    if (e = (b.find.ID(s.matches[0].replace(wt, bt), e) || [])[0],
                    !e)
                        return n;
                    u && (e = e.parentNode),
                    t = t.slice(o.shift().value.length)
                }
                for (r = dt.needsContext.test(t) ? 0 : o.length; r-- && (s = o[r],
                !b.relative[a = s.type]); )
                    if ((l = b.find[a]) && (i = l(s.matches[0].replace(wt, bt), _t.test(o[0].type) && c(e.parentNode) || e))) {
                        if (o.splice(r, 1),
                        t = i.length && p(o),
                        !t)
                            return Z.apply(n, i),
                            n;
                        break
                    }
            }
            return (u || k(t, h))(i, e, !N, n, _t.test(t) && c(e.parentNode) || e),
            n
        }
        ,
        w.sortStable = q.split("").sort(V).join("") === q,
        w.detectDuplicates = !!O,
        M(),
        w.sortDetached = r(function(t) {
            return 1 & t.compareDocumentPosition(D.createElement("div"))
        }),
        r(function(t) {
            return t.innerHTML = "<a href='#'></a>",
            "#" === t.firstChild.getAttribute("href")
        }) || o("type|href|height|width", function(t, e, n) {
            return n ? void 0 : t.getAttribute(e, "type" === e.toLowerCase() ? 1 : 2)
        }),
        w.attributes && r(function(t) {
            return t.innerHTML = "<input/>",
            t.firstChild.setAttribute("value", ""),
            "" === t.firstChild.getAttribute("value")
        }) || o("value", function(t, e, n) {
            return n || "input" !== t.nodeName.toLowerCase() ? void 0 : t.defaultValue
        }),
        r(function(t) {
            return null == t.getAttribute("disabled")
        }) || o(et, function(t, e, n) {
            var i;
            return n ? void 0 : t[e] === !0 ? e.toLowerCase() : (i = t.getAttributeNode(e)) && i.specified ? i.value : null
        }),
        e
    }(t);
    K.find = rt,
    K.expr = rt.selectors,
    K.expr[":"] = K.expr.pseudos,
    K.unique = rt.uniqueSort,
    K.text = rt.getText,
    K.isXMLDoc = rt.isXML,
    K.contains = rt.contains;
    var ot = K.expr.match.needsContext
      , st = /^<(\w+)\s*\/?>(?:<\/\1>|)$/
      , at = /^.[^:#\[\.,]*$/;
    K.filter = function(t, e, n) {
        var i = e[0];
        return n && (t = ":not(" + t + ")"),
        1 === e.length && 1 === i.nodeType ? K.find.matchesSelector(i, t) ? [i] : [] : K.find.matches(t, K.grep(e, function(t) {
            return 1 === t.nodeType
        }))
    }
    ,
    K.fn.extend({
        find: function(t) {
            var e, n = this.length, i = [], r = this;
            if ("string" != typeof t)
                return this.pushStack(K(t).filter(function() {
                    for (e = 0; n > e; e++)
                        if (K.contains(r[e], this))
                            return !0
                }));
            for (e = 0; n > e; e++)
                K.find(t, r[e], i);
            return i = this.pushStack(n > 1 ? K.unique(i) : i),
            i.selector = this.selector ? this.selector + " " + t : t,
            i
        },
        filter: function(t) {
            return this.pushStack(i(this, t || [], !1))
        },
        not: function(t) {
            return this.pushStack(i(this, t || [], !0))
        },
        is: function(t) {
            return !!i(this, "string" == typeof t && ot.test(t) ? K(t) : t || [], !1).length
        }
    });
    var lt, ut = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/, ct = K.fn.init = function(t, e) {
        var n, i;
        if (!t)
            return this;
        if ("string" == typeof t) {
            if (n = "<" === t[0] && ">" === t[t.length - 1] && t.length >= 3 ? [null, t, null] : ut.exec(t),
            !n || !n[1] && e)
                return !e || e.jquery ? (e || lt).find(t) : this.constructor(e).find(t);
            if (n[1]) {
                if (e = e instanceof K ? e[0] : e,
                K.merge(this, K.parseHTML(n[1], e && e.nodeType ? e.ownerDocument || e : J, !0)),
                st.test(n[1]) && K.isPlainObject(e))
                    for (n in e)
                        K.isFunction(this[n]) ? this[n](e[n]) : this.attr(n, e[n]);
                return this
            }
            return i = J.getElementById(n[2]),
            i && i.parentNode && (this.length = 1,
            this[0] = i),
            this.context = J,
            this.selector = t,
            this
        }
        return t.nodeType ? (this.context = this[0] = t,
        this.length = 1,
        this) : K.isFunction(t) ? "undefined" != typeof lt.ready ? lt.ready(t) : t(K) : (void 0 !== t.selector && (this.selector = t.selector,
        this.context = t.context),
        K.makeArray(t, this))
    }
    ;
    ct.prototype = K.fn,
    lt = K(J);
    var ht = /^(?:parents|prev(?:Until|All))/
      , pt = {
        children: !0,
        contents: !0,
        next: !0,
        prev: !0
    };
    K.extend({
        dir: function(t, e, n) {
            for (var i = [], r = void 0 !== n; (t = t[e]) && 9 !== t.nodeType; )
                if (1 === t.nodeType) {
                    if (r && K(t).is(n))
                        break;
                    i.push(t)
                }
            return i
        },
        sibling: function(t, e) {
            for (var n = []; t; t = t.nextSibling)
                1 === t.nodeType && t !== e && n.push(t);
            return n
        }
    }),
    K.fn.extend({
        has: function(t) {
            var e = K(t, this)
              , n = e.length;
            return this.filter(function() {
                for (var t = 0; n > t; t++)
                    if (K.contains(this, e[t]))
                        return !0
            })
        },
        closest: function(t, e) {
            for (var n, i = 0, r = this.length, o = [], s = ot.test(t) || "string" != typeof t ? K(t, e || this.context) : 0; r > i; i++)
                for (n = this[i]; n && n !== e; n = n.parentNode)
                    if (n.nodeType < 11 && (s ? s.index(n) > -1 : 1 === n.nodeType && K.find.matchesSelector(n, t))) {
                        o.push(n);
                        break
                    }
            return this.pushStack(o.length > 1 ? K.unique(o) : o)
        },
        index: function(t) {
            return t ? "string" == typeof t ? V.call(K(t), this[0]) : V.call(this, t.jquery ? t[0] : t) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
        },
        add: function(t, e) {
            return this.pushStack(K.unique(K.merge(this.get(), K(t, e))))
        },
        addBack: function(t) {
            return this.add(null == t ? this.prevObject : this.prevObject.filter(t))
        }
    }),
    K.each({
        parent: function(t) {
            var e = t.parentNode;
            return e && 11 !== e.nodeType ? e : null
        },
        parents: function(t) {
            return K.dir(t, "parentNode")
        },
        parentsUntil: function(t, e, n) {
            return K.dir(t, "parentNode", n)
        },
        next: function(t) {
            return r(t, "nextSibling")
        },
        prev: function(t) {
            return r(t, "previousSibling")
        },
        nextAll: function(t) {
            return K.dir(t, "nextSibling")
        },
        prevAll: function(t) {
            return K.dir(t, "previousSibling")
        },
        nextUntil: function(t, e, n) {
            return K.dir(t, "nextSibling", n)
        },
        prevUntil: function(t, e, n) {
            return K.dir(t, "previousSibling", n)
        },
        siblings: function(t) {
            return K.sibling((t.parentNode || {}).firstChild, t)
        },
        children: function(t) {
            return K.sibling(t.firstChild)
        },
        contents: function(t) {
            return t.contentDocument || K.merge([], t.childNodes)
        }
    }, function(t, e) {
        K.fn[t] = function(n, i) {
            var r = K.map(this, e, n);
            return "Until" !== t.slice(-5) && (i = n),
            i && "string" == typeof i && (r = K.filter(i, r)),
            this.length > 1 && (pt[t] || K.unique(r),
            ht.test(t) && r.reverse()),
            this.pushStack(r)
        }
    });
    var ft = /\S+/g
      , dt = {};
    K.Callbacks = function(t) {
        t = "string" == typeof t ? dt[t] || o(t) : K.extend({}, t);
        var e, n, i, r, s, a, l = [], u = !t.once && [], c = function(o) {
            for (e = t.memory && o,
            n = !0,
            a = r || 0,
            r = 0,
            s = l.length,
            i = !0; l && s > a; a++)
                if (l[a].apply(o[0], o[1]) === !1 && t.stopOnFalse) {
                    e = !1;
                    break
                }
            i = !1,
            l && (u ? u.length && c(u.shift()) : e ? l = [] : h.disable())
        }, h = {
            add: function() {
                if (l) {
                    var n = l.length;
                    !function o(e) {
                        K.each(e, function(e, n) {
                            var i = K.type(n);
                            "function" === i ? t.unique && h.has(n) || l.push(n) : n && n.length && "string" !== i && o(n)
                        })
                    }(arguments),
                    i ? s = l.length : e && (r = n,
                    c(e))
                }
                return this
            },
            remove: function() {
                return l && K.each(arguments, function(t, e) {
                    for (var n; (n = K.inArray(e, l, n)) > -1; )
                        l.splice(n, 1),
                        i && (s >= n && s--,
                        a >= n && a--)
                }),
                this
            },
            has: function(t) {
                return t ? K.inArray(t, l) > -1 : !(!l || !l.length)
            },
            empty: function() {
                return l = [],
                s = 0,
                this
            },
            disable: function() {
                return l = u = e = void 0,
                this
            },
            disabled: function() {
                return !l
            },
            lock: function() {
                return u = void 0,
                e || h.disable(),
                this
            },
            locked: function() {
                return !u
            },
            fireWith: function(t, e) {
                return !l || n && !u || (e = e || [],
                e = [t, e.slice ? e.slice() : e],
                i ? u.push(e) : c(e)),
                this
            },
            fire: function() {
                return h.fireWith(this, arguments),
                this
            },
            fired: function() {
                return !!n
            }
        };
        return h
    }
    ,
    K.extend({
        Deferred: function(t) {
            var e = [["resolve", "done", K.Callbacks("once memory"), "resolved"], ["reject", "fail", K.Callbacks("once memory"), "rejected"], ["notify", "progress", K.Callbacks("memory")]]
              , n = "pending"
              , i = {
                state: function() {
                    return n
                },
                always: function() {
                    return r.done(arguments).fail(arguments),
                    this
                },
                then: function() {
                    var t = arguments;
                    return K.Deferred(function(n) {
                        K.each(e, function(e, o) {
                            var s = K.isFunction(t[e]) && t[e];
                            r[o[1]](function() {
                                var t = s && s.apply(this, arguments);
                                t && K.isFunction(t.promise) ? t.promise().done(n.resolve).fail(n.reject).progress(n.notify) : n[o[0] + "With"](this === i ? n.promise() : this, s ? [t] : arguments)
                            })
                        }),
                        t = null
                    }).promise()
                },
                promise: function(t) {
                    return null != t ? K.extend(t, i) : i
                }
            }
              , r = {};
            return i.pipe = i.then,
            K.each(e, function(t, o) {
                var s = o[2]
                  , a = o[3];
                i[o[1]] = s.add,
                a && s.add(function() {
                    n = a
                }, e[1 ^ t][2].disable, e[2][2].lock),
                r[o[0]] = function() {
                    return r[o[0] + "With"](this === r ? i : this, arguments),
                    this
                }
                ,
                r[o[0] + "With"] = s.fireWith
            }),
            i.promise(r),
            t && t.call(r, r),
            r
        },
        when: function(t) {
            var e, n, i, r = 0, o = z.call(arguments), s = o.length, a = 1 !== s || t && K.isFunction(t.promise) ? s : 0, l = 1 === a ? t : K.Deferred(), u = function(t, n, i) {
                return function(r) {
                    n[t] = this,
                    i[t] = arguments.length > 1 ? z.call(arguments) : r,
                    i === e ? l.notifyWith(n, i) : --a || l.resolveWith(n, i)
                }
            };
            if (s > 1)
                for (e = new Array(s),
                n = new Array(s),
                i = new Array(s); s > r; r++)
                    o[r] && K.isFunction(o[r].promise) ? o[r].promise().done(u(r, i, o)).fail(l.reject).progress(u(r, n, e)) : --a;
            return a || l.resolveWith(i, o),
            l.promise()
        }
    });
    var mt;
    K.fn.ready = function(t) {
        return K.ready.promise().done(t),
        this
    }
    ,
    K.extend({
        isReady: !1,
        readyWait: 1,
        holdReady: function(t) {
            t ? K.readyWait++ : K.ready(!0)
        },
        ready: function(t) {
            (t === !0 ? --K.readyWait : K.isReady) || (K.isReady = !0,
            t !== !0 && --K.readyWait > 0 || (mt.resolveWith(J, [K]),
            K.fn.triggerHandler && (K(J).triggerHandler("ready"),
            K(J).off("ready"))))
        }
    }),
    K.ready.promise = function(e) {
        return mt || (mt = K.Deferred(),
        "complete" === J.readyState ? setTimeout(K.ready) : (J.addEventListener("DOMContentLoaded", s, !1),
        t.addEventListener("load", s, !1))),
        mt.promise(e)
    }
    ,
    K.ready.promise();
    var gt = K.access = function(t, e, n, i, r, o, s) {
        var a = 0
          , l = t.length
          , u = null == n;
        if ("object" === K.type(n)) {
            r = !0;
            for (a in n)
                K.access(t, e, a, n[a], !0, o, s)
        } else if (void 0 !== i && (r = !0,
        K.isFunction(i) || (s = !0),
        u && (s ? (e.call(t, i),
        e = null) : (u = e,
        e = function(t, e, n) {
            return u.call(K(t), n)
        }
        )),
        e))
            for (; l > a; a++)
                e(t[a], n, s ? i : i.call(t[a], a, e(t[a], n)));
        return r ? t : u ? e.call(t) : l ? e(t[0], n) : o
    }
    ;
    K.acceptData = function(t) {
        return 1 === t.nodeType || 9 === t.nodeType || !+t.nodeType
    }
    ,
    a.uid = 1,
    a.accepts = K.acceptData,
    a.prototype = {
        key: function(t) {
            if (!a.accepts(t))
                return 0;
            var e = {}
              , n = t[this.expando];
            if (!n) {
                n = a.uid++;
                try {
                    e[this.expando] = {
                        value: n
                    },
                    Object.defineProperties(t, e)
                } catch (i) {
                    e[this.expando] = n,
                    K.extend(t, e)
                }
            }
            return this.cache[n] || (this.cache[n] = {}),
            n
        },
        set: function(t, e, n) {
            var i, r = this.key(t), o = this.cache[r];
            if ("string" == typeof e)
                o[e] = n;
            else if (K.isEmptyObject(o))
                K.extend(this.cache[r], e);
            else
                for (i in e)
                    o[i] = e[i];
            return o
        },
        get: function(t, e) {
            var n = this.cache[this.key(t)];
            return void 0 === e ? n : n[e]
        },
        access: function(t, e, n) {
            var i;
            return void 0 === e || e && "string" == typeof e && void 0 === n ? (i = this.get(t, e),
            void 0 !== i ? i : this.get(t, K.camelCase(e))) : (this.set(t, e, n),
            void 0 !== n ? n : e)
        },
        remove: function(t, e) {
            var n, i, r, o = this.key(t), s = this.cache[o];
            if (void 0 === e)
                this.cache[o] = {};
            else {
                K.isArray(e) ? i = e.concat(e.map(K.camelCase)) : (r = K.camelCase(e),
                e in s ? i = [e, r] : (i = r,
                i = i in s ? [i] : i.match(ft) || [])),
                n = i.length;
                for (; n--; )
                    delete s[i[n]]
            }
        },
        hasData: function(t) {
            return !K.isEmptyObject(this.cache[t[this.expando]] || {})
        },
        discard: function(t) {
            t[this.expando] && delete this.cache[t[this.expando]]
        }
    };
    var vt = new a
      , yt = new a
      , _t = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/
      , xt = /([A-Z])/g;
    K.extend({
        hasData: function(t) {
            return yt.hasData(t) || vt.hasData(t)
        },
        data: function(t, e, n) {
            return yt.access(t, e, n)
        },
        removeData: function(t, e) {
            yt.remove(t, e)
        },
        _data: function(t, e, n) {
            return vt.access(t, e, n)
        },
        _removeData: function(t, e) {
            vt.remove(t, e)
        }
    }),
    K.fn.extend({
        data: function(t, e) {
            var n, i, r, o = this[0], s = o && o.attributes;
            if (void 0 === t) {
                if (this.length && (r = yt.get(o),
                1 === o.nodeType && !vt.get(o, "hasDataAttrs"))) {
                    for (n = s.length; n--; )
                        s[n] && (i = s[n].name,
                        0 === i.indexOf("data-") && (i = K.camelCase(i.slice(5)),
                        l(o, i, r[i])));
                    vt.set(o, "hasDataAttrs", !0)
                }
                return r
            }
            return "object" == typeof t ? this.each(function() {
                yt.set(this, t)
            }) : gt(this, function(e) {
                var n, i = K.camelCase(t);
                if (o && void 0 === e) {
                    if (n = yt.get(o, t),
                    void 0 !== n)
                        return n;
                    if (n = yt.get(o, i),
                    void 0 !== n)
                        return n;
                    if (n = l(o, i, void 0),
                    void 0 !== n)
                        return n
                } else
                    this.each(function() {
                        var n = yt.get(this, i);
                        yt.set(this, i, e),
                        -1 !== t.indexOf("-") && void 0 !== n && yt.set(this, t, e)
                    })
            }, null, e, arguments.length > 1, null, !0)
        },
        removeData: function(t) {
            return this.each(function() {
                yt.remove(this, t)
            })
        }
    }),
    K.extend({
        queue: function(t, e, n) {
            var i;
            return t ? (e = (e || "fx") + "queue",
            i = vt.get(t, e),
            n && (!i || K.isArray(n) ? i = vt.access(t, e, K.makeArray(n)) : i.push(n)),
            i || []) : void 0
        },
        dequeue: function(t, e) {
            e = e || "fx";
            var n = K.queue(t, e)
              , i = n.length
              , r = n.shift()
              , o = K._queueHooks(t, e)
              , s = function() {
                K.dequeue(t, e)
            };
            "inprogress" === r && (r = n.shift(),
            i--),
            r && ("fx" === e && n.unshift("inprogress"),
            delete o.stop,
            r.call(t, s, o)),
            !i && o && o.empty.fire()
        },
        _queueHooks: function(t, e) {
            var n = e + "queueHooks";
            return vt.get(t, n) || vt.access(t, n, {
                empty: K.Callbacks("once memory").add(function() {
                    vt.remove(t, [e + "queue", n])
                })
            })
        }
    }),
    K.fn.extend({
        queue: function(t, e) {
            var n = 2;
            return "string" != typeof t && (e = t,
            t = "fx",
            n--),
            arguments.length < n ? K.queue(this[0], t) : void 0 === e ? this : this.each(function() {
                var n = K.queue(this, t, e);
                K._queueHooks(this, t),
                "fx" === t && "inprogress" !== n[0] && K.dequeue(this, t)
            })
        },
        dequeue: function(t) {
            return this.each(function() {
                K.dequeue(this, t)
            })
        },
        clearQueue: function(t) {
            return this.queue(t || "fx", [])
        },
        promise: function(t, e) {
            var n, i = 1, r = K.Deferred(), o = this, s = this.length, a = function() {
                --i || r.resolveWith(o, [o])
            };
            for ("string" != typeof t && (e = t,
            t = void 0),
            t = t || "fx"; s--; )
                n = vt.get(o[s], t + "queueHooks"),
                n && n.empty && (i++,
                n.empty.add(a));
            return a(),
            r.promise(e)
        }
    });
    var wt = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source
      , bt = ["Top", "Right", "Bottom", "Left"]
      , Tt = function(t, e) {
        return t = e || t,
        "none" === K.css(t, "display") || !K.contains(t.ownerDocument, t)
    }
      , Ct = /^(?:checkbox|radio)$/i;
    !function() {
        var t = J.createDocumentFragment()
          , e = t.appendChild(J.createElement("div"))
          , n = J.createElement("input");
        n.setAttribute("type", "radio"),
        n.setAttribute("checked", "checked"),
        n.setAttribute("name", "t"),
        e.appendChild(n),
        Q.checkClone = e.cloneNode(!0).cloneNode(!0).lastChild.checked,
        e.innerHTML = "<textarea>x</textarea>",
        Q.noCloneChecked = !!e.cloneNode(!0).lastChild.defaultValue
    }();
    var St = "undefined";
    Q.focusinBubbles = "onfocusin"in t;
    var kt = /^key/
      , jt = /^(?:mouse|pointer|contextmenu)|click/
      , Pt = /^(?:focusinfocus|focusoutblur)$/
      , Et = /^([^.]*)(?:\.(.+)|)$/;
    K.event = {
        global: {},
        add: function(t, e, n, i, r) {
            var o, s, a, l, u, c, h, p, f, d, m, g = vt.get(t);
            if (g)
                for (n.handler && (o = n,
                n = o.handler,
                r = o.selector),
                n.guid || (n.guid = K.guid++),
                (l = g.events) || (l = g.events = {}),
                (s = g.handle) || (s = g.handle = function(e) {
                    return typeof K !== St && K.event.triggered !== e.type ? K.event.dispatch.apply(t, arguments) : void 0
                }
                ),
                e = (e || "").match(ft) || [""],
                u = e.length; u--; )
                    a = Et.exec(e[u]) || [],
                    f = m = a[1],
                    d = (a[2] || "").split(".").sort(),
                    f && (h = K.event.special[f] || {},
                    f = (r ? h.delegateType : h.bindType) || f,
                    h = K.event.special[f] || {},
                    c = K.extend({
                        type: f,
                        origType: m,
                        data: i,
                        handler: n,
                        guid: n.guid,
                        selector: r,
                        needsContext: r && K.expr.match.needsContext.test(r),
                        namespace: d.join(".")
                    }, o),
                    (p = l[f]) || (p = l[f] = [],
                    p.delegateCount = 0,
                    h.setup && h.setup.call(t, i, d, s) !== !1 || t.addEventListener && t.addEventListener(f, s, !1)),
                    h.add && (h.add.call(t, c),
                    c.handler.guid || (c.handler.guid = n.guid)),
                    r ? p.splice(p.delegateCount++, 0, c) : p.push(c),
                    K.event.global[f] = !0)
        },
        remove: function(t, e, n, i, r) {
            var o, s, a, l, u, c, h, p, f, d, m, g = vt.hasData(t) && vt.get(t);
            if (g && (l = g.events)) {
                for (e = (e || "").match(ft) || [""],
                u = e.length; u--; )
                    if (a = Et.exec(e[u]) || [],
                    f = m = a[1],
                    d = (a[2] || "").split(".").sort(),
                    f) {
                        for (h = K.event.special[f] || {},
                        f = (i ? h.delegateType : h.bindType) || f,
                        p = l[f] || [],
                        a = a[2] && new RegExp("(^|\\.)" + d.join("\\.(?:.*\\.|)") + "(\\.|$)"),
                        s = o = p.length; o--; )
                            c = p[o],
                            !r && m !== c.origType || n && n.guid !== c.guid || a && !a.test(c.namespace) || i && i !== c.selector && ("**" !== i || !c.selector) || (p.splice(o, 1),
                            c.selector && p.delegateCount--,
                            h.remove && h.remove.call(t, c));
                        s && !p.length && (h.teardown && h.teardown.call(t, d, g.handle) !== !1 || K.removeEvent(t, f, g.handle),
                        delete l[f])
                    } else
                        for (f in l)
                            K.event.remove(t, f + e[u], n, i, !0);
                K.isEmptyObject(l) && (delete g.handle,
                vt.remove(t, "events"))
            }
        },
        trigger: function(e, n, i, r) {
            var o, s, a, l, u, c, h, p = [i || J], f = G.call(e, "type") ? e.type : e, d = G.call(e, "namespace") ? e.namespace.split(".") : [];
            if (s = a = i = i || J,
            3 !== i.nodeType && 8 !== i.nodeType && !Pt.test(f + K.event.triggered) && (f.indexOf(".") >= 0 && (d = f.split("."),
            f = d.shift(),
            d.sort()),
            u = f.indexOf(":") < 0 && "on" + f,
            e = e[K.expando] ? e : new K.Event(f,"object" == typeof e && e),
            e.isTrigger = r ? 2 : 3,
            e.namespace = d.join("."),
            e.namespace_re = e.namespace ? new RegExp("(^|\\.)" + d.join("\\.(?:.*\\.|)") + "(\\.|$)") : null,
            e.result = void 0,
            e.target || (e.target = i),
            n = null == n ? [e] : K.makeArray(n, [e]),
            h = K.event.special[f] || {},
            r || !h.trigger || h.trigger.apply(i, n) !== !1)) {
                if (!r && !h.noBubble && !K.isWindow(i)) {
                    for (l = h.delegateType || f,
                    Pt.test(l + f) || (s = s.parentNode); s; s = s.parentNode)
                        p.push(s),
                        a = s;
                    a === (i.ownerDocument || J) && p.push(a.defaultView || a.parentWindow || t)
                }
                for (o = 0; (s = p[o++]) && !e.isPropagationStopped(); )
                    e.type = o > 1 ? l : h.bindType || f,
                    c = (vt.get(s, "events") || {})[e.type] && vt.get(s, "handle"),
                    c && c.apply(s, n),
                    c = u && s[u],
                    c && c.apply && K.acceptData(s) && (e.result = c.apply(s, n),
                    e.result === !1 && e.preventDefault());
                return e.type = f,
                r || e.isDefaultPrevented() || h._default && h._default.apply(p.pop(), n) !== !1 || !K.acceptData(i) || u && K.isFunction(i[f]) && !K.isWindow(i) && (a = i[u],
                a && (i[u] = null),
                K.event.triggered = f,
                i[f](),
                K.event.triggered = void 0,
                a && (i[u] = a)),
                e.result
            }
        },
        dispatch: function(t) {
            t = K.event.fix(t);
            var e, n, i, r, o, s = [], a = z.call(arguments), l = (vt.get(this, "events") || {})[t.type] || [], u = K.event.special[t.type] || {};
            if (a[0] = t,
            t.delegateTarget = this,
            !u.preDispatch || u.preDispatch.call(this, t) !== !1) {
                for (s = K.event.handlers.call(this, t, l),
                e = 0; (r = s[e++]) && !t.isPropagationStopped(); )
                    for (t.currentTarget = r.elem,
                    n = 0; (o = r.handlers[n++]) && !t.isImmediatePropagationStopped(); )
                        (!t.namespace_re || t.namespace_re.test(o.namespace)) && (t.handleObj = o,
                        t.data = o.data,
                        i = ((K.event.special[o.origType] || {}).handle || o.handler).apply(r.elem, a),
                        void 0 !== i && (t.result = i) === !1 && (t.preventDefault(),
                        t.stopPropagation()));
                return u.postDispatch && u.postDispatch.call(this, t),
                t.result
            }
        },
        handlers: function(t, e) {
            var n, i, r, o, s = [], a = e.delegateCount, l = t.target;
            if (a && l.nodeType && (!t.button || "click" !== t.type))
                for (; l !== this; l = l.parentNode || this)
                    if (l.disabled !== !0 || "click" !== t.type) {
                        for (i = [],
                        n = 0; a > n; n++)
                            o = e[n],
                            r = o.selector + " ",
                            void 0 === i[r] && (i[r] = o.needsContext ? K(r, this).index(l) >= 0 : K.find(r, this, null, [l]).length),
                            i[r] && i.push(o);
                        i.length && s.push({
                            elem: l,
                            handlers: i
                        })
                    }
            return a < e.length && s.push({
                elem: this,
                handlers: e.slice(a)
            }),
            s
        },
        props: "altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),
        fixHooks: {},
        keyHooks: {
            props: "char charCode key keyCode".split(" "),
            filter: function(t, e) {
                return null == t.which && (t.which = null != e.charCode ? e.charCode : e.keyCode),
                t
            }
        },
        mouseHooks: {
            props: "button buttons clientX clientY offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
            filter: function(t, e) {
                var n, i, r, o = e.button;
                return null == t.pageX && null != e.clientX && (n = t.target.ownerDocument || J,
                i = n.documentElement,
                r = n.body,
                t.pageX = e.clientX + (i && i.scrollLeft || r && r.scrollLeft || 0) - (i && i.clientLeft || r && r.clientLeft || 0),
                t.pageY = e.clientY + (i && i.scrollTop || r && r.scrollTop || 0) - (i && i.clientTop || r && r.clientTop || 0)),
                t.which || void 0 === o || (t.which = 1 & o ? 1 : 2 & o ? 3 : 4 & o ? 2 : 0),
                t
            }
        },
        fix: function(t) {
            if (t[K.expando])
                return t;
            var e, n, i, r = t.type, o = t, s = this.fixHooks[r];
            for (s || (this.fixHooks[r] = s = jt.test(r) ? this.mouseHooks : kt.test(r) ? this.keyHooks : {}),
            i = s.props ? this.props.concat(s.props) : this.props,
            t = new K.Event(o),
            e = i.length; e--; )
                n = i[e],
                t[n] = o[n];
            return t.target || (t.target = J),
            3 === t.target.nodeType && (t.target = t.target.parentNode),
            s.filter ? s.filter(t, o) : t
        },
        special: {
            load: {
                noBubble: !0
            },
            focus: {
                trigger: function() {
                    return this !== h() && this.focus ? (this.focus(),
                    !1) : void 0
                },
                delegateType: "focusin"
            },
            blur: {
                trigger: function() {
                    return this === h() && this.blur ? (this.blur(),
                    !1) : void 0
                },
                delegateType: "focusout"
            },
            click: {
                trigger: function() {
                    return "checkbox" === this.type && this.click && K.nodeName(this, "input") ? (this.click(),
                    !1) : void 0
                },
                _default: function(t) {
                    return K.nodeName(t.target, "a")
                }
            },
            beforeunload: {
                postDispatch: function(t) {
                    void 0 !== t.result && t.originalEvent && (t.originalEvent.returnValue = t.result)
                }
            }
        },
        simulate: function(t, e, n, i) {
            var r = K.extend(new K.Event, n, {
                type: t,
                isSimulated: !0,
                originalEvent: {}
            });
            i ? K.event.trigger(r, null, e) : K.event.dispatch.call(e, r),
            r.isDefaultPrevented() && n.preventDefault()
        }
    },
    K.removeEvent = function(t, e, n) {
        t.removeEventListener && t.removeEventListener(e, n, !1)
    }
    ,
    K.Event = function(t, e) {
        return this instanceof K.Event ? (t && t.type ? (this.originalEvent = t,
        this.type = t.type,
        this.isDefaultPrevented = t.defaultPrevented || void 0 === t.defaultPrevented && t.returnValue === !1 ? u : c) : this.type = t,
        e && K.extend(this, e),
        this.timeStamp = t && t.timeStamp || K.now(),
        void (this[K.expando] = !0)) : new K.Event(t,e)
    }
    ,
    K.Event.prototype = {
        isDefaultPrevented: c,
        isPropagationStopped: c,
        isImmediatePropagationStopped: c,
        preventDefault: function() {
            var t = this.originalEvent;
            this.isDefaultPrevented = u,
            t && t.preventDefault && t.preventDefault()
        },
        stopPropagation: function() {
            var t = this.originalEvent;
            this.isPropagationStopped = u,
            t && t.stopPropagation && t.stopPropagation()
        },
        stopImmediatePropagation: function() {
            var t = this.originalEvent;
            this.isImmediatePropagationStopped = u,
            t && t.stopImmediatePropagation && t.stopImmediatePropagation(),
            this.stopPropagation()
        }
    },
    K.each({
        mouseenter: "mouseover",
        mouseleave: "mouseout",
        pointerenter: "pointerover",
        pointerleave: "pointerout"
    }, function(t, e) {
        K.event.special[t] = {
            delegateType: e,
            bindType: e,
            handle: function(t) {
                var n, i = this, r = t.relatedTarget, o = t.handleObj;
                return (!r || r !== i && !K.contains(i, r)) && (t.type = o.origType,
                n = o.handler.apply(this, arguments),
                t.type = e),
                n
            }
        }
    }),
    Q.focusinBubbles || K.each({
        focus: "focusin",
        blur: "focusout"
    }, function(t, e) {
        var n = function(t) {
            K.event.simulate(e, t.target, K.event.fix(t), !0)
        };
        K.event.special[e] = {
            setup: function() {
                var i = this.ownerDocument || this
                  , r = vt.access(i, e);
                r || i.addEventListener(t, n, !0),
                vt.access(i, e, (r || 0) + 1)
            },
            teardown: function() {
                var i = this.ownerDocument || this
                  , r = vt.access(i, e) - 1;
                r ? vt.access(i, e, r) : (i.removeEventListener(t, n, !0),
                vt.remove(i, e))
            }
        }
    }),
    K.fn.extend({
        on: function(t, e, n, i, r) {
            var o, s;
            if ("object" == typeof t) {
                "string" != typeof e && (n = n || e,
                e = void 0);
                for (s in t)
                    this.on(s, e, n, t[s], r);
                return this
            }
            if (null == n && null == i ? (i = e,
            n = e = void 0) : null == i && ("string" == typeof e ? (i = n,
            n = void 0) : (i = n,
            n = e,
            e = void 0)),
            i === !1)
                i = c;
            else if (!i)
                return this;
            return 1 === r && (o = i,
            i = function(t) {
                return K().off(t),
                o.apply(this, arguments)
            }
            ,
            i.guid = o.guid || (o.guid = K.guid++)),
            this.each(function() {
                K.event.add(this, t, i, n, e)
            })
        },
        one: function(t, e, n, i) {
            return this.on(t, e, n, i, 1)
        },
        off: function(t, e, n) {
            var i, r;
            if (t && t.preventDefault && t.handleObj)
                return i = t.handleObj,
                K(t.delegateTarget).off(i.namespace ? i.origType + "." + i.namespace : i.origType, i.selector, i.handler),
                this;
            if ("object" == typeof t) {
                for (r in t)
                    this.off(r, e, t[r]);
                return this
            }
            return (e === !1 || "function" == typeof e) && (n = e,
            e = void 0),
            n === !1 && (n = c),
            this.each(function() {
                K.event.remove(this, t, n, e)
            })
        },
        trigger: function(t, e) {
            return this.each(function() {
                K.event.trigger(t, e, this)
            })
        },
        triggerHandler: function(t, e) {
            var n = this[0];
            return n ? K.event.trigger(t, e, n, !0) : void 0
        }
    });
    var Ot = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi
      , Mt = /<([\w:]+)/
      , Dt = /<|&#?\w+;/
      , At = /<(?:script|style|link)/i
      , Nt = /checked\s*(?:[^=]|=\s*.checked.)/i
      , Lt = /^$|\/(?:java|ecma)script/i
      , Rt = /^true\/(.*)/
      , $t = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g
      , It = {
        option: [1, "<select multiple='multiple'>", "</select>"],
        thead: [1, "<table>", "</table>"],
        col: [2, "<table><colgroup>", "</colgroup></table>"],
        tr: [2, "<table><tbody>", "</tbody></table>"],
        td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
        _default: [0, "", ""]
    };
    It.optgroup = It.option,
    It.tbody = It.tfoot = It.colgroup = It.caption = It.thead,
    It.th = It.td,
    K.extend({
        clone: function(t, e, n) {
            var i, r, o, s, a = t.cloneNode(!0), l = K.contains(t.ownerDocument, t);
            if (!(Q.noCloneChecked || 1 !== t.nodeType && 11 !== t.nodeType || K.isXMLDoc(t)))
                for (s = v(a),
                o = v(t),
                i = 0,
                r = o.length; r > i; i++)
                    y(o[i], s[i]);
            if (e)
                if (n)
                    for (o = o || v(t),
                    s = s || v(a),
                    i = 0,
                    r = o.length; r > i; i++)
                        g(o[i], s[i]);
                else
                    g(t, a);
            return s = v(a, "script"),
            s.length > 0 && m(s, !l && v(t, "script")),
            a
        },
        buildFragment: function(t, e, n, i) {
            for (var r, o, s, a, l, u, c = e.createDocumentFragment(), h = [], p = 0, f = t.length; f > p; p++)
                if (r = t[p],
                r || 0 === r)
                    if ("object" === K.type(r))
                        K.merge(h, r.nodeType ? [r] : r);
                    else if (Dt.test(r)) {
                        for (o = o || c.appendChild(e.createElement("div")),
                        s = (Mt.exec(r) || ["", ""])[1].toLowerCase(),
                        a = It[s] || It._default,
                        o.innerHTML = a[1] + r.replace(Ot, "<$1></$2>") + a[2],
                        u = a[0]; u--; )
                            o = o.lastChild;
                        K.merge(h, o.childNodes),
                        o = c.firstChild,
                        o.textContent = ""
                    } else
                        h.push(e.createTextNode(r));
            for (c.textContent = "",
            p = 0; r = h[p++]; )
                if ((!i || -1 === K.inArray(r, i)) && (l = K.contains(r.ownerDocument, r),
                o = v(c.appendChild(r), "script"),
                l && m(o),
                n))
                    for (u = 0; r = o[u++]; )
                        Lt.test(r.type || "") && n.push(r);
            return c
        },
        cleanData: function(t) {
            for (var e, n, i, r, o = K.event.special, s = 0; void 0 !== (n = t[s]); s++) {
                if (K.acceptData(n) && (r = n[vt.expando],
                r && (e = vt.cache[r]))) {
                    if (e.events)
                        for (i in e.events)
                            o[i] ? K.event.remove(n, i) : K.removeEvent(n, i, e.handle);
                    vt.cache[r] && delete vt.cache[r]
                }
                delete yt.cache[n[yt.expando]]
            }
        }
    }),
    K.fn.extend({
        text: function(t) {
            return gt(this, function(t) {
                return void 0 === t ? K.text(this) : this.empty().each(function() {
                    (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) && (this.textContent = t)
                })
            }, null, t, arguments.length)
        },
        append: function() {
            return this.domManip(arguments, function(t) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var e = p(this, t);
                    e.appendChild(t)
                }
            })
        },
        prepend: function() {
            return this.domManip(arguments, function(t) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var e = p(this, t);
                    e.insertBefore(t, e.firstChild)
                }
            })
        },
        before: function() {
            return this.domManip(arguments, function(t) {
                this.parentNode && this.parentNode.insertBefore(t, this)
            })
        },
        after: function() {
            return this.domManip(arguments, function(t) {
                this.parentNode && this.parentNode.insertBefore(t, this.nextSibling)
            })
        },
        remove: function(t, e) {
            for (var n, i = t ? K.filter(t, this) : this, r = 0; null != (n = i[r]); r++)
                e || 1 !== n.nodeType || K.cleanData(v(n)),
                n.parentNode && (e && K.contains(n.ownerDocument, n) && m(v(n, "script")),
                n.parentNode.removeChild(n));
            return this
        },
        empty: function() {
            for (var t, e = 0; null != (t = this[e]); e++)
                1 === t.nodeType && (K.cleanData(v(t, !1)),
                t.textContent = "");
            return this
        },
        clone: function(t, e) {
            return t = null == t ? !1 : t,
            e = null == e ? t : e,
            this.map(function() {
                return K.clone(this, t, e)
            })
        },
        html: function(t) {
            return gt(this, function(t) {
                var e = this[0] || {}
                  , n = 0
                  , i = this.length;
                if (void 0 === t && 1 === e.nodeType)
                    return e.innerHTML;
                if ("string" == typeof t && !At.test(t) && !It[(Mt.exec(t) || ["", ""])[1].toLowerCase()]) {
                    t = t.replace(Ot, "<$1></$2>");
                    try {
                        for (; i > n; n++)
                            e = this[n] || {},
                            1 === e.nodeType && (K.cleanData(v(e, !1)),
                            e.innerHTML = t);
                        e = 0
                    } catch (r) {}
                }
                e && this.empty().append(t)
            }, null, t, arguments.length)
        },
        replaceWith: function() {
            var t = arguments[0];
            return this.domManip(arguments, function(e) {
                t = this.parentNode,
                K.cleanData(v(this)),
                t && t.replaceChild(e, this)
            }),
            t && (t.length || t.nodeType) ? this : this.remove()
        },
        detach: function(t) {
            return this.remove(t, !0)
        },
        domManip: function(t, e) {
            t = W.apply([], t);
            var n, i, r, o, s, a, l = 0, u = this.length, c = this, h = u - 1, p = t[0], m = K.isFunction(p);
            if (m || u > 1 && "string" == typeof p && !Q.checkClone && Nt.test(p))
                return this.each(function(n) {
                    var i = c.eq(n);
                    m && (t[0] = p.call(this, n, i.html())),
                    i.domManip(t, e)
                });
            if (u && (n = K.buildFragment(t, this[0].ownerDocument, !1, this),
            i = n.firstChild,
            1 === n.childNodes.length && (n = i),
            i)) {
                for (r = K.map(v(n, "script"), f),
                o = r.length; u > l; l++)
                    s = n,
                    l !== h && (s = K.clone(s, !0, !0),
                    o && K.merge(r, v(s, "script"))),
                    e.call(this[l], s, l);
                if (o)
                    for (a = r[r.length - 1].ownerDocument,
                    K.map(r, d),
                    l = 0; o > l; l++)
                        s = r[l],
                        Lt.test(s.type || "") && !vt.access(s, "globalEval") && K.contains(a, s) && (s.src ? K._evalUrl && K._evalUrl(s.src) : K.globalEval(s.textContent.replace($t, "")))
            }
            return this
        }
    }),
    K.each({
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, function(t, e) {
        K.fn[t] = function(t) {
            for (var n, i = [], r = K(t), o = r.length - 1, s = 0; o >= s; s++)
                n = s === o ? this : this.clone(!0),
                K(r[s])[e](n),
                X.apply(i, n.get());
            return this.pushStack(i)
        }
    });
    var qt, Ht = {}, Ft = /^margin/, Bt = new RegExp("^(" + wt + ")(?!px)[a-z%]+$","i"), zt = function(e) {
        return e.ownerDocument.defaultView.opener ? e.ownerDocument.defaultView.getComputedStyle(e, null) : t.getComputedStyle(e, null)
    };
    !function() {
        function e() {
            s.style.cssText = "-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;display:block;margin-top:1%;top:1%;border:1px;padding:1px;width:4px;position:absolute",
            s.innerHTML = "",
            r.appendChild(o);
            var e = t.getComputedStyle(s, null);
            n = "1%" !== e.top,
            i = "4px" === e.width,
            r.removeChild(o)
        }
        var n, i, r = J.documentElement, o = J.createElement("div"), s = J.createElement("div");
        s.style && (s.style.backgroundClip = "content-box",
        s.cloneNode(!0).style.backgroundClip = "",
        Q.clearCloneStyle = "content-box" === s.style.backgroundClip,
        o.style.cssText = "border:0;width:0;height:0;top:0;left:-9999px;margin-top:1px;position:absolute",
        o.appendChild(s),
        t.getComputedStyle && K.extend(Q, {
            pixelPosition: function() {
                return e(),
                n
            },
            boxSizingReliable: function() {
                return null == i && e(),
                i
            },
            reliableMarginRight: function() {
                var e, n = s.appendChild(J.createElement("div"));
                return n.style.cssText = s.style.cssText = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0",
                n.style.marginRight = n.style.width = "0",
                s.style.width = "1px",
                r.appendChild(o),
                e = !parseFloat(t.getComputedStyle(n, null).marginRight),
                r.removeChild(o),
                s.removeChild(n),
                e
            }
        }))
    }(),
    K.swap = function(t, e, n, i) {
        var r, o, s = {};
        for (o in e)
            s[o] = t.style[o],
            t.style[o] = e[o];
        r = n.apply(t, i || []);
        for (o in e)
            t.style[o] = s[o];
        return r
    }
    ;
    var Wt = /^(none|table(?!-c[ea]).+)/
      , Xt = new RegExp("^(" + wt + ")(.*)$","i")
      , Vt = new RegExp("^([+-])=(" + wt + ")","i")
      , Yt = {
        position: "absolute",
        visibility: "hidden",
        display: "block"
    }
      , Ut = {
        letterSpacing: "0",
        fontWeight: "400"
    }
      , Gt = ["Webkit", "O", "Moz", "ms"];
    K.extend({
        cssHooks: {
            opacity: {
                get: function(t, e) {
                    if (e) {
                        var n = w(t, "opacity");
                        return "" === n ? "1" : n
                    }
                }
            }
        },
        cssNumber: {
            columnCount: !0,
            fillOpacity: !0,
            flexGrow: !0,
            flexShrink: !0,
            fontWeight: !0,
            lineHeight: !0,
            opacity: !0,
            order: !0,
            orphans: !0,
            widows: !0,
            zIndex: !0,
            zoom: !0
        },
        cssProps: {
            "float": "cssFloat"
        },
        style: function(t, e, n, i) {
            if (t && 3 !== t.nodeType && 8 !== t.nodeType && t.style) {
                var r, o, s, a = K.camelCase(e), l = t.style;
                return e = K.cssProps[a] || (K.cssProps[a] = T(l, a)),
                s = K.cssHooks[e] || K.cssHooks[a],
                void 0 === n ? s && "get"in s && void 0 !== (r = s.get(t, !1, i)) ? r : l[e] : (o = typeof n,
                "string" === o && (r = Vt.exec(n)) && (n = (r[1] + 1) * r[2] + parseFloat(K.css(t, e)),
                o = "number"),
                void (null != n && n === n && ("number" !== o || K.cssNumber[a] || (n += "px"),
                Q.clearCloneStyle || "" !== n || 0 !== e.indexOf("background") || (l[e] = "inherit"),
                s && "set"in s && void 0 === (n = s.set(t, n, i)) || (l[e] = n))))
            }
        },
        css: function(t, e, n, i) {
            var r, o, s, a = K.camelCase(e);
            return e = K.cssProps[a] || (K.cssProps[a] = T(t.style, a)),
            s = K.cssHooks[e] || K.cssHooks[a],
            s && "get"in s && (r = s.get(t, !0, n)),
            void 0 === r && (r = w(t, e, i)),
            "normal" === r && e in Ut && (r = Ut[e]),
            "" === n || n ? (o = parseFloat(r),
            n === !0 || K.isNumeric(o) ? o || 0 : r) : r
        }
    }),
    K.each(["height", "width"], function(t, e) {
        K.cssHooks[e] = {
            get: function(t, n, i) {
                return n ? Wt.test(K.css(t, "display")) && 0 === t.offsetWidth ? K.swap(t, Yt, function() {
                    return k(t, e, i)
                }) : k(t, e, i) : void 0
            },
            set: function(t, n, i) {
                var r = i && zt(t);
                return C(t, n, i ? S(t, e, i, "border-box" === K.css(t, "boxSizing", !1, r), r) : 0)
            }
        }
    }),
    K.cssHooks.marginRight = b(Q.reliableMarginRight, function(t, e) {
        return e ? K.swap(t, {
            display: "inline-block"
        }, w, [t, "marginRight"]) : void 0
    }),
    K.each({
        margin: "",
        padding: "",
        border: "Width"
    }, function(t, e) {
        K.cssHooks[t + e] = {
            expand: function(n) {
                for (var i = 0, r = {}, o = "string" == typeof n ? n.split(" ") : [n]; 4 > i; i++)
                    r[t + bt[i] + e] = o[i] || o[i - 2] || o[0];
                return r
            }
        },
        Ft.test(t) || (K.cssHooks[t + e].set = C)
    }),
    K.fn.extend({
        css: function(t, e) {
            return gt(this, function(t, e, n) {
                var i, r, o = {}, s = 0;
                if (K.isArray(e)) {
                    for (i = zt(t),
                    r = e.length; r > s; s++)
                        o[e[s]] = K.css(t, e[s], !1, i);
                    return o
                }
                return void 0 !== n ? K.style(t, e, n) : K.css(t, e)
            }, t, e, arguments.length > 1)
        },
        show: function() {
            return j(this, !0)
        },
        hide: function() {
            return j(this)
        },
        toggle: function(t) {
            return "boolean" == typeof t ? t ? this.show() : this.hide() : this.each(function() {
                Tt(this) ? K(this).show() : K(this).hide()
            })
        }
    }),
    K.Tween = P,
    P.prototype = {
        constructor: P,
        init: function(t, e, n, i, r, o) {
            this.elem = t,
            this.prop = n,
            this.easing = r || "swing",
            this.options = e,
            this.start = this.now = this.cur(),
            this.end = i,
            this.unit = o || (K.cssNumber[n] ? "" : "px")
        },
        cur: function() {
            var t = P.propHooks[this.prop];
            return t && t.get ? t.get(this) : P.propHooks._default.get(this)
        },
        run: function(t) {
            var e, n = P.propHooks[this.prop];
            return this.options.duration ? this.pos = e = K.easing[this.easing](t, this.options.duration * t, 0, 1, this.options.duration) : this.pos = e = t,
            this.now = (this.end - this.start) * e + this.start,
            this.options.step && this.options.step.call(this.elem, this.now, this),
            n && n.set ? n.set(this) : P.propHooks._default.set(this),
            this
        }
    },
    P.prototype.init.prototype = P.prototype,
    P.propHooks = {
        _default: {
            get: function(t) {
                var e;
                return null == t.elem[t.prop] || t.elem.style && null != t.elem.style[t.prop] ? (e = K.css(t.elem, t.prop, ""),
                e && "auto" !== e ? e : 0) : t.elem[t.prop]
            },
            set: function(t) {
                K.fx.step[t.prop] ? K.fx.step[t.prop](t) : t.elem.style && (null != t.elem.style[K.cssProps[t.prop]] || K.cssHooks[t.prop]) ? K.style(t.elem, t.prop, t.now + t.unit) : t.elem[t.prop] = t.now
            }
        }
    },
    P.propHooks.scrollTop = P.propHooks.scrollLeft = {
        set: function(t) {
            t.elem.nodeType && t.elem.parentNode && (t.elem[t.prop] = t.now)
        }
    },
    K.easing = {
        linear: function(t) {
            return t
        },
        swing: function(t) {
            return .5 - Math.cos(t * Math.PI) / 2
        }
    },
    K.fx = P.prototype.init,
    K.fx.step = {};
    var Qt, Jt, Zt = /^(?:toggle|show|hide)$/, Kt = new RegExp("^(?:([+-])=|)(" + wt + ")([a-z%]*)$","i"), te = /queueHooks$/, ee = [D], ne = {
        "*": [function(t, e) {
            var n = this.createTween(t, e)
              , i = n.cur()
              , r = Kt.exec(e)
              , o = r && r[3] || (K.cssNumber[t] ? "" : "px")
              , s = (K.cssNumber[t] || "px" !== o && +i) && Kt.exec(K.css(n.elem, t))
              , a = 1
              , l = 20;
            if (s && s[3] !== o) {
                o = o || s[3],
                r = r || [],
                s = +i || 1;
                do
                    a = a || ".5",
                    s /= a,
                    K.style(n.elem, t, s + o);
                while (a !== (a = n.cur() / i) && 1 !== a && --l)
            }
            return r && (s = n.start = +s || +i || 0,
            n.unit = o,
            n.end = r[1] ? s + (r[1] + 1) * r[2] : +r[2]),
            n
        }
        ]
    };
    K.Animation = K.extend(N, {
        tweener: function(t, e) {
            K.isFunction(t) ? (e = t,
            t = ["*"]) : t = t.split(" ");
            for (var n, i = 0, r = t.length; r > i; i++)
                n = t[i],
                ne[n] = ne[n] || [],
                ne[n].unshift(e)
        },
        prefilter: function(t, e) {
            e ? ee.unshift(t) : ee.push(t)
        }
    }),
    K.speed = function(t, e, n) {
        var i = t && "object" == typeof t ? K.extend({}, t) : {
            complete: n || !n && e || K.isFunction(t) && t,
            duration: t,
            easing: n && e || e && !K.isFunction(e) && e
        };
        return i.duration = K.fx.off ? 0 : "number" == typeof i.duration ? i.duration : i.duration in K.fx.speeds ? K.fx.speeds[i.duration] : K.fx.speeds._default,
        (null == i.queue || i.queue === !0) && (i.queue = "fx"),
        i.old = i.complete,
        i.complete = function() {
            K.isFunction(i.old) && i.old.call(this),
            i.queue && K.dequeue(this, i.queue)
        }
        ,
        i
    }
    ,
    K.fn.extend({
        fadeTo: function(t, e, n, i) {
            return this.filter(Tt).css("opacity", 0).show().end().animate({
                opacity: e
            }, t, n, i)
        },
        animate: function(t, e, n, i) {
            var r = K.isEmptyObject(t)
              , o = K.speed(e, n, i)
              , s = function() {
                var e = N(this, K.extend({}, t), o);
                (r || vt.get(this, "finish")) && e.stop(!0)
            };
            return s.finish = s,
            r || o.queue === !1 ? this.each(s) : this.queue(o.queue, s)
        },
        stop: function(t, e, n) {
            var i = function(t) {
                var e = t.stop;
                delete t.stop,
                e(n)
            };
            return "string" != typeof t && (n = e,
            e = t,
            t = void 0),
            e && t !== !1 && this.queue(t || "fx", []),
            this.each(function() {
                var e = !0
                  , r = null != t && t + "queueHooks"
                  , o = K.timers
                  , s = vt.get(this);
                if (r)
                    s[r] && s[r].stop && i(s[r]);
                else
                    for (r in s)
                        s[r] && s[r].stop && te.test(r) && i(s[r]);
                for (r = o.length; r--; )
                    o[r].elem !== this || null != t && o[r].queue !== t || (o[r].anim.stop(n),
                    e = !1,
                    o.splice(r, 1));
                (e || !n) && K.dequeue(this, t)
            })
        },
        finish: function(t) {
            return t !== !1 && (t = t || "fx"),
            this.each(function() {
                var e, n = vt.get(this), i = n[t + "queue"], r = n[t + "queueHooks"], o = K.timers, s = i ? i.length : 0;
                for (n.finish = !0,
                K.queue(this, t, []),
                r && r.stop && r.stop.call(this, !0),
                e = o.length; e--; )
                    o[e].elem === this && o[e].queue === t && (o[e].anim.stop(!0),
                    o.splice(e, 1));
                for (e = 0; s > e; e++)
                    i[e] && i[e].finish && i[e].finish.call(this);
                delete n.finish
            })
        }
    }),
    K.each(["toggle", "show", "hide"], function(t, e) {
        var n = K.fn[e];
        K.fn[e] = function(t, i, r) {
            return null == t || "boolean" == typeof t ? n.apply(this, arguments) : this.animate(O(e, !0), t, i, r)
        }
    }),
    K.each({
        slideDown: O("show"),
        slideUp: O("hide"),
        slideToggle: O("toggle"),
        fadeIn: {
            opacity: "show"
        },
        fadeOut: {
            opacity: "hide"
        },
        fadeToggle: {
            opacity: "toggle"
        }
    }, function(t, e) {
        K.fn[t] = function(t, n, i) {
            return this.animate(e, t, n, i)
        }
    }),
    K.timers = [],
    K.fx.tick = function() {
        var t, e = 0, n = K.timers;
        for (Qt = K.now(); e < n.length; e++)
            t = n[e],
            t() || n[e] !== t || n.splice(e--, 1);
        n.length || K.fx.stop(),
        Qt = void 0
    }
    ,
    K.fx.timer = function(t) {
        K.timers.push(t),
        t() ? K.fx.start() : K.timers.pop()
    }
    ,
    K.fx.interval = 13,
    K.fx.start = function() {
        Jt || (Jt = setInterval(K.fx.tick, K.fx.interval))
    }
    ,
    K.fx.stop = function() {
        clearInterval(Jt),
        Jt = null
    }
    ,
    K.fx.speeds = {
        slow: 600,
        fast: 200,
        _default: 400
    },
    K.fn.delay = function(t, e) {
        return t = K.fx ? K.fx.speeds[t] || t : t,
        e = e || "fx",
        this.queue(e, function(e, n) {
            var i = setTimeout(e, t);
            n.stop = function() {
                clearTimeout(i)
            }
        })
    }
    ,
    function() {
        var t = J.createElement("input")
          , e = J.createElement("select")
          , n = e.appendChild(J.createElement("option"));
        t.type = "checkbox",
        Q.checkOn = "" !== t.value,
        Q.optSelected = n.selected,
        e.disabled = !0,
        Q.optDisabled = !n.disabled,
        t = J.createElement("input"),
        t.value = "t",
        t.type = "radio",
        Q.radioValue = "t" === t.value
    }();
    var ie, re, oe = K.expr.attrHandle;
    K.fn.extend({
        attr: function(t, e) {
            return gt(this, K.attr, t, e, arguments.length > 1)
        },
        removeAttr: function(t) {
            return this.each(function() {
                K.removeAttr(this, t)
            })
        }
    }),
    K.extend({
        attr: function(t, e, n) {
            var i, r, o = t.nodeType;
            return t && 3 !== o && 8 !== o && 2 !== o ? typeof t.getAttribute === St ? K.prop(t, e, n) : (1 === o && K.isXMLDoc(t) || (e = e.toLowerCase(),
            i = K.attrHooks[e] || (K.expr.match.bool.test(e) ? re : ie)),
            void 0 === n ? i && "get"in i && null !== (r = i.get(t, e)) ? r : (r = K.find.attr(t, e),
            null == r ? void 0 : r) : null !== n ? i && "set"in i && void 0 !== (r = i.set(t, n, e)) ? r : (t.setAttribute(e, n + ""),
            n) : void K.removeAttr(t, e)) : void 0
        },
        removeAttr: function(t, e) {
            var n, i, r = 0, o = e && e.match(ft);
            if (o && 1 === t.nodeType)
                for (; n = o[r++]; )
                    i = K.propFix[n] || n,
                    K.expr.match.bool.test(n) && (t[i] = !1),
                    t.removeAttribute(n)
        },
        attrHooks: {
            type: {
                set: function(t, e) {
                    if (!Q.radioValue && "radio" === e && K.nodeName(t, "input")) {
                        var n = t.value;
                        return t.setAttribute("type", e),
                        n && (t.value = n),
                        e
                    }
                }
            }
        }
    }),
    re = {
        set: function(t, e, n) {
            return e === !1 ? K.removeAttr(t, n) : t.setAttribute(n, n),
            n
        }
    },
    K.each(K.expr.match.bool.source.match(/\w+/g), function(t, e) {
        var n = oe[e] || K.find.attr;
        oe[e] = function(t, e, i) {
            var r, o;
            return i || (o = oe[e],
            oe[e] = r,
            r = null != n(t, e, i) ? e.toLowerCase() : null,
            oe[e] = o),
            r
        }
    });
    var se = /^(?:input|select|textarea|button)$/i;
    K.fn.extend({
        prop: function(t, e) {
            return gt(this, K.prop, t, e, arguments.length > 1)
        },
        removeProp: function(t) {
            return this.each(function() {
                delete this[K.propFix[t] || t]
            })
        }
    }),
    K.extend({
        propFix: {
            "for": "htmlFor",
            "class": "className"
        },
        prop: function(t, e, n) {
            var i, r, o, s = t.nodeType;
            return t && 3 !== s && 8 !== s && 2 !== s ? (o = 1 !== s || !K.isXMLDoc(t),
            o && (e = K.propFix[e] || e,
            r = K.propHooks[e]),
            void 0 !== n ? r && "set"in r && void 0 !== (i = r.set(t, n, e)) ? i : t[e] = n : r && "get"in r && null !== (i = r.get(t, e)) ? i : t[e]) : void 0
        },
        propHooks: {
            tabIndex: {
                get: function(t) {
                    return t.hasAttribute("tabindex") || se.test(t.nodeName) || t.href ? t.tabIndex : -1
                }
            }
        }
    }),
    Q.optSelected || (K.propHooks.selected = {
        get: function(t) {
            var e = t.parentNode;
            return e && e.parentNode && e.parentNode.selectedIndex,
            null
        }
    }),
    K.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function() {
        K.propFix[this.toLowerCase()] = this
    });
    var ae = /[\t\r\n\f]/g;
    K.fn.extend({
        addClass: function(t) {
            var e, n, i, r, o, s, a = "string" == typeof t && t, l = 0, u = this.length;
            if (K.isFunction(t))
                return this.each(function(e) {
                    K(this).addClass(t.call(this, e, this.className))
                });
            if (a)
                for (e = (t || "").match(ft) || []; u > l; l++)
                    if (n = this[l],
                    i = 1 === n.nodeType && (n.className ? (" " + n.className + " ").replace(ae, " ") : " ")) {
                        for (o = 0; r = e[o++]; )
                            i.indexOf(" " + r + " ") < 0 && (i += r + " ");
                        s = K.trim(i),
                        n.className !== s && (n.className = s)
                    }
            return this
        },
        removeClass: function(t) {
            var e, n, i, r, o, s, a = 0 === arguments.length || "string" == typeof t && t, l = 0, u = this.length;
            if (K.isFunction(t))
                return this.each(function(e) {
                    K(this).removeClass(t.call(this, e, this.className))
                });
            if (a)
                for (e = (t || "").match(ft) || []; u > l; l++)
                    if (n = this[l],
                    i = 1 === n.nodeType && (n.className ? (" " + n.className + " ").replace(ae, " ") : "")) {
                        for (o = 0; r = e[o++]; )
                            for (; i.indexOf(" " + r + " ") >= 0; )
                                i = i.replace(" " + r + " ", " ");
                        s = t ? K.trim(i) : "",
                        n.className !== s && (n.className = s)
                    }
            return this
        },
        toggleClass: function(t, e) {
            var n = typeof t;
            return "boolean" == typeof e && "string" === n ? e ? this.addClass(t) : this.removeClass(t) : this.each(K.isFunction(t) ? function(n) {
                K(this).toggleClass(t.call(this, n, this.className, e), e)
            }
            : function() {
                if ("string" === n)
                    for (var e, i = 0, r = K(this), o = t.match(ft) || []; e = o[i++]; )
                        r.hasClass(e) ? r.removeClass(e) : r.addClass(e);
                else
                    (n === St || "boolean" === n) && (this.className && vt.set(this, "__className__", this.className),
                    this.className = this.className || t === !1 ? "" : vt.get(this, "__className__") || "")
            }
            )
        },
        hasClass: function(t) {
            for (var e = " " + t + " ", n = 0, i = this.length; i > n; n++)
                if (1 === this[n].nodeType && (" " + this[n].className + " ").replace(ae, " ").indexOf(e) >= 0)
                    return !0;
            return !1
        }
    });
    var le = /\r/g;
    K.fn.extend({
        val: function(t) {
            var e, n, i, r = this[0];
            return arguments.length ? (i = K.isFunction(t),
            this.each(function(n) {
                var r;
                1 === this.nodeType && (r = i ? t.call(this, n, K(this).val()) : t,
                null == r ? r = "" : "number" == typeof r ? r += "" : K.isArray(r) && (r = K.map(r, function(t) {
                    return null == t ? "" : t + ""
                })),
                e = K.valHooks[this.type] || K.valHooks[this.nodeName.toLowerCase()],
                e && "set"in e && void 0 !== e.set(this, r, "value") || (this.value = r))
            })) : r ? (e = K.valHooks[r.type] || K.valHooks[r.nodeName.toLowerCase()],
            e && "get"in e && void 0 !== (n = e.get(r, "value")) ? n : (n = r.value,
            "string" == typeof n ? n.replace(le, "") : null == n ? "" : n)) : void 0
        }
    }),
    K.extend({
        valHooks: {
            option: {
                get: function(t) {
                    var e = K.find.attr(t, "value");
                    return null != e ? e : K.trim(K.text(t))
                }
            },
            select: {
                get: function(t) {
                    for (var e, n, i = t.options, r = t.selectedIndex, o = "select-one" === t.type || 0 > r, s = o ? null : [], a = o ? r + 1 : i.length, l = 0 > r ? a : o ? r : 0; a > l; l++)
                        if (n = i[l],
                        !(!n.selected && l !== r || (Q.optDisabled ? n.disabled : null !== n.getAttribute("disabled")) || n.parentNode.disabled && K.nodeName(n.parentNode, "optgroup"))) {
                            if (e = K(n).val(),
                            o)
                                return e;
                            s.push(e)
                        }
                    return s
                },
                set: function(t, e) {
                    for (var n, i, r = t.options, o = K.makeArray(e), s = r.length; s--; )
                        i = r[s],
                        (i.selected = K.inArray(i.value, o) >= 0) && (n = !0);
                    return n || (t.selectedIndex = -1),
                    o
                }
            }
        }
    }),
    K.each(["radio", "checkbox"], function() {
        K.valHooks[this] = {
            set: function(t, e) {
                return K.isArray(e) ? t.checked = K.inArray(K(t).val(), e) >= 0 : void 0
            }
        },
        Q.checkOn || (K.valHooks[this].get = function(t) {
            return null === t.getAttribute("value") ? "on" : t.value
        }
        )
    }),
    K.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "), function(t, e) {
        K.fn[e] = function(t, n) {
            return arguments.length > 0 ? this.on(e, null, t, n) : this.trigger(e)
        }
    }),
    K.fn.extend({
        hover: function(t, e) {
            return this.mouseenter(t).mouseleave(e || t)
        },
        bind: function(t, e, n) {
            return this.on(t, null, e, n)
        },
        unbind: function(t, e) {
            return this.off(t, null, e)
        },
        delegate: function(t, e, n, i) {
            return this.on(e, t, n, i)
        },
        undelegate: function(t, e, n) {
            return 1 === arguments.length ? this.off(t, "**") : this.off(e, t || "**", n)
        }
    });
    var ue = K.now()
      , ce = /\?/;
    K.parseJSON = function(t) {
        return JSON.parse(t + "")
    }
    ,
    K.parseXML = function(t) {
        var e, n;
        if (!t || "string" != typeof t)
            return null;
        try {
            n = new DOMParser,
            e = n.parseFromString(t, "text/xml")
        } catch (i) {
            e = void 0
        }
        return (!e || e.getElementsByTagName("parsererror").length) && K.error("Invalid XML: " + t),
        e
    }
    ;
    var he = /#.*$/
      , pe = /([?&])_=[^&]*/
      , fe = /^(.*?):[ \t]*([^\r\n]*)$/gm
      , de = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/
      , me = /^(?:GET|HEAD)$/
      , ge = /^\/\//
      , ve = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/
      , ye = {}
      , _e = {}
      , xe = "*/".concat("*")
      , we = t.location.href
      , be = ve.exec(we.toLowerCase()) || [];
    K.extend({
        active: 0,
        lastModified: {},
        etag: {},
        ajaxSettings: {
            url: we,
            type: "GET",
            isLocal: de.test(be[1]),
            global: !0,
            processData: !0,
            async: !0,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            accepts: {
                "*": xe,
                text: "text/plain",
                html: "text/html",
                xml: "application/xml, text/xml",
                json: "application/json, text/javascript"
            },
            contents: {
                xml: /xml/,
                html: /html/,
                json: /json/
            },
            responseFields: {
                xml: "responseXML",
                text: "responseText",
                json: "responseJSON"
            },
            converters: {
                "* text": String,
                "text html": !0,
                "text json": K.parseJSON,
                "text xml": K.parseXML
            },
            flatOptions: {
                url: !0,
                context: !0
            }
        },
        ajaxSetup: function(t, e) {
            return e ? $($(t, K.ajaxSettings), e) : $(K.ajaxSettings, t)
        },
        ajaxPrefilter: L(ye),
        ajaxTransport: L(_e),
        ajax: function(t, e) {
            function n(t, e, n, s) {
                var l, c, v, y, x, b = e;
                2 !== _ && (_ = 2,
                a && clearTimeout(a),
                i = void 0,
                o = s || "",
                w.readyState = t > 0 ? 4 : 0,
                l = t >= 200 && 300 > t || 304 === t,
                n && (y = I(h, w, n)),
                y = q(h, y, w, l),
                l ? (h.ifModified && (x = w.getResponseHeader("Last-Modified"),
                x && (K.lastModified[r] = x),
                x = w.getResponseHeader("etag"),
                x && (K.etag[r] = x)),
                204 === t || "HEAD" === h.type ? b = "nocontent" : 304 === t ? b = "notmodified" : (b = y.state,
                c = y.data,
                v = y.error,
                l = !v)) : (v = b,
                (t || !b) && (b = "error",
                0 > t && (t = 0))),
                w.status = t,
                w.statusText = (e || b) + "",
                l ? d.resolveWith(p, [c, b, w]) : d.rejectWith(p, [w, b, v]),
                w.statusCode(g),
                g = void 0,
                u && f.trigger(l ? "ajaxSuccess" : "ajaxError", [w, h, l ? c : v]),
                m.fireWith(p, [w, b]),
                u && (f.trigger("ajaxComplete", [w, h]),
                --K.active || K.event.trigger("ajaxStop")))
            }
            "object" == typeof t && (e = t,
            t = void 0),
            e = e || {};
            var i, r, o, s, a, l, u, c, h = K.ajaxSetup({}, e), p = h.context || h, f = h.context && (p.nodeType || p.jquery) ? K(p) : K.event, d = K.Deferred(), m = K.Callbacks("once memory"), g = h.statusCode || {}, v = {}, y = {}, _ = 0, x = "canceled", w = {
                readyState: 0,
                getResponseHeader: function(t) {
                    var e;
                    if (2 === _) {
                        if (!s)
                            for (s = {}; e = fe.exec(o); )
                                s[e[1].toLowerCase()] = e[2];
                        e = s[t.toLowerCase()]
                    }
                    return null == e ? null : e
                },
                getAllResponseHeaders: function() {
                    return 2 === _ ? o : null
                },
                setRequestHeader: function(t, e) {
                    var n = t.toLowerCase();
                    return _ || (t = y[n] = y[n] || t,
                    v[t] = e),
                    this
                },
                overrideMimeType: function(t) {
                    return _ || (h.mimeType = t),
                    this
                },
                statusCode: function(t) {
                    var e;
                    if (t)
                        if (2 > _)
                            for (e in t)
                                g[e] = [g[e], t[e]];
                        else
                            w.always(t[w.status]);
                    return this
                },
                abort: function(t) {
                    var e = t || x;
                    return i && i.abort(e),
                    n(0, e),
                    this
                }
            };
            if (d.promise(w).complete = m.add,
            w.success = w.done,
            w.error = w.fail,
            h.url = ((t || h.url || we) + "").replace(he, "").replace(ge, be[1] + "//"),
            h.type = e.method || e.type || h.method || h.type,
            h.dataTypes = K.trim(h.dataType || "*").toLowerCase().match(ft) || [""],
            null == h.crossDomain && (l = ve.exec(h.url.toLowerCase()),
            h.crossDomain = !(!l || l[1] === be[1] && l[2] === be[2] && (l[3] || ("http:" === l[1] ? "80" : "443")) === (be[3] || ("http:" === be[1] ? "80" : "443")))),
            h.data && h.processData && "string" != typeof h.data && (h.data = K.param(h.data, h.traditional)),
            R(ye, h, e, w),
            2 === _)
                return w;
            u = K.event && h.global,
            u && 0 === K.active++ && K.event.trigger("ajaxStart"),
            h.type = h.type.toUpperCase(),
            h.hasContent = !me.test(h.type),
            r = h.url,
            h.hasContent || (h.data && (r = h.url += (ce.test(r) ? "&" : "?") + h.data,
            delete h.data),
            h.cache === !1 && (h.url = pe.test(r) ? r.replace(pe, "$1_=" + ue++) : r + (ce.test(r) ? "&" : "?") + "_=" + ue++)),
            h.ifModified && (K.lastModified[r] && w.setRequestHeader("If-Modified-Since", K.lastModified[r]),
            K.etag[r] && w.setRequestHeader("If-None-Match", K.etag[r])),
            (h.data && h.hasContent && h.contentType !== !1 || e.contentType) && w.setRequestHeader("Content-Type", h.contentType),
            w.setRequestHeader("Accept", h.dataTypes[0] && h.accepts[h.dataTypes[0]] ? h.accepts[h.dataTypes[0]] + ("*" !== h.dataTypes[0] ? ", " + xe + "; q=0.01" : "") : h.accepts["*"]);
            for (c in h.headers)
                w.setRequestHeader(c, h.headers[c]);
            if (h.beforeSend && (h.beforeSend.call(p, w, h) === !1 || 2 === _))
                return w.abort();
            x = "abort";
            for (c in {
                success: 1,
                error: 1,
                complete: 1
            })
                w[c](h[c]);
            if (i = R(_e, h, e, w)) {
                w.readyState = 1,
                u && f.trigger("ajaxSend", [w, h]),
                h.async && h.timeout > 0 && (a = setTimeout(function() {
                    w.abort("timeout")
                }, h.timeout));
                try {
                    _ = 1,
                    i.send(v, n)
                } catch (b) {
                    if (!(2 > _))
                        throw b;
                    n(-1, b)
                }
            } else
                n(-1, "No Transport");
            return w
        },
        getJSON: function(t, e, n) {
          console.log(">>>>>>>>>>getJSON>>>>>>>>>",K.get(t, e, n, "json"));
          console.log(">>>>>>>>>>getJSON>>>>>>>>>");
            return K.get(t, e, n, "json")
        },
        getScript: function(t, e) {
            return K.get(t, void 0, e, "script")
        }
    }),
    K.each(["get", "post"], function(t, e) {
        K[e] = function(t, n, i, r) {
            return K.isFunction(n) && (r = r || i,
            i = n,
            n = void 0),
            K.ajax({
                url: t,
                type: e,
                dataType: r,
                data: n,
                success: i
            })
        }
    }),
    K._evalUrl = function(t) {
        return K.ajax({
            url: t,
            type: "GET",
            dataType: "script",
            async: !1,
            global: !1,
            "throws": !0
        })
    }
    ,
    K.fn.extend({
        wrapAll: function(t) {
            var e;
            return K.isFunction(t) ? this.each(function(e) {
                K(this).wrapAll(t.call(this, e))
            }) : (this[0] && (e = K(t, this[0].ownerDocument).eq(0).clone(!0),
            this[0].parentNode && e.insertBefore(this[0]),
            e.map(function() {
                for (var t = this; t.firstElementChild; )
                    t = t.firstElementChild;
                return t
            }).append(this)),
            this)
        },
        wrapInner: function(t) {
            return this.each(K.isFunction(t) ? function(e) {
                K(this).wrapInner(t.call(this, e))
            }
            : function() {
                var e = K(this)
                  , n = e.contents();
                n.length ? n.wrapAll(t) : e.append(t)
            }
            )
        },
        wrap: function(t) {
            var e = K.isFunction(t);
            return this.each(function(n) {
                K(this).wrapAll(e ? t.call(this, n) : t)
            })
        },
        unwrap: function() {
            return this.parent().each(function() {
                K.nodeName(this, "body") || K(this).replaceWith(this.childNodes)
            }).end()
        }
    }),
    K.expr.filters.hidden = function(t) {
        return t.offsetWidth <= 0 && t.offsetHeight <= 0
    }
    ,
    K.expr.filters.visible = function(t) {
        return !K.expr.filters.hidden(t)
    }
    ;
    var Te = /%20/g
      , Ce = /\[\]$/
      , Se = /\r?\n/g
      , ke = /^(?:submit|button|image|reset|file)$/i
      , je = /^(?:input|select|textarea|keygen)/i;
    K.param = function(t, e) {
        var n, i = [], r = function(t, e) {
            e = K.isFunction(e) ? e() : null == e ? "" : e,
            i[i.length] = encodeURIComponent(t) + "=" + encodeURIComponent(e)
        };
        if (void 0 === e && (e = K.ajaxSettings && K.ajaxSettings.traditional),
        K.isArray(t) || t.jquery && !K.isPlainObject(t))
            K.each(t, function() {
                r(this.name, this.value)
            });
        else
            for (n in t)
                H(n, t[n], e, r);
        return i.join("&").replace(Te, "+")
    }
    ,
    K.fn.extend({
        serialize: function() {
            return K.param(this.serializeArray())
        },
        serializeArray: function() {
            return this.map(function() {
                var t = K.prop(this, "elements");
                return t ? K.makeArray(t) : this
            }).filter(function() {
                var t = this.type;
                return this.name && !K(this).is(":disabled") && je.test(this.nodeName) && !ke.test(t) && (this.checked || !Ct.test(t))
            }).map(function(t, e) {
                var n = K(this).val();
                return null == n ? null : K.isArray(n) ? K.map(n, function(t) {
                    return {
                        name: e.name,
                        value: t.replace(Se, "\r\n")
                    }
                }) : {
                    name: e.name,
                    value: n.replace(Se, "\r\n")
                }
            }).get()
        }
    }),
    K.ajaxSettings.xhr = function() {
        try {
            return new XMLHttpRequest
        } catch (t) {}
    }
    ;
    var Pe = 0
      , Ee = {}
      , Oe = {
        0: 200,
        1223: 204
    }
      , Me = K.ajaxSettings.xhr();
    t.attachEvent && t.attachEvent("onunload", function() {
        for (var t in Ee)
            Ee[t]()
    }),
    Q.cors = !!Me && "withCredentials"in Me,
    Q.ajax = Me = !!Me,
    K.ajaxTransport(function(t) {
        var e;
        return Q.cors || Me && !t.crossDomain ? {
            send: function(n, i) {
                var r, o = t.xhr(), s = ++Pe;
                if (o.open(t.type, t.url, t.async, t.username, t.password),
                t.xhrFields)
                    for (r in t.xhrFields)
                        o[r] = t.xhrFields[r];
                t.mimeType && o.overrideMimeType && o.overrideMimeType(t.mimeType),
                t.crossDomain || n["X-Requested-With"] || (n["X-Requested-With"] = "XMLHttpRequest");
                for (r in n)
                    o.setRequestHeader(r, n[r]);
                e = function(t) {
                    return function() {
                        e && (delete Ee[s],
                        e = o.onload = o.onerror = null,
                        "abort" === t ? o.abort() : "error" === t ? i(o.status, o.statusText) : i(Oe[o.status] || o.status, o.statusText, "string" == typeof o.responseText ? {
                            text: o.responseText
                        } : void 0, o.getAllResponseHeaders()))
                    }
                }
                ,
                o.onload = e(),
                o.onerror = e("error"),
                e = Ee[s] = e("abort");
                try {
                    console.log(">>>>>>>>>>>>>>t.hasContent>>>>>>>>>>>>>>>>>",t.hasContent);
                    console.log(">>>>>>>>>>>>>>t.data>>>>>>>>>>>>>>>>>",t.data);
                    o.send(t.hasContent && t.data || null)
                } catch (a) {
                    if (e)
                        throw a
                }
            },
            abort: function() {
                e && e()
            }
        } : void 0
    }),
    K.ajaxSetup({
        accepts: {
            script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
        },
        contents: {
            script: /(?:java|ecma)script/
        },
        converters: {
            "text script": function(t) {
                return K.globalEval(t),
                t
            }
        }
    }),
    K.ajaxPrefilter("script", function(t) {
        void 0 === t.cache && (t.cache = !1),
        t.crossDomain && (t.type = "GET")
    }),
    K.ajaxTransport("script", function(t) {
        if (t.crossDomain) {
            var e, n;
            return {
                send: function(i, r) {
                    e = K("<script>").prop({
                        async: !0,
                        charset: t.scriptCharset,
                        src: t.url
                    }).on("load error", n = function(t) {
                        e.remove(),
                        n = null,
                        t && r("error" === t.type ? 404 : 200, t.type)
                    }
                    ),
                    J.head.appendChild(e[0])
                },
                abort: function() {
                    n && n()
                }
            }
        }
    });
    var De = []
      , Ae = /(=)\?(?=&|$)|\?\?/;
    K.ajaxSetup({
        jsonp: "callback",
        jsonpCallback: function() {
            var t = De.pop() || K.expando + "_" + ue++;
            return this[t] = !0,
            t
        }
    }),
    K.ajaxPrefilter("json jsonp", function(e, n, i) {
        var r, o, s, a = e.jsonp !== !1 && (Ae.test(e.url) ? "url" : "string" == typeof e.data && !(e.contentType || "").indexOf("application/x-www-form-urlencoded") && Ae.test(e.data) && "data");
        return a || "jsonp" === e.dataTypes[0] ? (r = e.jsonpCallback = K.isFunction(e.jsonpCallback) ? e.jsonpCallback() : e.jsonpCallback,
        a ? e[a] = e[a].replace(Ae, "$1" + r) : e.jsonp !== !1 && (e.url += (ce.test(e.url) ? "&" : "?") + e.jsonp + "=" + r),
        e.converters["script json"] = function() {
            return s || K.error(r + " was not called"),
            s[0]
        }
        ,
        e.dataTypes[0] = "json",
        o = t[r],
        t[r] = function() {
            s = arguments
        }
        ,
        i.always(function() {
            t[r] = o,
            e[r] && (e.jsonpCallback = n.jsonpCallback,
            De.push(r)),
            s && K.isFunction(o) && o(s[0]),
            s = o = void 0
        }),
        "script") : void 0
    }),
    K.parseHTML = function(t, e, n) {
        if (!t || "string" != typeof t)
            return null;
        "boolean" == typeof e && (n = e,
        e = !1),
        e = e || J;
        var i = st.exec(t)
          , r = !n && [];
        return i ? [e.createElement(i[1])] : (i = K.buildFragment([t], e, r),
        r && r.length && K(r).remove(),
        K.merge([], i.childNodes))
    }
    ;
    var Ne = K.fn.load;
    K.fn.load = function(t, e, n) {
        if ("string" != typeof t && Ne)
            return Ne.apply(this, arguments);
        var i, r, o, s = this, a = t.indexOf(" ");
        return a >= 0 && (i = K.trim(t.slice(a)),
        t = t.slice(0, a)),
        K.isFunction(e) ? (n = e,
        e = void 0) : e && "object" == typeof e && (r = "POST"),
        s.length > 0 && K.ajax({
            url: t,
            type: r,
            dataType: "html",
            data: e
        }).done(function(t) {
            o = arguments,
            s.html(i ? K("<div>").append(K.parseHTML(t)).find(i) : t)
        }).complete(n && function(t, e) {
            s.each(n, o || [t.responseText, e, t])
        }
        ),
        this
    }
    ,
    K.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function(t, e) {
        K.fn[e] = function(t) {
            return this.on(e, t)
        }
    }),
    K.expr.filters.animated = function(t) {
        return K.grep(K.timers, function(e) {
            return t === e.elem
        }).length
    }
    ;
    var Le = t.document.documentElement;
    K.offset = {
        setOffset: function(t, e, n) {
            var i, r, o, s, a, l, u, c = K.css(t, "position"), h = K(t), p = {};
            "static" === c && (t.style.position = "relative"),
            a = h.offset(),
            o = K.css(t, "top"),
            l = K.css(t, "left"),
            u = ("absolute" === c || "fixed" === c) && (o + l).indexOf("auto") > -1,
            u ? (i = h.position(),
            s = i.top,
            r = i.left) : (s = parseFloat(o) || 0,
            r = parseFloat(l) || 0),
            K.isFunction(e) && (e = e.call(t, n, a)),
            null != e.top && (p.top = e.top - a.top + s),
            null != e.left && (p.left = e.left - a.left + r),
            "using"in e ? e.using.call(t, p) : h.css(p)
        }
    },
    K.fn.extend({
        offset: function(t) {
            if (arguments.length)
                return void 0 === t ? this : this.each(function(e) {
                    K.offset.setOffset(this, t, e)
                });
            var e, n, i = this[0], r = {
                top: 0,
                left: 0
            }, o = i && i.ownerDocument;
            return o ? (e = o.documentElement,
            K.contains(e, i) ? (typeof i.getBoundingClientRect !== St && (r = i.getBoundingClientRect()),
            n = F(o),
            {
                top: r.top + n.pageYOffset - e.clientTop,
                left: r.left + n.pageXOffset - e.clientLeft
            }) : r) : void 0
        },
        position: function() {
            if (this[0]) {
                var t, e, n = this[0], i = {
                    top: 0,
                    left: 0
                };
                return "fixed" === K.css(n, "position") ? e = n.getBoundingClientRect() : (t = this.offsetParent(),
                e = this.offset(),
                K.nodeName(t[0], "html") || (i = t.offset()),
                i.top += K.css(t[0], "borderTopWidth", !0),
                i.left += K.css(t[0], "borderLeftWidth", !0)),
                {
                    top: e.top - i.top - K.css(n, "marginTop", !0),
                    left: e.left - i.left - K.css(n, "marginLeft", !0)
                }
            }
        },
        offsetParent: function() {
            return this.map(function() {
                for (var t = this.offsetParent || Le; t && !K.nodeName(t, "html") && "static" === K.css(t, "position"); )
                    t = t.offsetParent;
                return t || Le
            })
        }
    }),
    K.each({
        scrollLeft: "pageXOffset",
        scrollTop: "pageYOffset"
    }, function(e, n) {
        var i = "pageYOffset" === n;
        K.fn[e] = function(r) {
            return gt(this, function(e, r, o) {
                var s = F(e);
                return void 0 === o ? s ? s[n] : e[r] : void (s ? s.scrollTo(i ? t.pageXOffset : o, i ? o : t.pageYOffset) : e[r] = o)
            }, e, r, arguments.length, null)
        }
    }),
    K.each(["top", "left"], function(t, e) {
        K.cssHooks[e] = b(Q.pixelPosition, function(t, n) {
            return n ? (n = w(t, e),
            Bt.test(n) ? K(t).position()[e] + "px" : n) : void 0
        })
    }),
    K.each({
        Height: "height",
        Width: "width"
    }, function(t, e) {
        K.each({
            padding: "inner" + t,
            content: e,
            "": "outer" + t
        }, function(n, i) {
            K.fn[i] = function(i, r) {
                var o = arguments.length && (n || "boolean" != typeof i)
                  , s = n || (i === !0 || r === !0 ? "margin" : "border");
                return gt(this, function(e, n, i) {
                    var r;
                    return K.isWindow(e) ? e.document.documentElement["client" + t] : 9 === e.nodeType ? (r = e.documentElement,
                    Math.max(e.body["scroll" + t], r["scroll" + t], e.body["offset" + t], r["offset" + t], r["client" + t])) : void 0 === i ? K.css(e, n, s) : K.style(e, n, i, s)
                }, e, o ? i : void 0, o, null)
            }
        })
    }),
    K.fn.size = function() {
        return this.length
    }
    ,
    K.fn.andSelf = K.fn.addBack,
    "function" == typeof define && define.amd && define("jquery", [], function() {
        return K
    });
    var Re = t.jQuery
      , $e = t.$;
    return K.noConflict = function(e) {
        return t.$ === K && (t.$ = $e),
        e && t.jQuery === K && (t.jQuery = Re),
        K
    }
    ,
    typeof e === St && (t.jQuery = t.$ = K),
    K
}),
function() {
    var t = this
      , e = t._
      , n = {}
      , i = Array.prototype
      , r = Object.prototype
      , o = Function.prototype
      , s = i.push
      , a = i.slice
      , l = i.concat
      , u = r.toString
      , c = r.hasOwnProperty
      , h = i.forEach
      , p = i.map
      , f = i.reduce
      , d = i.reduceRight
      , m = i.filter
      , g = i.every
      , v = i.some
      , y = i.indexOf
      , _ = i.lastIndexOf
      , x = Array.isArray
      , w = Object.keys
      , b = o.bind
      , T = function(t) {
        return t instanceof T ? t : this instanceof T ? void (this._wrapped = t) : new T(t)
    };
    "undefined" != typeof exports ? ("undefined" != typeof module && module.exports && (exports = module.exports = T),
    exports._ = T) : t._ = T,
    T.VERSION = "1.5.2";
    var C = T.each = T.forEach = function(t, e, i) {
        if (null != t)
            if (h && t.forEach === h)
                t.forEach(e, i);
            else if (t.length === +t.length) {
                for (var r = 0, o = t.length; o > r; r++)
                    if (e.call(i, t[r], r, t) === n)
                        return
            } else
                for (var s = T.keys(t), r = 0, o = s.length; o > r; r++)
                    if (e.call(i, t[s[r]], s[r], t) === n)
                        return
    }
    ;
    T.map = T.collect = function(t, e, n) {
        var i = [];
        return null == t ? i : p && t.map === p ? t.map(e, n) : (C(t, function(t, r, o) {
            i.push(e.call(n, t, r, o))
        }),
        i)
    }
    ;
    var S = "Reduce of empty array with no initial value";
    T.reduce = T.foldl = T.inject = function(t, e, n, i) {
        var r = arguments.length > 2;
        if (null == t && (t = []),
        f && t.reduce === f)
            return i && (e = T.bind(e, i)),
            r ? t.reduce(e, n) : t.reduce(e);
        if (C(t, function(t, o, s) {
            r ? n = e.call(i, n, t, o, s) : (n = t,
            r = !0)
        }),
        !r)
            throw new TypeError(S);
        return n
    }
    ,
    T.reduceRight = T.foldr = function(t, e, n, i) {
        var r = arguments.length > 2;
        if (null == t && (t = []),
        d && t.reduceRight === d)
            return i && (e = T.bind(e, i)),
            r ? t.reduceRight(e, n) : t.reduceRight(e);
        var o = t.length;
        if (o !== +o) {
            var s = T.keys(t);
            o = s.length
        }
        if (C(t, function(a, l, u) {
            l = s ? s[--o] : --o,
            r ? n = e.call(i, n, t[l], l, u) : (n = t[l],
            r = !0)
        }),
        !r)
            throw new TypeError(S);
        return n
    }
    ,
    T.find = T.detect = function(t, e, n) {
        var i;
        return k(t, function(t, r, o) {
            return e.call(n, t, r, o) ? (i = t,
            !0) : void 0
        }),
        i
    }
    ,
    T.filter = T.select = function(t, e, n) {
        var i = [];
        return null == t ? i : m && t.filter === m ? t.filter(e, n) : (C(t, function(t, r, o) {
            e.call(n, t, r, o) && i.push(t)
        }),
        i)
    }
    ,
    T.reject = function(t, e, n) {
        return T.filter(t, function(t, i, r) {
            return !e.call(n, t, i, r)
        }, n)
    }
    ,
    T.every = T.all = function(t, e, i) {
        e || (e = T.identity);
        var r = !0;
        return null == t ? r : g && t.every === g ? t.every(e, i) : (C(t, function(t, o, s) {
            return (r = r && e.call(i, t, o, s)) ? void 0 : n
        }),
        !!r)
    }
    ;
    var k = T.some = T.any = function(t, e, i) {
        e || (e = T.identity);
        var r = !1;
        return null == t ? r : v && t.some === v ? t.some(e, i) : (C(t, function(t, o, s) {
            return r || (r = e.call(i, t, o, s)) ? n : void 0
        }),
        !!r)
    }
    ;
    T.contains = T.include = function(t, e) {
        return null == t ? !1 : y && t.indexOf === y ? -1 != t.indexOf(e) : k(t, function(t) {
            return t === e
        })
    }
    ,
    T.invoke = function(t, e) {
        var n = a.call(arguments, 2)
          , i = T.isFunction(e);
        return T.map(t, function(t) {
            return (i ? e : t[e]).apply(t, n)
        })
    }
    ,
    T.pluck = function(t, e) {
        return T.map(t, function(t) {
            return t[e]
        })
    }
    ,
    T.where = function(t, e, n) {
        return T.isEmpty(e) ? n ? void 0 : [] : T[n ? "find" : "filter"](t, function(t) {
            for (var n in e)
                if (e[n] !== t[n])
                    return !1;
            return !0
        })
    }
    ,
    T.findWhere = function(t, e) {
        return T.where(t, e, !0)
    }
    ,
    T.max = function(t, e, n) {
        if (!e && T.isArray(t) && t[0] === +t[0] && t.length < 65535)
            return Math.max.apply(Math, t);
        if (!e && T.isEmpty(t))
            return -(1 / 0);
        var i = {
            computed: -(1 / 0),
            value: -(1 / 0)
        };
        return C(t, function(t, r, o) {
            var s = e ? e.call(n, t, r, o) : t;
            s > i.computed && (i = {
                value: t,
                computed: s
            })
        }),
        i.value
    }
    ,
    T.min = function(t, e, n) {
        if (!e && T.isArray(t) && t[0] === +t[0] && t.length < 65535)
            return Math.min.apply(Math, t);
        if (!e && T.isEmpty(t))
            return 1 / 0;
        var i = {
            computed: 1 / 0,
            value: 1 / 0
        };
        return C(t, function(t, r, o) {
            var s = e ? e.call(n, t, r, o) : t;
            s < i.computed && (i = {
                value: t,
                computed: s
            })
        }),
        i.value
    }
    ,
    T.shuffle = function(t) {
        var e, n = 0, i = [];
        return C(t, function(t) {
            e = T.random(n++),
            i[n - 1] = i[e],
            i[e] = t
        }),
        i
    }
    ,
    T.sample = function(t, e, n) {
        return arguments.length < 2 || n ? t[T.random(t.length - 1)] : T.shuffle(t).slice(0, Math.max(0, e))
    }
    ;
    var j = function(t) {
        return T.isFunction(t) ? t : function(e) {
            return e[t]
        }
    };
    T.sortBy = function(t, e, n) {
        var i = j(e);
        return T.pluck(T.map(t, function(t, e, r) {
            return {
                value: t,
                index: e,
                criteria: i.call(n, t, e, r)
            }
        }).sort(function(t, e) {
            var n = t.criteria
              , i = e.criteria;
            if (n !== i) {
                if (n > i || void 0 === n)
                    return 1;
                if (i > n || void 0 === i)
                    return -1
            }
            return t.index - e.index
        }), "value")
    }
    ;
    var P = function(t) {
        return function(e, n, i) {
            var r = {}
              , o = null == n ? T.identity : j(n);
            return C(e, function(n, s) {
                var a = o.call(i, n, s, e);
                t(r, a, n)
            }),
            r
        }
    };
    T.groupBy = P(function(t, e, n) {
        (T.has(t, e) ? t[e] : t[e] = []).push(n)
    }),
    T.indexBy = P(function(t, e, n) {
        t[e] = n
    }),
    T.countBy = P(function(t, e) {
        T.has(t, e) ? t[e]++ : t[e] = 1
    }),
    T.sortedIndex = function(t, e, n, i) {
        n = null == n ? T.identity : j(n);
        for (var r = n.call(i, e), o = 0, s = t.length; s > o; ) {
            var a = o + s >>> 1;
            n.call(i, t[a]) < r ? o = a + 1 : s = a
        }
        return o
    }
    ,
    T.toArray = function(t) {
        return t ? T.isArray(t) ? a.call(t) : t.length === +t.length ? T.map(t, T.identity) : T.values(t) : []
    }
    ,
    T.size = function(t) {
        return null == t ? 0 : t.length === +t.length ? t.length : T.keys(t).length
    }
    ,
    T.first = T.head = T.take = function(t, e, n) {
        return null == t ? void 0 : null == e || n ? t[0] : a.call(t, 0, e)
    }
    ,
    T.initial = function(t, e, n) {
        return a.call(t, 0, t.length - (null == e || n ? 1 : e))
    }
    ,
    T.last = function(t, e, n) {
        return null == t ? void 0 : null == e || n ? t[t.length - 1] : a.call(t, Math.max(t.length - e, 0))
    }
    ,
    T.rest = T.tail = T.drop = function(t, e, n) {
        return a.call(t, null == e || n ? 1 : e)
    }
    ,
    T.compact = function(t) {
        return T.filter(t, T.identity)
    }
    ;
    var E = function(t, e, n) {
        return e && T.every(t, T.isArray) ? l.apply(n, t) : (C(t, function(t) {
            T.isArray(t) || T.isArguments(t) ? e ? s.apply(n, t) : E(t, e, n) : n.push(t)
        }),
        n)
    };
    T.flatten = function(t, e) {
        return E(t, e, [])
    }
    ,
    T.without = function(t) {
        return T.difference(t, a.call(arguments, 1))
    }
    ,
    T.uniq = T.unique = function(t, e, n, i) {
        T.isFunction(e) && (i = n,
        n = e,
        e = !1);
        var r = n ? T.map(t, n, i) : t
          , o = []
          , s = [];
        return C(r, function(n, i) {
            (e ? i && s[s.length - 1] === n : T.contains(s, n)) || (s.push(n),
            o.push(t[i]))
        }),
        o
    }
    ,
    T.union = function() {
        return T.uniq(T.flatten(arguments, !0))
    }
    ,
    T.intersection = function(t) {
        var e = a.call(arguments, 1);
        return T.filter(T.uniq(t), function(t) {
            return T.every(e, function(e) {
                return T.indexOf(e, t) >= 0
            })
        })
    }
    ,
    T.difference = function(t) {
        var e = l.apply(i, a.call(arguments, 1));
        return T.filter(t, function(t) {
            return !T.contains(e, t)
        })
    }
    ,
    T.zip = function() {
        for (var t = T.max(T.pluck(arguments, "length").concat(0)), e = new Array(t), n = 0; t > n; n++)
            e[n] = T.pluck(arguments, "" + n);
        return e
    }
    ,
    T.object = function(t, e) {
        if (null == t)
            return {};
        for (var n = {}, i = 0, r = t.length; r > i; i++)
            e ? n[t[i]] = e[i] : n[t[i][0]] = t[i][1];
        return n
    }
    ,
    T.indexOf = function(t, e, n) {
        if (null == t)
            return -1;
        var i = 0
          , r = t.length;
        if (n) {
            if ("number" != typeof n)
                return i = T.sortedIndex(t, e),
                t[i] === e ? i : -1;
            i = 0 > n ? Math.max(0, r + n) : n
        }
        if (y && t.indexOf === y)
            return t.indexOf(e, n);
        for (; r > i; i++)
            if (t[i] === e)
                return i;
        return -1
    }
    ,
    T.lastIndexOf = function(t, e, n) {
        if (null == t)
            return -1;
        var i = null != n;
        if (_ && t.lastIndexOf === _)
            return i ? t.lastIndexOf(e, n) : t.lastIndexOf(e);
        for (var r = i ? n : t.length; r--; )
            if (t[r] === e)
                return r;
        return -1
    }
    ,
    T.range = function(t, e, n) {
        arguments.length <= 1 && (e = t || 0,
        t = 0),
        n = arguments[2] || 1;
        for (var i = Math.max(Math.ceil((e - t) / n), 0), r = 0, o = new Array(i); i > r; )
            o[r++] = t,
            t += n;
        return o
    }
    ;
    var O = function() {};
    T.bind = function(t, e) {
        var n, i;
        if (b && t.bind === b)
            return b.apply(t, a.call(arguments, 1));
        if (!T.isFunction(t))
            throw new TypeError;
        return n = a.call(arguments, 2),
        i = function() {
            if (!(this instanceof i))
                return t.apply(e, n.concat(a.call(arguments)));
            O.prototype = t.prototype;
            var r = new O;
            O.prototype = null;
            var o = t.apply(r, n.concat(a.call(arguments)));
            return Object(o) === o ? o : r
        }
    }
    ,
    T.partial = function(t) {
        var e = a.call(arguments, 1);
        return function() {
            return t.apply(this, e.concat(a.call(arguments)))
        }
    }
    ,
    T.bindAll = function(t) {
        var e = a.call(arguments, 1);
        if (0 === e.length)
            throw new Error("bindAll must be passed function names");
        return C(e, function(e) {
            t[e] = T.bind(t[e], t)
        }),
        t
    }
    ,
    T.memoize = function(t, e) {
        var n = {};
        return e || (e = T.identity),
        function() {
            var i = e.apply(this, arguments);
            return T.has(n, i) ? n[i] : n[i] = t.apply(this, arguments)
        }
    }
    ,
    T.delay = function(t, e) {
        var n = a.call(arguments, 2);
        return setTimeout(function() {
            return t.apply(null, n)
        }, e)
    }
    ,
    T.defer = function(t) {
        return T.delay.apply(T, [t, 1].concat(a.call(arguments, 1)))
    }
    ,
    T.throttle = function(t, e, n) {
        var i, r, o, s = null, a = 0;
        n || (n = {});
        var l = function() {
            a = n.leading === !1 ? 0 : new Date,
            s = null,
            o = t.apply(i, r)
        };
        return function() {
            var u = new Date;
            a || n.leading !== !1 || (a = u);
            var c = e - (u - a);
            return i = this,
            r = arguments,
            0 >= c ? (clearTimeout(s),
            s = null,
            a = u,
            o = t.apply(i, r)) : s || n.trailing === !1 || (s = setTimeout(l, c)),
            o
        }
    }
    ,
    T.debounce = function(t, e, n) {
        var i, r, o, s, a;
        return function() {
            o = this,
            r = arguments,
            s = new Date;
            var l = function() {
                var u = new Date - s;
                e > u ? i = setTimeout(l, e - u) : (i = null,
                n || (a = t.apply(o, r)))
            }
              , u = n && !i;
            return i || (i = setTimeout(l, e)),
            u && (a = t.apply(o, r)),
            a
        }
    }
    ,
    T.once = function(t) {
        var e, n = !1;
        return function() {
            return n ? e : (n = !0,
            e = t.apply(this, arguments),
            t = null,
            e)
        }
    }
    ,
    T.wrap = function(t, e) {
        return function() {
            var n = [t];
            return s.apply(n, arguments),
            e.apply(this, n)
        }
    }
    ,
    T.compose = function() {
        var t = arguments;
        return function() {
            for (var e = arguments, n = t.length - 1; n >= 0; n--)
                e = [t[n].apply(this, e)];
            return e[0]
        }
    }
    ,
    T.after = function(t, e) {
        return function() {
            return --t < 1 ? e.apply(this, arguments) : void 0
        }
    }
    ,
    T.keys = w || function(t) {
        if (t !== Object(t))
            throw new TypeError("Invalid object");
        var e = [];
        for (var n in t)
            T.has(t, n) && e.push(n);
        return e
    }
    ,
    T.values = function(t) {
        for (var e = T.keys(t), n = e.length, i = new Array(n), r = 0; n > r; r++)
            i[r] = t[e[r]];
        return i
    }
    ,
    T.pairs = function(t) {
        for (var e = T.keys(t), n = e.length, i = new Array(n), r = 0; n > r; r++)
            i[r] = [e[r], t[e[r]]];
        return i
    }
    ,
    T.invert = function(t) {
        for (var e = {}, n = T.keys(t), i = 0, r = n.length; r > i; i++)
            e[t[n[i]]] = n[i];
        return e
    }
    ,
    T.functions = T.methods = function(t) {
        var e = [];
        for (var n in t)
            T.isFunction(t[n]) && e.push(n);
        return e.sort()
    }
    ,
    T.extend = function(t) {
        return C(a.call(arguments, 1), function(e) {
            if (e)
                for (var n in e)
                    t[n] = e[n]
        }),
        t
    }
    ,
    T.pick = function(t) {
        var e = {}
          , n = l.apply(i, a.call(arguments, 1));
        return C(n, function(n) {
            n in t && (e[n] = t[n])
        }),
        e
    }
    ,
    T.omit = function(t) {
        var e = {}
          , n = l.apply(i, a.call(arguments, 1));
        for (var r in t)
            T.contains(n, r) || (e[r] = t[r]);
        return e
    }
    ,
    T.defaults = function(t) {
        return C(a.call(arguments, 1), function(e) {
            if (e)
                for (var n in e)
                    void 0 === t[n] && (t[n] = e[n])
        }),
        t
    }
    ,
    T.clone = function(t) {
        return T.isObject(t) ? T.isArray(t) ? t.slice() : T.extend({}, t) : t
    }
    ,
    T.tap = function(t, e) {
        return e(t),
        t
    }
    ;
    var M = function(t, e, n, i) {
        if (t === e)
            return 0 !== t || 1 / t == 1 / e;
        if (null == t || null == e)
            return t === e;
        t instanceof T && (t = t._wrapped),
        e instanceof T && (e = e._wrapped);
        var r = u.call(t);
        if (r != u.call(e))
            return !1;
        switch (r) {
        case "[object String]":
            return t == String(e);
        case "[object Number]":
            return t != +t ? e != +e : 0 == t ? 1 / t == 1 / e : t == +e;
        case "[object Date]":
        case "[object Boolean]":
            return +t == +e;
        case "[object RegExp]":
            return t.source == e.source && t.global == e.global && t.multiline == e.multiline && t.ignoreCase == e.ignoreCase
        }
        if ("object" != typeof t || "object" != typeof e)
            return !1;
        for (var o = n.length; o--; )
            if (n[o] == t)
                return i[o] == e;
        var s = t.constructor
          , a = e.constructor;
        if (s !== a && !(T.isFunction(s) && s instanceof s && T.isFunction(a) && a instanceof a))
            return !1;
        n.push(t),
        i.push(e);
        var l = 0
          , c = !0;
        if ("[object Array]" == r) {
            if (l = t.length,
            c = l == e.length)
                for (; l-- && (c = M(t[l], e[l], n, i)); )
                    ;
        } else {
            for (var h in t)
                if (T.has(t, h) && (l++,
                !(c = T.has(e, h) && M(t[h], e[h], n, i))))
                    break;
            if (c) {
                for (h in e)
                    if (T.has(e, h) && !l--)
                        break;
                c = !l
            }
        }
        return n.pop(),
        i.pop(),
        c
    };
    T.isEqual = function(t, e) {
        return M(t, e, [], [])
    }
    ,
    T.isEmpty = function(t) {
        if (null == t)
            return !0;
        if (T.isArray(t) || T.isString(t))
            return 0 === t.length;
        for (var e in t)
            if (T.has(t, e))
                return !1;
        return !0
    }
    ,
    T.isElement = function(t) {
        return !(!t || 1 !== t.nodeType)
    }
    ,
    T.isArray = x || function(t) {
        return "[object Array]" == u.call(t)
    }
    ,
    T.isObject = function(t) {
        return t === Object(t)
    }
    ,
    C(["Arguments", "Function", "String", "Number", "Date", "RegExp"], function(t) {
        T["is" + t] = function(e) {
            return u.call(e) == "[object " + t + "]"
        }
    }),
    T.isArguments(arguments) || (T.isArguments = function(t) {
        return !(!t || !T.has(t, "callee"))
    }
    ),
    "function" != typeof /./ && (T.isFunction = function(t) {
        return "function" == typeof t
    }
    ),
    T.isFinite = function(t) {
        return isFinite(t) && !isNaN(parseFloat(t))
    }
    ,
    T.isNaN = function(t) {
        return T.isNumber(t) && t != +t
    }
    ,
    T.isBoolean = function(t) {
        return t === !0 || t === !1 || "[object Boolean]" == u.call(t)
    }
    ,
    T.isNull = function(t) {
        return null === t
    }
    ,
    T.isUndefined = function(t) {
        return void 0 === t
    }
    ,
    T.has = function(t, e) {
        return c.call(t, e)
    }
    ,
    T.noConflict = function() {
        return t._ = e,
        this
    }
    ,
    T.identity = function(t) {
        return t
    }
    ,
    T.times = function(t, e, n) {
        for (var i = Array(Math.max(0, t)), r = 0; t > r; r++)
            i[r] = e.call(n, r);
        return i
    }
    ,
    T.random = function(t, e) {
        return null == e && (e = t,
        t = 0),
        t + Math.floor(Math.random() * (e - t + 1))
    }
    ;
    var D = {
        escape: {
            "&": "&amp;",
            "<": "&lt;",
            ">": "&gt;",
            '"': "&quot;",
            "'": "&#x27;"
        }
    };
    D.unescape = T.invert(D.escape);
    var A = {
        escape: new RegExp("[" + T.keys(D.escape).join("") + "]","g"),
        unescape: new RegExp("(" + T.keys(D.unescape).join("|") + ")","g")
    };
    T.each(["escape", "unescape"], function(t) {
        T[t] = function(e) {
            return null == e ? "" : ("" + e).replace(A[t], function(e) {
                return D[t][e]
            })
        }
    }),
    T.result = function(t, e) {
        if (null == t)
            return void 0;
        var n = t[e];
        return T.isFunction(n) ? n.call(t) : n
    }
    ,
    T.mixin = function(t) {
        C(T.functions(t), function(e) {
            var n = T[e] = t[e];
            T.prototype[e] = function() {
                var t = [this._wrapped];
                return s.apply(t, arguments),
                I.call(this, n.apply(T, t))
            }
        })
    }
    ;
    var N = 0;
    T.uniqueId = function(t) {
        var e = ++N + "";
        return t ? t + e : e
    }
    ,
    T.templateSettings = {
        evaluate: /<%([\s\S]+?)%>/g,
        interpolate: /<%=([\s\S]+?)%>/g,
        escape: /<%-([\s\S]+?)%>/g
    };
    var L = /(.)^/
      , R = {
        "'": "'",
        "\\": "\\",
        "\r": "r",
        "\n": "n",
        "	": "t",
        "\u2028": "u2028",
        "\u2029": "u2029"
    }
      , $ = /\\|'|\r|\n|\t|\u2028|\u2029/g;
    T.template = function(t, e, n) {
        var i;
        n = T.defaults({}, n, T.templateSettings);
        var r = new RegExp([(n.escape || L).source, (n.interpolate || L).source, (n.evaluate || L).source].join("|") + "|$","g")
          , o = 0
          , s = "__p+='";
        t.replace(r, function(e, n, i, r, a) {
            return s += t.slice(o, a).replace($, function(t) {
                return "\\" + R[t]
            }),
            n && (s += "'+\n((__t=(" + n + "))==null?'':_.escape(__t))+\n'"),
            i && (s += "'+\n((__t=(" + i + "))==null?'':__t)+\n'"),
            r && (s += "';\n" + r + "\n__p+='"),
            o = a + e.length,
            e
        }),
        s += "';\n",
        n.variable || (s = "with(obj||{}){\n" + s + "}\n"),
        s = "var __t,__p='',__j=Array.prototype.join,print=function(){__p+=__j.call(arguments,'');};\n" + s + "return __p;\n";
        try {
            i = new Function(n.variable || "obj","_",s)
        } catch (a) {
            throw a.source = s,
            a
        }
        if (e)
            return i(e, T);
        var l = function(t) {
            return i.call(this, t, T)
        };
        return l.source = "function(" + (n.variable || "obj") + "){\n" + s + "}",
        l
    }
    ,
    T.chain = function(t) {
        return T(t).chain()
    }
    ;
    var I = function(t) {
        return this._chain ? T(t).chain() : t
    };
    T.mixin(T),
    C(["pop", "push", "reverse", "shift", "sort", "splice", "unshift"], function(t) {
        var e = i[t];
        T.prototype[t] = function() {
            var n = this._wrapped;
            return e.apply(n, arguments),
            "shift" != t && "splice" != t || 0 !== n.length || delete n[0],
            I.call(this, n)
        }
    }),
    C(["concat", "join", "slice"], function(t) {
        var e = i[t];
        T.prototype[t] = function() {
            return I.call(this, e.apply(this._wrapped, arguments))
        }
    }),
    T.extend(T.prototype, {
        chain: function() {
            return this._chain = !0,
            this
        },
        value: function() {
            return this._wrapped
        }
    }),
    "function" == typeof define && define.amd && define("underscore", [], function() {
        return T
    })
}
.call(this),
function(t, e) {
    "undefined" != typeof exports ? e(t, exports, require("underscore")) : "function" == typeof define && define.amd ? define("backbone", ["underscore", "jquery", "exports"], function(n, i, r) {
        t.Backbone = e(t, r, n, i)
    }) : t.Backbone = e(t, {}, t._, t.jQuery || t.Zepto || t.ender || t.$)
}(this, function(t, e, n, i) {
    var r = t.Backbone
      , o = []
      , s = o.push
      , a = o.slice
      , l = o.splice;
    e.VERSION = "1.0.0",
    e.$ = i,
    e.noConflict = function() {
        return t.Backbone = r,
        this
    }
    ,
    e.emulateHTTP = !1,
    e.emulateJSON = !1;
    var u = e.Events = {
        on: function(t, e, n) {
            if (!h(this, "on", t, [e, n]) || !e)
                return this;
            this._events || (this._events = {});
            var i = this._events[t] || (this._events[t] = []);
            return i.push({
                callback: e,
                context: n,
                ctx: n || this
            }),
            this
        },
        once: function(t, e, i) {
            if (!h(this, "once", t, [e, i]) || !e)
                return this;
            var r = this
              , o = n.once(function() {
                r.off(t, o),
                e.apply(this, arguments)
            });
            return o._callback = e,
            this.on(t, o, i)
        },
        off: function(t, e, i) {
            var r, o, s, a, l, u, c, p;
            if (!this._events || !h(this, "off", t, [e, i]))
                return this;
            if (!t && !e && !i)
                return this._events = {},
                this;
            for (a = t ? [t] : n.keys(this._events),
            l = 0,
            u = a.length; u > l; l++)
                if (t = a[l],
                s = this._events[t]) {
                    if (this._events[t] = r = [],
                    e || i)
                        for (c = 0,
                        p = s.length; p > c; c++)
                            o = s[c],
                            (e && e !== o.callback && e !== o.callback._callback || i && i !== o.context) && r.push(o);
                    r.length || delete this._events[t]
                }
            return this
        },
        trigger: function(t) {
            if (!this._events)
                return this;
            var e = a.call(arguments, 1);
            if (!h(this, "trigger", t, e))
                return this;
            var n = this._events[t]
              , i = this._events.all;
            return n && p(n, e),
            i && p(i, arguments),
            this
        },
        stopListening: function(t, e, n) {
            var i = this._listeners;
            if (!i)
                return this;
            var r = !e && !n;
            "object" == typeof e && (n = this),
            t && ((i = {})[t._listenerId] = t);
            for (var o in i)
                i[o].off(e, n, this),
                r && delete this._listeners[o];
            return this
        }
    }
      , c = /\s+/
      , h = function(t, e, n, i) {
        if (!n)
            return !0;
        if ("object" == typeof n) {
            for (var r in n)
                t[e].apply(t, [r, n[r]].concat(i));
            return !1
        }
        if (c.test(n)) {
            for (var o = n.split(c), s = 0, a = o.length; a > s; s++)
                t[e].apply(t, [o[s]].concat(i));
            return !1
        }
        return !0
    }
      , p = function(t, e) {
        var n, i = -1, r = t.length, o = e[0], s = e[1], a = e[2];
        switch (e.length) {
        case 0:
            for (; ++i < r; )
                (n = t[i]).callback.call(n.ctx);
            return;
        case 1:
            for (; ++i < r; )
                (n = t[i]).callback.call(n.ctx, o);
            return;
        case 2:
            for (; ++i < r; )
                (n = t[i]).callback.call(n.ctx, o, s);
            return;
        case 3:
            for (; ++i < r; )
                (n = t[i]).callback.call(n.ctx, o, s, a);
            return;
        default:
            for (; ++i < r; )
                (n = t[i]).callback.apply(n.ctx, e)
        }
    }
      , f = {
        listenTo: "on",
        listenToOnce: "once"
    };
    n.each(f, function(t, e) {
        u[e] = function(e, i, r) {
            var o = this._listeners || (this._listeners = {})
              , s = e._listenerId || (e._listenerId = n.uniqueId("l"));
            return o[s] = e,
            "object" == typeof i && (r = this),
            e[t](i, r, this),
            this
        }
    }),
    u.bind = u.on,
    u.unbind = u.off,
    n.extend(e, u);
    var d = e.Model = function(t, e) {
        var i, r = t || {};
        e || (e = {}),
        this.cid = n.uniqueId("c"),
        this.attributes = {},
        n.extend(this, n.pick(e, m)),
        e.parse && (r = this.parse(r, e) || {}),
        (i = n.result(this, "defaults")) && (r = n.defaults({}, r, i)),
        this.set(r, e),
        this.changed = {},
        this.initialize.apply(this, arguments);
    }
      , m = ["url", "urlRoot", "collection"];
    n.extend(d.prototype, u, {
        changed: null,
        validationError: null,
        idAttribute: "id",
        initialize: function() {},
        toJSON: function(t) {
            return n.clone(this.attributes)
        },
        sync: function() {
            return e.sync.apply(this, arguments)
        },
        get: function(t) {
            return this.attributes[t]
        },
        escape: function(t) {
            return n.escape(this.get(t))
        },
        has: function(t) {
            return null != this.get(t)
        },
        set: function(t, e, i) {
            var r, o, s, a, l, u, c, h;
            if (null == t)
                return this;
            if ("object" == typeof t ? (o = t,
            i = e) : (o = {})[t] = e,
            i || (i = {}),
            !this._validate(o, i))
                return !1;
            s = i.unset,
            l = i.silent,
            a = [],
            u = this._changing,
            this._changing = !0,
            u || (this._previousAttributes = n.clone(this.attributes),
            this.changed = {}),
            h = this.attributes,
            c = this._previousAttributes,
            this.idAttribute in o && (this.id = o[this.idAttribute]);
            for (r in o)
                e = o[r],
                n.isEqual(h[r], e) || a.push(r),
                n.isEqual(c[r], e) ? delete this.changed[r] : this.changed[r] = e,
                s ? delete h[r] : h[r] = e;
            if (!l) {
                a.length && (this._pending = !0);
                for (var p = 0, f = a.length; f > p; p++)
                    this.trigger("change:" + a[p], this, h[a[p]], i)
            }
            if (u)
                return this;
            if (!l)
                for (; this._pending; )
                    this._pending = !1,
                    this.trigger("change", this, i);
            return this._pending = !1,
            this._changing = !1,
            this
        },
        unset: function(t, e) {
            return this.set(t, void 0, n.extend({}, e, {
                unset: !0
            }))
        },
        clear: function(t) {
            var e = {};
            for (var i in this.attributes)
                e[i] = void 0;
            return this.set(e, n.extend({}, t, {
                unset: !0
            }))
        },
        hasChanged: function(t) {
            return null == t ? !n.isEmpty(this.changed) : n.has(this.changed, t)
        },
        changedAttributes: function(t) {
            if (!t)
                return this.hasChanged() ? n.clone(this.changed) : !1;
            var e, i = !1, r = this._changing ? this._previousAttributes : this.attributes;
            for (var o in t)
                n.isEqual(r[o], e = t[o]) || ((i || (i = {}))[o] = e);
            return i
        },
        previous: function(t) {
            return null != t && this._previousAttributes ? this._previousAttributes[t] : null
        },
        previousAttributes: function() {
            return n.clone(this._previousAttributes)
        },
        fetch: function(t) {
            t = t ? n.clone(t) : {},
            void 0 === t.parse && (t.parse = !0);
            var e = this
              , i = t.success;
            return t.success = function(n) {
                return e.set(e.parse(n, t), t) ? (i && i(e, n, t),
                void e.trigger("sync", e, n, t)) : !1
            }
            ,
            I(this, t),
            this.sync("read", this, t)
        },
        save: function(t, e, i) {
            var r, o, s, a = this.attributes;
            if (null == t || "object" == typeof t ? (r = t,
            i = e) : (r = {})[t] = e,
            !(!r || i && i.wait || this.set(r, i)))
                return !1;
            if (i = n.extend({
                validate: !0
            }, i),
            !this._validate(r, i))
                return !1;
            r && i.wait && (this.attributes = n.extend({}, a, r)),
            void 0 === i.parse && (i.parse = !0);
            var l = this
              , u = i.success;
            return i.success = function(t) {
                l.attributes = a;
                var e = l.parse(t, i);
                return i.wait && (e = n.extend(r || {}, e)),
                n.isObject(e) && !l.set(e, i) ? !1 : (u && u(l, t, i),
                void l.trigger("sync", l, t, i))
            }
            ,
            I(this, i),
            o = this.isNew() ? "create" : i.patch ? "patch" : "update",
            "patch" === o && (i.attrs = r),
            s = this.sync(o, this, i),
            r && i.wait && (this.attributes = a),
            s
        },
        destroy: function(t) {
            t = t ? n.clone(t) : {};
            var e = this
              , i = t.success
              , r = function() {
                e.trigger("destroy", e, e.collection, t)
            };
            if (t.success = function(n) {
                (t.wait || e.isNew()) && r(),
                i && i(e, n, t),
                e.isNew() || e.trigger("sync", e, n, t)
            }
            ,
            this.isNew())
                return t.success(),
                !1;
            I(this, t);
            var o = this.sync("delete", this, t);
            return t.wait || r(),
            o
        },
        url: function() {
            var t = n.result(this, "urlRoot") || n.result(this.collection, "url") || $();
            console.log(">>>>>>>>>>>>>>>>>>url>>>>>>>>>>>>>>>>");
            console.log(">>>>>>>>>>>>>>>>>>url>>>>>>>>>>>>>>>>",this.isNew() ? t : t + ("/" === t.charAt(t.length - 1) ? "" : "/") + encodeURIComponent(this.id));
            return this.isNew() ? t : t + ("/" === t.charAt(t.length - 1) ? "" : "/") + encodeURIComponent(this.id)
        },
        parse: function(t, e) {
            return t
        },
        clone: function() {
            return new this.constructor(this.attributes)
        },
        isNew: function() {
            return null == this.id
        },
        isValid: function(t) {
            return this._validate({}, n.extend(t || {}, {
                validate: !0
            }))
        },
        _validate: function(t, e) {
            if (!e.validate || !this.validate)
                return !0;
            t = n.extend({}, this.attributes, t);
            var i = this.validationError = this.validate(t, e) || null;
            return i ? (this.trigger("invalid", this, i, n.extend(e || {}, {
                validationError: i
            })),
            !1) : !0
        }
    });
    var g = ["keys", "values", "pairs", "invert", "pick", "omit"];
    n.each(g, function(t) {
        d.prototype[t] = function() {
            var e = a.call(arguments);
            return e.unshift(this.attributes),
            n[t].apply(n, e)
        }
    });
    var v = e.Collection = function(t, e) {
        e || (e = {}),
        e.url && (this.url = e.url),
        e.model && (this.model = e.model),
        void 0 !== e.comparator && (this.comparator = e.comparator),
        this._reset(),
        this.initialize.apply(this, arguments),
        t && this.reset(t, n.extend({
            silent: !0
        }, e))
    }
      , y = {
        add: !0,
        remove: !0,
        merge: !0
    }
      , _ = {
        add: !0,
        merge: !1,
        remove: !1
    };
    n.extend(v.prototype, u, {
        model: d,
        initialize: function() {},
        toJSON: function(t) {
            return this.map(function(e) {
                return e.toJSON(t)
            })
        },
        sync: function() {
            return e.sync.apply(this, arguments)
        },
        add: function(t, e) {
            return this.set(t, n.defaults(e || {}, _))
        },
        remove: function(t, e) {
            t = n.isArray(t) ? t.slice() : [t],
            e || (e = {});
            var i, r, o, s;
            for (i = 0,
            r = t.length; r > i; i++)
                s = this.get(t[i]),
                s && (delete this._byId[s.id],
                delete this._byId[s.cid],
                o = this.indexOf(s),
                this.models.splice(o, 1),
                this.length--,
                e.silent || (e.index = o,
                s.trigger("remove", s, this, e)),
                this._removeReference(s));
            return this
        },
        set: function(t, e) {
            e = n.defaults(e || {}, y),
            e.parse && (t = this.parse(t, e)),
            n.isArray(t) || (t = t ? [t] : []);
            var i, r, o, a, u, c = e.at, h = this.comparator && null == c && e.sort !== !1, p = n.isString(this.comparator) ? this.comparator : null, f = [], d = [], m = {};
            for (i = 0,
            r = t.length; r > i; i++)
                (o = this._prepareModel(t[i], e)) && ((a = this.get(o)) ? (e.remove && (m[a.cid] = !0),
                e.merge && (a.set(o.attributes, e),
                h && !u && a.hasChanged(p) && (u = !0))) : e.add && (f.push(o),
                o.on("all", this._onModelEvent, this),
                this._byId[o.cid] = o,
                null != o.id && (this._byId[o.id] = o)));
            if (e.remove) {
                for (i = 0,
                r = this.length; r > i; ++i)
                    m[(o = this.models[i]).cid] || d.push(o);
                d.length && this.remove(d, e)
            }
            if (f.length && (h && (u = !0),
            this.length += f.length,
            null != c ? l.apply(this.models, [c, 0].concat(f)) : s.apply(this.models, f)),
            u && this.sort({
                silent: !0
            }),
            e.silent)
                return this;
            for (i = 0,
            r = f.length; r > i; i++)
                (o = f[i]).trigger("add", o, this, e);
            return u && this.trigger("sort", this, e),
            this
        },
        reset: function(t, e) {
            e || (e = {});
            for (var i = 0, r = this.models.length; r > i; i++)
                this._removeReference(this.models[i]);
            return e.previousModels = this.models,
            this._reset(),
            this.add(t, n.extend({
                silent: !0
            }, e)),
            e.silent || this.trigger("reset", this, e),
            this
        },
        push: function(t, e) {
            return t = this._prepareModel(t, e),
            this.add(t, n.extend({
                at: this.length
            }, e)),
            t
        },
        pop: function(t) {
            var e = this.at(this.length - 1);
            return this.remove(e, t),
            e
        },
        unshift: function(t, e) {
            return t = this._prepareModel(t, e),
            this.add(t, n.extend({
                at: 0
            }, e)),
            t
        },
        shift: function(t) {
            var e = this.at(0);
            return this.remove(e, t),
            e
        },
        slice: function(t, e) {
            return this.models.slice(t, e)
        },
        get: function(t) {
            return null == t ? void 0 : this._byId[null != t.id ? t.id : t.cid || t]
        },
        at: function(t) {
            return this.models[t]
        },
        where: function(t, e) {
            return n.isEmpty(t) ? e ? void 0 : [] : this[e ? "find" : "filter"](function(e) {
                for (var n in t)
                    if (t[n] !== e.get(n))
                        return !1;
                return !0
            })
        },
        findWhere: function(t) {
            return this.where(t, !0)
        },
        sort: function(t) {
            if (!this.comparator)
                throw new Error("Cannot sort a set without a comparator");
            return t || (t = {}),
            n.isString(this.comparator) || 1 === this.comparator.length ? this.models = this.sortBy(this.comparator, this) : this.models.sort(n.bind(this.comparator, this)),
            t.silent || this.trigger("sort", this, t),
            this
        },
        sortedIndex: function(t, e, i) {
            e || (e = this.comparator);
            var r = n.isFunction(e) ? e : function(t) {
                return t.get(e)
            }
            ;
            return n.sortedIndex(this.models, t, r, i)
        },
        pluck: function(t) {
            return n.invoke(this.models, "get", t)
        },
        fetch: function(t) {
            t = t ? n.clone(t) : {},
            void 0 === t.parse && (t.parse = !0);
            var e = t.success
              , i = this;
            return t.success = function(n) {
                var r = t.reset ? "reset" : "set";
                i[r](n, t),
                e && e(i, n, t),
                i.trigger("sync", i, n, t)
            }
            ,
            I(this, t),
            this.sync("read", this, t)
        },
        create: function(t, e) {
            if (e = e ? n.clone(e) : {},
            !(t = this._prepareModel(t, e)))
                return !1;
            e.wait || this.add(t, e);
            var i = this
              , r = e.success;
            return e.success = function(n) {
                e.wait && i.add(t, e),
                r && r(t, n, e)
            }
            ,
            t.save(null, e),
            t
        },
        parse: function(t, e) {
            return t
        },
        clone: function() {
            return new this.constructor(this.models)
        },
        _reset: function() {
            this.length = 0,
            this.models = [],
            this._byId = {}
        },
        _prepareModel: function(t, e) {
            if (t instanceof d)
                return t.collection || (t.collection = this),
                t;
            e || (e = {}),
            e.collection = this;
            var n = new this.model(t,e);
            return n._validate(t, e) ? n : (this.trigger("invalid", this, t, e),
            !1)
        },
        _removeReference: function(t) {
            this === t.collection && delete t.collection,
            t.off("all", this._onModelEvent, this)
        },
        _onModelEvent: function(t, e, n, i) {
            ("add" !== t && "remove" !== t || n === this) && ("destroy" === t && this.remove(e, i),
            e && t === "change:" + e.idAttribute && (delete this._byId[e.previous(e.idAttribute)],
            null != e.id && (this._byId[e.id] = e)),
            this.trigger.apply(this, arguments))
        }
    });
    var x = ["forEach", "each", "map", "collect", "reduce", "foldl", "inject", "reduceRight", "foldr", "find", "detect", "filter", "select", "reject", "every", "all", "some", "any", "include", "contains", "invoke", "max", "min", "toArray", "size", "first", "head", "take", "initial", "rest", "tail", "drop", "last", "without", "indexOf", "shuffle", "lastIndexOf", "isEmpty", "chain"];
    n.each(x, function(t) {
        v.prototype[t] = function() {
            var e = a.call(arguments);
            return e.unshift(this.models),
            n[t].apply(n, e)
        }
    });
    var w = ["groupBy", "countBy", "sortBy"];
    n.each(w, function(t) {
        v.prototype[t] = function(e, i) {
            var r = n.isFunction(e) ? e : function(t) {
                return t.get(e)
            }
            ;
            return n[t](this.models, r, i)
        }
    });
    var b = e.View = function(t) {
        this.cid = n.uniqueId("view"),
        this._configure(t || {}),
        this._ensureElement(),
        this.initialize.apply(this, arguments),
        this.delegateEvents()
    }
      , T = /^(\S+)\s*(.*)$/
      , C = ["model", "collection", "el", "id", "attributes", "className", "tagName", "events"];
    n.extend(b.prototype, u, {
        tagName: "div",
        $: function(t) {
            return this.$el.find(t)
        },
        initialize: function() {},
        render: function() {
            return this
        },
        remove: function() {
            return this.$el.remove(),
            this.stopListening(),
            this
        },
        setElement: function(t, n) {
            return this.$el && this.undelegateEvents(),
            this.$el = t instanceof e.$ ? t : e.$(t),
            this.el = this.$el[0],
            n !== !1 && this.delegateEvents(),
            this
        },
        delegateEvents: function(t) {
            if (!t && !(t = n.result(this, "events")))
                return this;
            this.undelegateEvents();
            for (var e in t) {
                var i = t[e];
                if (n.isFunction(i) || (i = this[t[e]]),
                i) {
                    var r = e.match(T)
                      , o = r[1]
                      , s = r[2];
                    i = n.bind(i, this),
                    o += ".delegateEvents" + this.cid,
                    "" === s ? this.$el.on(o, i) : this.$el.on(o, s, i)
                }
            }
            return this
        },
        undelegateEvents: function() {
            return this.$el.off(".delegateEvents" + this.cid),
            this
        },
        _configure: function(t) {
            this.options && (t = n.extend({}, n.result(this, "options"), t)),
            n.extend(this, n.pick(t, C)),
            this.options = t
        },
        _ensureElement: function() {
            if (this.el)
                this.setElement(n.result(this, "el"), !1);
            else {
                var t = n.extend({}, n.result(this, "attributes"));
                this.id && (t.id = n.result(this, "id")),
                this.className && (t["class"] = n.result(this, "className"));
                var i = e.$("<" + n.result(this, "tagName") + ">").attr(t);
                this.setElement(i, !1)
            }
        }
    }),
    e.sync = function(t, i, r) {
        var o = S[t];
        n.defaults(r || (r = {}), {
            emulateHTTP: e.emulateHTTP,
            emulateJSON: e.emulateJSON
        });
        var s = {
            type: o,
            dataType: "json"
        };
        if (r.url || (s.url = n.result(i, "url") || $()),
        null != r.data || !i || "create" !== t && "update" !== t && "patch" !== t || (s.contentType = "application/json",
        s.data = JSON.stringify(r.attrs || i.toJSON(r))),
        r.emulateJSON && (s.contentType = "application/x-www-form-urlencoded",
        s.data = s.data ? {
            model: s.data
        } : {}),
        r.emulateHTTP && ("PUT" === o || "DELETE" === o || "PATCH" === o)) {
            s.type = "POST",
            r.emulateJSON && (s.data._method = o);
            var a = r.beforeSend;
            r.beforeSend = function(t) {
                return t.setRequestHeader("X-HTTP-Method-Override", o),
                a ? a.apply(this, arguments) : void 0
            }
        }
        "GET" === s.type || r.emulateJSON || (s.processData = !1),
        "PATCH" !== s.type || !window.ActiveXObject || window.external && window.external.msActiveXFilteringEnabled || (s.xhr = function() {
            return new ActiveXObject("Microsoft.XMLHTTP")
        }
        );
        var l = r.xhr = e.ajax(n.extend(s, r));
        return i.trigger("request", i, l, r),
        l
    }
    ;
    var S = {
        create: "POST",
        update: "PUT",
        patch: "PATCH",
        "delete": "DELETE",
        read: "GET"
    };
    e.ajax = function() {
        return e.$.ajax.apply(e.$, arguments)
    }
    ;
    var k = e.Router = function(t) {
        t || (t = {}),
        t.routes && (this.routes = t.routes),
        this._bindRoutes(),
        this.initialize.apply(this, arguments)
    }
      , j = /\((.*?)\)/g
      , P = /(\(\?)?:\w+/g
      , E = /\*\w+/g
      , O = /[\-{}\[\]+?.,\\\^$|#\s]/g;
    n.extend(k.prototype, u, {
        initialize: function() {},
        route: function(t, i, r) {
            n.isRegExp(t) || (t = this._routeToRegExp(t)),
            n.isFunction(i) && (r = i,
            i = ""),
            r || (r = this[i]);
            var o = this;
            return e.history.route(t, function(n) {
                var s = o._extractParameters(t, n);
                r && r.apply(o, s),
                o.trigger.apply(o, ["route:" + i].concat(s)),
                o.trigger("route", i, s),
                e.history.trigger("route", o, i, s)
            }),
            this
        },
        navigate: function(t, n) {
            return e.history.navigate(t, n),
            this
        },
        _bindRoutes: function() {
            if (this.routes) {
                this.routes = n.result(this, "routes");
                for (var t, e = n.keys(this.routes); null != (t = e.pop()); )
                    this.route(t, this.routes[t])
            }
        },
        _routeToRegExp: function(t) {
            return t = t.replace(O, "\\$&").replace(j, "(?:$1)?").replace(P, function(t, e) {
                return e ? t : "([^/]+)"
            }).replace(E, "(.*?)"),
            new RegExp("^" + t + "$")
        },
        _extractParameters: function(t, e) {
            var i = t.exec(e).slice(1);
            return n.map(i, function(t) {
                return t ? decodeURIComponent(t) : null
            })
        }
    });
    var M = e.History = function() {
        this.handlers = [],
        n.bindAll(this, "checkUrl"),
        "undefined" != typeof window && (this.location = window.location,
        this.history = window.history)
    }
      , D = /^[#\/]|\s+$/g
      , A = /^\/+|\/+$/g
      , N = /msie [\w.]+/
      , L = /\/$/;
    M.started = !1,
    n.extend(M.prototype, u, {
        interval: 50,
        getHash: function(t) {
            var e = (t || this).location.href.match(/#(.*)$/);
            return e ? e[1] : ""
        },
        getFragment: function(t, e) {
            if (null == t)
                if (this._hasPushState || !this._wantsHashChange || e) {
                    t = this.location.pathname;
                    var n = this.root.replace(L, "");
                    t.indexOf(n) || (t = t.substr(n.length))
                } else
                    t = this.getHash();
            return t.replace(D, "")
        },
        start: function(t) {
            if (M.started)
                throw new Error("Backbone.history has already been started");
            M.started = !0,
            this.options = n.extend({}, {
                root: "/"
            }, this.options, t),
            this.root = this.options.root,
            this._wantsHashChange = this.options.hashChange !== !1,
            this._wantsPushState = !!this.options.pushState,
            this._hasPushState = !!(this.options.pushState && this.history && this.history.pushState);
            var i = this.getFragment()
              , r = document.documentMode
              , o = N.exec(navigator.userAgent.toLowerCase()) && (!r || 7 >= r);
            this.root = ("/" + this.root + "/").replace(A, "/"),
            o && this._wantsHashChange && (this.iframe = e.$('<iframe src="javascript:0" tabindex="-1" />').hide().appendTo("body")[0].contentWindow,
            this.navigate(i)),
            this._hasPushState ? e.$(window).on("popstate", this.checkUrl) : this._wantsHashChange && "onhashchange"in window && !o ? e.$(window).on("hashchange", this.checkUrl) : this._wantsHashChange && (this._checkUrlInterval = setInterval(this.checkUrl, this.interval)),
            this.fragment = i;
            var s = this.location
              , a = s.pathname.replace(/[^\/]$/, "$&/") === this.root;
            return this._wantsHashChange && this._wantsPushState && !this._hasPushState && !a ? (this.fragment = this.getFragment(null, !0),
            this.location.replace(this.root + this.location.search + "#" + this.fragment),
            !0) : (this._wantsPushState && this._hasPushState && a && s.hash && (this.fragment = this.getHash().replace(D, ""),
            this.history.replaceState({}, document.title, this.root + this.fragment + s.search)),
            this.options.silent ? void 0 : this.loadUrl())
        },
        stop: function() {
            e.$(window).off("popstate", this.checkUrl).off("hashchange", this.checkUrl),
            clearInterval(this._checkUrlInterval),
            M.started = !1
        },
        route: function(t, e) {
            this.handlers.unshift({
                route: t,
                callback: e
            })
        },
        checkUrl: function(t) {
            var e = this.getFragment();
            return e === this.fragment && this.iframe && (e = this.getFragment(this.getHash(this.iframe))),
            e === this.fragment ? !1 : (this.iframe && this.navigate(e),
            void (this.loadUrl() || this.loadUrl(this.getHash())))
        },
        loadUrl: function(t) {
            var e = this.fragment = this.getFragment(t)
              , i = n.any(this.handlers, function(t) {
                return t.route.test(e) ? (t.callback(e),
                !0) : void 0
            });
            return i
        },
        navigate: function(t, e) {
            if (!M.started)
                return !1;
            if (e && e !== !0 || (e = {
                trigger: e
            }),
            t = this.getFragment(t || ""),
            this.fragment !== t) {
                this.fragment = t;
                var n = this.root + t;
                if (this._hasPushState)
                    this.history[e.replace ? "replaceState" : "pushState"]({}, document.title, n);
                else {
                    if (!this._wantsHashChange)
                        return this.location.assign(n);
                    this._updateHash(this.location, t, e.replace),
                    this.iframe && t !== this.getFragment(this.getHash(this.iframe)) && (e.replace || this.iframe.document.open().close(),
                    this._updateHash(this.iframe.location, t, e.replace))
                }
                e.trigger && this.loadUrl(t)
            }
        },
        _updateHash: function(t, e, n) {
            if (n) {
                var i = t.href.replace(/(javascript:|#).*$/, "");
                t.replace(i + "#" + e)
            } else
                t.hash = "#" + e
        }
    }),
    e.history = new M;
    var R = function(t, e) {
        var i, r = this;
        i = t && n.has(t, "constructor") ? t.constructor : function() {
            return r.apply(this, arguments)
        }
        ,
        n.extend(i, r, e);
        var o = function() {
            this.constructor = i
        };
        return o.prototype = r.prototype,
        i.prototype = new o,
        t && n.extend(i.prototype, t),
        i.__super__ = r.prototype,
        i
    };
    d.extend = v.extend = k.extend = b.extend = M.extend = R;
    var $ = function() {
        throw new Error('A "url" property or function must be specified')
    }
      , I = function(t, e) {
        var n = e.error;
        e.error = function(i) {
            n && n(t, i, e),
            t.trigger("error", t, i, e)
        }
    };
    return e
});
var extend = function(t, e) {
    function n() {
        this.constructor = t
    }
    for (var i in e)
        hasProp.call(e, i) && (t[i] = e[i]);
    return n.prototype = e.prototype,
    t.prototype = new n,
    t.__super__ = e.prototype,
    t
}
  , hasProp = {}.hasOwnProperty;
define("models/project", ["backbone"], function(t) {
    var e;
    return e = function(t) {
        function e() {
            return e.__super__.constructor.apply(this, arguments)
        }
        return extend(e, t),
        e.prototype.defaults = {
            name: null,
            thumb: null,
            thumbSize: "large"
        },
        e.prototype.initialize = function() {
            var t, e;
            switch (e = 0,
            t = 0,
            this.get("thumbSize")) {
            case "large":
                e = 436,
                t = 300;
                break;
            default:
                e = 218,
                t = 150
            }
            return this.set({
                thumbWidth: e,
                thumbHeight: t
            })
        }
        ,
        e.prototype.url = function() {
            //return "data/project/" + this.id + ".json?v=" + Math.random()
            return "data/project/" + this.id + ".json"
        }
        ,
        e
    }(t.Model)
});
var extend = function(t, e) {
    function n() {
        this.constructor = t
    }
    for (var i in e)
        hasProp.call(e, i) && (t[i] = e[i]);
    return n.prototype = e.prototype,
    t.prototype = new n,
    t.__super__ = e.prototype,
    t
}
  , hasProp = {}.hasOwnProperty;
define("collections/projects", ["underscore", "backbone", "models/project"], function(t, e, n) {
    var i;
    return i = function(t) {
        function e() {
            return e.__super__.constructor.apply(this, arguments)
        }
        return extend(e, t),
        e.prototype.model = n,
        e.prototype.selectedModel = null,
        //e.prototype.url = "data/projects.json?v=" + Math.random(),
        e.prototype.url = "data/projects.json",
        e.prototype.setSelected = function(t) {
            var e;
            return t && this.get(t) ? (e = this.get(t),
            this.selectedModel = e) : this.selectedModel = null,
            this.trigger("change")
        }
        ,
        e.prototype.getSelected = function() {
            return this.selectedModel
        }
        ,
        e.prototype.getPrevious = function() {
            return this.selectedModel ? this.indexOf(this.selectedModel) > 0 ? this.at(this.indexOf(this.selectedModel) - 1) : this.at(this.length - 1) : null
        }
        ,
        e.prototype.getNext = function() {
            return this.selectedModel ? this.indexOf(this.selectedModel) >= this.length - 1 ? this.at(0) : this.at(this.indexOf(this.selectedModel) + 1) : null
        }
        ,
        e.prototype.getSlug = function(t) {
            var e;
            return e = this.where({
                slug: t
            }),
            1 === e.length ? e[0] : null
        }
        ,
        e
    }(e.Collection)
});
var extend = function(t, e) {
    function n() {
        this.constructor = t
    }
    for (var i in e)
        hasProp.call(e, i) && (t[i] = e[i]);
    return n.prototype = e.prototype,
    t.prototype = new n,
    t.__super__ = e.prototype,
    t
}
  , hasProp = {}.hasOwnProperty;
define("views/abstract", [], function() {
    var t;
    return t = function(t) {
        function e(t) {
            this.options = t || {},
            e.__super__.constructor.apply(this, arguments)
        }
        return extend(e, t),
        e
    }(Backbone.View)
}),
define("config", ["module"], function(t) {
    var e;
    return new (e = function() {
        function e() {}
        return e.prototype.mobileBreakWidth = 568,
        e.prototype.isHandheld = function() {
            var t, e;
            return e = void 0,
            t = void 0,
            e = "undefined" != typeof window.innerWidth ? window.innerWidth : "undefined" != typeof document.documentElement && "undefined" != typeof document.documentElement.clientWidth && 0 !== document.documentElement.clientWidth ? document.documentElement.clientWidth : document.getElementsByTagName("body")([0].clientWidth),
            e < this.mobileBreakWidth
        }
        ,
        e.prototype.isHandheldDevice = function() {
            return screen.width < this.mobileBreakWidth
        }
        ,
        e.prototype.relUri = "/",
        e.prototype.rootUri = t.config().rootUri,
        e
    }())
}),
(window._gsQueue || (window._gsQueue = [])).push(function() {
    "use strict";
    window._gsDefine("TweenMax", ["core.Animation", "core.SimpleTimeline", "TweenLite"], function(t, e, n) {
        var i = [].slice
          , r = function(t, e, i) {
            n.call(this, t, e, i),
            this._cycle = 0,
            this._yoyo = this.vars.yoyo === !0,
            this._repeat = this.vars.repeat || 0,
            this._repeatDelay = this.vars.repeatDelay || 0,
            this._dirty = !0,
            this.render = r.prototype.render
        }
          , o = function(t) {
            return t.jquery || t.length && t !== window && t[0] && (t[0] === window || t[0].nodeType && t[0].style && !t.nodeType)
        }
          , s = r.prototype = n.to({}, .1, {})
          , a = [];
        r.version = "1.10.2",
        s.constructor = r,
        s.kill()._gc = !1,
        r.killTweensOf = r.killDelayedCallsTo = n.killTweensOf,
        r.getTweensOf = n.getTweensOf,
        r.ticker = n.ticker,
        s.invalidate = function() {
            return this._yoyo = this.vars.yoyo === !0,
            this._repeat = this.vars.repeat || 0,
            this._repeatDelay = this.vars.repeatDelay || 0,
            this._uncache(!0),
            n.prototype.invalidate.call(this)
        }
        ,
        s.updateTo = function(t, e) {
            var i, r = this.ratio;
            e && this.timeline && this._startTime < this._timeline._time && (this._startTime = this._timeline._time,
            this._uncache(!1),
            this._gc ? this._enabled(!0, !1) : this._timeline.insert(this, this._startTime - this._delay));
            for (i in t)
                this.vars[i] = t[i];
            if (this._initted)
                if (e)
                    this._initted = !1;
                else if (this._notifyPluginsOfEnabled && this._firstPT && n._onPluginEvent("_onDisable", this),
                this._time / this._duration > .998) {
                    var o = this._time;
                    this.render(0, !0, !1),
                    this._initted = !1,
                    this.render(o, !0, !1)
                } else if (this._time > 0) {
                    this._initted = !1,
                    this._init();
                    for (var s, a = 1 / (1 - r), l = this._firstPT; l; )
                        s = l.s + l.c,
                        l.c *= a,
                        l.s = s - l.c,
                        l = l._next
                }
            return this
        }
        ,
        s.render = function(t, e, n) {
            var i, r, o, s, l, u, c, h = this._dirty ? this.totalDuration() : this._totalDuration, p = this._time, f = this._totalTime, d = this._cycle;
            if (t >= h ? (this._totalTime = h,
            this._cycle = this._repeat,
            this._yoyo && 0 !== (1 & this._cycle) ? (this._time = 0,
            this.ratio = this._ease._calcEnd ? this._ease.getRatio(0) : 0) : (this._time = this._duration,
            this.ratio = this._ease._calcEnd ? this._ease.getRatio(1) : 1),
            this._reversed || (i = !0,
            r = "onComplete"),
            0 === this._duration && ((0 === t || 0 > this._rawPrevTime) && this._rawPrevTime !== t && (n = !0,
            this._rawPrevTime > 0 && (r = "onReverseComplete",
            e && (t = -1))),
            this._rawPrevTime = t)) : 1e-7 > t ? (this._totalTime = this._time = this._cycle = 0,
            this.ratio = this._ease._calcEnd ? this._ease.getRatio(0) : 0,
            (0 !== f || 0 === this._duration && this._rawPrevTime > 0) && (r = "onReverseComplete",
            i = this._reversed),
            0 > t ? (this._active = !1,
            0 === this._duration && (this._rawPrevTime >= 0 && (n = !0),
            this._rawPrevTime = t)) : this._initted || (n = !0)) : (this._totalTime = this._time = t,
            0 !== this._repeat && (s = this._duration + this._repeatDelay,
            this._cycle = this._totalTime / s >> 0,
            0 !== this._cycle && this._cycle === this._totalTime / s && this._cycle--,
            this._time = this._totalTime - this._cycle * s,
            this._yoyo && 0 !== (1 & this._cycle) && (this._time = this._duration - this._time),
            this._time > this._duration ? this._time = this._duration : 0 > this._time && (this._time = 0)),
            this._easeType ? (l = this._time / this._duration,
            u = this._easeType,
            c = this._easePower,
            (1 === u || 3 === u && l >= .5) && (l = 1 - l),
            3 === u && (l *= 2),
            1 === c ? l *= l : 2 === c ? l *= l * l : 3 === c ? l *= l * l * l : 4 === c && (l *= l * l * l * l),
            this.ratio = 1 === u ? 1 - l : 2 === u ? l : .5 > this._time / this._duration ? l / 2 : 1 - l / 2) : this.ratio = this._ease.getRatio(this._time / this._duration)),
            p === this._time && !n)
                return void (f !== this._totalTime && this._onUpdate && (e || this._onUpdate.apply(this.vars.onUpdateScope || this, this.vars.onUpdateParams || a)));
            if (!this._initted) {
                if (this._init(),
                !this._initted)
                    return;
                this._time && !i ? this.ratio = this._ease.getRatio(this._time / this._duration) : i && this._ease._calcEnd && (this.ratio = this._ease.getRatio(0 === this._time ? 0 : 1))
            }
            for (this._active || !this._paused && this._time !== p && t >= 0 && (this._active = !0),
            0 === f && (this._startAt && (t >= 0 ? this._startAt.render(t, e, n) : r || (r = "_dummyGS")),
            this.vars.onStart && (0 !== this._totalTime || 0 === this._duration) && (e || this.vars.onStart.apply(this.vars.onStartScope || this, this.vars.onStartParams || a))),
            o = this._firstPT; o; )
                o.f ? o.t[o.p](o.c * this.ratio + o.s) : o.t[o.p] = o.c * this.ratio + o.s,
                o = o._next;
            this._onUpdate && (0 > t && this._startAt && this._startAt.render(t, e, n),
            e || this._onUpdate.apply(this.vars.onUpdateScope || this, this.vars.onUpdateParams || a)),
            this._cycle !== d && (e || this._gc || this.vars.onRepeat && this.vars.onRepeat.apply(this.vars.onRepeatScope || this, this.vars.onRepeatParams || a)),
            r && (this._gc || (0 > t && this._startAt && !this._onUpdate && this._startAt.render(t, e, n),
            i && (this._timeline.autoRemoveChildren && this._enabled(!1, !1),
            this._active = !1),
            !e && this.vars[r] && this.vars[r].apply(this.vars[r + "Scope"] || this, this.vars[r + "Params"] || a)))
        }
        ,
        r.to = function(t, e, n) {
            return new r(t,e,n)
        }
        ,
        r.from = function(t, e, n) {
            return n.runBackwards = !0,
            n.immediateRender = 0 != n.immediateRender,
            new r(t,e,n)
        }
        ,
        r.fromTo = function(t, e, n, i) {
            return i.startAt = n,
            i.immediateRender = 0 != i.immediateRender && 0 != n.immediateRender,
            new r(t,e,i)
        }
        ,
        r.staggerTo = r.allTo = function(t, e, s, l, u, c, h) {
            l = l || 0;
            var p, f, d, m, g = s.delay || 0, v = [], y = function() {
                s.onComplete && s.onComplete.apply(s.onCompleteScope || this, arguments),
                u.apply(h || this, c || a)
            };
            for (t instanceof Array || ("string" == typeof t && (t = n.selector(t) || t),
            o(t) && (t = i.call(t, 0))),
            p = t.length,
            d = 0; p > d; d++) {
                f = {};
                for (m in s)
                    f[m] = s[m];
                f.delay = g,
                d === p - 1 && u && (f.onComplete = y),
                v[d] = new r(t[d],e,f),
                g += l
            }
            return v
        }
        ,
        r.staggerFrom = r.allFrom = function(t, e, n, i, o, s, a) {
            return n.runBackwards = !0,
            n.immediateRender = 0 != n.immediateRender,
            r.staggerTo(t, e, n, i, o, s, a)
        }
        ,
        r.staggerFromTo = r.allFromTo = function(t, e, n, i, o, s, a, l) {
            return i.startAt = n,
            i.immediateRender = 0 != i.immediateRender && 0 != n.immediateRender,
            r.staggerTo(t, e, i, o, s, a, l)
        }
        ,
        r.delayedCall = function(t, e, n, i, o) {
            return new r(e,0,{
                delay: t,
                onComplete: e,
                onCompleteParams: n,
                onCompleteScope: i,
                onReverseComplete: e,
                onReverseCompleteParams: n,
                onReverseCompleteScope: i,
                immediateRender: !1,
                useFrames: o,
                overwrite: 0
            })
        }
        ,
        r.set = function(t, e) {
            return new r(t,0,e)
        }
        ,
        r.isTweening = function(t) {
            for (var e, i = n.getTweensOf(t), r = i.length; --r > -1; )
                if (e = i[r],
                e._active || e._startTime === e._timeline._time && e._timeline._active)
                    return !0;
            return !1
        }
        ;
        var l = function(t, e) {
            for (var i = [], r = 0, o = t._first; o; )
                o instanceof n ? i[r++] = o : (e && (i[r++] = o),
                i = i.concat(l(o, e)),
                r = i.length),
                o = o._next;
            return i
        }
          , u = r.getAllTweens = function(e) {
            return l(t._rootTimeline, e).concat(l(t._rootFramesTimeline, e))
        }
        ;
        r.killAll = function(t, n, i, r) {
            null == n && (n = !0),
            null == i && (i = !0);
            var o, s, a, l = u(0 != r), c = l.length, h = n && i && r;
            for (a = 0; c > a; a++)
                s = l[a],
                (h || s instanceof e || (o = s.target === s.vars.onComplete) && i || n && !o) && (t ? s.totalTime(s.totalDuration()) : s._enabled(!1, !1))
        }
        ,
        r.killChildTweensOf = function(t, e) {
            if (null != t) {
                var s, a, l, u, c, h = n._tweenLookup;
                if ("string" == typeof t && (t = n.selector(t) || t),
                o(t) && (t = i(t, 0)),
                t instanceof Array)
                    for (u = t.length; --u > -1; )
                        r.killChildTweensOf(t[u], e);
                else {
                    s = [];
                    for (l in h)
                        for (a = h[l].target.parentNode; a; )
                            a === t && (s = s.concat(h[l].tweens)),
                            a = a.parentNode;
                    for (c = s.length,
                    u = 0; c > u; u++)
                        e && s[u].totalTime(s[u].totalDuration()),
                        s[u]._enabled(!1, !1)
                }
            }
        }
        ;
        var c = function(t, n, i, r) {
            n = n !== !1,
            i = i !== !1,
            r = r !== !1;
            for (var o, s, a = u(r), l = n && i && r, c = a.length; --c > -1; )
                s = a[c],
                (l || s instanceof e || (o = s.target === s.vars.onComplete) && i || n && !o) && s.paused(t)
        };
        return r.pauseAll = function(t, e, n) {
            c(!0, t, e, n)
        }
        ,
        r.resumeAll = function(t, e, n) {
            c(!1, t, e, n)
        }
        ,
        r.globalTimeScale = function(e) {
            var i = t._rootTimeline
              , r = n.ticker.time;
            return arguments.length ? (e = e || 1e-6,
            i._startTime = r - (r - i._startTime) * i._timeScale / e,
            i = t._rootFramesTimeline,
            r = n.ticker.frame,
            i._startTime = r - (r - i._startTime) * i._timeScale / e,
            i._timeScale = t._rootTimeline._timeScale = e,
            e) : i._timeScale
        }
        ,
        s.progress = function(t) {
            return arguments.length ? this.totalTime(this.duration() * (this._yoyo && 0 !== (1 & this._cycle) ? 1 - t : t) + this._cycle * (this._duration + this._repeatDelay), !1) : this._time / this.duration()
        }
        ,
        s.totalProgress = function(t) {
            return arguments.length ? this.totalTime(this.totalDuration() * t, !1) : this._totalTime / this.totalDuration()
        }
        ,
        s.time = function(t, e) {
            return arguments.length ? (this._dirty && this.totalDuration(),
            t > this._duration && (t = this._duration),
            this._yoyo && 0 !== (1 & this._cycle) ? t = this._duration - t + this._cycle * (this._duration + this._repeatDelay) : 0 !== this._repeat && (t += this._cycle * (this._duration + this._repeatDelay)),
            this.totalTime(t, e)) : this._time
        }
        ,
        s.duration = function(e) {
            return arguments.length ? t.prototype.duration.call(this, e) : this._duration
        }
        ,
        s.totalDuration = function(t) {
            return arguments.length ? -1 === this._repeat ? this : this.duration((t - this._repeat * this._repeatDelay) / (this._repeat + 1)) : (this._dirty && (this._totalDuration = -1 === this._repeat ? 999999999999 : this._duration * (this._repeat + 1) + this._repeatDelay * this._repeat,
            this._dirty = !1),
            this._totalDuration)
        }
        ,
        s.repeat = function(t) {
            return arguments.length ? (this._repeat = t,
            this._uncache(!0)) : this._repeat
        }
        ,
        s.repeatDelay = function(t) {
            return arguments.length ? (this._repeatDelay = t,
            this._uncache(!0)) : this._repeatDelay
        }
        ,
        s.yoyo = function(t) {
            return arguments.length ? (this._yoyo = t,
            this) : this._yoyo
        }
        ,
        r
    }, !0),
    window._gsDefine("TimelineLite", ["core.Animation", "core.SimpleTimeline", "TweenLite"], function(t, e, n) {
        var i = function(t) {
            e.call(this, t),
            this._labels = {},
            this.autoRemoveChildren = this.vars.autoRemoveChildren === !0,
            this.smoothChildTiming = this.vars.smoothChildTiming === !0,
            this._sortChildren = !0,
            this._onUpdate = this.vars.onUpdate;
            var n, i, r = this.vars;
            for (i in r)
                n = r[i],
                n instanceof Array && -1 !== n.join("").indexOf("{self}") && (r[i] = this._swapSelfInParams(n));
            r.tweens instanceof Array && this.add(r.tweens, 0, r.align, r.stagger)
        }
          , r = []
          , o = function(t) {
            var e, n = {};
            for (e in t)
                n[e] = t[e];
            return n
        }
          , s = function(t, e, n, i) {
            t._timeline.pause(t._startTime),
            e && e.apply(i || t._timeline, n || r)
        }
          , a = r.slice
          , l = i.prototype = new e;
        return i.version = "1.10.2",
        l.constructor = i,
        l.kill()._gc = !1,
        l.to = function(t, e, i, r) {
            return e ? this.add(new n(t,e,i), r) : this.set(t, i, r)
        }
        ,
        l.from = function(t, e, i, r) {
            return this.add(n.from(t, e, i), r)
        }
        ,
        l.fromTo = function(t, e, i, r, o) {
            return e ? this.add(n.fromTo(t, e, i, r), o) : this.set(t, r, o)
        }
        ,
        l.staggerTo = function(t, e, r, s, l, u, c, h) {
            var p, f = new i({
                onComplete: u,
                onCompleteParams: c,
                onCompleteScope: h
            });
            for ("string" == typeof t && (t = n.selector(t) || t),
            !(t instanceof Array) && t.length && t !== window && t[0] && (t[0] === window || t[0].nodeType && t[0].style && !t.nodeType) && (t = a.call(t, 0)),
            s = s || 0,
            p = 0; t.length > p; p++)
                r.startAt && (r.startAt = o(r.startAt)),
                f.to(t[p], e, o(r), p * s);
            return this.add(f, l)
        }
        ,
        l.staggerFrom = function(t, e, n, i, r, o, s, a) {
            return n.immediateRender = 0 != n.immediateRender,
            n.runBackwards = !0,
            this.staggerTo(t, e, n, i, r, o, s, a)
        }
        ,
        l.staggerFromTo = function(t, e, n, i, r, o, s, a, l) {
            return i.startAt = n,
            i.immediateRender = 0 != i.immediateRender && 0 != n.immediateRender,
            this.staggerTo(t, e, i, r, o, s, a, l)
        }
        ,
        l.call = function(t, e, i, r) {
            return this.add(n.delayedCall(0, t, e, i), r)
        }
        ,
        l.set = function(t, e, i) {
            return i = this._parseTimeOrLabel(i, 0, !0),
            null == e.immediateRender && (e.immediateRender = i === this._time && !this._paused),
            this.add(new n(t,0,e), i)
        }
        ,
        i.exportRoot = function(t, e) {
            t = t || {},
            null == t.smoothChildTiming && (t.smoothChildTiming = !0);
            var r, o, s = new i(t), a = s._timeline;
            for (null == e && (e = !0),
            a._remove(s, !0),
            s._startTime = 0,
            s._rawPrevTime = s._time = s._totalTime = a._time,
            r = a._first; r; )
                o = r._next,
                e && r instanceof n && r.target === r.vars.onComplete || s.add(r, r._startTime - r._delay),
                r = o;
            return a.add(s, 0),
            s
        }
        ,
        l.add = function(r, o, s, a) {
            var l, u, c, h, p;
            if ("number" != typeof o && (o = this._parseTimeOrLabel(o, 0, !0, r)),
            !(r instanceof t)) {
                if (r instanceof Array) {
                    for (s = s || "normal",
                    a = a || 0,
                    l = o,
                    u = r.length,
                    c = 0; u > c; c++)
                        (h = r[c])instanceof Array && (h = new i({
                            tweens: h
                        })),
                        this.add(h, l),
                        "string" != typeof h && "function" != typeof h && ("sequence" === s ? l = h._startTime + h.totalDuration() / h._timeScale : "start" === s && (h._startTime -= h.delay())),
                        l += a;
                    return this._uncache(!0)
                }
                if ("string" == typeof r)
                    return this.addLabel(r, o);
                if ("function" != typeof r)
                    throw "Cannot add " + r + " into the timeline; it is neither a tween, timeline, function, nor a string.";
                r = n.delayedCall(0, r)
            }
            if (e.prototype.add.call(this, r, o),
            this._gc && !this._paused && this._time === this._duration && this._time < this.duration())
                for (p = this; p._gc && p._timeline; )
                    p._timeline.smoothChildTiming ? p.totalTime(p._totalTime, !0) : p._enabled(!0, !1),
                    p = p._timeline;
            return this
        }
        ,
        l.remove = function(e) {
            if (e instanceof t)
                return this._remove(e, !1);
            if (e instanceof Array) {
                for (var n = e.length; --n > -1; )
                    this.remove(e[n]);
                return this
            }
            return "string" == typeof e ? this.removeLabel(e) : this.kill(null, e)
        }
        ,
        l._remove = function(t, n) {
            return e.prototype._remove.call(this, t, n),
            this._last ? this._time > this._last._startTime && (this._time = this.duration(),
            this._totalTime = this._totalDuration) : this._time = this._totalTime = 0,
            this
        }
        ,
        l.append = function(t, e) {
            return this.add(t, this._parseTimeOrLabel(null, e, !0, t))
        }
        ,
        l.insert = l.insertMultiple = function(t, e, n, i) {
            return this.add(t, e || 0, n, i)
        }
        ,
        l.appendMultiple = function(t, e, n, i) {
            return this.add(t, this._parseTimeOrLabel(null, e, !0, t), n, i)
        }
        ,
        l.addLabel = function(t, e) {
            return this._labels[t] = this._parseTimeOrLabel(e),
            this
        }
        ,
        l.addPause = function(t, e, n, i) {
            return this.call(s, ["{self}", e, n, i], this, t)
        }
        ,
        l.removeLabel = function(t) {
            return delete this._labels[t],
            this
        }
        ,
        l.getLabelTime = function(t) {
            return null != this._labels[t] ? this._labels[t] : -1
        }
        ,
        l._parseTimeOrLabel = function(e, n, i, r) {
            var o;
            if (r instanceof t && r.timeline === this)
                this.remove(r);
            else if (r instanceof Array)
                for (o = r.length; --o > -1; )
                    r[o]instanceof t && r[o].timeline === this && this.remove(r[o]);
            if ("string" == typeof n)
                return this._parseTimeOrLabel(n, i && "number" == typeof e && null == this._labels[n] ? e - this.duration() : 0, i);
            if (n = n || 0,
            "string" != typeof e || !isNaN(e) && null == this._labels[e])
                null == e && (e = this.duration());
            else {
                if (o = e.indexOf("="),
                -1 === o)
                    return null == this._labels[e] ? i ? this._labels[e] = this.duration() + n : n : this._labels[e] + n;
                n = parseInt(e.charAt(o - 1) + "1", 10) * Number(e.substr(o + 1)),
                e = o > 1 ? this._parseTimeOrLabel(e.substr(0, o - 1), 0, i) : this.duration()
            }
            return Number(e) + n
        }
        ,
        l.seek = function(t, e) {
            return this.totalTime("number" == typeof t ? t : this._parseTimeOrLabel(t), e !== !1)
        }
        ,
        l.stop = function() {
            return this.paused(!0)
        }
        ,
        l.gotoAndPlay = function(t, e) {
            return this.play(t, e)
        }
        ,
        l.gotoAndStop = function(t, e) {
            return this.pause(t, e)
        }
        ,
        l.render = function(t, e, n) {
            this._gc && this._enabled(!0, !1);
            var i, o, s, a, l, u = this._dirty ? this.totalDuration() : this._totalDuration, c = this._time, h = this._startTime, p = this._timeScale, f = this._paused;
            if (t >= u ? (this._totalTime = this._time = u,
            this._reversed || this._hasPausedChild() || (o = !0,
            a = "onComplete",
            0 === this._duration && (0 === t || 0 > this._rawPrevTime) && this._rawPrevTime !== t && this._first && (l = !0,
            this._rawPrevTime > 0 && (a = "onReverseComplete"))),
            this._rawPrevTime = t,
            t = u + 1e-6) : 1e-7 > t ? (this._totalTime = this._time = 0,
            (0 !== c || 0 === this._duration && this._rawPrevTime > 0) && (a = "onReverseComplete",
            o = this._reversed),
            0 > t ? (this._active = !1,
            0 === this._duration && this._rawPrevTime >= 0 && this._first && (l = !0),
            this._rawPrevTime = t) : (this._rawPrevTime = t,
            t = 0,
            this._initted || (l = !0))) : this._totalTime = this._time = this._rawPrevTime = t,
            this._time !== c && this._first || n || l) {
                if (this._initted || (this._initted = !0),
                this._active || !this._paused && this._time !== c && t > 0 && (this._active = !0),
                0 === c && this.vars.onStart && 0 !== this._time && (e || this.vars.onStart.apply(this.vars.onStartScope || this, this.vars.onStartParams || r)),
                this._time >= c)
                    for (i = this._first; i && (s = i._next,
                    !this._paused || f); )
                        (i._active || i._startTime <= this._time && !i._paused && !i._gc) && (i._reversed ? i.render((i._dirty ? i.totalDuration() : i._totalDuration) - (t - i._startTime) * i._timeScale, e, n) : i.render((t - i._startTime) * i._timeScale, e, n)),
                        i = s;
                else
                    for (i = this._last; i && (s = i._prev,
                    !this._paused || f); )
                        (i._active || c >= i._startTime && !i._paused && !i._gc) && (i._reversed ? i.render((i._dirty ? i.totalDuration() : i._totalDuration) - (t - i._startTime) * i._timeScale, e, n) : i.render((t - i._startTime) * i._timeScale, e, n)),
                        i = s;
                this._onUpdate && (e || this._onUpdate.apply(this.vars.onUpdateScope || this, this.vars.onUpdateParams || r)),
                a && (this._gc || (h === this._startTime || p !== this._timeScale) && (0 === this._time || u >= this.totalDuration()) && (o && (this._timeline.autoRemoveChildren && this._enabled(!1, !1),
                this._active = !1),
                !e && this.vars[a] && this.vars[a].apply(this.vars[a + "Scope"] || this, this.vars[a + "Params"] || r)))
            }
        }
        ,
        l._hasPausedChild = function() {
            for (var t = this._first; t; ) {
                if (t._paused || t instanceof i && t._hasPausedChild())
                    return !0;
                t = t._next
            }
            return !1
        }
        ,
        l.getChildren = function(t, e, i, r) {
            r = r || -9999999999;
            for (var o = [], s = this._first, a = 0; s; )
                r > s._startTime || (s instanceof n ? e !== !1 && (o[a++] = s) : (i !== !1 && (o[a++] = s),
                t !== !1 && (o = o.concat(s.getChildren(!0, e, i)),
                a = o.length))),
                s = s._next;
            return o
        }
        ,
        l.getTweensOf = function(t, e) {
            for (var i = n.getTweensOf(t), r = i.length, o = [], s = 0; --r > -1; )
                (i[r].timeline === this || e && this._contains(i[r])) && (o[s++] = i[r]);
            return o
        }
        ,
        l._contains = function(t) {
            for (var e = t.timeline; e; ) {
                if (e === this)
                    return !0;
                e = e.timeline
            }
            return !1
        }
        ,
        l.shiftChildren = function(t, e, n) {
            n = n || 0;
            for (var i, r = this._first, o = this._labels; r; )
                r._startTime >= n && (r._startTime += t),
                r = r._next;
            if (e)
                for (i in o)
                    o[i] >= n && (o[i] += t);
            return this._uncache(!0)
        }
        ,
        l._kill = function(t, e) {
            if (!t && !e)
                return this._enabled(!1, !1);
            for (var n = e ? this.getTweensOf(e) : this.getChildren(!0, !0, !1), i = n.length, r = !1; --i > -1; )
                n[i]._kill(t, e) && (r = !0);
            return r
        }
        ,
        l.clear = function(t) {
            var e = this.getChildren(!1, !0, !0)
              , n = e.length;
            for (this._time = this._totalTime = 0; --n > -1; )
                e[n]._enabled(!1, !1);
            return t !== !1 && (this._labels = {}),
            this._uncache(!0)
        }
        ,
        l.invalidate = function() {
            for (var t = this._first; t; )
                t.invalidate(),
                t = t._next;
            return this
        }
        ,
        l._enabled = function(t, n) {
            if (t === this._gc)
                for (var i = this._first; i; )
                    i._enabled(t, !0),
                    i = i._next;
            return e.prototype._enabled.call(this, t, n)
        }
        ,
        l.progress = function(t) {
            return arguments.length ? this.totalTime(this.duration() * t, !1) : this._time / this.duration()
        }
        ,
        l.duration = function(t) {
            return arguments.length ? (0 !== this.duration() && 0 !== t && this.timeScale(this._duration / t),
            this) : (this._dirty && this.totalDuration(),
            this._duration)
        }
        ,
        l.totalDuration = function(t) {
            if (!arguments.length) {
                if (this._dirty) {
                    for (var e, n, i = 0, r = this._last, o = 999999999999; r; )
                        e = r._prev,
                        r._dirty && r.totalDuration(),
                        r._startTime > o && this._sortChildren && !r._paused ? this.add(r, r._startTime - r._delay) : o = r._startTime,
                        0 > r._startTime && !r._paused && (i -= r._startTime,
                        this._timeline.smoothChildTiming && (this._startTime += r._startTime / this._timeScale),
                        this.shiftChildren(-r._startTime, !1, -9999999999),
                        o = 0),
                        n = r._startTime + r._totalDuration / r._timeScale,
                        n > i && (i = n),
                        r = e;
                    this._duration = this._totalDuration = i,
                    this._dirty = !1
                }
                return this._totalDuration
            }
            return 0 !== this.totalDuration() && 0 !== t && this.timeScale(this._totalDuration / t),
            this
        }
        ,
        l.usesFrames = function() {
            for (var e = this._timeline; e._timeline; )
                e = e._timeline;
            return e === t._rootFramesTimeline
        }
        ,
        l.rawTime = function() {
            return this._paused || 0 !== this._totalTime && this._totalTime !== this._totalDuration ? this._totalTime : (this._timeline.rawTime() - this._startTime) * this._timeScale
        }
        ,
        i
    }, !0),
    window._gsDefine("TimelineMax", ["TimelineLite", "TweenLite", "easing.Ease"], function(t, e, n) {
        var i = function(e) {
            t.call(this, e),
            this._repeat = this.vars.repeat || 0,
            this._repeatDelay = this.vars.repeatDelay || 0,
            this._cycle = 0,
            this._yoyo = this.vars.yoyo === !0,
            this._dirty = !0
        }
          , r = []
          , o = new n(null,null,1,0)
          , s = function(t) {
            for (; t; ) {
                if (t._paused)
                    return !0;
                t = t._timeline
            }
            return !1
        }
          , a = i.prototype = new t;
        return a.constructor = i,
        a.kill()._gc = !1,
        i.version = "1.10.2",
        a.invalidate = function() {
            return this._yoyo = this.vars.yoyo === !0,
            this._repeat = this.vars.repeat || 0,
            this._repeatDelay = this.vars.repeatDelay || 0,
            this._uncache(!0),
            t.prototype.invalidate.call(this)
        }
        ,
        a.addCallback = function(t, n, i, r) {
            return this.add(e.delayedCall(0, t, i, r), n)
        }
        ,
        a.removeCallback = function(t, e) {
            if (t)
                if (null == e)
                    this._kill(null, t);
                else
                    for (var n = this.getTweensOf(t, !1), i = n.length, r = this._parseTimeOrLabel(e); --i > -1; )
                        n[i]._startTime === r && n[i]._enabled(!1, !1);
            return this
        }
        ,
        a.tweenTo = function(t, n) {
            n = n || {};
            var i, s, a = {
                ease: o,
                overwrite: 2,
                useFrames: this.usesFrames(),
                immediateRender: !1
            };
            for (i in n)
                a[i] = n[i];
            return a.time = this._parseTimeOrLabel(t),
            s = new e(this,Math.abs(Number(a.time) - this._time) / this._timeScale || .001,a),
            a.onStart = function() {
                s.target.paused(!0),
                s.vars.time !== s.target.time() && s.duration(Math.abs(s.vars.time - s.target.time()) / s.target._timeScale),
                n.onStart && n.onStart.apply(n.onStartScope || s, n.onStartParams || r)
            }
            ,
            s
        }
        ,
        a.tweenFromTo = function(t, e, n) {
            n = n || {},
            t = this._parseTimeOrLabel(t),
            n.startAt = {
                onComplete: this.seek,
                onCompleteParams: [t],
                onCompleteScope: this
            },
            n.immediateRender = n.immediateRender !== !1;
            var i = this.tweenTo(e, n);
            return i.duration(Math.abs(i.vars.time - t) / this._timeScale || .001)
        }
        ,
        a.render = function(t, e, n) {
            this._gc && this._enabled(!0, !1);
            var i, o, s, a, l, u, c = this._dirty ? this.totalDuration() : this._totalDuration, h = this._duration, p = this._time, f = this._totalTime, d = this._startTime, m = this._timeScale, g = this._rawPrevTime, v = this._paused, y = this._cycle;
            if (t >= c ? (this._locked || (this._totalTime = c,
            this._cycle = this._repeat),
            this._reversed || this._hasPausedChild() || (o = !0,
            a = "onComplete",
            0 === h && (0 === t || 0 > this._rawPrevTime) && this._rawPrevTime !== t && this._first && (l = !0,
            this._rawPrevTime > 0 && (a = "onReverseComplete"))),
            this._rawPrevTime = t,
            this._yoyo && 0 !== (1 & this._cycle) ? this._time = t = 0 : (this._time = h,
            t = h + 1e-6)) : 1e-7 > t ? (this._locked || (this._totalTime = this._cycle = 0),
            this._time = 0,
            (0 !== p || 0 === h && this._rawPrevTime > 0 && !this._locked) && (a = "onReverseComplete",
            o = this._reversed),
            0 > t ? (this._active = !1,
            0 === h && this._rawPrevTime >= 0 && this._first && (l = !0),
            this._rawPrevTime = t) : (this._rawPrevTime = t,
            t = 0,
            this._initted || (l = !0))) : (this._time = this._rawPrevTime = t,
            this._locked || (this._totalTime = t,
            0 !== this._repeat && (u = h + this._repeatDelay,
            this._cycle = this._totalTime / u >> 0,
            0 !== this._cycle && this._cycle === this._totalTime / u && this._cycle--,
            this._time = this._totalTime - this._cycle * u,
            this._yoyo && 0 !== (1 & this._cycle) && (this._time = h - this._time),
            this._time > h ? (this._time = h,
            t = h + 1e-6) : 0 > this._time ? this._time = t = 0 : t = this._time))),
            this._cycle !== y && !this._locked) {
                var _ = this._yoyo && 0 !== (1 & y)
                  , x = _ === (this._yoyo && 0 !== (1 & this._cycle))
                  , w = this._totalTime
                  , b = this._cycle
                  , T = this._rawPrevTime
                  , C = this._time;
                if (this._totalTime = y * h,
                y > this._cycle ? _ = !_ : this._totalTime += h,
                this._time = p,
                this._rawPrevTime = 0 === h ? g - 1e-5 : g,
                this._cycle = y,
                this._locked = !0,
                p = _ ? 0 : h,
                this.render(p, e, 0 === h),
                e || this._gc || this.vars.onRepeat && this.vars.onRepeat.apply(this.vars.onRepeatScope || this, this.vars.onRepeatParams || r),
                x && (p = _ ? h + 1e-6 : -1e-6,
                this.render(p, !0, !1)),
                this._locked = !1,
                this._paused && !v)
                    return;
                this._time = C,
                this._totalTime = w,
                this._cycle = b,
                this._rawPrevTime = T
            }
            if (!(this._time !== p && this._first || n || l))
                return void (f !== this._totalTime && this._onUpdate && (e || this._onUpdate.apply(this.vars.onUpdateScope || this, this.vars.onUpdateParams || r)));
            if (this._initted || (this._initted = !0),
            this._active || !this._paused && this._totalTime !== f && t > 0 && (this._active = !0),
            0 === f && this.vars.onStart && 0 !== this._totalTime && (e || this.vars.onStart.apply(this.vars.onStartScope || this, this.vars.onStartParams || r)),
            this._time >= p)
                for (i = this._first; i && (s = i._next,
                !this._paused || v); )
                    (i._active || i._startTime <= this._time && !i._paused && !i._gc) && (i._reversed ? i.render((i._dirty ? i.totalDuration() : i._totalDuration) - (t - i._startTime) * i._timeScale, e, n) : i.render((t - i._startTime) * i._timeScale, e, n)),
                    i = s;
            else
                for (i = this._last; i && (s = i._prev,
                !this._paused || v); )
                    (i._active || p >= i._startTime && !i._paused && !i._gc) && (i._reversed ? i.render((i._dirty ? i.totalDuration() : i._totalDuration) - (t - i._startTime) * i._timeScale, e, n) : i.render((t - i._startTime) * i._timeScale, e, n)),
                    i = s;
            this._onUpdate && (e || this._onUpdate.apply(this.vars.onUpdateScope || this, this.vars.onUpdateParams || r)),
            a && (this._locked || this._gc || (d === this._startTime || m !== this._timeScale) && (0 === this._time || c >= this.totalDuration()) && (o && (this._timeline.autoRemoveChildren && this._enabled(!1, !1),
            this._active = !1),
            !e && this.vars[a] && this.vars[a].apply(this.vars[a + "Scope"] || this, this.vars[a + "Params"] || r)))
        }
        ,
        a.getActive = function(t, e, n) {
            null == t && (t = !0),
            null == e && (e = !0),
            null == n && (n = !1);
            var i, r, o = [], a = this.getChildren(t, e, n), l = 0, u = a.length;
            for (i = 0; u > i; i++)
                r = a[i],
                r._paused || r._timeline._time >= r._startTime && r._timeline._time < r._startTime + r._totalDuration / r._timeScale && (s(r._timeline) || (o[l++] = r));
            return o
        }
        ,
        a.getLabelAfter = function(t) {
            t || 0 !== t && (t = this._time);
            var e, n = this.getLabelsArray(), i = n.length;
            for (e = 0; i > e; e++)
                if (n[e].time > t)
                    return n[e].name;
            return null
        }
        ,
        a.getLabelBefore = function(t) {
            null == t && (t = this._time);
            for (var e = this.getLabelsArray(), n = e.length; --n > -1; )
                if (t > e[n].time)
                    return e[n].name;
            return null
        }
        ,
        a.getLabelsArray = function() {
            var t, e = [], n = 0;
            for (t in this._labels)
                e[n++] = {
                    time: this._labels[t],
                    name: t
                };
            return e.sort(function(t, e) {
                return t.time - e.time
            }),
            e
        }
        ,
        a.progress = function(t) {
            return arguments.length ? this.totalTime(this.duration() * (this._yoyo && 0 !== (1 & this._cycle) ? 1 - t : t) + this._cycle * (this._duration + this._repeatDelay), !1) : this._time / this.duration()
        }
        ,
        a.totalProgress = function(t) {
            return arguments.length ? this.totalTime(this.totalDuration() * t, !1) : this._totalTime / this.totalDuration()
        }
        ,
        a.totalDuration = function(e) {
            return arguments.length ? -1 === this._repeat ? this : this.duration((e - this._repeat * this._repeatDelay) / (this._repeat + 1)) : (this._dirty && (t.prototype.totalDuration.call(this),
            this._totalDuration = -1 === this._repeat ? 999999999999 : this._duration * (this._repeat + 1) + this._repeatDelay * this._repeat),
            this._totalDuration)
        }
        ,
        a.time = function(t, e) {
            return arguments.length ? (this._dirty && this.totalDuration(),
            t > this._duration && (t = this._duration),
            this._yoyo && 0 !== (1 & this._cycle) ? t = this._duration - t + this._cycle * (this._duration + this._repeatDelay) : 0 !== this._repeat && (t += this._cycle * (this._duration + this._repeatDelay)),
            this.totalTime(t, e)) : this._time
        }
        ,
        a.repeat = function(t) {
            return arguments.length ? (this._repeat = t,
            this._uncache(!0)) : this._repeat
        }
        ,
        a.repeatDelay = function(t) {
            return arguments.length ? (this._repeatDelay = t,
            this._uncache(!0)) : this._repeatDelay
        }
        ,
        a.yoyo = function(t) {
            return arguments.length ? (this._yoyo = t,
            this) : this._yoyo
        }
        ,
        a.currentLabel = function(t) {
            return arguments.length ? this.seek(t, !0) : this.getLabelBefore(this._time + 1e-8)
        }
        ,
        i
    }, !0),
    function() {
        var t = 180 / Math.PI
          , e = Math.PI / 180
          , n = []
          , i = []
          , r = []
          , o = {}
          , s = function(t, e, n, i) {
            this.a = t,
            this.b = e,
            this.c = n,
            this.d = i,
            this.da = i - t,
            this.ca = n - t,
            this.ba = e - t
        }
          , a = ",x,y,z,left,top,right,bottom,marginTop,marginLeft,marginRight,marginBottom,paddingLeft,paddingTop,paddingRight,paddingBottom,backgroundPosition,backgroundPosition_y,"
          , l = function(t, e, n, i) {
            var r = {
                a: t
            }
              , o = {}
              , s = {}
              , a = {
                c: i
            }
              , l = (t + e) / 2
              , u = (e + n) / 2
              , c = (n + i) / 2
              , h = (l + u) / 2
              , p = (u + c) / 2
              , f = (p - h) / 8;
            return r.b = l + (t - l) / 4,
            o.b = h + f,
            r.c = o.a = (r.b + o.b) / 2,
            o.c = s.a = (h + p) / 2,
            s.b = p - f,
            a.b = c + (i - c) / 4,
            s.c = a.a = (s.b + a.b) / 2,
            [r, o, s, a]
        }
          , u = function(t, e, o, s, a) {
            var u, c, h, p, f, d, m, g, v, y, _, x, w, b = t.length - 1, T = 0, C = t[0].a;
            for (u = 0; b > u; u++)
                f = t[T],
                c = f.a,
                h = f.d,
                p = t[T + 1].d,
                a ? (_ = n[u],
                x = i[u],
                w = .25 * (x + _) * e / (s ? .5 : r[u] || .5),
                d = h - (h - c) * (s ? .5 * e : 0 !== _ ? w / _ : 0),
                m = h + (p - h) * (s ? .5 * e : 0 !== x ? w / x : 0),
                g = h - (d + ((m - d) * (3 * _ / (_ + x) + .5) / 4 || 0))) : (d = h - .5 * (h - c) * e,
                m = h + .5 * (p - h) * e,
                g = h - (d + m) / 2),
                d += g,
                m += g,
                f.c = v = d,
                f.b = 0 !== u ? C : C = f.a + .6 * (f.c - f.a),
                f.da = h - c,
                f.ca = v - c,
                f.ba = C - c,
                o ? (y = l(c, C, v, h),
                t.splice(T, 1, y[0], y[1], y[2], y[3]),
                T += 4) : T++,
                C = m;
            f = t[T],
            f.b = C,
            f.c = C + .4 * (f.d - C),
            f.da = f.d - f.a,
            f.ca = f.c - f.a,
            f.ba = C - f.a,
            o && (y = l(f.a, C, f.c, f.d),
            t.splice(T, 1, y[0], y[1], y[2], y[3]))
        }
          , c = function(t, e, r, o) {
            var a, l, u, c, h, p, f = [];
            if (o)
                for (t = [o].concat(t),
                l = t.length; --l > -1; )
                    "string" == typeof (p = t[l][e]) && "=" === p.charAt(1) && (t[l][e] = o[e] + Number(p.charAt(0) + p.substr(2)));
            if (a = t.length - 2,
            0 > a)
                return f[0] = new s(t[0][e],0,0,t[-1 > a ? 0 : 1][e]),
                f;
            for (l = 0; a > l; l++)
                u = t[l][e],
                c = t[l + 1][e],
                f[l] = new s(u,0,0,c),
                r && (h = t[l + 2][e],
                n[l] = (n[l] || 0) + (c - u) * (c - u),
                i[l] = (i[l] || 0) + (h - c) * (h - c));
            return f[l] = new s(t[l][e],0,0,t[l + 1][e]),
            f
        }
          , h = function(t, e, s, l, h, p) {
            var f, d, m, g, v, y, _, x, w = {}, b = [], T = p || t[0];
            h = "string" == typeof h ? "," + h + "," : a,
            null == e && (e = 1);
            for (d in t[0])
                b.push(d);
            if (t.length > 1) {
                for (x = t[t.length - 1],
                _ = !0,
                f = b.length; --f > -1; )
                    if (d = b[f],
                    Math.abs(T[d] - x[d]) > .05) {
                        _ = !1;
                        break
                    }
                _ && (t = t.concat(),
                p && t.unshift(p),
                t.push(t[1]),
                p = t[t.length - 3])
            }
            for (n.length = i.length = r.length = 0,
            f = b.length; --f > -1; )
                d = b[f],
                o[d] = -1 !== h.indexOf("," + d + ","),
                w[d] = c(t, d, o[d], p);
            for (f = n.length; --f > -1; )
                n[f] = Math.sqrt(n[f]),
                i[f] = Math.sqrt(i[f]);
            if (!l) {
                for (f = b.length; --f > -1; )
                    if (o[d])
                        for (m = w[b[f]],
                        y = m.length - 1,
                        g = 0; y > g; g++)
                            v = m[g + 1].da / i[g] + m[g].da / n[g],
                            r[g] = (r[g] || 0) + v * v;
                for (f = r.length; --f > -1; )
                    r[f] = Math.sqrt(r[f])
            }
            for (f = b.length,
            g = s ? 4 : 1; --f > -1; )
                d = b[f],
                m = w[d],
                u(m, e, s, l, o[d]),
                _ && (m.splice(0, g),
                m.splice(m.length - g, g));
            return w
        }
          , p = function(t, e, n) {
            e = e || "soft";
            var i, r, o, a, l, u, c, h, p, f, d, m = {}, g = "cubic" === e ? 3 : 2, v = "soft" === e, y = [];
            if (v && n && (t = [n].concat(t)),
            null == t || g + 1 > t.length)
                throw "invalid Bezier data";
            for (p in t[0])
                y.push(p);
            for (u = y.length; --u > -1; ) {
                for (p = y[u],
                m[p] = l = [],
                f = 0,
                h = t.length,
                c = 0; h > c; c++)
                    i = null == n ? t[c][p] : "string" == typeof (d = t[c][p]) && "=" === d.charAt(1) ? n[p] + Number(d.charAt(0) + d.substr(2)) : Number(d),
                    v && c > 1 && h - 1 > c && (l[f++] = (i + l[f - 2]) / 2),
                    l[f++] = i;
                for (h = f - g + 1,
                f = 0,
                c = 0; h > c; c += g)
                    i = l[c],
                    r = l[c + 1],
                    o = l[c + 2],
                    a = 2 === g ? 0 : l[c + 3],
                    l[f++] = d = 3 === g ? new s(i,r,o,a) : new s(i,(2 * r + i) / 3,(2 * r + o) / 3,o);
                l.length = f
            }
            return m
        }
          , f = function(t, e, n) {
            for (var i, r, o, s, a, l, u, c, h, p, f, d = 1 / n, m = t.length; --m > -1; )
                for (p = t[m],
                o = p.a,
                s = p.d - o,
                a = p.c - o,
                l = p.b - o,
                i = r = 0,
                c = 1; n >= c; c++)
                    u = d * c,
                    h = 1 - u,
                    i = r - (r = (u * u * s + 3 * h * (u * a + h * l)) * u),
                    f = m * n + c - 1,
                    e[f] = (e[f] || 0) + i * i
        }
          , d = function(t, e) {
            e = e >> 0 || 6;
            var n, i, r, o, s = [], a = [], l = 0, u = 0, c = e - 1, h = [], p = [];
            for (n in t)
                f(t[n], s, e);
            for (r = s.length,
            i = 0; r > i; i++)
                l += Math.sqrt(s[i]),
                o = i % e,
                p[o] = l,
                o === c && (u += l,
                o = i / e >> 0,
                h[o] = p,
                a[o] = u,
                l = 0,
                p = []);
            return {
                length: u,
                lengths: a,
                segments: h
            }
        }
          , m = window._gsDefine.plugin({
            propName: "bezier",
            priority: -1,
            API: 2,
            global: !0,
            init: function(t, e, n) {
                this._target = t,
                e instanceof Array && (e = {
                    values: e
                }),
                this._func = {},
                this._round = {},
                this._props = [],
                this._timeRes = null == e.timeResolution ? 6 : parseInt(e.timeResolution, 10);
                var i, r, o, s, a, l = e.values || [], u = {}, c = l[0], f = e.autoRotate || n.vars.orientToBezier;
                this._autoRotate = f ? f instanceof Array ? f : [["x", "y", "rotation", f === !0 ? 0 : Number(f) || 0]] : null;
                for (i in c)
                    this._props.push(i);
                for (o = this._props.length; --o > -1; )
                    i = this._props[o],
                    this._overwriteProps.push(i),
                    r = this._func[i] = "function" == typeof t[i],
                    u[i] = r ? t[i.indexOf("set") || "function" != typeof t["get" + i.substr(3)] ? i : "get" + i.substr(3)]() : parseFloat(t[i]),
                    a || u[i] !== l[0][i] && (a = u);
                if (this._beziers = "cubic" !== e.type && "quadratic" !== e.type && "soft" !== e.type ? h(l, isNaN(e.curviness) ? 1 : e.curviness, !1, "thruBasic" === e.type, e.correlate, a) : p(l, e.type, u),
                this._segCount = this._beziers[i].length,
                this._timeRes) {
                    var m = d(this._beziers, this._timeRes);
                    this._length = m.length,
                    this._lengths = m.lengths,
                    this._segments = m.segments,
                    this._l1 = this._li = this._s1 = this._si = 0,
                    this._l2 = this._lengths[0],
                    this._curSeg = this._segments[0],
                    this._s2 = this._curSeg[0],
                    this._prec = 1 / this._curSeg.length
                }
                if (f = this._autoRotate)
                    for (f[0]instanceof Array || (this._autoRotate = f = [f]),
                    o = f.length; --o > -1; )
                        for (s = 0; 3 > s; s++)
                            i = f[o][s],
                            this._func[i] = "function" == typeof t[i] ? t[i.indexOf("set") || "function" != typeof t["get" + i.substr(3)] ? i : "get" + i.substr(3)] : !1;
                return !0
            },
            set: function(e) {
                var n, i, r, o, s, a, l, u, c, h, p = this._segCount, f = this._func, d = this._target;
                if (this._timeRes) {
                    if (c = this._lengths,
                    h = this._curSeg,
                    e *= this._length,
                    r = this._li,
                    e > this._l2 && p - 1 > r) {
                        for (u = p - 1; u > r && e >= (this._l2 = c[++r]); )
                            ;
                        this._l1 = c[r - 1],
                        this._li = r,
                        this._curSeg = h = this._segments[r],
                        this._s2 = h[this._s1 = this._si = 0]
                    } else if (this._l1 > e && r > 0) {
                        for (; r > 0 && (this._l1 = c[--r]) >= e; )
                            ;
                        0 === r && this._l1 > e ? this._l1 = 0 : r++,
                        this._l2 = c[r],
                        this._li = r,
                        this._curSeg = h = this._segments[r],
                        this._s1 = h[(this._si = h.length - 1) - 1] || 0,
                        this._s2 = h[this._si]
                    }
                    if (n = r,
                    e -= this._l1,
                    r = this._si,
                    e > this._s2 && h.length - 1 > r) {
                        for (u = h.length - 1; u > r && e >= (this._s2 = h[++r]); )
                            ;
                        this._s1 = h[r - 1],
                        this._si = r
                    } else if (this._s1 > e && r > 0) {
                        for (; r > 0 && (this._s1 = h[--r]) >= e; )
                            ;
                        0 === r && this._s1 > e ? this._s1 = 0 : r++,
                        this._s2 = h[r],
                        this._si = r
                    }
                    a = (r + (e - this._s1) / (this._s2 - this._s1)) * this._prec
                } else
                    n = 0 > e ? 0 : e >= 1 ? p - 1 : p * e >> 0,
                    a = (e - n * (1 / p)) * p;
                for (i = 1 - a,
                r = this._props.length; --r > -1; )
                    o = this._props[r],
                    s = this._beziers[o][n],
                    l = (a * a * s.da + 3 * i * (a * s.ca + i * s.ba)) * a + s.a,
                    this._round[o] && (l = l + (l > 0 ? .5 : -.5) >> 0),
                    f[o] ? d[o](l) : d[o] = l;
                if (this._autoRotate) {
                    var m, g, v, y, _, x, w, b = this._autoRotate;
                    for (r = b.length; --r > -1; )
                        o = b[r][2],
                        x = b[r][3] || 0,
                        w = b[r][4] === !0 ? 1 : t,
                        s = this._beziers[b[r][0]],
                        m = this._beziers[b[r][1]],
                        s && m && (s = s[n],
                        m = m[n],
                        g = s.a + (s.b - s.a) * a,
                        y = s.b + (s.c - s.b) * a,
                        g += (y - g) * a,
                        y += (s.c + (s.d - s.c) * a - y) * a,
                        v = m.a + (m.b - m.a) * a,
                        _ = m.b + (m.c - m.b) * a,
                        v += (_ - v) * a,
                        _ += (m.c + (m.d - m.c) * a - _) * a,
                        l = Math.atan2(_ - v, y - g) * w + x,
                        f[o] ? d[o](l) : d[o] = l)
                }
            }
        })
          , g = m.prototype;
        m.bezierThrough = h,
        m.cubicToQuadratic = l,
        m._autoCSS = !0,
        m.quadraticToCubic = function(t, e, n) {
            return new s(t,(2 * e + t) / 3,(2 * e + n) / 3,n)
        }
        ,
        m._cssRegister = function() {
            var t = window._gsDefine.globals.CSSPlugin;
            if (t) {
                var n = t._internals
                  , i = n._parseToProxy
                  , r = n._setPluginRatio
                  , o = n.CSSPropTween;
                n._registerComplexSpecialProp("bezier", {
                    parser: function(t, n, s, a, l, u) {
                        n instanceof Array && (n = {
                            values: n
                        }),
                        u = new m;
                        var c, h, p, f = n.values, d = f.length - 1, g = [], v = {};
                        if (0 > d)
                            return l;
                        for (c = 0; d >= c; c++)
                            p = i(t, f[c], a, l, u, d !== c),
                            g[c] = p.end;
                        for (h in n)
                            v[h] = n[h];
                        return v.values = g,
                        l = new o(t,"bezier",0,0,p.pt,2),
                        l.data = p,
                        l.plugin = u,
                        l.setRatio = r,
                        0 === v.autoRotate && (v.autoRotate = !0),
                        !v.autoRotate || v.autoRotate instanceof Array || (c = v.autoRotate === !0 ? 0 : Number(v.autoRotate) * e,
                        v.autoRotate = null != p.end.left ? [["left", "top", "rotation", c, !0]] : null != p.end.x ? [["x", "y", "rotation", c, !0]] : !1),
                        v.autoRotate && (a._transform || a._enableTransforms(!1),
                        p.autoRotate = a._target._gsTransform),
                        u._onInitTween(p.proxy, v, a._tween),
                        l
                    }
                })
            }
        }
        ,
        g._roundProps = function(t, e) {
            for (var n = this._overwriteProps, i = n.length; --i > -1; )
                (t[n[i]] || t.bezier || t.bezierThrough) && (this._round[n[i]] = e)
        }
        ,
        g._kill = function(t) {
            var e, n, i = this._props;
            for (e in this._beziers)
                if (e in t)
                    for (delete this._beziers[e],
                    delete this._func[e],
                    n = i.length; --n > -1; )
                        i[n] === e && i.splice(n, 1);
            return this._super._kill.call(this, t)
        }
    }(),
    window._gsDefine("plugins.CSSPlugin", ["plugins.TweenPlugin", "TweenLite"], function(t, e) {
        var n, i, r, o, s = function() {
            t.call(this, "css"),
            this._overwriteProps.length = 0,
            this.setRatio = s.prototype.setRatio
        }, a = {}, l = s.prototype = new t("css");
        l.constructor = s,
        s.version = "1.10.2",
        s.API = 2,
        s.defaultTransformPerspective = 0,
        l = "px",
        s.suffixMap = {
            top: l,
            right: l,
            bottom: l,
            left: l,
            width: l,
            height: l,
            fontSize: l,
            padding: l,
            margin: l,
            perspective: l
        };
        var u, c, h, p, f, d, m = /(?:\d|\-\d|\.\d|\-\.\d)+/g, g = /(?:\d|\-\d|\.\d|\-\.\d|\+=\d|\-=\d|\+=.\d|\-=\.\d)+/g, v = /(?:\+=|\-=|\-|\b)[\d\-\.]+[a-zA-Z0-9]*(?:%|\b)/gi, y = /[^\d\-\.]/g, _ = /(?:\d|\-|\+|=|#|\.)*/g, x = /opacity *= *([^)]*)/, w = /opacity:([^;]*)/, b = /alpha\(opacity *=.+?\)/i, T = /^(rgb|hsl)/, C = /([A-Z])/g, S = /-([a-z])/gi, k = /(^(?:url\(\"|url\())|(?:(\"\))$|\)$)/gi, j = function(t, e) {
            return e.toUpperCase()
        }, P = /(?:Left|Right|Width)/i, E = /(M11|M12|M21|M22)=[\d\-\.e]+/gi, O = /progid\:DXImageTransform\.Microsoft\.Matrix\(.+?\)/i, M = /,(?=[^\)]*(?:\(|$))/gi, D = Math.PI / 180, A = 180 / Math.PI, N = {}, L = document, R = L.createElement("div"), $ = L.createElement("img"), I = s._internals = {
            _specialProps: a
        }, q = navigator.userAgent, H = function() {
            var t, e = q.indexOf("Android"), n = L.createElement("div");
            return h = -1 !== q.indexOf("Safari") && -1 === q.indexOf("Chrome") && (-1 === e || Number(q.substr(e + 8, 1)) > 3),
            f = h && 6 > Number(q.substr(q.indexOf("Version/") + 8, 1)),
            p = -1 !== q.indexOf("Firefox"),
            /MSIE ([0-9]{1,}[\.0-9]{0,})/.exec(q),
            d = parseFloat(RegExp.$1),
            n.innerHTML = "<a style='top:1px;opacity:.55;'>a</a>",
            t = n.getElementsByTagName("a")[0],
            t ? /^0.55/.test(t.style.opacity) : !1
        }(), F = function(t) {
            return x.test("string" == typeof t ? t : (t.currentStyle ? t.currentStyle.filter : t.style.filter) || "") ? parseFloat(RegExp.$1) / 100 : 1
        }, B = function(t) {
            window.console && console.log(t)
        }, z = "", W = "", X = function(t, e) {
            e = e || R;
            var n, i, r = e.style;
            if (void 0 !== r[t])
                return t;
            for (t = t.charAt(0).toUpperCase() + t.substr(1),
            n = ["O", "Moz", "ms", "Ms", "Webkit"],
            i = 5; --i > -1 && void 0 === r[n[i] + t]; )
                ;
            return i >= 0 ? (W = 3 === i ? "ms" : n[i],
            z = "-" + W.toLowerCase() + "-",
            W + t) : null
        }, V = L.defaultView ? L.defaultView.getComputedStyle : function() {}
        , Y = s.getStyle = function(t, e, n, i, r) {
            var o;
            return H || "opacity" !== e ? (!i && t.style[e] ? o = t.style[e] : (n = n || V(t, null)) ? (t = n.getPropertyValue(e.replace(C, "-$1").toLowerCase()),
            o = t || n.length ? t : n[e]) : t.currentStyle && (o = t.currentStyle[e]),
            null == r || o && "none" !== o && "auto" !== o && "auto auto" !== o ? o : r) : F(t)
        }
        , U = function(t, e, n, i, r) {
            if ("px" === i || !i)
                return n;
            if ("auto" === i || !n)
                return 0;
            var o, s = P.test(e), a = t, l = R.style, u = 0 > n;
            return u && (n = -n),
            "%" === i && -1 !== e.indexOf("border") ? o = n / 100 * (s ? t.clientWidth : t.clientHeight) : (l.cssText = "border-style:solid; border-width:0; position:absolute; line-height:0;",
            "%" !== i && a.appendChild ? l[s ? "borderLeftWidth" : "borderTopWidth"] = n + i : (a = t.parentNode || L.body,
            l[s ? "width" : "height"] = n + i),
            a.appendChild(R),
            o = parseFloat(R[s ? "offsetWidth" : "offsetHeight"]),
            a.removeChild(R),
            0 !== o || r || (o = U(t, e, n, i, !0))),
            u ? -o : o
        }, G = function(t, e, n) {
            if ("absolute" !== Y(t, "position", n))
                return 0;
            var i = "left" === e ? "Left" : "Top"
              , r = Y(t, "margin" + i, n);
            return t["offset" + i] - (U(t, e, parseFloat(r), r.replace(_, "")) || 0)
        }, Q = function(t, e) {
            var n, i, r = {};
            if (e = e || V(t, null))
                if (n = e.length)
                    for (; --n > -1; )
                        r[e[n].replace(S, j)] = e.getPropertyValue(e[n]);
                else
                    for (n in e)
                        r[n] = e[n];
            else if (e = t.currentStyle || t.style)
                for (n in e)
                    r[n.replace(S, j)] = e[n];
            return H || (r.opacity = F(t)),
            i = Tt(t, e, !1),
            r.rotation = i.rotation * A,
            r.skewX = i.skewX * A,
            r.scaleX = i.scaleX,
            r.scaleY = i.scaleY,
            r.x = i.x,
            r.y = i.y,
            bt && (r.z = i.z,
            r.rotationX = i.rotationX * A,
            r.rotationY = i.rotationY * A,
            r.scaleZ = i.scaleZ),
            r.filters && delete r.filters,
            r
        }, J = function(t, e, n, i, r) {
            var o, s, a, l = {}, u = t.style;
            for (s in n)
                "cssText" !== s && "length" !== s && isNaN(s) && (e[s] !== (o = n[s]) || r && r[s]) && -1 === s.indexOf("Origin") && ("number" == typeof o || "string" == typeof o) && (l[s] = "auto" !== o || "left" !== s && "top" !== s ? "" !== o && "auto" !== o && "none" !== o || "string" != typeof e[s] || "" === e[s].replace(y, "") ? o : 0 : G(t, s),
                void 0 !== u[s] && (a = new ht(u,s,u[s],a)));
            if (i)
                for (s in i)
                    "className" !== s && (l[s] = i[s]);
            return {
                difs: l,
                firstMPT: a
            }
        }, Z = {
            width: ["Left", "Right"],
            height: ["Top", "Bottom"]
        }, K = ["marginLeft", "marginRight", "marginTop", "marginBottom"], tt = function(t, e, n) {
            var i = parseFloat("width" === e ? t.offsetWidth : t.offsetHeight)
              , r = Z[e]
              , o = r.length;
            for (n = n || V(t, null); --o > -1; )
                i -= parseFloat(Y(t, "padding" + r[o], n, !0)) || 0,
                i -= parseFloat(Y(t, "border" + r[o] + "Width", n, !0)) || 0;
            return i
        }, et = function(t, e) {
            (null == t || "" === t || "auto" === t || "auto auto" === t) && (t = "0 0");
            var n = t.split(" ")
              , i = -1 !== t.indexOf("left") ? "0%" : -1 !== t.indexOf("right") ? "100%" : n[0]
              , r = -1 !== t.indexOf("top") ? "0%" : -1 !== t.indexOf("bottom") ? "100%" : n[1];
            return null == r ? r = "0" : "center" === r && (r = "50%"),
            ("center" === i || isNaN(parseFloat(i)) && -1 === (i + "").indexOf("=")) && (i = "50%"),
            e && (e.oxp = -1 !== i.indexOf("%"),
            e.oyp = -1 !== r.indexOf("%"),
            e.oxr = "=" === i.charAt(1),
            e.oyr = "=" === r.charAt(1),
            e.ox = parseFloat(i.replace(y, "")),
            e.oy = parseFloat(r.replace(y, ""))),
            i + " " + r + (n.length > 2 ? " " + n[2] : "")
        }, nt = function(t, e) {
            return "string" == typeof t && "=" === t.charAt(1) ? parseInt(t.charAt(0) + "1", 10) * parseFloat(t.substr(2)) : parseFloat(t) - parseFloat(e)
        }, it = function(t, e) {
            return null == t ? e : "string" == typeof t && "=" === t.charAt(1) ? parseInt(t.charAt(0) + "1", 10) * Number(t.substr(2)) + e : parseFloat(t)
        }, rt = function(t, e, n, i) {
            var r, o, s, a, l = 1e-6;
            return null == t ? a = e : "number" == typeof t ? a = t * D : (r = 2 * Math.PI,
            o = t.split("_"),
            s = Number(o[0].replace(y, "")) * (-1 === t.indexOf("rad") ? D : 1) - ("=" === t.charAt(1) ? 0 : e),
            o.length && (i && (i[n] = e + s),
            -1 !== t.indexOf("short") && (s %= r,
            s !== s % (r / 2) && (s = 0 > s ? s + r : s - r)),
            -1 !== t.indexOf("_cw") && 0 > s ? s = (s + 9999999999 * r) % r - (0 | s / r) * r : -1 !== t.indexOf("ccw") && s > 0 && (s = (s - 9999999999 * r) % r - (0 | s / r) * r)),
            a = e + s),
            l > a && a > -l && (a = 0),
            a
        }, ot = {
            aqua: [0, 255, 255],
            lime: [0, 255, 0],
            silver: [192, 192, 192],
            black: [0, 0, 0],
            maroon: [128, 0, 0],
            teal: [0, 128, 128],
            blue: [0, 0, 255],
            navy: [0, 0, 128],
            white: [255, 255, 255],
            fuchsia: [255, 0, 255],
            olive: [128, 128, 0],
            yellow: [255, 255, 0],
            orange: [255, 165, 0],
            gray: [128, 128, 128],
            purple: [128, 0, 128],
            green: [0, 128, 0],
            red: [255, 0, 0],
            pink: [255, 192, 203],
            cyan: [0, 255, 255],
            transparent: [255, 255, 255, 0]
        }, st = function(t, e, n) {
            return t = 0 > t ? t + 1 : t > 1 ? t - 1 : t,
            0 | 255 * (1 > 6 * t ? e + 6 * (n - e) * t : .5 > t ? n : 2 > 3 * t ? e + 6 * (n - e) * (2 / 3 - t) : e) + .5
        }, at = function(t) {
            var e, n, i, r, o, s;
            return t && "" !== t ? "number" == typeof t ? [t >> 16, 255 & t >> 8, 255 & t] : ("," === t.charAt(t.length - 1) && (t = t.substr(0, t.length - 1)),
            ot[t] ? ot[t] : "#" === t.charAt(0) ? (4 === t.length && (e = t.charAt(1),
            n = t.charAt(2),
            i = t.charAt(3),
            t = "#" + e + e + n + n + i + i),
            t = parseInt(t.substr(1), 16),
            [t >> 16, 255 & t >> 8, 255 & t]) : "hsl" === t.substr(0, 3) ? (t = t.match(m),
            r = Number(t[0]) % 360 / 360,
            o = Number(t[1]) / 100,
            s = Number(t[2]) / 100,
            n = .5 >= s ? s * (o + 1) : s + o - s * o,
            e = 2 * s - n,
            t.length > 3 && (t[3] = Number(t[3])),
            t[0] = st(r + 1 / 3, e, n),
            t[1] = st(r, e, n),
            t[2] = st(r - 1 / 3, e, n),
            t) : (t = t.match(m) || ot.transparent,
            t[0] = Number(t[0]),
            t[1] = Number(t[1]),
            t[2] = Number(t[2]),
            t.length > 3 && (t[3] = Number(t[3])),
            t)) : ot.black
        }, lt = "(?:\\b(?:(?:rgb|rgba|hsl|hsla)\\(.+?\\))|\\B#.+?\\b";
        for (l in ot)
            lt += "|" + l + "\\b";
        lt = RegExp(lt + ")", "gi");
        var ut = function(t, e, n, i) {
            if (null == t)
                return function(t) {
                    return t
                }
                ;
            var r, o = e ? (t.match(lt) || [""])[0] : "", s = t.split(o).join("").match(v) || [], a = t.substr(0, t.indexOf(s[0])), l = ")" === t.charAt(t.length - 1) ? ")" : "", u = -1 !== t.indexOf(" ") ? " " : ",", c = s.length, h = c > 0 ? s[0].replace(m, "") : "";
            return c ? r = e ? function(t) {
                var e, p, f, d;
                if ("number" == typeof t)
                    t += h;
                else if (i && M.test(t)) {
                    for (d = t.replace(M, "|").split("|"),
                    f = 0; d.length > f; f++)
                        d[f] = r(d[f]);
                    return d.join(",")
                }
                if (e = (t.match(lt) || [o])[0],
                p = t.split(e).join("").match(v) || [],
                f = p.length,
                c > f--)
                    for (; c > ++f; )
                        p[f] = n ? p[0 | (f - 1) / 2] : s[f];
                return a + p.join(u) + u + e + l + (-1 !== t.indexOf("inset") ? " inset" : "")
            }
            : function(t) {
                var e, o, p;
                if ("number" == typeof t)
                    t += h;
                else if (i && M.test(t)) {
                    for (o = t.replace(M, "|").split("|"),
                    p = 0; o.length > p; p++)
                        o[p] = r(o[p]);
                    return o.join(",")
                }
                if (e = t.match(v) || [],
                p = e.length,
                c > p--)
                    for (; c > ++p; )
                        e[p] = n ? e[0 | (p - 1) / 2] : s[p];
                return a + e.join(u) + l
            }
            : function(t) {
                return t
            }
        }
          , ct = function(t) {
            return t = t.split(","),
            function(e, n, i, r, o, s, a) {
                var l, u = (n + "").split(" ");
                for (a = {},
                l = 0; 4 > l; l++)
                    a[t[l]] = u[l] = u[l] || u[(l - 1) / 2 >> 0];
                return r.parse(e, a, o, s)
            }
        }
          , ht = (I._setPluginRatio = function(t) {
            this.plugin.setRatio(t);
            for (var e, n, i, r, o = this.data, s = o.proxy, a = o.firstMPT, l = 1e-6; a; )
                e = s[a.v],
                a.r ? e = e > 0 ? 0 | e + .5 : 0 | e - .5 : l > e && e > -l && (e = 0),
                a.t[a.p] = e,
                a = a._next;
            if (o.autoRotate && (o.autoRotate.rotation = s.rotation),
            1 === t)
                for (a = o.firstMPT; a; ) {
                    if (n = a.t,
                    n.type) {
                        if (1 === n.type) {
                            for (r = n.xs0 + n.s + n.xs1,
                            i = 1; n.l > i; i++)
                                r += n["xn" + i] + n["xs" + (i + 1)];
                            n.e = r
                        }
                    } else
                        n.e = n.s + n.xs0;
                    a = a._next
                }
        }
        ,
        function(t, e, n, i, r) {
            this.t = t,
            this.p = e,
            this.v = n,
            this.r = r,
            i && (i._prev = this,
            this._next = i)
        }
        )
          , pt = (I._parseToProxy = function(t, e, n, i, r, o) {
            var s, a, l, u, c, h = i, p = {}, f = {}, d = n._transform, m = N;
            for (n._transform = null,
            N = e,
            i = c = n.parse(t, e, i, r),
            N = m,
            o && (n._transform = d,
            h && (h._prev = null,
            h._prev && (h._prev._next = null))); i && i !== h; ) {
                if (1 >= i.type && (a = i.p,
                f[a] = i.s + i.c,
                p[a] = i.s,
                o || (u = new ht(i,"s",a,u,i.r),
                i.c = 0),
                1 === i.type))
                    for (s = i.l; --s > 0; )
                        l = "xn" + s,
                        a = i.p + "_" + l,
                        f[a] = i.data[l],
                        p[a] = i[l],
                        o || (u = new ht(i,l,a,u,i.rxp[l]));
                i = i._next
            }
            return {
                proxy: p,
                end: f,
                firstMPT: u,
                pt: c
            }
        }
        ,
        I.CSSPropTween = function(t, e, i, r, s, a, l, u, c, h, p) {
            this.t = t,
            this.p = e,
            this.s = i,
            this.c = r,
            this.n = l || e,
            t instanceof pt || o.push(this.n),
            this.r = u,
            this.type = a || 0,
            c && (this.pr = c,
            n = !0),
            this.b = void 0 === h ? i : h,
            this.e = void 0 === p ? i + r : p,
            s && (this._next = s,
            s._prev = this)
        }
        )
          , ft = s.parseComplex = function(t, e, n, i, r, o, s, a, l, c) {
            n = n || o || "",
            s = new pt(t,e,0,0,s,c ? 2 : 1,null,!1,a,n,i),
            i += "";
            var h, p, f, d, v, y, _, x, w, b, C, S, k = n.split(", ").join(",").split(" "), j = i.split(", ").join(",").split(" "), P = k.length, E = u !== !1;
            for ((-1 !== i.indexOf(",") || -1 !== n.indexOf(",")) && (k = k.join(" ").replace(M, ", ").split(" "),
            j = j.join(" ").replace(M, ", ").split(" "),
            P = k.length),
            P !== j.length && (k = (o || "").split(" "),
            P = k.length),
            s.plugin = l,
            s.setRatio = c,
            h = 0; P > h; h++)
                if (d = k[h],
                v = j[h],
                x = parseFloat(d),
                x || 0 === x)
                    s.appendXtra("", x, nt(v, x), v.replace(g, ""), E && -1 !== v.indexOf("px"), !0);
                else if (r && ("#" === d.charAt(0) || ot[d] || T.test(d)))
                    S = "," === v.charAt(v.length - 1) ? ")," : ")",
                    d = at(d),
                    v = at(v),
                    w = d.length + v.length > 6,
                    w && !H && 0 === v[3] ? (s["xs" + s.l] += s.l ? " transparent" : "transparent",
                    s.e = s.e.split(j[h]).join("transparent")) : (H || (w = !1),
                    s.appendXtra(w ? "rgba(" : "rgb(", d[0], v[0] - d[0], ",", !0, !0).appendXtra("", d[1], v[1] - d[1], ",", !0).appendXtra("", d[2], v[2] - d[2], w ? "," : S, !0),
                    w && (d = 4 > d.length ? 1 : d[3],
                    s.appendXtra("", d, (4 > v.length ? 1 : v[3]) - d, S, !1)));
                else if (y = d.match(m)) {
                    if (_ = v.match(g),
                    !_ || _.length !== y.length)
                        return s;
                    for (f = 0,
                    p = 0; y.length > p; p++)
                        C = y[p],
                        b = d.indexOf(C, f),
                        s.appendXtra(d.substr(f, b - f), Number(C), nt(_[p], C), "", E && "px" === d.substr(b + C.length, 2), 0 === p),
                        f = b + C.length;
                    s["xs" + s.l] += d.substr(f)
                } else
                    s["xs" + s.l] += s.l ? " " + d : d;
            if (-1 !== i.indexOf("=") && s.data) {
                for (S = s.xs0 + s.data.s,
                h = 1; s.l > h; h++)
                    S += s["xs" + h] + s.data["xn" + h];
                s.e = S + s["xs" + h]
            }
            return s.l || (s.type = -1,
            s.xs0 = s.e),
            s.xfirst || s
        }
          , dt = 9;
        for (l = pt.prototype,
        l.l = l.pr = 0; --dt > 0; )
            l["xn" + dt] = 0,
            l["xs" + dt] = "";
        l.xs0 = "",
        l._next = l._prev = l.xfirst = l.data = l.plugin = l.setRatio = l.rxp = null,
        l.appendXtra = function(t, e, n, i, r, o) {
            var s = this
              , a = s.l;
            return s["xs" + a] += o && a ? " " + t : t || "",
            n || 0 === a || s.plugin ? (s.l++,
            s.type = s.setRatio ? 2 : 1,
            s["xs" + s.l] = i || "",
            a > 0 ? (s.data["xn" + a] = e + n,
            s.rxp["xn" + a] = r,
            s["xn" + a] = e,
            s.plugin || (s.xfirst = new pt(s,"xn" + a,e,n,s.xfirst || s,0,s.n,r,s.pr),
            s.xfirst.xs0 = 0),
            s) : (s.data = {
                s: e + n
            },
            s.rxp = {},
            s.s = e,
            s.c = n,
            s.r = r,
            s)) : (s["xs" + a] += e + (i || ""),
            s)
        }
        ;
        var mt = function(t, e) {
            e = e || {},
            this.p = e.prefix ? X(t) || t : t,
            a[t] = a[this.p] = this,
            this.format = e.formatter || ut(e.defaultValue, e.color, e.collapsible, e.multi),
            e.parser && (this.parse = e.parser),
            this.clrs = e.color,
            this.multi = e.multi,
            this.keyword = e.keyword,
            this.dflt = e.defaultValue,
            this.pr = e.priority || 0
        }
          , gt = I._registerComplexSpecialProp = function(t, e, n) {
            "object" != typeof e && (e = {
                parser: n
            });
            var i, r, o = t.split(","), s = e.defaultValue;
            for (n = n || [s],
            i = 0; o.length > i; i++)
                e.prefix = 0 === i && e.prefix,
                e.defaultValue = n[i] || s,
                r = new mt(o[i],e)
        }
          , vt = function(t) {
            if (!a[t]) {
                var e = t.charAt(0).toUpperCase() + t.substr(1) + "Plugin";
                gt(t, {
                    parser: function(t, n, i, r, o, s, l) {
                        var u = (window.GreenSockGlobals || window).com.greensock.plugins[e];
                        return u ? (u._cssRegister(),
                        a[i].parse(t, n, i, r, o, s, l)) : (B("Error: " + e + " js file not loaded."),
                        o)
                    }
                })
            }
        };
        l = mt.prototype,
        l.parseComplex = function(t, e, n, i, r, o) {
            var s, a, l, u, c, h, p = this.keyword;
            if (this.multi && (M.test(n) || M.test(e) ? (a = e.replace(M, "|").split("|"),
            l = n.replace(M, "|").split("|")) : p && (a = [e],
            l = [n])),
            l) {
                for (u = l.length > a.length ? l.length : a.length,
                s = 0; u > s; s++)
                    e = a[s] = a[s] || this.dflt,
                    n = l[s] = l[s] || this.dflt,
                    p && (c = e.indexOf(p),
                    h = n.indexOf(p),
                    c !== h && (n = -1 === h ? l : a,
                    n[s] += " " + p));
                e = a.join(", "),
                n = l.join(", ")
            }
            return ft(t, this.p, e, n, this.clrs, this.dflt, i, this.pr, r, o)
        }
        ,
        l.parse = function(t, e, n, i, o, s) {
            return this.parseComplex(t.style, this.format(Y(t, this.p, r, !1, this.dflt)), this.format(e), o, s)
        }
        ,
        s.registerSpecialProp = function(t, e, n) {
            gt(t, {
                parser: function(t, i, r, o, s, a) {
                    var l = new pt(t,r,0,0,s,2,r,!1,n);
                    return l.plugin = a,
                    l.setRatio = e(t, i, o._tween, r),
                    l
                },
                priority: n
            })
        }
        ;
        var yt = "scaleX,scaleY,scaleZ,x,y,z,skewX,rotation,rotationX,rotationY,perspective".split(",")
          , _t = X("transform")
          , xt = z + "transform"
          , wt = X("transformOrigin")
          , bt = null !== X("perspective")
          , Tt = function(t, e, n, i) {
            if (t._gsTransform && n && !i)
                return t._gsTransform;
            var r, o, a, l, u, c, h, p, f, d, m, g, v, y = n ? t._gsTransform || {
                skewY: 0
            } : {
                skewY: 0
            }, _ = 0 > y.scaleX, x = 2e-5, w = 1e5, b = -Math.PI + 1e-4, T = Math.PI - 1e-4, C = bt ? parseFloat(Y(t, wt, e, !1, "0 0 0").split(" ")[2]) || y.zOrigin || 0 : 0;
            for (_t ? r = Y(t, xt, e, !0) : t.currentStyle && (r = t.currentStyle.filter.match(E),
            r = r && 4 === r.length ? [r[0].substr(4), Number(r[2].substr(4)), Number(r[1].substr(4)), r[3].substr(4), y.x || 0, y.y || 0].join(",") : ""),
            o = (r || "").match(/(?:\-|\b)[\d\-\.e]+\b/gi) || [],
            a = o.length; --a > -1; )
                l = Number(o[a]),
                o[a] = (u = l - (l |= 0)) ? (0 | u * w + (0 > u ? -.5 : .5)) / w + l : l;
            if (16 === o.length) {
                var S = o[8]
                  , k = o[9]
                  , j = o[10]
                  , P = o[12]
                  , O = o[13]
                  , M = o[14];
                if (y.zOrigin && (M = -y.zOrigin,
                P = S * M - o[12],
                O = k * M - o[13],
                M = j * M + y.zOrigin - o[14]),
                !n || i || null == y.rotationX) {
                    var D, A, N, L, R, $, I, q = o[0], H = o[1], F = o[2], B = o[3], z = o[4], W = o[5], X = o[6], V = o[7], U = o[11], G = y.rotationX = Math.atan2(X, j), Q = b > G || G > T;
                    G && (L = Math.cos(-G),
                    R = Math.sin(-G),
                    D = z * L + S * R,
                    A = W * L + k * R,
                    N = X * L + j * R,
                    S = z * -R + S * L,
                    k = W * -R + k * L,
                    j = X * -R + j * L,
                    U = V * -R + U * L,
                    z = D,
                    W = A,
                    X = N),
                    G = y.rotationY = Math.atan2(S, q),
                    G && ($ = b > G || G > T,
                    L = Math.cos(-G),
                    R = Math.sin(-G),
                    D = q * L - S * R,
                    A = H * L - k * R,
                    N = F * L - j * R,
                    k = H * R + k * L,
                    j = F * R + j * L,
                    U = B * R + U * L,
                    q = D,
                    H = A,
                    F = N),
                    G = y.rotation = Math.atan2(H, W),
                    G && (I = b > G || G > T,
                    L = Math.cos(-G),
                    R = Math.sin(-G),
                    q = q * L + z * R,
                    A = H * L + W * R,
                    W = H * -R + W * L,
                    X = F * -R + X * L,
                    H = A),
                    I && Q ? y.rotation = y.rotationX = 0 : I && $ ? y.rotation = y.rotationY = 0 : $ && Q && (y.rotationY = y.rotationX = 0),
                    y.scaleX = (0 | Math.sqrt(q * q + H * H) * w + .5) / w,
                    y.scaleY = (0 | Math.sqrt(W * W + k * k) * w + .5) / w,
                    y.scaleZ = (0 | Math.sqrt(X * X + j * j) * w + .5) / w,
                    y.skewX = 0,
                    y.perspective = U ? 1 / (0 > U ? -U : U) : 0,
                    y.x = P,
                    y.y = O,
                    y.z = M
                }
            } else if (!(bt && !i && o.length && y.x === o[4] && y.y === o[5] && (y.rotationX || y.rotationY) || void 0 !== y.x && "none" === Y(t, "display", e))) {
                var J = o.length >= 6
                  , Z = J ? o[0] : 1
                  , K = o[1] || 0
                  , tt = o[2] || 0
                  , et = J ? o[3] : 1;
                y.x = o[4] || 0,
                y.y = o[5] || 0,
                c = Math.sqrt(Z * Z + K * K),
                h = Math.sqrt(et * et + tt * tt),
                p = Z || K ? Math.atan2(K, Z) : y.rotation || 0,
                f = tt || et ? Math.atan2(tt, et) + p : y.skewX || 0,
                d = c - Math.abs(y.scaleX || 0),
                m = h - Math.abs(y.scaleY || 0),
                Math.abs(f) > Math.PI / 2 && Math.abs(f) < 1.5 * Math.PI && (_ ? (c *= -1,
                f += 0 >= p ? Math.PI : -Math.PI,
                p += 0 >= p ? Math.PI : -Math.PI) : (h *= -1,
                f += 0 >= f ? Math.PI : -Math.PI)),
                g = (p - y.rotation) % Math.PI,
                v = (f - y.skewX) % Math.PI,
                (void 0 === y.skewX || d > x || -x > d || m > x || -x > m || g > b && T > g && !1 | g * w || v > b && T > v && !1 | v * w) && (y.scaleX = c,
                y.scaleY = h,
                y.rotation = p,
                y.skewX = f),
                bt && (y.rotationX = y.rotationY = y.z = 0,
                y.perspective = parseFloat(s.defaultTransformPerspective) || 0,
                y.scaleZ = 1)
            }
            y.zOrigin = C;
            for (a in y)
                x > y[a] && y[a] > -x && (y[a] = 0);
            return n && (t._gsTransform = y),
            y
        }
          , Ct = function(t) {
            var e, n, i = this.data, r = -i.rotation, o = r + i.skewX, s = 1e5, a = (0 | Math.cos(r) * i.scaleX * s) / s, l = (0 | Math.sin(r) * i.scaleX * s) / s, u = (0 | Math.sin(o) * -i.scaleY * s) / s, c = (0 | Math.cos(o) * i.scaleY * s) / s, h = this.t.style, p = this.t.currentStyle;
            if (p) {
                n = l,
                l = -u,
                u = -n,
                e = p.filter,
                h.filter = "";
                var f, m, g = this.t.offsetWidth, v = this.t.offsetHeight, y = "absolute" !== p.position, w = "progid:DXImageTransform.Microsoft.Matrix(M11=" + a + ", M12=" + l + ", M21=" + u + ", M22=" + c, b = i.x, T = i.y;
                if (null != i.ox && (f = (i.oxp ? .01 * g * i.ox : i.ox) - g / 2,
                m = (i.oyp ? .01 * v * i.oy : i.oy) - v / 2,
                b += f - (f * a + m * l),
                T += m - (f * u + m * c)),
                y)
                    f = g / 2,
                    m = v / 2,
                    w += ", Dx=" + (f - (f * a + m * l) + b) + ", Dy=" + (m - (f * u + m * c) + T) + ")";
                else {
                    var C, S, k, j = 8 > d ? 1 : -1;
                    for (f = i.ieOffsetX || 0,
                    m = i.ieOffsetY || 0,
                    i.ieOffsetX = Math.round((g - ((0 > a ? -a : a) * g + (0 > l ? -l : l) * v)) / 2 + b),
                    i.ieOffsetY = Math.round((v - ((0 > c ? -c : c) * v + (0 > u ? -u : u) * g)) / 2 + T),
                    dt = 0; 4 > dt; dt++)
                        S = K[dt],
                        C = p[S],
                        n = -1 !== C.indexOf("px") ? parseFloat(C) : U(this.t, S, parseFloat(C), C.replace(_, "")) || 0,
                        k = n !== i[S] ? 2 > dt ? -i.ieOffsetX : -i.ieOffsetY : 2 > dt ? f - i.ieOffsetX : m - i.ieOffsetY,
                        h[S] = (i[S] = Math.round(n - k * (0 === dt || 2 === dt ? 1 : j))) + "px";
                    w += ", sizingMethod='auto expand')"
                }
                h.filter = -1 !== e.indexOf("DXImageTransform.Microsoft.Matrix(") ? e.replace(O, w) : w + " " + e,
                (0 === t || 1 === t) && 1 === a && 0 === l && 0 === u && 1 === c && (y && -1 === w.indexOf("Dx=0, Dy=0") || x.test(e) && 100 !== parseFloat(RegExp.$1) || -1 === e.indexOf("gradient(") && h.removeAttribute("filter"))
            }
        }
          , St = function() {
            var t, e, n, i, r, o, s, a, l, u, c, h, f, d, m, g, v, y, _, x, w, b, T, C, S, k, j, P = this.data, E = this.t.style, O = P.rotation, M = P.scaleX, D = P.scaleY, A = P.scaleZ;
            if (p && (S = E.top ? "top" : E.bottom ? "bottom" : parseFloat(Y(this.t, "top", null, !1)) ? "bottom" : "top",
            w = Y(this.t, S, null, !1),
            k = parseFloat(w) || 0,
            j = w.substr((k + "").length) || "px",
            P._ffFix = !P._ffFix,
            E[S] = (P._ffFix ? k + .05 : k - .05) + j),
            O || P.skewX)
                _ = Math.cos(O),
                x = Math.sin(O),
                t = _,
                r = x,
                P.skewX && (O -= P.skewX,
                _ = Math.cos(O),
                x = Math.sin(O)),
                e = -x,
                o = _;
            else {
                if (!P.rotationY && !P.rotationX && 1 === A)
                    return void (E[_t] = "translate3d(" + P.x + "px," + P.y + "px," + P.z + "px)" + (1 !== M || 1 !== D ? " scale(" + M + "," + D + ")" : ""));
                t = o = 1,
                e = r = 0
            }
            c = 1,
            n = i = s = a = l = u = h = f = d = 0,
            g = P.perspective,
            m = g ? -1 / g : 0,
            v = P.zOrigin,
            y = 1e5,
            O = P.rotationY,
            O && (_ = Math.cos(O),
            x = Math.sin(O),
            l = c * -x,
            f = m * -x,
            n = t * x,
            s = r * x,
            c *= _,
            m *= _,
            t *= _,
            r *= _),
            O = P.rotationX,
            O && (_ = Math.cos(O),
            x = Math.sin(O),
            w = e * _ + n * x,
            b = o * _ + s * x,
            T = u * _ + c * x,
            C = d * _ + m * x,
            n = e * -x + n * _,
            s = o * -x + s * _,
            c = u * -x + c * _,
            m = d * -x + m * _,
            e = w,
            o = b,
            u = T,
            d = C),
            1 !== A && (n *= A,
            s *= A,
            c *= A,
            m *= A),
            1 !== D && (e *= D,
            o *= D,
            u *= D,
            d *= D),
            1 !== M && (t *= M,
            r *= M,
            l *= M,
            f *= M),
            v && (h -= v,
            i = n * h,
            a = s * h,
            h = c * h + v),
            i = (w = (i += P.x) - (i |= 0)) ? (0 | w * y + (0 > w ? -.5 : .5)) / y + i : i,
            a = (w = (a += P.y) - (a |= 0)) ? (0 | w * y + (0 > w ? -.5 : .5)) / y + a : a,
            h = (w = (h += P.z) - (h |= 0)) ? (0 | w * y + (0 > w ? -.5 : .5)) / y + h : h,
            E[_t] = "matrix3d(" + [(0 | t * y) / y, (0 | r * y) / y, (0 | l * y) / y, (0 | f * y) / y, (0 | e * y) / y, (0 | o * y) / y, (0 | u * y) / y, (0 | d * y) / y, (0 | n * y) / y, (0 | s * y) / y, (0 | c * y) / y, (0 | m * y) / y, i, a, h, g ? 1 + -h / g : 1].join(",") + ")"
        }
          , kt = function() {
            var t, e, n, i, r, o, s, a, l, u = this.data, c = this.t, h = c.style;
            p && (t = h.top ? "top" : h.bottom ? "bottom" : parseFloat(Y(c, "top", null, !1)) ? "bottom" : "top",
            e = Y(c, t, null, !1),
            n = parseFloat(e) || 0,
            i = e.substr((n + "").length) || "px",
            u._ffFix = !u._ffFix,
            h[t] = (u._ffFix ? n + .05 : n - .05) + i),
            u.rotation || u.skewX ? (r = u.rotation,
            o = r - u.skewX,
            s = 1e5,
            a = u.scaleX * s,
            l = u.scaleY * s,
            h[_t] = "matrix(" + (0 | Math.cos(r) * a) / s + "," + (0 | Math.sin(r) * a) / s + "," + (0 | Math.sin(o) * -l) / s + "," + (0 | Math.cos(o) * l) / s + "," + u.x + "," + u.y + ")") : h[_t] = "matrix(" + u.scaleX + ",0,0," + u.scaleY + "," + u.x + "," + u.y + ")"
        };
        gt("transform,scale,scaleX,scaleY,scaleZ,x,y,z,rotation,rotationX,rotationY,rotationZ,skewX,skewY,shortRotation,shortRotationX,shortRotationY,shortRotationZ,transformOrigin,transformPerspective,directionalRotation,parseTransform,force3D", {
            parser: function(t, e, n, i, o, s, a) {
                if (i._transform)
                    return o;
                var l, u, c, h, p, f, d, m = i._transform = Tt(t, r, !0, a.parseTransform), g = t.style, v = 1e-6, y = yt.length, _ = a, x = {};
                if ("string" == typeof _.transform && _t)
                    c = g.cssText,
                    g[_t] = _.transform,
                    g.display = "block",
                    l = Tt(t, null, !1),
                    g.cssText = c;
                else if ("object" == typeof _) {
                    if (l = {
                        scaleX: it(null != _.scaleX ? _.scaleX : _.scale, m.scaleX),
                        scaleY: it(null != _.scaleY ? _.scaleY : _.scale, m.scaleY),
                        scaleZ: it(null != _.scaleZ ? _.scaleZ : _.scale, m.scaleZ),
                        x: it(_.x, m.x),
                        y: it(_.y, m.y),
                        z: it(_.z, m.z),
                        perspective: it(_.transformPerspective, m.perspective)
                    },
                    d = _.directionalRotation,
                    null != d)
                        if ("object" == typeof d)
                            for (c in d)
                                _[c] = d[c];
                        else
                            _.rotation = d;
                    l.rotation = rt("rotation"in _ ? _.rotation : "shortRotation"in _ ? _.shortRotation + "_short" : "rotationZ"in _ ? _.rotationZ : m.rotation * A, m.rotation, "rotation", x),
                    bt && (l.rotationX = rt("rotationX"in _ ? _.rotationX : "shortRotationX"in _ ? _.shortRotationX + "_short" : m.rotationX * A || 0, m.rotationX, "rotationX", x),
                    l.rotationY = rt("rotationY"in _ ? _.rotationY : "shortRotationY"in _ ? _.shortRotationY + "_short" : m.rotationY * A || 0, m.rotationY, "rotationY", x)),
                    l.skewX = null == _.skewX ? m.skewX : rt(_.skewX, m.skewX),
                    l.skewY = null == _.skewY ? m.skewY : rt(_.skewY, m.skewY),
                    (u = l.skewY - m.skewY) && (l.skewX += u,
                    l.rotation += u)
                }
                for (null != _.force3D && (m.force3D = _.force3D,
                f = !0),
                p = m.force3D || m.z || m.rotationX || m.rotationY || l.z || l.rotationX || l.rotationY || l.perspective,
                p || null == _.scale || (l.scaleZ = 1); --y > -1; )
                    n = yt[y],
                    h = l[n] - m[n],
                    (h > v || -v > h || null != N[n]) && (f = !0,
                    o = new pt(m,n,m[n],h,o),
                    n in x && (o.e = x[n]),
                    o.xs0 = 0,
                    o.plugin = s,
                    i._overwriteProps.push(o.n));
                return h = _.transformOrigin,
                (h || bt && p && m.zOrigin) && (_t ? (f = !0,
                n = wt,
                h = (h || Y(t, n, r, !1, "50% 50%")) + "",
                o = new pt(g,n,0,0,o,-1,"transformOrigin"),
                o.b = g[n],
                o.plugin = s,
                bt ? (c = m.zOrigin,
                h = h.split(" "),
                m.zOrigin = (h.length > 2 && (0 === c || "0px" !== h[2]) ? parseFloat(h[2]) : c) || 0,
                o.xs0 = o.e = g[n] = h[0] + " " + (h[1] || "50%") + " 0px",
                o = new pt(m,"zOrigin",0,0,o,-1,o.n),
                o.b = c,
                o.xs0 = o.e = m.zOrigin) : o.xs0 = o.e = g[n] = h) : et(h + "", m)),
                f && (i._transformType = p || 3 === this._transformType ? 3 : 2),
                o
            },
            prefix: !0
        }),
        gt("boxShadow", {
            defaultValue: "0px 0px 0px 0px #999",
            prefix: !0,
            color: !0,
            multi: !0,
            keyword: "inset"
        }),
        gt("borderRadius", {
            defaultValue: "0px",
            parser: function(t, e, n, o, s) {
                e = this.format(e);
                var a, l, u, c, h, p, f, d, m, g, v, y, _, x, w, b, T = ["borderTopLeftRadius", "borderTopRightRadius", "borderBottomRightRadius", "borderBottomLeftRadius"], C = t.style;
                for (m = parseFloat(t.offsetWidth),
                g = parseFloat(t.offsetHeight),
                a = e.split(" "),
                l = 0; T.length > l; l++)
                    this.p.indexOf("border") && (T[l] = X(T[l])),
                    h = c = Y(t, T[l], r, !1, "0px"),
                    -1 !== h.indexOf(" ") && (c = h.split(" "),
                    h = c[0],
                    c = c[1]),
                    p = u = a[l],
                    f = parseFloat(h),
                    y = h.substr((f + "").length),
                    _ = "=" === p.charAt(1),
                    _ ? (d = parseInt(p.charAt(0) + "1", 10),
                    p = p.substr(2),
                    d *= parseFloat(p),
                    v = p.substr((d + "").length - (0 > d ? 1 : 0)) || "") : (d = parseFloat(p),
                    v = p.substr((d + "").length)),
                    "" === v && (v = i[n] || y),
                    v !== y && (x = U(t, "borderLeft", f, y),
                    w = U(t, "borderTop", f, y),
                    "%" === v ? (h = 100 * (x / m) + "%",
                    c = 100 * (w / g) + "%") : "em" === v ? (b = U(t, "borderLeft", 1, "em"),
                    h = x / b + "em",
                    c = w / b + "em") : (h = x + "px",
                    c = w + "px"),
                    _ && (p = parseFloat(h) + d + v,
                    u = parseFloat(c) + d + v)),
                    s = ft(C, T[l], h + " " + c, p + " " + u, !1, "0px", s);
                return s
            },
            prefix: !0,
            formatter: ut("0px 0px 0px 0px", !1, !0)
        }),
        gt("backgroundPosition", {
            defaultValue: "0 0",
            parser: function(t, e, n, i, o, s) {
                var a, l, u, c, h, p, f = "background-position", m = r || V(t, null), g = this.format((m ? d ? m.getPropertyValue(f + "-x") + " " + m.getPropertyValue(f + "-y") : m.getPropertyValue(f) : t.currentStyle.backgroundPositionX + " " + t.currentStyle.backgroundPositionY) || "0 0"), v = this.format(e);
                if (-1 !== g.indexOf("%") != (-1 !== v.indexOf("%")) && (p = Y(t, "backgroundImage").replace(k, ""),
                p && "none" !== p)) {
                    for (a = g.split(" "),
                    l = v.split(" "),
                    $.setAttribute("src", p),
                    u = 2; --u > -1; )
                        g = a[u],
                        c = -1 !== g.indexOf("%"),
                        c !== (-1 !== l[u].indexOf("%")) && (h = 0 === u ? t.offsetWidth - $.width : t.offsetHeight - $.height,
                        a[u] = c ? parseFloat(g) / 100 * h + "px" : 100 * (parseFloat(g) / h) + "%");
                    g = a.join(" ")
                }
                return this.parseComplex(t.style, g, v, o, s)
            },
            formatter: et
        }),
        gt("backgroundSize", {
            defaultValue: "0 0",
            formatter: et
        }),
        gt("perspective", {
            defaultValue: "0px",
            prefix: !0
        }),
        gt("perspectiveOrigin", {
            defaultValue: "50% 50%",
            prefix: !0
        }),
        gt("transformStyle", {
            prefix: !0
        }),
        gt("backfaceVisibility", {
            prefix: !0
        }),
        gt("margin", {
            parser: ct("marginTop,marginRight,marginBottom,marginLeft")
        }),
        gt("padding", {
            parser: ct("paddingTop,paddingRight,paddingBottom,paddingLeft")
        }),
        gt("clip", {
            defaultValue: "rect(0px,0px,0px,0px)",
            parser: function(t, e, n, i, o, s) {
                var a, l, u;
                return 9 > d ? (l = t.currentStyle,
                u = 8 > d ? " " : ",",
                a = "rect(" + l.clipTop + u + l.clipRight + u + l.clipBottom + u + l.clipLeft + ")",
                e = this.format(e).split(",").join(u)) : (a = this.format(Y(t, this.p, r, !1, this.dflt)),
                e = this.format(e)),
                this.parseComplex(t.style, a, e, o, s)
            }
        }),
        gt("textShadow", {
            defaultValue: "0px 0px 0px #999",
            color: !0,
            multi: !0
        }),
        gt("autoRound,strictUnits", {
            parser: function(t, e, n, i, r) {
                return r
            }
        }),
        gt("border", {
            defaultValue: "0px solid #000",
            parser: function(t, e, n, i, o, s) {
                return this.parseComplex(t.style, this.format(Y(t, "borderTopWidth", r, !1, "0px") + " " + Y(t, "borderTopStyle", r, !1, "solid") + " " + Y(t, "borderTopColor", r, !1, "#000")), this.format(e), o, s)
            },
            color: !0,
            formatter: function(t) {
                var e = t.split(" ");
                return e[0] + " " + (e[1] || "solid") + " " + (t.match(lt) || ["#000"])[0]
            }
        }),
        gt("float,cssFloat,styleFloat", {
            parser: function(t, e, n, i, r) {
                var o = t.style
                  , s = "cssFloat"in o ? "cssFloat" : "styleFloat";
                return new pt(o,s,0,0,r,-1,n,!1,0,o[s],e)
            }
        });
        var jt = function(t) {
            var e, n = this.t, i = n.filter || Y(this.data, "filter"), r = 0 | this.s + this.c * t;
            100 === r && (-1 === i.indexOf("atrix(") && -1 === i.indexOf("radient(") ? (n.removeAttribute("filter"),
            e = !Y(this.data, "filter")) : (n.filter = i.replace(b, ""),
            e = !0)),
            e || (this.xn1 && (n.filter = i = i || "alpha(opacity=" + r + ")"),
            -1 === i.indexOf("opacity") ? 0 === r && this.xn1 || (n.filter += " alpha(opacity=" + r + ")") : n.filter = i.replace(x, "opacity=" + r))
        };
        gt("opacity,alpha,autoAlpha", {
            defaultValue: "1",
            parser: function(t, e, n, i, o, s) {
                var a = parseFloat(Y(t, "opacity", r, !1, "1"))
                  , l = t.style
                  , u = "autoAlpha" === n;
                return e = parseFloat(e),
                u && 1 === a && "hidden" === Y(t, "visibility", r) && 0 !== e && (a = 0),
                H ? o = new pt(l,"opacity",a,e - a,o) : (o = new pt(l,"opacity",100 * a,100 * (e - a),o),
                o.xn1 = u ? 1 : 0,
                l.zoom = 1,
                o.type = 2,
                o.b = "alpha(opacity=" + o.s + ")",
                o.e = "alpha(opacity=" + (o.s + o.c) + ")",
                o.data = t,
                o.plugin = s,
                o.setRatio = jt),
                u && (o = new pt(l,"visibility",0,0,o,-1,null,!1,0,0 !== a ? "inherit" : "hidden",0 === e ? "hidden" : "inherit"),
                o.xs0 = "inherit",
                i._overwriteProps.push(o.n)),
                o
            }
        });
        var Pt = function(t, e) {
            e && (t.removeProperty ? t.removeProperty(e.replace(C, "-$1").toLowerCase()) : t.removeAttribute(e))
        }
          , Et = function(t) {
            if (this.t._gsClassPT = this,
            1 === t || 0 === t) {
                this.t.className = 0 === t ? this.b : this.e;
                for (var e = this.data, n = this.t.style; e; )
                    e.v ? n[e.p] = e.v : Pt(n, e.p),
                    e = e._next;
                1 === t && this.t._gsClassPT === this && (this.t._gsClassPT = null)
            } else
                this.t.className !== this.e && (this.t.className = this.e)
        };
        gt("className", {
            parser: function(t, e, i, o, s, a, l) {
                var u, c, h, p, f, d = t.className, m = t.style.cssText;
                if (s = o._classNamePT = new pt(t,i,0,0,s,2),
                s.setRatio = Et,
                s.pr = -11,
                n = !0,
                s.b = d,
                c = Q(t, r),
                h = t._gsClassPT) {
                    for (p = {},
                    f = h.data; f; )
                        p[f.p] = 1,
                        f = f._next;
                    h.setRatio(1)
                }
                return t._gsClassPT = s,
                s.e = "=" !== e.charAt(1) ? e : d.replace(RegExp("\\s*\\b" + e.substr(2) + "\\b"), "") + ("+" === e.charAt(0) ? " " + e.substr(2) : ""),
                o._tween._duration && (t.className = s.e,
                u = J(t, c, Q(t), l, p),
                t.className = d,
                s.data = u.firstMPT,
                t.style.cssText = m,
                s = s.xfirst = o.parse(t, u.difs, s, a)),
                s
            }
        });
        var Ot = function(t) {
            if ((1 === t || 0 === t) && this.data._totalTime === this.data._totalDuration) {
                if ("all" === this.e)
                    return this.t.style.cssText = "",
                    void (this.t._gsTransform && delete this.t._gsTransform);
                for (var e, n = this.t.style, i = this.e.split(","), r = i.length, o = a.transform.parse; --r > -1; )
                    e = i[r],
                    a[e] && (e = a[e].parse === o ? _t : a[e].p),
                    Pt(n, e)
            }
        };
        for (gt("clearProps", {
            parser: function(t, e, i, r, o) {
                return o = new pt(t,i,0,0,o,2),
                o.setRatio = Ot,
                o.e = e,
                o.pr = -10,
                o.data = r._tween,
                n = !0,
                o
            }
        }),
        l = "bezier,throwProps,physicsProps,physics2D".split(","),
        dt = l.length; dt--; )
            vt(l[dt]);
        l = s.prototype,
        l._firstPT = null,
        l._onInitTween = function(t, e, a) {
            if (!t.nodeType)
                return !1;
            this._target = t,
            this._tween = a,
            this._vars = e,
            u = e.autoRound,
            n = !1,
            i = e.suffixMap || s.suffixMap,
            r = V(t, ""),
            o = this._overwriteProps;
            var l, p, d, m, g, v, y, _, x, b = t.style;
            if (c && "" === b.zIndex && (l = Y(t, "zIndex", r),
            ("auto" === l || "" === l) && (b.zIndex = 0)),
            "string" == typeof e && (m = b.cssText,
            l = Q(t, r),
            b.cssText = m + ";" + e,
            l = J(t, l, Q(t)).difs,
            !H && w.test(e) && (l.opacity = parseFloat(RegExp.$1)),
            e = l,
            b.cssText = m),
            this._firstPT = p = this.parse(t, e, null),
            this._transformType) {
                for (x = 3 === this._transformType,
                _t ? h && (c = !0,
                "" === b.zIndex && (y = Y(t, "zIndex", r),
                ("auto" === y || "" === y) && (b.zIndex = 0)),
                f && (b.WebkitBackfaceVisibility = this._vars.WebkitBackfaceVisibility || (x ? "visible" : "hidden"))) : b.zoom = 1,
                d = p; d && d._next; )
                    d = d._next;
                _ = new pt(t,"transform",0,0,null,2),
                this._linkCSSP(_, null, d),
                _.setRatio = x && bt ? St : _t ? kt : Ct,
                _.data = this._transform || Tt(t, r, !0),
                o.pop()
            }
            if (n) {
                for (; p; ) {
                    for (v = p._next,
                    d = m; d && d.pr > p.pr; )
                        d = d._next;
                    (p._prev = d ? d._prev : g) ? p._prev._next = p : m = p,
                    (p._next = d) ? d._prev = p : g = p,
                    p = v
                }
                this._firstPT = m
            }
            return !0
        }
        ,
        l.parse = function(t, e, n, o) {
            var s, l, c, h, p, f, d, m, g, v, y = t.style;
            for (s in e)
                f = e[s],
                l = a[s],
                l ? n = l.parse(t, f, s, this, n, o, e) : (p = Y(t, s, r) + "",
                g = "string" == typeof f,
                "color" === s || "fill" === s || "stroke" === s || -1 !== s.indexOf("Color") || g && T.test(f) ? (g || (f = at(f),
                f = (f.length > 3 ? "rgba(" : "rgb(") + f.join(",") + ")"),
                n = ft(y, s, p, f, !0, "transparent", n, 0, o)) : !g || -1 === f.indexOf(" ") && -1 === f.indexOf(",") ? (c = parseFloat(p),
                d = c || 0 === c ? p.substr((c + "").length) : "",
                ("" === p || "auto" === p) && ("width" === s || "height" === s ? (c = tt(t, s, r),
                d = "px") : "left" === s || "top" === s ? (c = G(t, s, r),
                d = "px") : (c = "opacity" !== s ? 0 : 1,
                d = "")),
                v = g && "=" === f.charAt(1),
                v ? (h = parseInt(f.charAt(0) + "1", 10),
                f = f.substr(2),
                h *= parseFloat(f),
                m = f.replace(_, "")) : (h = parseFloat(f),
                m = g ? f.substr((h + "").length) || "" : ""),
                "" === m && (m = i[s] || d),
                f = h || 0 === h ? (v ? h + c : h) + m : e[s],
                d !== m && "" !== m && (h || 0 === h) && (c || 0 === c) && (c = U(t, s, c, d),
                "%" === m ? (c /= U(t, s, 100, "%") / 100,
                c > 100 && (c = 100),
                e.strictUnits !== !0 && (p = c + "%")) : "em" === m ? c /= U(t, s, 1, "em") : (h = U(t, s, h, m),
                m = "px"),
                v && (h || 0 === h) && (f = h + c + m)),
                v && (h += c),
                !c && 0 !== c || !h && 0 !== h ? void 0 !== y[s] && (f || "NaN" != f + "" && null != f) ? (n = new pt(y,s,h || c || 0,0,n,-1,s,!1,0,p,f),
                n.xs0 = "none" !== f || "display" !== s && -1 === s.indexOf("Style") ? f : p) : B("invalid " + s + " tween value: " + e[s]) : (n = new pt(y,s,c,h - c,n,0,s,u !== !1 && ("px" === m || "zIndex" === s),0,p,f),
                n.xs0 = m)) : n = ft(y, s, p, f, !0, null, n, 0, o)),
                o && n && !n.plugin && (n.plugin = o);
            return n
        }
        ,
        l.setRatio = function(t) {
            var e, n, i, r = this._firstPT, o = 1e-6;
            if (1 !== t || this._tween._time !== this._tween._duration && 0 !== this._tween._time)
                if (t || this._tween._time !== this._tween._duration && 0 !== this._tween._time || this._tween._rawPrevTime === -1e-6)
                    for (; r; ) {
                        if (e = r.c * t + r.s,
                        r.r ? e = e > 0 ? 0 | e + .5 : 0 | e - .5 : o > e && e > -o && (e = 0),
                        r.type)
                            if (1 === r.type)
                                if (i = r.l,
                                2 === i)
                                    r.t[r.p] = r.xs0 + e + r.xs1 + r.xn1 + r.xs2;
                                else if (3 === i)
                                    r.t[r.p] = r.xs0 + e + r.xs1 + r.xn1 + r.xs2 + r.xn2 + r.xs3;
                                else if (4 === i)
                                    r.t[r.p] = r.xs0 + e + r.xs1 + r.xn1 + r.xs2 + r.xn2 + r.xs3 + r.xn3 + r.xs4;
                                else if (5 === i)
                                    r.t[r.p] = r.xs0 + e + r.xs1 + r.xn1 + r.xs2 + r.xn2 + r.xs3 + r.xn3 + r.xs4 + r.xn4 + r.xs5;
                                else {
                                    for (n = r.xs0 + e + r.xs1,
                                    i = 1; r.l > i; i++)
                                        n += r["xn" + i] + r["xs" + (i + 1)];
                                    r.t[r.p] = n
                                }
                            else
                                -1 === r.type ? r.t[r.p] = r.xs0 : r.setRatio && r.setRatio(t);
                        else
                            r.t[r.p] = e + r.xs0;
                        r = r._next
                    }
                else
                    for (; r; )
                        2 !== r.type ? r.t[r.p] = r.b : r.setRatio(t),
                        r = r._next;
            else
                for (; r; )
                    2 !== r.type ? r.t[r.p] = r.e : r.setRatio(t),
                    r = r._next
        }
        ,
        l._enableTransforms = function(t) {
            this._transformType = t || 3 === this._transformType ? 3 : 2,
            this._transform = this._transform || Tt(this._target, r, !0)
        }
        ,
        l._linkCSSP = function(t, e, n, i) {
            return t && (e && (e._prev = t),
            t._next && (t._next._prev = t._prev),
            t._prev ? t._prev._next = t._next : this._firstPT === t && (this._firstPT = t._next,
            i = !0),
            n ? n._next = t : i || null !== this._firstPT || (this._firstPT = t),
            t._next = e,
            t._prev = n),
            t
        }
        ,
        l._kill = function(e) {
            var n, i, r, o = e;
            if (e.autoAlpha || e.alpha) {
                o = {};
                for (i in e)
                    o[i] = e[i];
                o.opacity = 1,
                o.autoAlpha && (o.visibility = 1)
            }
            return e.className && (n = this._classNamePT) && (r = n.xfirst,
            r && r._prev ? this._linkCSSP(r._prev, n._next, r._prev._prev) : r === this._firstPT && (this._firstPT = n._next),
            n._next && this._linkCSSP(n._next, n._next._next, r._prev),
            this._classNamePT = null),
            t.prototype._kill.call(this, o)
        }
        ;
        var Mt = function(t, e, n) {
            var i, r, o, s;
            if (t.slice)
                for (r = t.length; --r > -1; )
                    Mt(t[r], e, n);
            else
                for (i = t.childNodes,
                r = i.length; --r > -1; )
                    o = i[r],
                    s = o.type,
                    o.style && (e.push(Q(o)),
                    n && n.push(o)),
                    1 !== s && 9 !== s && 11 !== s || !o.childNodes.length || Mt(o, e, n)
        };
        return s.cascadeTo = function(t, n, i) {
            var r, o, s, a = e.to(t, n, i), l = [a], u = [], c = [], h = [], p = e._internals.reservedProps;
            for (t = a._targets || a.target,
            Mt(t, u, h),
            a.render(n, !0),
            Mt(t, c),
            a.render(0, !0),
            a._enabled(!0),
            r = h.length; --r > -1; )
                if (o = J(h[r], u[r], c[r]),
                o.firstMPT) {
                    o = o.difs;
                    for (s in i)
                        p[s] && (o[s] = i[s]);
                    l.push(e.to(h[r], n, o))
                }
            return l
        }
        ,
        t.activate([s]),
        s
    }, !0),
    function() {
        var t = window._gsDefine.plugin({
            propName: "roundProps",
            priority: -1,
            API: 2,
            init: function(t, e, n) {
                return this._tween = n,
                !0
            }
        })
          , e = t.prototype;
        e._onInitAllProps = function() {
            for (var t, e, n, i = this._tween, r = i.vars.roundProps instanceof Array ? i.vars.roundProps : i.vars.roundProps.split(","), o = r.length, s = {}, a = i._propLookup.roundProps; --o > -1; )
                s[r[o]] = 1;
            for (o = r.length; --o > -1; )
                for (t = r[o],
                e = i._firstPT; e; )
                    n = e._next,
                    e.pg ? e.t._roundProps(s, !0) : e.n === t && (this._add(e.t, t, e.s, e.c),
                    n && (n._prev = e._prev),
                    e._prev ? e._prev._next = n : i._firstPT === e && (i._firstPT = n),
                    e._next = e._prev = null,
                    i._propLookup[t] = a),
                    e = n;
            return !1
        }
        ,
        e._add = function(t, e, n, i) {
            this._addTween(t, e, n, n + i, e, !0),
            this._overwriteProps.push(e)
        }
    }(),
    window._gsDefine.plugin({
        propName: "attr",
        API: 2,
        init: function(t, e) {
            var n;
            if ("function" != typeof t.setAttribute)
                return !1;
            this._target = t,
            this._proxy = {};
            for (n in e)
                this._addTween(this._proxy, n, parseFloat(t.getAttribute(n)), e[n], n) && this._overwriteProps.push(n);
            return !0
        },
        set: function(t) {
            this._super.setRatio.call(this, t);
            for (var e, n = this._overwriteProps, i = n.length; --i > -1; )
                e = n[i],
                this._target.setAttribute(e, this._proxy[e] + "")
        }
    }),
    window._gsDefine.plugin({
        propName: "directionalRotation",
        API: 2,
        init: function(t, e) {
            "object" != typeof e && (e = {
                rotation: e
            }),
            this.finals = {};
            var n, i, r, o, s, a, l = e.useRadians === !0 ? 2 * Math.PI : 360, u = 1e-6;
            for (n in e)
                "useRadians" !== n && (a = (e[n] + "").split("_"),
                i = a[0],
                r = parseFloat("function" != typeof t[n] ? t[n] : t[n.indexOf("set") || "function" != typeof t["get" + n.substr(3)] ? n : "get" + n.substr(3)]()),
                o = this.finals[n] = "string" == typeof i && "=" === i.charAt(1) ? r + parseInt(i.charAt(0) + "1", 10) * Number(i.substr(2)) : Number(i) || 0,
                s = o - r,
                a.length && (i = a.join("_"),
                -1 !== i.indexOf("short") && (s %= l,
                s !== s % (l / 2) && (s = 0 > s ? s + l : s - l)),
                -1 !== i.indexOf("_cw") && 0 > s ? s = (s + 9999999999 * l) % l - (0 | s / l) * l : -1 !== i.indexOf("ccw") && s > 0 && (s = (s - 9999999999 * l) % l - (0 | s / l) * l)),
                (s > u || -u > s) && (this._addTween(t, n, r, r + s, n),
                this._overwriteProps.push(n)));
            return !0
        },
        set: function(t) {
            var e;
            if (1 !== t)
                this._super.setRatio.call(this, t);
            else
                for (e = this._firstPT; e; )
                    e.f ? e.t[e.p](this.finals[e.p]) : e.t[e.p] = this.finals[e.p],
                    e = e._next
        }
    })._autoCSS = !0,
    window._gsDefine("easing.Back", ["easing.Ease"], function(t) {
        var e, n, i, r = window.GreenSockGlobals || window, o = r.com.greensock, s = 2 * Math.PI, a = Math.PI / 2, l = o._class, u = function(e, n) {
            var i = l("easing." + e, function() {}, !0)
              , r = i.prototype = new t;
            return r.constructor = i,
            r.getRatio = n,
            i
        }, c = t.register || function() {}
        , h = function(t, e, n, i) {
            var r = l("easing." + t, {
                easeOut: new e,
                easeIn: new n,
                easeInOut: new i
            }, !0);
            return c(r, t),
            r
        }, p = function(t, e, n) {
            this.t = t,
            this.v = e,
            n && (this.next = n,
            n.prev = this,
            this.c = n.v - e,
            this.gap = n.t - t)
        }, f = function(e, n) {
            var i = l("easing." + e, function(t) {
                this._p1 = t || 0 === t ? t : 1.70158,
                this._p2 = 1.525 * this._p1
            }, !0)
              , r = i.prototype = new t;
            return r.constructor = i,
            r.getRatio = n,
            r.config = function(t) {
                return new i(t)
            }
            ,
            i
        }, d = h("Back", f("BackOut", function(t) {
            return (t -= 1) * t * ((this._p1 + 1) * t + this._p1) + 1
        }), f("BackIn", function(t) {
            return t * t * ((this._p1 + 1) * t - this._p1)
        }), f("BackInOut", function(t) {
            return 1 > (t *= 2) ? .5 * t * t * ((this._p2 + 1) * t - this._p2) : .5 * ((t -= 2) * t * ((this._p2 + 1) * t + this._p2) + 2)
        })), m = l("easing.SlowMo", function(t, e, n) {
            e = e || 0 === e ? e : .7,
            null == t ? t = .7 : t > 1 && (t = 1),
            this._p = 1 !== t ? e : 0,
            this._p1 = (1 - t) / 2,
            this._p2 = t,
            this._p3 = this._p1 + this._p2,
            this._calcEnd = n === !0
        }, !0), g = m.prototype = new t;
        return g.constructor = m,
        g.getRatio = function(t) {
            var e = t + (.5 - t) * this._p;
            return this._p1 > t ? this._calcEnd ? 1 - (t = 1 - t / this._p1) * t : e - (t = 1 - t / this._p1) * t * t * t * e : t > this._p3 ? this._calcEnd ? 1 - (t = (t - this._p3) / this._p1) * t : e + (t - e) * (t = (t - this._p3) / this._p1) * t * t * t : this._calcEnd ? 1 : e
        }
        ,
        m.ease = new m(.7,.7),
        g.config = m.config = function(t, e, n) {
            return new m(t,e,n)
        }
        ,
        e = l("easing.SteppedEase", function(t) {
            t = t || 1,
            this._p1 = 1 / t,
            this._p2 = t + 1
        }, !0),
        g = e.prototype = new t,
        g.constructor = e,
        g.getRatio = function(t) {
            return 0 > t ? t = 0 : t >= 1 && (t = .999999999),
            (this._p2 * t >> 0) * this._p1
        }
        ,
        g.config = e.config = function(t) {
            return new e(t)
        }
        ,
        n = l("easing.RoughEase", function(e) {
            e = e || {};
            for (var n, i, r, o, s, a, l = e.taper || "none", u = [], c = 0, h = 0 | (e.points || 20), f = h, d = e.randomize !== !1, m = e.clamp === !0, g = e.template instanceof t ? e.template : null, v = "number" == typeof e.strength ? .4 * e.strength : .4; --f > -1; )
                n = d ? Math.random() : 1 / h * f,
                i = g ? g.getRatio(n) : n,
                "none" === l ? r = v : "out" === l ? (o = 1 - n,
                r = o * o * v) : "in" === l ? r = n * n * v : .5 > n ? (o = 2 * n,
                r = .5 * o * o * v) : (o = 2 * (1 - n),
                r = .5 * o * o * v),
                d ? i += Math.random() * r - .5 * r : f % 2 ? i += .5 * r : i -= .5 * r,
                m && (i > 1 ? i = 1 : 0 > i && (i = 0)),
                u[c++] = {
                    x: n,
                    y: i
                };
            for (u.sort(function(t, e) {
                return t.x - e.x
            }),
            a = new p(1,1,null),
            f = h; --f > -1; )
                s = u[f],
                a = new p(s.x,s.y,a);
            this._prev = new p(0,0,0 !== a.t ? a : a.next)
        }, !0),
        g = n.prototype = new t,
        g.constructor = n,
        g.getRatio = function(t) {
            var e = this._prev;
            if (t > e.t) {
                for (; e.next && t >= e.t; )
                    e = e.next;
                e = e.prev
            } else
                for (; e.prev && e.t >= t; )
                    e = e.prev;
            return this._prev = e,
            e.v + (t - e.t) / e.gap * e.c
        }
        ,
        g.config = function(t) {
            return new n(t)
        }
        ,
        n.ease = new n,
        h("Bounce", u("BounceOut", function(t) {
            return 1 / 2.75 > t ? 7.5625 * t * t : 2 / 2.75 > t ? 7.5625 * (t -= 1.5 / 2.75) * t + .75 : 2.5 / 2.75 > t ? 7.5625 * (t -= 2.25 / 2.75) * t + .9375 : 7.5625 * (t -= 2.625 / 2.75) * t + .984375
        }), u("BounceIn", function(t) {
            return 1 / 2.75 > (t = 1 - t) ? 1 - 7.5625 * t * t : 2 / 2.75 > t ? 1 - (7.5625 * (t -= 1.5 / 2.75) * t + .75) : 2.5 / 2.75 > t ? 1 - (7.5625 * (t -= 2.25 / 2.75) * t + .9375) : 1 - (7.5625 * (t -= 2.625 / 2.75) * t + .984375)
        }), u("BounceInOut", function(t) {
            var e = .5 > t;
            return t = e ? 1 - 2 * t : 2 * t - 1,
            t = 1 / 2.75 > t ? 7.5625 * t * t : 2 / 2.75 > t ? 7.5625 * (t -= 1.5 / 2.75) * t + .75 : 2.5 / 2.75 > t ? 7.5625 * (t -= 2.25 / 2.75) * t + .9375 : 7.5625 * (t -= 2.625 / 2.75) * t + .984375,
            e ? .5 * (1 - t) : .5 * t + .5
        })),
        h("Circ", u("CircOut", function(t) {
            return Math.sqrt(1 - (t -= 1) * t)
        }), u("CircIn", function(t) {
            return -(Math.sqrt(1 - t * t) - 1)
        }), u("CircInOut", function(t) {
            return 1 > (t *= 2) ? -.5 * (Math.sqrt(1 - t * t) - 1) : .5 * (Math.sqrt(1 - (t -= 2) * t) + 1)
        })),
        i = function(e, n, i) {
            var r = l("easing." + e, function(t, e) {
                this._p1 = t || 1,
                this._p2 = e || i,
                this._p3 = this._p2 / s * (Math.asin(1 / this._p1) || 0)
            }, !0)
              , o = r.prototype = new t;
            return o.constructor = r,
            o.getRatio = n,
            o.config = function(t, e) {
                return new r(t,e)
            }
            ,
            r
        }
        ,
        h("Elastic", i("ElasticOut", function(t) {
            return this._p1 * Math.pow(2, -10 * t) * Math.sin((t - this._p3) * s / this._p2) + 1
        }, .3), i("ElasticIn", function(t) {
            return -(this._p1 * Math.pow(2, 10 * (t -= 1)) * Math.sin((t - this._p3) * s / this._p2))
        }, .3), i("ElasticInOut", function(t) {
            return 1 > (t *= 2) ? -.5 * this._p1 * Math.pow(2, 10 * (t -= 1)) * Math.sin((t - this._p3) * s / this._p2) : .5 * this._p1 * Math.pow(2, -10 * (t -= 1)) * Math.sin((t - this._p3) * s / this._p2) + 1
        }, .45)),
        h("Expo", u("ExpoOut", function(t) {
            return 1 - Math.pow(2, -10 * t)
        }), u("ExpoIn", function(t) {
            return Math.pow(2, 10 * (t - 1)) - .001
        }), u("ExpoInOut", function(t) {
            return 1 > (t *= 2) ? .5 * Math.pow(2, 10 * (t - 1)) : .5 * (2 - Math.pow(2, -10 * (t - 1)))
        })),
        h("Sine", u("SineOut", function(t) {
            return Math.sin(t * a)
        }), u("SineIn", function(t) {
            return -Math.cos(t * a) + 1
        }), u("SineInOut", function(t) {
            return -.5 * (Math.cos(Math.PI * t) - 1)
        })),
        l("easing.EaseLookup", {
            find: function(e) {
                return t.map[e]
            }
        }, !0),
        c(r.SlowMo, "SlowMo", "ease,"),
        c(n, "RoughEase", "ease,"),
        c(e, "SteppedEase", "ease,"),
        d
    }, !0)
}),
function(t) {
    "use strict";
    var e, n, i, r, o, s = t.GreenSockGlobals || t, a = function(t) {
        var e, n = t.split("."), i = s;
        for (e = 0; n.length > e; e++)
            i[n[e]] = i = i[n[e]] || {};
        return i
    }, l = a("com.greensock"), u = [].slice, c = function() {}, h = {}, p = function(e, n, i, r) {
        this.sc = h[e] ? h[e].sc : [],
        h[e] = this,
        this.gsClass = null,
        this.func = i;
        var o = [];
        this.check = function(l) {
            for (var u, c, f, d, m = n.length, g = m; --m > -1; )
                (u = h[n[m]] || new p(n[m],[])).gsClass ? (o[m] = u.gsClass,
                g--) : l && u.sc.push(this);
            if (0 === g && i)
                for (c = ("com.greensock." + e).split("."),
                f = c.pop(),
                d = a(c.join("."))[f] = this.gsClass = i.apply(i, o),
                r && (s[f] = d,
                "function" == typeof define && define.amd ? define((t.GreenSockAMDPath ? t.GreenSockAMDPath + "/" : "") + e.split(".").join("/"), [], function() {
                    return d
                }) : "undefined" != typeof module && module.exports && (module.exports = d)),
                m = 0; this.sc.length > m; m++)
                    this.sc[m].check()
        }
        ,
        this.check(!0)
    }, f = t._gsDefine = function(t, e, n, i) {
        return new p(t,e,n,i)
    }
    , d = l._class = function(t, e, n) {
        return e = e || function() {}
        ,
        f(t, [], function() {
            return e
        }, n),
        e
    }
    ;
    f.globals = s;
    var m = [0, 0, 1, 1]
      , g = []
      , v = d("easing.Ease", function(t, e, n, i) {
        this._func = t,
        this._type = n || 0,
        this._power = i || 0,
        this._params = e ? m.concat(e) : m
    }, !0)
      , y = v.map = {}
      , _ = v.register = function(t, e, n, i) {
        for (var r, o, s, a, u = e.split(","), c = u.length, h = (n || "easeIn,easeOut,easeInOut").split(","); --c > -1; )
            for (o = u[c],
            r = i ? d("easing." + o, null, !0) : l.easing[o] || {},
            s = h.length; --s > -1; )
                a = h[s],
                y[o + "." + a] = y[a + o] = r[a] = t.getRatio ? t : t[a] || new t
    }
    ;
    for (i = v.prototype,
    i._calcEnd = !1,
    i.getRatio = function(t) {
        if (this._func)
            return this._params[0] = t,
            this._func.apply(null, this._params);
        var e = this._type
          , n = this._power
          , i = 1 === e ? 1 - t : 2 === e ? t : .5 > t ? 2 * t : 2 * (1 - t);
        return 1 === n ? i *= i : 2 === n ? i *= i * i : 3 === n ? i *= i * i * i : 4 === n && (i *= i * i * i * i),
        1 === e ? 1 - i : 2 === e ? i : .5 > t ? i / 2 : 1 - i / 2
    }
    ,
    e = ["Linear", "Quad", "Cubic", "Quart", "Quint,Strong"],
    n = e.length; --n > -1; )
        i = e[n] + ",Power" + n,
        _(new v(null,null,1,n), i, "easeOut", !0),
        _(new v(null,null,2,n), i, "easeIn" + (0 === n ? ",easeNone" : "")),
        _(new v(null,null,3,n), i, "easeInOut");
    y.linear = l.easing.Linear.easeIn,
    y.swing = l.easing.Quad.easeInOut;
    var x = d("events.EventDispatcher", function(t) {
        this._listeners = {},
        this._eventTarget = t || this
    });
    i = x.prototype,
    i.addEventListener = function(t, e, n, i, s) {
        s = s || 0;
        var a, l, u = this._listeners[t], c = 0;
        for (null == u && (this._listeners[t] = u = []),
        l = u.length; --l > -1; )
            a = u[l],
            a.c === e && a.s === n ? u.splice(l, 1) : 0 === c && s > a.pr && (c = l + 1);
        u.splice(c, 0, {
            c: e,
            s: n,
            up: i,
            pr: s
        }),
        this !== r || o || r.wake()
    }
    ,
    i.removeEventListener = function(t, e) {
        var n, i = this._listeners[t];
        if (i)
            for (n = i.length; --n > -1; )
                if (i[n].c === e)
                    return void i.splice(n, 1)
    }
    ,
    i.dispatchEvent = function(t) {
        var e, n, i, r = this._listeners[t];
        if (r)
            for (e = r.length,
            n = this._eventTarget; --e > -1; )
                i = r[e],
                i.up ? i.c.call(i.s || n, {
                    type: t,
                    target: n
                }) : i.c.call(i.s || n)
    }
    ;
    var w = t.requestAnimationFrame
      , b = t.cancelAnimationFrame
      , T = Date.now || function() {
        return (new Date).getTime()
    }
      , C = T();
    for (e = ["ms", "moz", "webkit", "o"],
    n = e.length; --n > -1 && !w; )
        w = t[e[n] + "RequestAnimationFrame"],
        b = t[e[n] + "CancelAnimationFrame"] || t[e[n] + "CancelRequestAnimationFrame"];
    d("Ticker", function(t, e) {
        var n, i, s, a, l, u = this, h = T(), p = e !== !1 && w, f = function(t) {
            C = T(),
            u.time = (C - h) / 1e3;
            var e, r = u.time - l;
            (!n || r > 0 || t === !0) && (u.frame++,
            l += r + (r >= a ? .004 : a - r),
            e = !0),
            t !== !0 && (s = i(f)),
            e && u.dispatchEvent("tick")
        };
        x.call(u),
        this.time = this.frame = 0,
        this.tick = function() {
            f(!0)
        }
        ,
        this.sleep = function() {
            null != s && (p && b ? b(s) : clearTimeout(s),
            i = c,
            s = null,
            u === r && (o = !1))
        }
        ,
        this.wake = function() {
            null !== s && u.sleep(),
            i = 0 === n ? c : p && w ? w : function(t) {
                return setTimeout(t, 0 | 1e3 * (l - u.time) + 1)
            }
            ,
            u === r && (o = !0),
            f(2)
        }
        ,
        this.fps = function(t) {
            return arguments.length ? (n = t,
            a = 1 / (n || 60),
            l = this.time + a,
            void u.wake()) : n
        }
        ,
        this.useRAF = function(t) {
            return arguments.length ? (u.sleep(),
            p = t,
            void u.fps(n)) : p
        }
        ,
        u.fps(t),
        setTimeout(function() {
            p && (!s || 5 > u.frame) && u.useRAF(!1)
        }, 1500)
    }),
    i = l.Ticker.prototype = new l.events.EventDispatcher,
    i.constructor = l.Ticker;
    var S = d("core.Animation", function(t, e) {
        if (this.vars = e = e || {},
        this._duration = this._totalDuration = t || 0,
        this._delay = Number(e.delay) || 0,
        this._timeScale = 1,
        this._active = e.immediateRender === !0,
        this.data = e.data,
        this._reversed = e.reversed === !0,
        I) {
            o || r.wake();
            var n = this.vars.useFrames ? $ : I;
            n.add(this, n._time),
            this.vars.paused && this.paused(!0)
        }
    });
    r = S.ticker = new l.Ticker,
    i = S.prototype,
    i._dirty = i._gc = i._initted = i._paused = !1,
    i._totalTime = i._time = 0,
    i._rawPrevTime = -1,
    i._next = i._last = i._onUpdate = i._timeline = i.timeline = null,
    i._paused = !1;
    var k = function() {
        T() - C > 2e3 && r.wake(),
        setTimeout(k, 2e3)
    };
    k(),
    i.play = function(t, e) {
        return arguments.length && this.seek(t, e),
        this.reversed(!1).paused(!1)
    }
    ,
    i.pause = function(t, e) {
        return arguments.length && this.seek(t, e),
        this.paused(!0)
    }
    ,
    i.resume = function(t, e) {
        return arguments.length && this.seek(t, e),
        this.paused(!1)
    }
    ,
    i.seek = function(t, e) {
        return this.totalTime(Number(t), e !== !1)
    }
    ,
    i.restart = function(t, e) {
        return this.reversed(!1).paused(!1).totalTime(t ? -this._delay : 0, e !== !1, !0)
    }
    ,
    i.reverse = function(t, e) {
        return arguments.length && this.seek(t || this.totalDuration(), e),
        this.reversed(!0).paused(!1)
    }
    ,
    i.render = function() {}
    ,
    i.invalidate = function() {
        return this
    }
    ,
    i._enabled = function(t, e) {
        return o || r.wake(),
        this._gc = !t,
        this._active = t && !this._paused && this._totalTime > 0 && this._totalTime < this._totalDuration,
        e !== !0 && (t && !this.timeline ? this._timeline.add(this, this._startTime - this._delay) : !t && this.timeline && this._timeline._remove(this, !0)),
        !1
    }
    ,
    i._kill = function() {
        return this._enabled(!1, !1)
    }
    ,
    i.kill = function(t, e) {
        return this._kill(t, e),
        this
    }
    ,
    i._uncache = function(t) {
        for (var e = t ? this : this.timeline; e; )
            e._dirty = !0,
            e = e.timeline;
        return this
    }
    ,
    i._swapSelfInParams = function(t) {
        for (var e = t.length, n = t.concat(); --e > -1; )
            "{self}" === t[e] && (n[e] = this);
        return n
    }
    ,
    i.eventCallback = function(t, e, n, i) {
        if ("on" === (t || "").substr(0, 2)) {
            var r = this.vars;
            if (1 === arguments.length)
                return r[t];
            null == e ? delete r[t] : (r[t] = e,
            r[t + "Params"] = n instanceof Array && -1 !== n.join("").indexOf("{self}") ? this._swapSelfInParams(n) : n,
            r[t + "Scope"] = i),
            "onUpdate" === t && (this._onUpdate = e)
        }
        return this
    }
    ,
    i.delay = function(t) {
        return arguments.length ? (this._timeline.smoothChildTiming && this.startTime(this._startTime + t - this._delay),
        this._delay = t,
        this) : this._delay
    }
    ,
    i.duration = function(t) {
        return arguments.length ? (this._duration = this._totalDuration = t,
        this._uncache(!0),
        this._timeline.smoothChildTiming && this._time > 0 && this._time < this._duration && 0 !== t && this.totalTime(this._totalTime * (t / this._duration), !0),
        this) : (this._dirty = !1,
        this._duration)
    }
    ,
    i.totalDuration = function(t) {
        return this._dirty = !1,
        arguments.length ? this.duration(t) : this._totalDuration
    }
    ,
    i.time = function(t, e) {
        return arguments.length ? (this._dirty && this.totalDuration(),
        this.totalTime(t > this._duration ? this._duration : t, e)) : this._time
    }
    ,
    i.totalTime = function(t, e, n) {
        if (o || r.wake(),
        !arguments.length)
            return this._totalTime;
        if (this._timeline) {
            if (0 > t && !n && (t += this.totalDuration()),
            this._timeline.smoothChildTiming) {
                this._dirty && this.totalDuration();
                var i = this._totalDuration
                  , s = this._timeline;
                if (t > i && !n && (t = i),
                this._startTime = (this._paused ? this._pauseTime : s._time) - (this._reversed ? i - t : t) / this._timeScale,
                s._dirty || this._uncache(!1),
                s._timeline)
                    for (; s._timeline; )
                        s._timeline._time !== (s._startTime + s._totalTime) / s._timeScale && s.totalTime(s._totalTime, !0),
                        s = s._timeline
            }
            this._gc && this._enabled(!0, !1),
            this._totalTime !== t && this.render(t, e, !1)
        }
        return this
    }
    ,
    i.startTime = function(t) {
        return arguments.length ? (t !== this._startTime && (this._startTime = t,
        this.timeline && this.timeline._sortChildren && this.timeline.add(this, t - this._delay)),
        this) : this._startTime
    }
    ,
    i.timeScale = function(t) {
        if (!arguments.length)
            return this._timeScale;
        if (t = t || 1e-6,
        this._timeline && this._timeline.smoothChildTiming) {
            var e = this._pauseTime
              , n = e || 0 === e ? e : this._timeline.totalTime();
            this._startTime = n - (n - this._startTime) * this._timeScale / t
        }
        return this._timeScale = t,
        this._uncache(!1)
    }
    ,
    i.reversed = function(t) {
        return arguments.length ? (t != this._reversed && (this._reversed = t,
        this.totalTime(this._totalTime, !0)),
        this) : this._reversed
    }
    ,
    i.paused = function(t) {
        if (!arguments.length)
            return this._paused;
        if (t != this._paused && this._timeline) {
            o || t || r.wake();
            var e = this._timeline
              , n = e.rawTime()
              , i = n - this._pauseTime;
            !t && e.smoothChildTiming && (this._startTime += i,
            this._uncache(!1)),
            this._pauseTime = t ? n : null,
            this._paused = t,
            this._active = !t && this._totalTime > 0 && this._totalTime < this._totalDuration,
            t || 0 === i || 0 === this._duration || this.render(e.smoothChildTiming ? this._totalTime : (n - this._startTime) / this._timeScale, !0, !0)
        }
        return this._gc && !t && this._enabled(!0, !1),
        this
    }
    ;
    var j = d("core.SimpleTimeline", function(t) {
        S.call(this, 0, t),
        this.autoRemoveChildren = this.smoothChildTiming = !0
    });
    i = j.prototype = new S,
    i.constructor = j,
    i.kill()._gc = !1,
    i._first = i._last = null,
    i._sortChildren = !1,
    i.add = i.insert = function(t, e) {
        var n, i;
        if (t._startTime = Number(e || 0) + t._delay,
        t._paused && this !== t._timeline && (t._pauseTime = t._startTime + (this.rawTime() - t._startTime) / t._timeScale),
        t.timeline && t.timeline._remove(t, !0),
        t.timeline = t._timeline = this,
        t._gc && t._enabled(!0, !0),
        n = this._last,
        this._sortChildren)
            for (i = t._startTime; n && n._startTime > i; )
                n = n._prev;
        return n ? (t._next = n._next,
        n._next = t) : (t._next = this._first,
        this._first = t),
        t._next ? t._next._prev = t : this._last = t,
        t._prev = n,
        this._timeline && this._uncache(!0),
        this
    }
    ,
    i._remove = function(t, e) {
        return t.timeline === this && (e || t._enabled(!1, !0),
        t.timeline = null,
        t._prev ? t._prev._next = t._next : this._first === t && (this._first = t._next),
        t._next ? t._next._prev = t._prev : this._last === t && (this._last = t._prev),
        this._timeline && this._uncache(!0)),
        this
    }
    ,
    i.render = function(t, e, n) {
        var i, r = this._first;
        for (this._totalTime = this._time = this._rawPrevTime = t; r; )
            i = r._next,
            (r._active || t >= r._startTime && !r._paused) && (r._reversed ? r.render((r._dirty ? r.totalDuration() : r._totalDuration) - (t - r._startTime) * r._timeScale, e, n) : r.render((t - r._startTime) * r._timeScale, e, n)),
            r = i
    }
    ,
    i.rawTime = function() {
        return o || r.wake(),
        this._totalTime
    }
    ;
    var P = d("TweenLite", function(e, n, i) {
        if (S.call(this, n, i),
        this.render = P.prototype.render,
        null == e)
            throw "Cannot tween a null target.";
        this.target = e = "string" != typeof e ? e : P.selector(e) || e;
        var r, o, s, a = e.jquery || e.length && e !== t && e[0] && (e[0] === t || e[0].nodeType && e[0].style && !e.nodeType), l = this.vars.overwrite;
        if (this._overwrite = l = null == l ? R[P.defaultOverwrite] : "number" == typeof l ? l >> 0 : R[l],
        (a || e instanceof Array) && "number" != typeof e[0])
            for (this._targets = s = u.call(e, 0),
            this._propLookup = [],
            this._siblings = [],
            r = 0; s.length > r; r++)
                o = s[r],
                o ? "string" != typeof o ? o.length && o !== t && o[0] && (o[0] === t || o[0].nodeType && o[0].style && !o.nodeType) ? (s.splice(r--, 1),
                this._targets = s = s.concat(u.call(o, 0))) : (this._siblings[r] = q(o, this, !1),
                1 === l && this._siblings[r].length > 1 && H(o, this, null, 1, this._siblings[r])) : (o = s[r--] = P.selector(o),
                "string" == typeof o && s.splice(r + 1, 1)) : s.splice(r--, 1);
        else
            this._propLookup = {},
            this._siblings = q(e, this, !1),
            1 === l && this._siblings.length > 1 && H(e, this, null, 1, this._siblings);
        (this.vars.immediateRender || 0 === n && 0 === this._delay && this.vars.immediateRender !== !1) && this.render(-this._delay, !1, !0)
    }, !0)
      , E = function(e) {
        return e.length && e !== t && e[0] && (e[0] === t || e[0].nodeType && e[0].style && !e.nodeType)
    }
      , O = function(t, e) {
        var n, i = {};
        for (n in t)
            L[n] || n in e && "x" !== n && "y" !== n && "width" !== n && "height" !== n && "className" !== n && "border" !== n || !(!D[n] || D[n] && D[n]._autoCSS) || (i[n] = t[n],
            delete t[n]);
        t.css = i
    };
    i = P.prototype = new S,
    i.constructor = P,
    i.kill()._gc = !1,
    i.ratio = 0,
    i._firstPT = i._targets = i._overwrittenProps = i._startAt = null,
    i._notifyPluginsOfEnabled = !1,
    P.version = "1.10.2",
    P.defaultEase = i._ease = new v(null,null,1,1),
    P.defaultOverwrite = "auto",
    P.ticker = r,
    P.autoSleep = !0,
    P.selector = t.$ || t.jQuery || function(e) {
        return t.$ ? (P.selector = t.$,
        t.$(e)) : t.document ? t.document.getElementById("#" === e.charAt(0) ? e.substr(1) : e) : e
    }
    ;
    var M = P._internals = {}
      , D = P._plugins = {}
      , A = P._tweenLookup = {}
      , N = 0
      , L = M.reservedProps = {
        ease: 1,
        delay: 1,
        overwrite: 1,
        onComplete: 1,
        onCompleteParams: 1,
        onCompleteScope: 1,
        useFrames: 1,
        runBackwards: 1,
        startAt: 1,
        onUpdate: 1,
        onUpdateParams: 1,
        onUpdateScope: 1,
        onStart: 1,
        onStartParams: 1,
        onStartScope: 1,
        onReverseComplete: 1,
        onReverseCompleteParams: 1,
        onReverseCompleteScope: 1,
        onRepeat: 1,
        onRepeatParams: 1,
        onRepeatScope: 1,
        easeParams: 1,
        yoyo: 1,
        immediateRender: 1,
        repeat: 1,
        repeatDelay: 1,
        data: 1,
        paused: 1,
        reversed: 1,
        autoCSS: 1
    }
      , R = {
        none: 0,
        all: 1,
        auto: 2,
        concurrent: 3,
        allOnStart: 4,
        preexisting: 5,
        "true": 1,
        "false": 0
    }
      , $ = S._rootFramesTimeline = new j
      , I = S._rootTimeline = new j;
    I._startTime = r.time,
    $._startTime = r.frame,
    I._active = $._active = !0,
    S._updateRoot = function() {
        if (I.render((r.time - I._startTime) * I._timeScale, !1, !1),
        $.render((r.frame - $._startTime) * $._timeScale, !1, !1),
        !(r.frame % 120)) {
            var t, e, n;
            for (n in A) {
                for (e = A[n].tweens,
                t = e.length; --t > -1; )
                    e[t]._gc && e.splice(t, 1);
                0 === e.length && delete A[n]
            }
            if (n = I._first,
            (!n || n._paused) && P.autoSleep && !$._first && 1 === r._listeners.tick.length) {
                for (; n && n._paused; )
                    n = n._next;
                n || r.sleep()
            }
        }
    }
    ,
    r.addEventListener("tick", S._updateRoot);
    var q = function(t, e, n) {
        var i, r, o = t._gsTweenID;
        if (A[o || (t._gsTweenID = o = "t" + N++)] || (A[o] = {
            target: t,
            tweens: []
        }),
        e && (i = A[o].tweens,
        i[r = i.length] = e,
        n))
            for (; --r > -1; )
                i[r] === e && i.splice(r, 1);
        return A[o].tweens
    }
      , H = function(t, e, n, i, r) {
        var o, s, a, l;
        if (1 === i || i >= 4) {
            for (l = r.length,
            o = 0; l > o; o++)
                if ((a = r[o]) !== e)
                    a._gc || a._enabled(!1, !1) && (s = !0);
                else if (5 === i)
                    break;
            return s
        }
        var u, c = e._startTime + 1e-10, h = [], p = 0, f = 0 === e._duration;
        for (o = r.length; --o > -1; )
            (a = r[o]) === e || a._gc || a._paused || (a._timeline !== e._timeline ? (u = u || F(e, 0, f),
            0 === F(a, u, f) && (h[p++] = a)) : c >= a._startTime && a._startTime + a.totalDuration() / a._timeScale + 1e-10 > c && ((f || !a._initted) && 2e-10 >= c - a._startTime || (h[p++] = a)));
        for (o = p; --o > -1; )
            a = h[o],
            2 === i && a._kill(n, t) && (s = !0),
            (2 !== i || !a._firstPT && a._initted) && a._enabled(!1, !1) && (s = !0);
        return s
    }
      , F = function(t, e, n) {
        for (var i = t._timeline, r = i._timeScale, o = t._startTime, s = 1e-10; i._timeline; ) {
            if (o += i._startTime,
            r *= i._timeScale,
            i._paused)
                return -100;
            i = i._timeline
        }
        return o /= r,
        o > e ? o - e : n && o === e || !t._initted && 2 * s > o - e ? s : (o += t.totalDuration() / t._timeScale / r) > e + s ? 0 : o - e - s
    };
    i._init = function() {
        var t, e, n, i, r = this.vars, o = this._overwrittenProps, s = this._duration, a = r.immediateRender, l = r.ease;
        if (r.startAt) {
            if (this._startAt && this._startAt.render(-1, !0),
            r.startAt.overwrite = 0,
            r.startAt.immediateRender = !0,
            this._startAt = P.to(this.target, 0, r.startAt),
            a)
                if (this._time > 0)
                    this._startAt = null;
                else if (0 !== s)
                    return
        } else if (r.runBackwards && r.immediateRender && 0 !== s)
            if (this._startAt)
                this._startAt.render(-1, !0),
                this._startAt = null;
            else if (0 === this._time) {
                n = {};
                for (i in r)
                    L[i] && "autoCSS" !== i || (n[i] = r[i]);
                return n.overwrite = 0,
                void (this._startAt = P.to(this.target, 0, n))
            }
        if (this._ease = l ? l instanceof v ? r.easeParams instanceof Array ? l.config.apply(l, r.easeParams) : l : "function" == typeof l ? new v(l,r.easeParams) : y[l] || P.defaultEase : P.defaultEase,
        this._easeType = this._ease._type,
        this._easePower = this._ease._power,
        this._firstPT = null,
        this._targets)
            for (t = this._targets.length; --t > -1; )
                this._initProps(this._targets[t], this._propLookup[t] = {}, this._siblings[t], o ? o[t] : null) && (e = !0);
        else
            e = this._initProps(this.target, this._propLookup, this._siblings, o);
        if (e && P._onPluginEvent("_onInitAllProps", this),
        o && (this._firstPT || "function" != typeof this.target && this._enabled(!1, !1)),
        r.runBackwards)
            for (n = this._firstPT; n; )
                n.s += n.c,
                n.c = -n.c,
                n = n._next;
        this._onUpdate = r.onUpdate,
        this._initted = !0
    }
    ,
    i._initProps = function(e, n, i, r) {
        var o, s, a, l, u, c;
        if (null == e)
            return !1;
        this.vars.css || e.style && e !== t && e.nodeType && D.css && this.vars.autoCSS !== !1 && O(this.vars, e);
        for (o in this.vars) {
            if (c = this.vars[o],
            L[o])
                c instanceof Array && -1 !== c.join("").indexOf("{self}") && (this.vars[o] = c = this._swapSelfInParams(c, this));
            else if (D[o] && (l = new D[o])._onInitTween(e, this.vars[o], this)) {
                for (this._firstPT = u = {
                    _next: this._firstPT,
                    t: l,
                    p: "setRatio",
                    s: 0,
                    c: 1,
                    f: !0,
                    n: o,
                    pg: !0,
                    pr: l._priority
                },
                s = l._overwriteProps.length; --s > -1; )
                    n[l._overwriteProps[s]] = this._firstPT;
                (l._priority || l._onInitAllProps) && (a = !0),
                (l._onDisable || l._onEnable) && (this._notifyPluginsOfEnabled = !0)
            } else
                this._firstPT = n[o] = u = {
                    _next: this._firstPT,
                    t: e,
                    p: o,
                    f: "function" == typeof e[o],
                    n: o,
                    pg: !1,
                    pr: 0
                },
                u.s = u.f ? e[o.indexOf("set") || "function" != typeof e["get" + o.substr(3)] ? o : "get" + o.substr(3)]() : parseFloat(e[o]),
                u.c = "string" == typeof c && "=" === c.charAt(1) ? parseInt(c.charAt(0) + "1", 10) * Number(c.substr(2)) : Number(c) - u.s || 0;
            u && u._next && (u._next._prev = u)
        }
        return r && this._kill(r, e) ? this._initProps(e, n, i, r) : this._overwrite > 1 && this._firstPT && i.length > 1 && H(e, this, n, this._overwrite, i) ? (this._kill(n, e),
        this._initProps(e, n, i, r)) : a
    }
    ,
    i.render = function(t, e, n) {
        var i, r, o, s = this._time;
        if (t >= this._duration)
            this._totalTime = this._time = this._duration,
            this.ratio = this._ease._calcEnd ? this._ease.getRatio(1) : 1,
            this._reversed || (i = !0,
            r = "onComplete"),
            0 === this._duration && ((0 === t || 0 > this._rawPrevTime) && this._rawPrevTime !== t && (n = !0,
            this._rawPrevTime > 0 && (r = "onReverseComplete",
            e && (t = -1))),
            this._rawPrevTime = t);
        else if (1e-7 > t)
            this._totalTime = this._time = 0,
            this.ratio = this._ease._calcEnd ? this._ease.getRatio(0) : 0,
            (0 !== s || 0 === this._duration && this._rawPrevTime > 0) && (r = "onReverseComplete",
            i = this._reversed),
            0 > t ? (this._active = !1,
            0 === this._duration && (this._rawPrevTime >= 0 && (n = !0),
            this._rawPrevTime = t)) : this._initted || (n = !0);
        else if (this._totalTime = this._time = t,
        this._easeType) {
            var a = t / this._duration
              , l = this._easeType
              , u = this._easePower;
            (1 === l || 3 === l && a >= .5) && (a = 1 - a),
            3 === l && (a *= 2),
            1 === u ? a *= a : 2 === u ? a *= a * a : 3 === u ? a *= a * a * a : 4 === u && (a *= a * a * a * a),
            this.ratio = 1 === l ? 1 - a : 2 === l ? a : .5 > t / this._duration ? a / 2 : 1 - a / 2
        } else
            this.ratio = this._ease.getRatio(t / this._duration);
        if (this._time !== s || n) {
            if (!this._initted) {
                if (this._init(),
                !this._initted)
                    return;
                this._time && !i ? this.ratio = this._ease.getRatio(this._time / this._duration) : i && this._ease._calcEnd && (this.ratio = this._ease.getRatio(0 === this._time ? 0 : 1))
            }
            for (this._active || !this._paused && this._time !== s && t >= 0 && (this._active = !0),
            0 === s && (this._startAt && (t >= 0 ? this._startAt.render(t, e, n) : r || (r = "_dummyGS")),
            this.vars.onStart && (0 !== this._time || 0 === this._duration) && (e || this.vars.onStart.apply(this.vars.onStartScope || this, this.vars.onStartParams || g))),
            o = this._firstPT; o; )
                o.f ? o.t[o.p](o.c * this.ratio + o.s) : o.t[o.p] = o.c * this.ratio + o.s,
                o = o._next;
            this._onUpdate && (0 > t && this._startAt && this._startAt.render(t, e, n),
            e || this._onUpdate.apply(this.vars.onUpdateScope || this, this.vars.onUpdateParams || g)),
            r && (this._gc || (0 > t && this._startAt && !this._onUpdate && this._startAt.render(t, e, n),
            i && (this._timeline.autoRemoveChildren && this._enabled(!1, !1),
            this._active = !1),
            !e && this.vars[r] && this.vars[r].apply(this.vars[r + "Scope"] || this, this.vars[r + "Params"] || g)))
        }
    }
    ,
    i._kill = function(t, e) {
        if ("all" === t && (t = null),
        null == t && (null == e || e === this.target))
            return this._enabled(!1, !1);
        e = "string" != typeof e ? e || this._targets || this.target : P.selector(e) || e;
        var n, i, r, o, s, a, l, u;
        if ((e instanceof Array || E(e)) && "number" != typeof e[0])
            for (n = e.length; --n > -1; )
                this._kill(t, e[n]) && (a = !0);
        else {
            if (this._targets) {
                for (n = this._targets.length; --n > -1; )
                    if (e === this._targets[n]) {
                        s = this._propLookup[n] || {},
                        this._overwrittenProps = this._overwrittenProps || [],
                        i = this._overwrittenProps[n] = t ? this._overwrittenProps[n] || {} : "all";
                        break
                    }
            } else {
                if (e !== this.target)
                    return !1;
                s = this._propLookup,
                i = this._overwrittenProps = t ? this._overwrittenProps || {} : "all"
            }
            if (s) {
                l = t || s,
                u = t !== i && "all" !== i && t !== s && (null == t || t._tempKill !== !0);
                for (r in l)
                    (o = s[r]) && (o.pg && o.t._kill(l) && (a = !0),
                    o.pg && 0 !== o.t._overwriteProps.length || (o._prev ? o._prev._next = o._next : o === this._firstPT && (this._firstPT = o._next),
                    o._next && (o._next._prev = o._prev),
                    o._next = o._prev = null),
                    delete s[r]),
                    u && (i[r] = 1);
                !this._firstPT && this._initted && this._enabled(!1, !1)
            }
        }
        return a
    }
    ,
    i.invalidate = function() {
        return this._notifyPluginsOfEnabled && P._onPluginEvent("_onDisable", this),
        this._firstPT = null,
        this._overwrittenProps = null,
        this._onUpdate = null,
        this._startAt = null,
        this._initted = this._active = this._notifyPluginsOfEnabled = !1,
        this._propLookup = this._targets ? {} : [],
        this
    }
    ,
    i._enabled = function(t, e) {
        if (o || r.wake(),
        t && this._gc) {
            var n, i = this._targets;
            if (i)
                for (n = i.length; --n > -1; )
                    this._siblings[n] = q(i[n], this, !0);
            else
                this._siblings = q(this.target, this, !0)
        }
        return S.prototype._enabled.call(this, t, e),
        this._notifyPluginsOfEnabled && this._firstPT ? P._onPluginEvent(t ? "_onEnable" : "_onDisable", this) : !1
    }
    ,
    P.to = function(t, e, n) {
        return new P(t,e,n)
    }
    ,
    P.from = function(t, e, n) {
        return n.runBackwards = !0,
        n.immediateRender = 0 != n.immediateRender,
        new P(t,e,n)
    }
    ,
    P.fromTo = function(t, e, n, i) {
        return i.startAt = n,
        i.immediateRender = 0 != i.immediateRender && 0 != n.immediateRender,
        new P(t,e,i)
    }
    ,
    P.delayedCall = function(t, e, n, i, r) {
        return new P(e,0,{
            delay: t,
            onComplete: e,
            onCompleteParams: n,
            onCompleteScope: i,
            onReverseComplete: e,
            onReverseCompleteParams: n,
            onReverseCompleteScope: i,
            immediateRender: !1,
            useFrames: r,
            overwrite: 0
        })
    }
    ,
    P.set = function(t, e) {
        return new P(t,0,e)
    }
    ,
    P.killTweensOf = P.killDelayedCallsTo = function(t, e) {
        for (var n = P.getTweensOf(t), i = n.length; --i > -1; )
            n[i]._kill(e, t)
    }
    ,
    P.getTweensOf = function(t) {
        if (null == t)
            return [];
        t = "string" != typeof t ? t : P.selector(t) || t;
        var e, n, i, r;
        if ((t instanceof Array || E(t)) && "number" != typeof t[0]) {
            for (e = t.length,
            n = []; --e > -1; )
                n = n.concat(P.getTweensOf(t[e]));
            for (e = n.length; --e > -1; )
                for (r = n[e],
                i = e; --i > -1; )
                    r === n[i] && n.splice(e, 1)
        } else
            for (n = q(t).concat(),
            e = n.length; --e > -1; )
                n[e]._gc && n.splice(e, 1);
        return n
    }
    ;
    var B = d("plugins.TweenPlugin", function(t, e) {
        this._overwriteProps = (t || "").split(","),
        this._propName = this._overwriteProps[0],
        this._priority = e || 0,
        this._super = B.prototype
    }, !0);
    if (i = B.prototype,
    B.version = "1.10.1",
    B.API = 2,
    i._firstPT = null,
    i._addTween = function(t, e, n, i, r, o) {
        var s, a;
        return null != i && (s = "number" == typeof i || "=" !== i.charAt(1) ? Number(i) - n : parseInt(i.charAt(0) + "1", 10) * Number(i.substr(2))) ? (this._firstPT = a = {
            _next: this._firstPT,
            t: t,
            p: e,
            s: n,
            c: s,
            f: "function" == typeof t[e],
            n: r || e,
            r: o
        },
        a._next && (a._next._prev = a),
        a) : void 0
    }
    ,
    i.setRatio = function(t) {
        for (var e, n = this._firstPT, i = 1e-6; n; )
            e = n.c * t + n.s,
            n.r ? e = 0 | e + (e > 0 ? .5 : -.5) : i > e && e > -i && (e = 0),
            n.f ? n.t[n.p](e) : n.t[n.p] = e,
            n = n._next
    }
    ,
    i._kill = function(t) {
        var e, n = this._overwriteProps, i = this._firstPT;
        if (null != t[this._propName])
            this._overwriteProps = [];
        else
            for (e = n.length; --e > -1; )
                null != t[n[e]] && n.splice(e, 1);
        for (; i; )
            null != t[i.n] && (i._next && (i._next._prev = i._prev),
            i._prev ? (i._prev._next = i._next,
            i._prev = null) : this._firstPT === i && (this._firstPT = i._next)),
            i = i._next;
        return !1
    }
    ,
    i._roundProps = function(t, e) {
        for (var n = this._firstPT; n; )
            (t[this._propName] || null != n.n && t[n.n.split(this._propName + "_").join("")]) && (n.r = e),
            n = n._next
    }
    ,
    P._onPluginEvent = function(t, e) {
        var n, i, r, o, s, a = e._firstPT;
        if ("_onInitAllProps" === t) {
            for (; a; ) {
                for (s = a._next,
                i = r; i && i.pr > a.pr; )
                    i = i._next;
                (a._prev = i ? i._prev : o) ? a._prev._next = a : r = a,
                (a._next = i) ? i._prev = a : o = a,
                a = s
            }
            a = e._firstPT = r
        }
        for (; a; )
            a.pg && "function" == typeof a.t[t] && a.t[t]() && (n = !0),
            a = a._next;
        return n
    }
    ,
    B.activate = function(t) {
        for (var e = t.length; --e > -1; )
            t[e].API === B.API && (D[(new t[e])._propName] = t[e]);
        return !0
    }
    ,
    f.plugin = function(t) {
        if (!(t && t.propName && t.init && t.API))
            throw "illegal plugin definition.";
        var e, n = t.propName, i = t.priority || 0, r = t.overwriteProps, o = {
            init: "_onInitTween",
            set: "setRatio",
            kill: "_kill",
            round: "_roundProps",
            initAll: "_onInitAllProps"
        }, s = d("plugins." + n.charAt(0).toUpperCase() + n.substr(1) + "Plugin", function() {
            B.call(this, n, i),
            this._overwriteProps = r || []
        }, t.global === !0), a = s.prototype = new B(n);
        a.constructor = s,
        s.API = t.API;
        for (e in o)
            "function" == typeof t[e] && (a[o[e]] = t[e]);
        return s.version = t.version,
        B.activate([s]),
        s
    }
    ,
    e = t._gsQueue) {
        for (n = 0; e.length > n; n++)
            e[n]();
        for (i in h)
            h[i].func || t.console.log("GSAP encountered missing dependency: com.greensock." + i)
    }
    o = !1
}(window),
define("greensock", function() {});
var extend = function(t, e) {
    function n() {
        this.constructor = t
    }
    for (var i in e)
        hasProp.call(e, i) && (t[i] = e[i]);
    return n.prototype = e.prototype,
    t.prototype = new n,
    t.__super__ = e.prototype,
    t
}
  , hasProp = {}.hasOwnProperty;
define("views/loader/loader", ["jquery", "underscore", "backbone", "views/abstract", "collections/projects", "config", "greensock"], function(t, e, n, i, r, o) {
    var s;
    return s = function(e) {
        function i() {
            return i.__super__.constructor.apply(this, arguments)
        }
        return extend(i, e),
        i.prototype.id = "loaderView",
        i.prototype.el = t("#loaderView").get(0),
        i.prototype.progress = 0,
        i.prototype.$siteLoader = null,
        i.prototype.initialize = function(e) {
            return this.$siteLoader = t(this.el).find("#site-loader"),
            Modernizr.csstransitions ? (this.$siteLoader.addClass("animate-in"),
            this.$siteLoader.on("transitionend webkitTransitionEnd oTransitionEnd otransitionend", t.proxy(this.onAnimateInComplete, this))) : TweenMax.to(this.$siteLoader.get(0), .5, {
                opacity: 1,
                onComplete: t.proxy(this.onAnimateInComplete, this)
            }),
            n.Notifications.on("homeLoadStart", this.onHomeLoadStart, this),
            n.Notifications.on("homeLoadComplete", this.onHomeLoadComplete, this)
        }
        ,
        i.prototype.onAnimateInComplete = function() {
            return this.$siteLoader.off(),
            n.Notifications.trigger("loaderAnimateInComplete")
        }
        ,
        i.prototype.onHomeLoadComplete = function() {
            return Modernizr.csstransitions ? (this.$siteLoader.removeClass("animate-in").addClass("animate-out"),
            this.$siteLoader.on("transitionend webkitTransitionEnd oTransitionEnd otransitionend", t.proxy(this.onAnimateOutComplete, this))) : TweenMax.to(this.$siteLoader.get(0), .5, {
                opacity: 0,
                onComplete: t.proxy(this.onAnimateOutComplete, this)
            })
        }
        ,
        i.prototype.onAnimateOutComplete = function() {
            return this.$siteLoader.off().remove(),
            n.Notifications.trigger("loaderAnimateOutComplete")
        }
        ,
        i
    }(i)
}),
function(t) {
    for (var e = 0, n = ["webkit", "moz"], i = t.requestAnimationFrame, r = t.cancelAnimationFrame, o = n.length; --o >= 0 && !i; )
        i = t[n[o] + "RequestAnimationFrame"],
        r = t[n[o] + "CancelAnimationFrame"];
    i && r || (i = function(t) {
        var n = +new Date
          , i = Math.max(e + 16, n);
        return setTimeout(function() {
            t(e = i)
        }, i - n)
    }
    ,
    r = clearTimeout),
    t.requestAnimationFrame = i,
    t.cancelAnimationFrame = r
}(window),
define("requestAnimationFrame", function() {}),
define("helpers/PanElementMouse", ["jquery", "modernizr", "greensock", "requestAnimationFrame"], function(t) {
    function e() {
        this._el = null,
        this.$el = null,
        this._paused = !1,
        this._reset = null,
        this._prefixedTransform = Modernizr.prefixed("transform"),
        this._multiplier = .05,
        this._elXVal = 0,
        this._elYVal = 0,
        this._targetXVal = 0,
        this._targetYVal = 0
    }
    return e.prototype.initialize = function(e) {
        this._el = e,
        this.$el = t(this._el),
        Modernizr.csstransforms3d ? this._el.style[this._prefixedTransform] = "translate3d(0px, 0px, 0)" : this._el.style[this._prefixedTransform] = "translate(0px, 0px)",
        this.update()
    }
    ,
    e.prototype.reset = function() {
        this._reset !== !0 && (this._reset = !0,
        t(window).off("mousemove", t.proxy(this.onMouseMove, this)),
        this._el && (Modernizr.csstransforms3d ? this._el.style[this._prefixedTransform] = "translate3d(0px, 0px, 0)" : this._el.style[this._prefixedTransform] = "translate(0px, 0px)"))
    }
    ,
    e.prototype.unreset = function() {
        0 != this._reset && (this._reset = !1,
        t(window).on("mousemove", t.proxy(this.onMouseMove, this)),
        this.update())
    }
    ,
    e.prototype.pause = function() {
        this._paused = !0
    }
    ,
    e.prototype.resume = function() {
        this._paused = !1
    }
    ,
    e.prototype.onMouseMove = function(e) {
        if (this._reset)
            return TweenMax.killTweensOf(this._el),
            void (Modernizr.csstransforms3d ? this._el.style[this._prefixedTransform] = "translate3d(0px, 0px, 0)" : this._el.style[this._prefixedTransform] = "translate(0px, 0px)");
        if (!this._paused) {
            var n = e.pageX / t(window).width()
              , i = e.pageY / t(window).height()
              , r = 4410
              , o = 1968
              , s = Math.max(1.1, r / 2 / t(window).width());
            r *= s;
            var a = Math.max(1.1, o / 2 / t(window).height());
            o *= a,
            this._targetXVal = -(r / 2 * (n - .5)),
            this._targetYVal = -(o / 2 * (i - .5))
        }
    }
    ,
    e.prototype.update = function(e) {
        this._reset ? Modernizr.csstransforms3d ? this._el.style[this._prefixedTransform] = "translate3d(0px, 0px, 0)" : this._el.style[this._prefixedTransform] = "translate3d(0px, 0px)" : (Math.abs(this._targetXVal - this._elXVal) > 1 || Math.abs(this._targetYVal - this._elYVal) > 1) && (this._elXVal += (this._targetXVal - this._elXVal) * this._multiplier,
        this._elYVal += (this._targetYVal - this._elYVal) * this._multiplier,
        Modernizr.csstransforms3d ? this._el.style[this._prefixedTransform] = "translate3d(" + this._elXVal + "px, " + this._elYVal + "px, 0)" : this._el.style[this._prefixedTransform] = "translate(" + this._elXVal + "px, " + this._elYVal + "px)"),
        this._reset || requestAnimationFrame(t.proxy(this.update, this))
    }
    ,
    e
}),
define("mout/math/ceil", [], function() {
    function t(t, e) {
        return e = Math.abs(e || 1),
        Math.ceil(t / e) * e
    }
    return t
}),
define("mout/math/clamp", [], function() {
    function t(t, e, n) {
        return e > t ? e : t > n ? n : t
    }
    return t
}),
define("mout/math/countSteps", [], function() {
    function t(t, e, n) {
        return t = Math.floor(t / e),
        n ? t % n : t
    }
    return t
}),
define("mout/math/floor", [], function() {
    function t(t, e) {
        return e = Math.abs(e || 1),
        Math.floor(t / e) * e
    }
    return t
}),
define("mout/math/inRange", [], function() {
    function t(t, e, n, i) {
        return i = i || 0,
        t + i >= e && n >= t - i
    }
    return t
}),
define("mout/math/isNear", [], function() {
    function t(t, e, n) {
        return Math.abs(t - e) <= n
    }
    return t
}),
define("mout/math/lerp", [], function() {
    function t(t, e, n) {
        return e + (n - e) * t
    }
    return t
}),
define("mout/math/loop", [], function() {
    function t(t, e, n) {
        return e > t ? n : t > n ? e : t
    }
    return t
}),
define("mout/math/norm", [], function() {
    function t(t, e, n) {
        if (e > t || t > n)
            throw new RangeError("value (" + t + ") must be between " + e + " and " + n);
        return t === n ? 1 : (t - e) / (n - e)
    }
    return t
}),
define("mout/math/map", ["./lerp", "./norm"], function(t, e) {
    function n(n, i, r, o, s) {
        return t(e(n, i, r), o, s)
    }
    return n
}),
define("mout/math/round", [], function() {
    function t(t, e) {
        return e = e || 1,
        Math.round(t / e) * e
    }
    return t
}),
define("mout/math", ["require", "./math/ceil", "./math/clamp", "./math/countSteps", "./math/floor", "./math/inRange", "./math/isNear", "./math/lerp", "./math/loop", "./math/map", "./math/norm", "./math/round"], function(t) {
    return {
        ceil: t("./math/ceil"),
        clamp: t("./math/clamp"),
        countSteps: t("./math/countSteps"),
        floor: t("./math/floor"),
        inRange: t("./math/inRange"),
        isNear: t("./math/isNear"),
        lerp: t("./math/lerp"),
        loop: t("./math/loop"),
        map: t("./math/map"),
        norm: t("./math/norm"),
        round: t("./math/round")
    }
}),
define("helpers/PanElementTouch", ["jquery", "mout/math", "modernizr", "greensock", "requestAnimationFrame"], function(t, e) {
    function n() {
        this._el = null,
        this.$el = null,
        this._paused = !1,
        this._reset = null,
        this._move = null,
        this._currentDragX = 0,
        this._currentDragY = 0,
        this._dragOffsetStartX = 0,
        this._dragOffsetStartY = 0,
        this._dragOffsetX = 0,
        this._dragOffsetY = 0,
        this._elXVal = 0,
        this._elYVal = 0,
        this._isDragging = !1,
        this._prefixedTransform = Modernizr.prefixed("transform"),
        this._multiplier = .4,
        this._elXVal = 0,
        this._elYVal = 0,
        this._targetXVal = 0,
        this._targetYVal = 0,
        this._touchStartTime = 0,
        this._touchEndTime = 0,
        this._touchPageXOffset = 0,
        this._touchPageXOffsetDiff = 0,
        this._touchPageYOffset = 0,
        this._touchPageYOffsetDiff = 0
    }
    return n.prototype.initialize = function(e) {
        this._el = e,
        this.$el = t(this._el),
        Modernizr.csstransforms3d ? this._el.style[this._prefixedTransform] = "translate3d(0px, 0px, 0)" : this._el.style[this._prefixedTransform] = "translate(0px, 0px)",
        this.update()
    }
    ,
    n.prototype.reset = function() {
        this._reset !== !0 && (this._reset = !0,
        this._el && (this.$el.off("touchstart", t.proxy(this.onDragStart, this)),
        this.$el.off("touchmove", t.proxy(this.onDrag, this)),
        this.$el.off("touchend", t.proxy(this.onDragEnd, this)),
        Modernizr.csstransforms3d ? this._el.style[this._prefixedTransform] = "translate3d(0px, 0px, 0)" : this._el.style[this._prefixedTransform] = "translate(0px, 0px)"))
    }
    ,
    n.prototype.unreset = function() {
        0 != this._reset && (this._reset = !1,
        this._el && (this.$el.on("touchstart", t.proxy(this.onDragStart, this)),
        this.$el.on("touchmove", t.proxy(this.onDrag, this)),
        this.$el.on("touchend", t.proxy(this.onDragEnd, this))))
    }
    ,
    n.prototype.pause = function() {
        this._paused = !0
    }
    ,
    n.prototype.resume = function() {
        this._paused = !1
    }
    ,
    n.prototype.onDragStart = function(t) {
        this._multiplier = .4,
        TweenMax.killTweensOf(this._el);
        var e = event.touches[0].pageX - parseInt(this.$el.css("left")) - this._dragOffsetX
          , n = event.touches[0].pageY - parseInt(this.$el.css("top")) - this._dragOffsetY;
        this._currentDragX = 0,
        this._currentDragY = 0,
        this._dragOffsetStartX = e,
        this._dragOffsetStartY = n,
        this._xVals = [],
        this._yVals = [],
        this._isDragging && this.onDragEnd(t),
        this._isDragging = !0,
        this._touchStartTime = (new Date).getTime(),
        this._touchPageXOffset = this._touchPageXOffsetOriginal = event.touches[0].pageX,
        this._touchPageYOffset = this._touchPageYOffsetOriginal = event.touches[0].pageY,
        this.updateDrag(event.touches[0].pageX, event.touches[0].pageY)
    }
    ,
    n.prototype.onDrag = function(t) {
        t.preventDefault(),
        this.updateDrag(event.touches[0].pageX, event.touches[0].pageY)
    }
    ,
    n.prototype.onDragEnd = function(t) {
        this._isDragging = !1,
        this._multiplier = .1,
        this._touchEndTime = (new Date).getTime();
        var e = Math.max((this._touchEndTime - this._touchStartTime) / 1e3, .7)
          , n = this._touchPageXOffsetDiff / e
          , i = n / e
          , r = this._touchPageXOffset + Math.sqrt(Math.abs(n)) + 2 * i
          , o = this._touchPageYOffsetDiff / e
          , s = o / e
          , a = this._touchPageYOffset + Math.sqrt(Math.abs(o)) + 2 * s;
        this.updateDrag(r, a)
    }
    ,
    n.prototype.updateDrag = function(n, i) {
        this._currentDragX = n - parseInt(this.$el.css("left")) - this._dragOffsetStartX,
        this._currentDragY = i - parseInt(this.$el.css("top")) - this._dragOffsetStartY;
        var r = this._currentDragX
          , o = this._currentDragY;
        this._x2 = this._x1,
        this._y2 = this._y1,
        this._x1 = r,
        this._y1 = o;
        var s = this._x1 - this._x2
          , a = this._y1 - this._y2;
        if (this._xVals.push(s),
        this._yVals.push(a),
        this._reset)
            return TweenMax.killTweensOf(this._el),
            Modernizr.csstransforms3d ? this._el.style[this._prefixedTransform] = "translate3d(0px, 0px, 0)" : this._el.style[this._prefixedTransform] = "translate(0px, 0px)",
            !1;
        if (this._paused)
            return !1;
        var l = 4510
          , u = 2068
          , c = -(l - t(window).width()) / 2
          , h = (l - t(window).width()) / 2
          , p = -(u - t(window).height()) / 2
          , f = (u - t(window).height()) / 2;
        this._targetXVal = e.clamp(r, c, h),
        this._targetYVal = e.clamp(o, p, f),
        this._touchPageXOffsetDiff = n - this._touchPageXOffset,
        this._touchPageXOffset = n,
        this._touchPageYOffsetDiff = i - this._touchPageYOffset,
        this._touchPageYOffset = i
    }
    ,
    n.prototype.update = function(e) {
        this._reset ? Modernizr.csstransforms3d ? this._el.style[this._prefixedTransform] = "translate3d(0px, 0px, 0)" : this._el.style[this._prefixedTransform] = "translate(0px, 0px)" : (Math.abs(this._targetXVal - this._elXVal) > 1 || Math.abs(this._targetYVal - this._elYVal) > 1) && (this._elXVal += (this._targetXVal - this._elXVal) * this._multiplier,
        this._elYVal += (this._targetYVal - this._elYVal) * this._multiplier,
        this._dragOffsetX = this._elXVal,
        this._dragOffsetY = this._elYVal,
        Modernizr.csstransforms3d ? this._el.style[this._prefixedTransform] = "translate3d(" + this._elXVal + "px, " + this._elYVal + "px, 0)" : this._el.style[this._prefixedTransform] = "translate(" + this._elXVal + "px, " + this._elYVal + "px)"),
        this._reset || requestAnimationFrame(t.proxy(this.update, this))
    }
    ,
    n
}),
define("helpers/ImageLoaderQueue", [], function() {
    var t;
    return t = function() {
        function t(t) {
            this._items = t
        }
        return t.prototype._numLoaded = 0,
        t.prototype._cache = {},
        t.prototype.isComplete = function() {
            return this._numLoaded >= this._items.length
        }
        ,
        t.prototype.progress = function() {
            return this._numLoaded / this._items.length
        }
        ,
        t.prototype.loadAll = function(t, e) {
            var n, i, r, o, s, a, l, u, c;
            if (a = function(n) {
                var r;
                return n = n || window.event,
                r = n.target || n.srcElement,
                r && (delete r.onload,
                delete r.onerror),
                u._numLoaded += 1,
                i && e(r),
                u.isComplete() ? t() : void 0
            }
            ,
            r = 0,
            o = void 0,
            s = this._items.length,
            n = "function" == typeof t,
            i = "function" == typeof e,
            u = this,
            c = void 0,
            n && this.isComplete())
                return void t();
            for (l = []; s > r; )
                o = new Image,
                c = this._items[r],
                n && (o.onload = o.onerror = a),
                o.src = c,
                u._cache[c] = o,
                l.push(r++);
            return l
        }
        ,
        t.prototype.getImg = function(t) {
            return this._cache[t]
        }
        ,
        t.prototype.dispose = function() {
            return delete this._cache,
            delete this._items
        }
        ,
        t
    }()
}),
define("components/scroller/AbstractScroller", ["jquery", "underscore", "backbone", "mout/math", "greensock", "requestAnimationFrame"], function(t, e, n, i) {
    var r;
    return r = function() {
        function r(i, r, o, s) {
            e.extend(this, n.Events),
            this.el = i,
            this.$el = t(i),
            this._contentHeight = r,
            this._containerHeight = o,
            this._targetScrollTop = 0,
            this._scrollTop = 0,
            this._percentageScrollTop = 0,
            this._scrollMultiplier = s.multiplier || 1,
            this._enabled = s.enabled || !0,
            this._dirty = !1,
            this._touchStartTime = 0,
            this._touchEndTime = 0,
            this._touchPageYOffset = 0,
            this._touchPageYOffsetOriginal = 0,
            this._touchPageYOffsetDiff = 0,
            this._touchMinScroll = 30,
            this._touchStartedScrolling = !1,
            this._keyPressInterval = null,
            this.initialize()
        }
        return r.prototype.initialize = function() {
            return this.$el.on("mousewheel", t.proxy(this.onMouseWheel, this)),
            this.$el.on("DOMMouseScroll", t.proxy(this.onMouseWheel, this)),
            Modernizr.touch && (this.$el.on("touchstart", t.proxy(this.onTouchStart, this)),
            this.$el.on("touchmove", t.proxy(this.onTouchMove, this)),
            this.$el.on("touchend", t.proxy(this.onTouchEnd, this))),
            t(document).on("keydown", t.proxy(this.onKeyDown, this)),
            this.update()
        }
        ,
        r.prototype.destroy = function() {
            return this.$el.off("mousewheel", t.proxy(this.onMouseWheel, this)),
            this.$el.off("DOMMouseScroll", t.proxy(this.onMouseWheel, this)),
            Modernizr.touch && (this.$el.off("touchstart", t.proxy(this.onTouchStart, this)),
            this.$el.off("touchmove", t.proxy(this.onTouchMove, this)),
            this.$el.off("touchend", t.proxy(this.onTouchEnd, this))),
            t(document).off("keydown", t.proxy(this.onKeyDown, this)),
            this.$el = null,
            this.el = null
        }
        ,
        r.prototype.setContentHeight = function(t) {
            return this._contentHeight = t,
            this.scrollTop = this._percentageScrollTop * (this._contentHeight - this._containerHeight)
        }
        ,
        r.prototype.setContainerHeight = function(t) {
            return this._containerHeight = t,
            this.scrollTop = this._percentageScrollTop * (this._contentHeight - this._containerHeight)
        }
        ,
        Object.defineProperties(r.prototype, {
            scrollTop: {
                get: function() {
                    return this._targetScrollTop
                },
                set: function(t) {
                    return this._targetScrollTop = i.clamp(t, 0, this._contentHeight - this._containerHeight)
                }
            },
            scrollTopPercentage: {
                get: function() {
                    return this._percentageScrollTop
                },
                set: function(t) {
                    return this._targetScrollTop = i.clamp(t * (this._contentHeight - this._containerHeight), 0, this._contentHeight - this._containerHeight)
                }
            },
            enabled: {
                get: function() {
                    return this._enabled
                },
                set: function(t) {
                    return this._enabled = t
                }
            }
        }),
        r.prototype.onMouseWheel = function(t) {
            var e;
            return e = -(20 * t.originalEvent.detail) || t.originalEvent.wheelDelta,
            this.scrollTop = Math.min(this._contentHeight - this._containerHeight, Math.max(0, this._targetScrollTop - e * this._scrollMultiplier))
        }
        ,
        r.prototype.onTouchStart = function(t) {
            return this._touchStartedScrolling = !1,
            this._touchStartTime = (new Date).getTime(),
            this._touchPageYOffset = this._touchPageYOffsetOriginal = event.touches[0].pageY
        }
        ,
        r.prototype.onTouchMove = function(t) {
            var e;
            return t.preventDefault(),
            e = event.touches[0].pageY - this._touchPageYOffset,
            Math.abs(event.touches[0].pageY - this._touchPageYOffsetOriginal) < this._touchMinScroll && !this._touchStartedScrolling ? (this._touchStartedScrolling = !0,
            !1) : (this.scrollTop = Math.min(this._contentHeight - this._containerHeight, Math.max(0, this._targetScrollTop - e)),
            this._touchPageYOffsetDiff = event.touches[0].pageY - this._touchPageYOffset,
            this._touchPageYOffset = event.touches[0].pageY)
        }
        ,
        r.prototype.onTouchEnd = function(t) {
            var e, n, i, r;
            return this._touchStartedScrolling ? (this._touchEndTime = (new Date).getTime(),
            r = Math.max((this._touchEndTime - this._touchStartTime) / 1e3, .4),
            i = this._touchPageYOffsetDiff / r,
            e = i / r,
            n = Math.sqrt(Math.abs(i)) + 2 * e,
            this.scrollTop = Math.min(this._contentHeight - this._containerHeight, Math.max(0, this._targetScrollTop - n)),
            this._touchPageYOffset = 0,
            this._touchPageYOffsetDiff = 0) : !0
        }
        ,
        r.prototype.onKeyDown = function(t) {
            var e;
            switch (e = t.keyCode || t.which) {
            case 32:
                return this.scrollDownPage();
            case 33:
                return this.scrollUpPage();
            case 34:
                return this.scrollDownPage();
            case 35:
                return this.scrollEndPage();
            case 36:
                return this.scrollHomePage();
            case 38:
                return this.scrollUpScreen();
            case 40:
                return this.scrollDownScreen()
            }
        }
        ,
        r.prototype.scrollDownScreen = function() {
            return this.scrollTop += t(window).height() / 5
        }
        ,
        r.prototype.scrollUpScreen = function() {
            return this.scrollTop -= t(window).height() / 5
        }
        ,
        r.prototype.scrollDownPage = function() {
            return this.scrollTop += t(window).height()
        }
        ,
        r.prototype.scrollUpPage = function() {
            return this.scrollTop -= t(window).height()
        }
        ,
        r.prototype.scrollEndPage = function() {
            return this.scrollTopPercentage = 1
        }
        ,
        r.prototype.scrollHomePage = function() {
            return this.scrollTopPercentage = 0
        }
        ,
        r.prototype.update = function() {
            return Math.abs(this._targetScrollTop - this._scrollTop) > 1 ? (this._scrollTop += (this._targetScrollTop - this._scrollTop) * this._scrollMultiplier,
            this._percentageScrollTop = this._scrollTop / (this._contentHeight - this._containerHeight),
            this.trigger("update", {
                scrollTop: this._scrollTop,
                scrollTopPercentage: this._percentageScrollTop
            })) : Math.abs(this._targetScrollTop - this._scrollTop) > .1 && Math.abs(this._targetScrollTop - this._scrollTop) <= 1 && (this._scrollTop += (this._targetScrollTop - this._scrollTop) * this._scrollMultiplier,
            this._percentageScrollTop = this._scrollTop / (this._contentHeight - this._containerHeight),
            this.trigger("update", {
                scrollTop: this._scrollTop,
                scrollTopPercentage: this._percentageScrollTop
            })),
            this.el ? requestAnimationFrame(t.proxy(this.update, this)) : void 0
        }
        ,
        r
    }()
});
var extend = function(t, e) {
    function n() {
        this.constructor = t
    }
    for (var i in e)
        hasProp.call(e, i) && (t[i] = e[i]);
    return n.prototype = e.prototype,
    t.prototype = new n,
    t.__super__ = e.prototype,
    t
}
  , hasProp = {}.hasOwnProperty;
define("components/scroller/MinimalScroller", ["jquery", "./AbstractScroller"], function(t, e) {
    var n;
    return n = function(e) {
        function n(t, e, i, r) {
            this.$elWrapper = null,
            this.$elBackground = null,
            this.$elThumb = null,
            this._mouseDownOffsetY = 0,
            this._isOver = !1,
            this._isScrolling = !1,
            n.__super__.constructor.apply(this, arguments)
        }
        return extend(n, e),
        n.prototype.initialize = function() {
            var e;
            return this.$elWrapper = this.$el.append('<div id="scrollWrapper"></div>').find("#scrollWrapper"),
            this.$elWrapper.css("height", this._containerHeight - parseInt(this.$elWrapper.css("top"))),
            Modernizr.touch || (this.$elWrapper.on("mouseover", t.proxy(this.onMouseEnter, this)),
            this.$elWrapper.on("mouseout", t.proxy(this.onMouseLeave, this))),
            this.$elBackground = this.$elWrapper.append('<div id="scrollBackground"></div>').find("#scrollBackground"),
            this.$elThumb = this.$elBackground.append('<div id="scrollThumb"></div>').find("#scrollThumb"),
            Modernizr.touch || this.$elThumb.on("mousedown", t.proxy(this.onThumbMouseDown, this)),
            e = Math.max(50, Math.round(.1 * this._containerHeight)),
            this.$elThumb.css("height", e),
            n.__super__.initialize.apply(this, arguments)
        }
        ,
        n.prototype.destroy = function() {
            return this.$elBackground && (this.$elBackground.remove(),
            this.$elBackground = null),
            this.$elThumb && (this.$elThumb.off("mousedown", t.proxy(this.onThumbMouseDown, this)),
            this.$elThumb.remove(),
            this.$elThumb = null),
            this.$elWrapper && (this.$elWrapper.off("mouseover", t.proxy(this.onMouseEnter, this)),
            this.$elWrapper.off("mouseout", t.proxy(this.onMouseLeave, this)),
            this.$elWrapper.remove(),
            this.$elWrapper = null),
            window && (t(window).off("mousemove", t.proxy(this.onThumbDrag, this)),
            t(window).off("mouseup", t.proxy(this.onThumbMouseUp, this)),
            t(window).off("selectstart", t.proxy(this.onSelectStart, this))),
            n.__super__.destroy.apply(this, arguments)
        }
        ,
        n.prototype.calculatePercentage = function() {
            return this._isScrolling ? this.scrollTopPercentage = parseInt(this.$elThumb.css("top")) / (this.$elBackground.height() - this.$elThumb.height()) : void 0
        }
        ,
        n.prototype.updateThumbPosition = function(t) {
            return this.$elThumb.css("top", t),
            this.calculatePercentage()
        }
        ,
        n.prototype.onMouseWheel = function(t) {
            return this._isScrolling ? void 0 : n.__super__.onMouseWheel.apply(this, arguments)
        }
        ,
        n.prototype.onMouseEnter = function(t) {
            return this._isOver = !0,
            this.$elBackground.addClass("selected")
        }
        ,
        n.prototype.onMouseLeave = function(t) {
            return this._isOver = !1,
            this._isScrolling ? void 0 : this.$elBackground.removeClass("selected")
        }
        ,
        n.prototype.onThumbMouseDown = function(e) {
            return this._isScrolling = !0,
            this._mouseDownOffsetY = e.pageY - parseInt(this.$elThumb.css("top")),
            this.calculatePercentage(),
            t(window).on("mousemove", t.proxy(this.onThumbDrag, this)),
            t(window).on("mouseup", t.proxy(this.onThumbMouseUp, this)),
            t(window).on("selectstart", t.proxy(this.onSelectStart, this)),
            e.stopPropagation && e.stopPropagation(),
            e.preventDefault && e.preventDefault(),
            e.cancelBubble = !0,
            e.returnValue = !1
        }
        ,
        n.prototype.onThumbDrag = function(t) {
            var e;
            return e = t.clientY - this._mouseDownOffsetY,
            e + this.$elThumb.height() > this.$elBackground.height() ? e = this.$elBackground.height() - this.$elThumb.height() : 0 > e && (e = 0),
            this.updateThumbPosition(e),
            t.stopPropagation && t.stopPropagation(),
            t.preventDefault && t.preventDefault(),
            t.cancelBubble = !0,
            t.returnValue = !1
        }
        ,
        n.prototype.onThumbMouseUp = function(e) {
            return this._isScrolling = !1,
            this._isOver || this.$elBackground.removeClass("selected"),
            t(window).off("mousemove", t.proxy(this.onThumbDrag, this)),
            t(window).off("mouseup", t.proxy(this.onThumbMouseUp, this)),
            t(window).off("selectstart", t.proxy(this.onSelectStart, this)),
            this.calculatePercentage(),
            e.stopPropagation && e.stopPropagation(),
            e.preventDefault && e.preventDefault(),
            e.cancelBubble = !0,
            e.returnValue = !1
        }
        ,
        n.prototype.onSelectStart = function(t) {
            return t.preventDefault(),
            !1
        }
        ,
        n.prototype.update = function() {
            var t;
            return n.__super__.update.apply(this, arguments),
            !this._isScrolling && this.el ? (t = (this.$elBackground.height() - this.$elThumb.height()) * this.scrollTopPercentage,
            this.updateThumbPosition(t)) : void 0
        }
        ,
        n.prototype.setContentHeight = function(t) {
            return n.__super__.setContentHeight.apply(this, arguments),
            this.$elWrapper.css("height", this._containerHeight - parseInt(this.$elWrapper.css("top")))
        }
        ,
        n.prototype.setContainerHeight = function(t) {
            return n.__super__.setContainerHeight.apply(this, arguments),
            this.$elWrapper.css("height", this._containerHeight - parseInt(this.$elWrapper.css("top")))
        }
        ,
        n
    }(e)
}),
window.matchMedia || (window.matchMedia = function() {
    "use strict";
    var t = window.styleMedia || window.media;
    if (!t) {
        var e = document.createElement("style")
          , n = document.getElementsByTagName("script")[0]
          , i = null;
        e.type = "text/css",
        e.id = "matchmediajs-test",
        n.parentNode.insertBefore(e, n),
        i = "getComputedStyle"in window && window.getComputedStyle(e, null) || e.currentStyle,
        t = {
            matchMedium: function(t) {
                var n = "@media " + t + "{ #matchmediajs-test { width: 1px; } }";
                return e.styleSheet ? e.styleSheet.cssText = n : e.textContent = n,
                "1px" === i.width
            }
        }
    }
    return function(e) {
        return {
            matches: t.matchMedium(e || "all"),
            media: e || "all"
        }
    }
}()),
define("lib/matchmedia/matchMedia", function() {}),
function(t, e, n) {
    var i = window.matchMedia;
    "undefined" != typeof module && module.exports ? module.exports = n(i) : "function" == typeof define && define.amd ? define("enquire", [], function() {
        return e[t] = n(i)
    }) : e[t] = n(i)
}("enquire", this, function(t) {
    "use strict";
    function e(t, e) {
        var n, i = 0, r = t.length;
        for (i; r > i && (n = e(t[i], i),
        n !== !1); i++)
            ;
    }
    function n(t) {
        return "[object Array]" === Object.prototype.toString.apply(t)
    }
    function i(t) {
        return "function" == typeof t
    }
    function r(t) {
        this.options = t,
        !t.deferSetup && this.setup()
    }
    function o(e, n) {
        this.query = e,
        this.isUnconditional = n,
        this.handlers = [],
        this.mql = t(e);
        var i = this;
        this.listener = function(t) {
            i.mql = t,
            i.assess()
        }
        ,
        this.mql.addListener(this.listener)
    }
    function s() {
        if (!t)
            throw new Error("matchMedia not present, legacy browsers require a polyfill");
        this.queries = {},
        this.browserIsIncapable = !t("only all").matches
    }
    return r.prototype = {
        setup: function() {
            this.options.setup && this.options.setup(),
            this.initialised = !0
        },
        on: function() {
            !this.initialised && this.setup(),
            this.options.match && this.options.match()
        },
        off: function() {
            this.options.unmatch && this.options.unmatch()
        },
        destroy: function() {
            this.options.destroy ? this.options.destroy() : this.off()
        },
        equals: function(t) {
            return this.options === t || this.options.match === t
        }
    },
    o.prototype = {
        addHandler: function(t) {
            var e = new r(t);
            this.handlers.push(e),
            this.matches() && e.on()
        },
        removeHandler: function(t) {
            var n = this.handlers;
            e(n, function(e, i) {
                return e.equals(t) ? (e.destroy(),
                !n.splice(i, 1)) : void 0
            })
        },
        matches: function() {
            return this.mql.matches || this.isUnconditional
        },
        clear: function() {
            e(this.handlers, function(t) {
                t.destroy()
            }),
            this.mql.removeListener(this.listener),
            this.handlers.length = 0
        },
        assess: function() {
            var t = this.matches() ? "on" : "off";
            e(this.handlers, function(e) {
                e[t]()
            })
        }
    },
    s.prototype = {
        register: function(t, r, s) {
            var a = this.queries
              , l = s && this.browserIsIncapable;
            return a[t] || (a[t] = new o(t,l)),
            i(r) && (r = {
                match: r
            }),
            n(r) || (r = [r]),
            e(r, function(e) {
                i(e) && (e = {
                    match: e
                }),
                a[t].addHandler(e)
            }),
            this
        },
        unregister: function(t, e) {
            var n = this.queries[t];
            return n && (e ? n.removeHandler(e) : (n.clear(),
            delete this.queries[t])),
            this
        }
    },
    new s
});
var extend = function(t, e) {
    function n() {
        this.constructor = t
    }
    for (var i in e)
        hasProp.call(e, i) && (t[i] = e[i]);
    return n.prototype = e.prototype,
    t.prototype = new n,
    t.__super__ = e.prototype,
    t
}
  , hasProp = {}.hasOwnProperty;
define("views/home/home", ["jquery", "underscore", "backbone", "views/abstract", "helpers/PanElementMouse", "helpers/PanElementTouch", "collections/projects", "helpers/ImageLoaderQueue", "components/scroller/MinimalScroller", "config", "greensock", "enquire"], function(t, e, n, i, r, o, s, a, l, u) {
    var c;
    return c = function(i) {
        function s() {
            return s.__super__.constructor.apply(this, arguments)
        }
        return extend(s, i),
        s.prototype.id = "homeView",
        s.prototype.el = "#homeView",
        s.prototype._selectedProjectId = null,
        s.prototype._imageLoaderQueue = null,
        s.prototype._imagesLoaded = !1,
        s.prototype._panElement = null,
        s.prototype._touchStartY = null,
        s.prototype._prefixedTransform = Modernizr.prefixed("transform"),
        s.prototype.events = function() {
            return {
                "touchstart .homeProjectThumbLink": "touchStartHomeProjectThumbLink",
                "touchend .homeProjectThumbLink": "touchEndHomeProjectThumbLink",
                "click .homeProjectThumbLink": "clickHomeProjectThumbLink",
                "mouseenter .homeProjectThumbLink": "mouseOverHomeProjectThumbLink",
                "mouseleave .homeProjectThumbLink": "mouseOutHomeProjectThumbLink"
            }
        }
        ,
        s.prototype.initialize = function(e) {
            return e.collection && (this.collection = e.collection),
            this._panElement = Modernizr.touch ? new o : new r,
            this.render(),
            this.loadImages(),
            this.pause(!0),
            t(window).on("resize", t.proxy(this.resize, this)),
            this.resize(),
            enquire.register("screen and (max-width:" + u.mobileBreakWidth + "px)", {
                match: t.proxy(function() {
                    return this.fireEnquire()
                }, this),
                unmatch: t.proxy(function() {
                    return this.fireEnquire()
                }, this),
                setup: function() {},
                deferSetup: !1,
                destroy: function() {}
            })
        }
        ,
        s.prototype.getImagesList = function() {
            var t;
            return t = [],
            e.each(this.collection.models, function(e) {
                return t.push(e.get("thumb"))
            }),
            t
        }
        ,
        s.prototype.loadImages = function() {
            return n.Notifications.trigger("homeLoadStart"),
            this._imageLoaderQueue = new a(this.getImagesList()),
            this._imageLoaderQueue.loadAll(t.proxy(this.loadAllImagesComplete, this), t.proxy(this.loadImageComplete, this))
        }
        ,
        s.prototype.loadImageComplete = function(t) {
            return n.Notifications.trigger("homeLoadProgress", this._imageLoaderQueue.progress())
        }
        ,
        s.prototype.loadAllImagesComplete = function() {
            return this._imagesLoaded = !0,
            setTimeout(t.proxy(this.fadeInImages, this), 2e3)
        }
        ,
        s.prototype.initScrollBar = function() {
            return this._scroller && this.destroyScrollBar(),
            this._scroller = new l(this.$el.find("#homeViewScroller").get(0),this.$el.find("#homeProjectContainer").height(),t(window).height(),{
                multiplier: Modernizr.touch ? .2 : .35
            }),
            this._scroller.on("update", t.proxy(this.onScrollBarUpdate, this)),
            this.scrollUpdate(0)
        }
        ,
        s.prototype.destroyScrollBar = function() {
            return this._scroller ? (this._scroller.off("update", t.proxy(this.onScrollBarUpdate, this)),
            this._scroller.destroy(),
            this._scroller = null) : void 0
        }
        ,
        s.prototype.onScrollBarUpdate = function(t) {
            return this.scrollUpdate(t.scrollTopPercentage)
        }
        ,
        s.prototype.scrollUpdate = function(e) {
            var n;
            return n = -Math.round(e * (this.$el.find("#homeProjectContainer").height() + parseInt(this.$el.find("#homeProjectContainer").css("padding-top")) - t(window).height())),
            Modernizr.csstransforms3d ? this.$el.find("#homeProjectContainer").get(0).style[this._prefixedTransform] = "translate3d(0px, " + n + "px, 0px)" : this.$el.find("#homeProjectContainer").get(0).style[this._prefixedTransform] = "translate(0px, " + n + "px)"
        }
        ,
        s.prototype.fadeInImages = function() {
            if (u.isHandheld())
                return t(".homeProjectThumb").css({
                    opacity: 1
                }),
                void this.fadeInImagesComplete();
            var e;
            return e = this,
            this.$el.find(".homeProjectThumb").each(function(n, i) {
                var r, o;
                return o = .08 * n,
                r = e.$el.find(".homeProjectThumb").length - 1 === n ? t.proxy(e.fadeInImagesComplete, e) : null,
                Modernizr.csstransitions ? TweenMax.set(this, {
                    opacity: 1,
                    delay: o,
                    onComplete: r
                }) : TweenMax.to(this, .3, {
                    opacity: 1,
                    delay: o,
                    onComplete: r
                })
            })
        }
        ,
        s.prototype.fadeInImagesComplete = function() {
            return n.Notifications.trigger("homeLoadComplete"),
            this._panElement.initialize(this.$el.find("#homeProjectContainer").get(0)),
            this.fireEnquire()
        }
        ,
        s.prototype.fireEnquire = function() {
            return u.isHandheld() ? (this._panElement.reset(),
            this.align(),
            this._scroller || this.initScrollBar(),
            this.rollOutProjectThumbs()) : (this._scroller && this.destroyScrollBar(),
            this._panElement.unreset(),
            this.align())
        }
        ,
        s.prototype.render = function() {
            return Modernizr.touch && this.$el.find("#homeProjectContainer").prepend('<div id="homeProjectBG"></div>'),
            this.$el.show().css("visibility", "visible"),
            this.delegateEvents(),
            this.align(),
            this
        }
        ,
        s.prototype.resize = function() {
            return this.align()
        }
        ,
        s.prototype.align = function() {
            var e, n;
            return e = t(window).width() / 2,
            n = t(window).height() / 2,
            this.$el.find("#homeProjectContainer").css("left", e),
            this.$el.find("#homeProjectContainer").css("top", n),
            u.isHandheld() ? (this.$el.css("height", t(window).height()),
            this.$el.find("#homeViewScroller").css("height", t(window).height()),
            this.$el.find(".homeProjectThumb").each(function(e, n) {
                var i, r, o, s;
                return r = t(n).data("default-width"),
                i = t(n).data("default-height"),
                s = Math.round(t(window).width()),
                o = Math.ceil(s * (i / r)),
                t(n).css({
                    width: s,
                    height: o
                })
            })) : (this.$el.find("#homeViewScroller").css("height", ""),
            this.$el.find(".homeProjectThumb").each(function(e, n) {
                var i, r;
                return r = t(n).data("default-width"),
                i = t(n).data("default-height"),
                t(n).css({
                    width: r,
                    height: i
                })
            })),
            this._scroller ? (this._scroller.setContentHeight(this.$el.find("#homeProjectContainer").height()),
            this._scroller.setContainerHeight(t(window).height())) : void 0
        }
        ,
        s.prototype.clickHomeProjectThumbLink = function(e) {
            var n;
            return e.preventDefault(),
            Modernizr.touch || (this.rollOutProjectThumbs(),
            n = t(e.currentTarget).data("project-slug"),
            this.gotoProject(n)),
            !1
        }
        ,
        s.prototype.touchStartHomeProjectThumbLink = function(t) {
            return this._touchStartY = t.originalEvent.touches[0].pageY
        }
        ,
        s.prototype.touchEndHomeProjectThumbLink = function(e) {
            var n, i;
            return n = Math.abs(this._touchStartY - e.originalEvent.changedTouches[0].pageY),
            10 > n ? (this.rollOutProjectThumbs(),
            i = t(e.currentTarget).data("project-slug"),
            this.gotoProject(i)) : void 0
        }
        ,
        s.prototype.gotoProject = function(t) {
            return n.history.navigate("work/" + t + "/", {
                trigger: !0
            })
        }
        ,
        s.prototype.mouseOverHomeProjectThumbLink = function(e) {
            var n;
            return e.preventDefault(),
            n = parseInt(t(e.currentTarget).data("project-id")),
            this.$el.find(".homeProjectThumbLink").each(function(e, i) {
                var r, o;
                return o = t(i).find(".homeProjectThumb.desktop").eq(0),
                r = parseInt(t(i).data("project-id")),
                n === r || u.isHandheld() ? Modernizr.csstransitions ? (TweenMax.set(o, {
                    opacity: 1,
                    overwrite: "all"
                }),
                t(o).addClass("over")) : TweenMax.to(o, .3, {
                    opacity: 1,
                    overwrite: "all"
                }) : Modernizr.csstransitions ? (TweenMax.set(o, {
                    css: {
                        opacity: .3
                    },
                    overwrite: "all"
                }),
                t(o).removeClass("over")) : TweenMax.to(o, .3, {
                    opacity: .3,
                    overwrite: "all"
                })
            }),
            !1
        }
        ,
        s.prototype.mouseOutHomeProjectThumbLink = function(t) {
            return t.preventDefault(),
            this.rollOutProjectThumbs(),
            !1
        }
        ,
        s.prototype.rollOutProjectThumbs = function(e) {
            return u.isHandheld() ? void 0 : this.$el.find(".homeProjectThumbLink").each(function(e, n) {
                var i;
                return i = t(n).find(".homeProjectThumb.desktop").eq(0),
                Modernizr.csstransitions ? (TweenMax.set(i, {
                    css: {
                        opacity: 1
                    },
                    delay: .1,
                    overwrite: "all"
                }),
                t(i).removeClass("over")) : TweenMax.to(i, .3, {
                    opacity: 1,
                    delay: .1,
                    overwrite: "all"
                })
            })
        }
        ,
        s.prototype.pause = function(t) {
            return t ? (this.$el.addClass("paused"),
            this._panElement.pause()) : (this.$el.removeClass("paused"),
            this._panElement.resume())
        }
        ,
        s
    }(i)
});
var extend = function(t, e) {
    function n() {
        this.constructor = t
    }
    for (var i in e)
        hasProp.call(e, i) && (t[i] = e[i]);
    return n.prototype = e.prototype,
    t.prototype = new n,
    t.__super__ = e.prototype,
    t
}
  , hasProp = {}.hasOwnProperty;
define("views/home/header", ["jquery", "underscore", "backbone", "views/abstract", "config", "enquire"], function(t, e, n, i, r, o) {
    var s;
    return s = function(e) {
        function i(t) {
            this.options = t || {},
            i.__super__.constructor.apply(this, arguments)
        }
        return extend(i, e),
        i.prototype.id = "headerView",
        i.prototype.el = "#headerView",
        i.prototype.isMouseOverLogo = !1,
        i.prototype.nextState = null,
        i.prototype.isEnabled = !1,
        i.prototype.clickLogoTimeStamp = 0,
        i.prototype.events = function() {
            return this.isEnabled ? Modernizr.touch ? {
                "touchstart #logo": "clickLogo"
            } : {
                "click #logo": "clickLogo",
                "mouseenter #logo": "mouseEnterLogo",
                "mouseleave #logo": "mouseLeaveLogo"
            } : {}
        }
        ,
        i.prototype.initialize = function(e) {
            return this.enabled(this.options.enabled),
            this.render(),
            o.register("screen and (max-width:" + r.mobileBreakWidth + "px)", {
                match: t.proxy(function() {
                    return this.updateState()
                }, this),
                unmatch: t.proxy(function() {
                    return this.updateState()
                }, this),
                setup: function() {},
                deferSetup: !0,
                destroy: function() {}
            }),
            n.Notifications.on("overlayAnimateIn", this.onOverlayAnimateIn, this),
            n.Notifications.on("overlayAnimateInComplete", this.onOverlayAnimateInComplete, this),
            n.Notifications.on("overlayAnimateOut", this.onOverlayAnimateOut, this),
            n.Notifications.on("overlayAnimateOutComplete", this.onOverlayAnimateOutComplete, this)
        }
        ,
        i.prototype.enabled = function(t) {
            return this.isEnabled = t,
            this.delegateEvents()
        }
        ,
        i.prototype.animateIn = function() {
            var t;
            return t = this.$el.find("#logo").offset().top + this.$el.find("#logo").height(),
            r.isHandheld() || TweenMax.fromTo(this.$el.find("#logo").get(0), .5, {
                y: -t
            }, {
                y: 0,
                delay: 1,
                ease: Back.easeOut
            }),
            TweenMax.set(this.$el.find("#logo").get(0), {
                opacity: 1,
                delay: 1
            })
        }
        ,
        i.prototype.render = function() {
            return Modernizr.touch || (Modernizr.csstransforms3d ? (TweenMax.to(this.$el.find("#logo #heart-icon").get(0), 0, {
                css: {
                    rotationY: 0,
                    transformPerspective: 200
                },
                ease: Circ.easeInOut,
                overwrite: "auto"
            }),
            TweenMax.to(this.$el.find("#logo #close-icon").get(0), 0, {
                css: {
                    rotationY: 180,
                    transformPerspective: 200
                },
                ease: Circ.easeInOut,
                overwrite: "auto"
            }),
            TweenMax.to(this.$el.find("#logo #info-icon").get(0), 0, {
                css: {
                    rotationY: 180,
                    transformPerspective: 200
                },
                ease: Circ.easeInOut,
                overwrite: "auto"
            })) : (TweenMax.to(this.$el.find("#logo #heart-icon").get(0), 0, {
                opacity: 1
            }),
            TweenMax.to(this.$el.find("#logo #close-icon").get(0), 0, {
                opacity: 0
            }),
            TweenMax.to(this.$el.find("#logo #info-icon").get(0), 0, {
                opacity: 0
            }))),
            setTimeout(t.proxy(this.animateIn, this), 10),
            this
        }
        ,
        i.prototype.setState = function(t) {
            return t !== this.nextState ? (this.nextState = t,
            this.updateState()) : void 0
        }
        ,
        i.prototype.updateState = function() {
            if (Modernizr.touch && r.isHandheld())
                switch (this.nextState) {
                case "info":
                    return Modernizr.csstransforms3d ? (TweenMax.to(this.$el.find("#logo #heart-icon").get(0), .5, {
                        css: {
                            rotationY: 0,
                            transformPerspective: 200,
                            opacity: 1
                        },
                        ease: Circ.easeInOut,
                        overwrite: "auto"
                    }),
                    TweenMax.to(this.$el.find("#logo #close-icon").get(0), .5, {
                        css: {
                            rotationY: 0,
                            transformPerspective: 200,
                            opacity: 0
                        },
                        ease: Circ.easeInOut,
                        overwrite: "auto"
                    }),
                    TweenMax.to(this.$el.find("#logo #info-icon").get(0), .5, {
                        css: {
                            rotationY: 0,
                            transformPerspective: 200,
                            opacity: 0
                        },
                        ease: Circ.easeInOut,
                        overwrite: "auto"
                    })) : (TweenMax.to(this.$el.find("#logo #heart-icon").get(0), .3, {
                        opacity: 1
                    }),
                    TweenMax.to(this.$el.find("#logo #close-icon").get(0), .3, {
                        opacity: 0
                    }),
                    TweenMax.to(this.$el.find("#logo #info-icon").get(0), .3, {
                        opacity: 0
                    }));
                case "close":
                    return Modernizr.csstransforms3d ? (TweenMax.to(this.$el.find("#logo #heart-icon").get(0), .5, {
                        css: {
                            rotationY: 0,
                            transformPerspective: 200,
                            opacity: 0
                        },
                        ease: Circ.easeInOut,
                        overwrite: "auto"
                    }),
                    TweenMax.to(this.$el.find("#logo #close-icon").get(0), .5, {
                        css: {
                            rotationY: 0,
                            transformPerspective: 200,
                            opacity: 1
                        },
                        ease: Circ.easeInOut,
                        overwrite: "auto"
                    }),
                    TweenMax.to(this.$el.find("#logo #info-icon").get(0), .5, {
                        css: {
                            rotationY: 0,
                            transformPerspective: 200,
                            opacity: 0
                        },
                        ease: Circ.easeInOut,
                        overwrite: "auto"
                    })) : (TweenMax.to(this.$el.find("#logo #heart-icon").get(0), .3, {
                        opacity: 1
                    }),
                    TweenMax.to(this.$el.find("#logo #close-icon").get(0), .3, {
                        opacity: 0
                    }),
                    TweenMax.to(this.$el.find("#logo #info-icon").get(0), .3, {
                        opacity: 0
                    }))
                }
            else if (Modernizr.touch && !r.isHandheld())
                switch (this.nextState) {
                case "info":
                    return Modernizr.csstransforms3d ? (TweenMax.to(this.$el.find("#logo #heart-icon").get(0), .5, {
                        css: {
                            rotationY: 0,
                            transformPerspective: 200,
                            opacity: 1
                        },
                        ease: Circ.easeInOut,
                        overwrite: "auto"
                    }),
                    TweenMax.to(this.$el.find("#logo #close-icon").get(0), .5, {
                        css: {
                            rotationY: 0,
                            transformPerspective: 200,
                            opacity: 0
                        },
                        ease: Circ.easeInOut,
                        overwrite: "auto"
                    }),
                    TweenMax.to(this.$el.find("#logo #info-icon").get(0), .5, {
                        css: {
                            rotationY: 0,
                            transformPerspective: 200,
                            opacity: 0
                        },
                        ease: Circ.easeInOut,
                        overwrite: "auto"
                    })) : (TweenMax.to(this.$el.find("#logo #heart-icon").get(0), .3, {
                        opacity: 1
                    }),
                    TweenMax.to(this.$el.find("#logo #close-icon").get(0), .3, {
                        opacity: 0
                    }),
                    TweenMax.to(this.$el.find("#logo #info-icon").get(0), .3, {
                        opacity: 0
                    }));
                case "close":
                    return Modernizr.csstransforms3d ? (TweenMax.to(this.$el.find("#logo #heart-icon").get(0), .5, {
                        css: {
                            rotationY: 0,
                            transformPerspective: 200,
                            opacity: 0
                        },
                        ease: Circ.easeInOut,
                        overwrite: "auto"
                    }),
                    TweenMax.to(this.$el.find("#logo #close-icon").get(0), .5, {
                        css: {
                            rotationY: 0,
                            transformPerspective: 200,
                            opacity: 1
                        },
                        ease: Circ.easeInOut,
                        overwrite: "auto"
                    }),
                    TweenMax.to(this.$el.find("#logo #info-icon").get(0), .5, {
                        css: {
                            rotationY: 0,
                            transformPerspective: 200,
                            opacity: 0
                        },
                        ease: Circ.easeInOut,
                        overwrite: "auto"
                    })) : (TweenMax.to(this.$el.find("#logo #heart-icon").get(0), .3, {
                        opacity: 1
                    }),
                    TweenMax.to(this.$el.find("#logo #close-icon").get(0), .3, {
                        opacity: 0
                    }),
                    TweenMax.to(this.$el.find("#logo #info-icon").get(0), .3, {
                        opacity: 0
                    }))
                }
            else
                switch (this.nextState) {
                case "info":
                    return Modernizr.csstransforms3d ? (TweenMax.to(this.$el.find("#logo #close-icon").get(0), .5, {
                        css: {
                            opacity: 0
                        },
                        overwrite: "auto"
                    }),
                    TweenMax.to(this.$el.find("#logo #info-icon").get(0), .5, {
                        css: {
                            opacity: 1
                        },
                        overwrite: "auto"
                    })) : (TweenMax.to(this.$el.find("#logo #heart-icon").get(0), .3, {
                        opacity: 1
                    }),
                    TweenMax.to(this.$el.find("#logo #close-icon").get(0), .3, {
                        opacity: 0
                    }),
                    TweenMax.to(this.$el.find("#logo #info-icon").get(0), .3, {
                        opacity: 0
                    }));
                case "close":
                    return Modernizr.csstransforms3d ? (TweenMax.to(this.$el.find("#logo #close-icon").get(0), .5, {
                        css: {
                            opacity: 1
                        },
                        overwrite: "auto"
                    }),
                    TweenMax.to(this.$el.find("#logo #info-icon").get(0), .5, {
                        css: {
                            opacity: 0
                        },
                        overwrite: "auto"
                    })) : (TweenMax.to(this.$el.find("#logo #heart-icon").get(0), .3, {
                        opacity: 1
                    }),
                    TweenMax.to(this.$el.find("#logo #close-icon").get(0), .3, {
                        opacity: 0
                    }),
                    TweenMax.to(this.$el.find("#logo #info-icon").get(0), .3, {
                        opacity: 0
                    }))
                }
        }
        ,
        i.prototype.onOverlayAnimateIn = function() {}
        ,
        i.prototype.onOverlayAnimateInComplete = function() {}
        ,
        i.prototype.onOverlayAnimateOut = function() {}
        ,
        i.prototype.onOverlayAnimateOutComplete = function() {}
        ,
        i.prototype.clickLogo = function(t) {
            var e;
            return t.preventDefault(),
            e = t.timeStamp - this.clickLogoTimeStamp,
            500 > e ? !1 : (this.clickLogoTimeStamp = t.timeStamp,
            n.Notifications.trigger("headerLogoClick"),
            !1)
        }
        ,
        i.prototype.mouseEnterLogo = function(t) {
            if (this.isMouseOverLogo = !0,
            Modernizr.csstransforms3d)
                return TweenMax.to(this.$el.find("#logo #heart-icon").get(0), .5, {
                    css: {
                        rotationY: -180,
                        transformPerspective: 200
                    },
                    ease: Circ.easeInOut,
                    overwrite: "auto"
                }),
                TweenMax.to(this.$el.find("#logo #close-icon").get(0), .5, {
                    css: {
                        rotationY: 0,
                        transformPerspective: 200
                    },
                    ease: Circ.easeInOut,
                    overwrite: "auto"
                }),
                TweenMax.to(this.$el.find("#logo #info-icon").get(0), .5, {
                    css: {
                        rotationY: 0,
                        transformPerspective: 200
                    },
                    ease: Circ.easeInOut,
                    overwrite: "auto"
                });
            switch (this.nextState) {
            case "info":
                return TweenMax.to(this.$el.find("#logo #heart-icon").get(0), .3, {
                    opacity: 0
                }),
                TweenMax.to(this.$el.find("#logo #close-icon").get(0), .3, {
                    opacity: 0
                }),
                TweenMax.to(this.$el.find("#logo #info-icon").get(0), .3, {
                    opacity: 1
                });
            case "close":
                return TweenMax.to(this.$el.find("#logo #heart-icon").get(0), .3, {
                    opacity: 0
                }),
                TweenMax.to(this.$el.find("#logo #close-icon").get(0), .3, {
                    opacity: 1
                }),
                TweenMax.to(this.$el.find("#logo #info-icon").get(0), .3, {
                    opacity: 0
                })
            }
        }
        ,
        i.prototype.mouseLeaveLogo = function(t) {
            return this.isMouseOverLogo = !1,
            Modernizr.csstransforms3d ? (TweenMax.to(this.$el.find("#logo #heart-icon").get(0), .5, {
                css: {
                    rotationY: 0,
                    transformPerspective: 200
                },
                ease: Circ.easeInOut,
                overwrite: "auto"
            }),
            TweenMax.to(this.$el.find("#logo #close-icon").get(0), .5, {
                css: {
                    rotationY: 180,
                    transformPerspective: 200
                },
                ease: Circ.easeInOut,
                overwrite: "auto"
            }),
            TweenMax.to(this.$el.find("#logo #info-icon").get(0), .5, {
                css: {
                    rotationY: 180,
                    transformPerspective: 200
                },
                ease: Circ.easeInOut,
                overwrite: "auto"
            })) : (TweenMax.set(this.$el.find("#logo #heart-icon").get(0), {
                opacity: 1
            }),
            TweenMax.set(this.$el.find("#logo #close-icon").get(0), {
                opacity: 0
            }),
            TweenMax.set(this.$el.find("#logo #info-icon").get(0), {
                opacity: 0
            }))
        }
        ,
        i.prototype.animateRotateLogoIn = function() {}
        ,
        i.prototype.animateRotateLogoInComplete = function() {}
        ,
        i.prototype.animateRotateLogoOut = function() {}
        ,
        i.prototype.animateRotateLogoOutComplete = function() {}
        ,
        i
    }(i)
}),
function(t) {
    function e(t, e, n, i, r) {
        this._listener = e,
        this._isOnce = n,
        this.context = i,
        this._signal = t,
        this._priority = r || 0
    }
    function n(t, e) {
        if ("function" != typeof t)
            throw new Error("listener is a required param of {fn}() and should be a Function.".replace("{fn}", e))
    }
    function i() {
        this._bindings = [],
        this._prevParams = null;
        var t = this;
        this.dispatch = function() {
            i.prototype.dispatch.apply(t, arguments)
        }
    }
    e.prototype = {
        active: !0,
        params: null,
        execute: function(t) {
            var e, n;
            return this.active && this._listener && (n = this.params ? this.params.concat(t) : t,
            e = this._listener.apply(this.context, n),
            this._isOnce && this.detach()),
            e
        },
        detach: function() {
            return this.isBound() ? this._signal.remove(this._listener, this.context) : null
        },
        isBound: function() {
            return !!this._signal && !!this._listener
        },
        isOnce: function() {
            return this._isOnce
        },
        getListener: function() {
            return this._listener
        },
        getSignal: function() {
            return this._signal
        },
        _destroy: function() {
            delete this._signal,
            delete this._listener,
            delete this.context
        },
        toString: function() {
            return "[SignalBinding isOnce:" + this._isOnce + ", isBound:" + this.isBound() + ", active:" + this.active + "]"
        }
    },
    i.prototype = {
        VERSION: "1.0.0",
        memorize: !1,
        _shouldPropagate: !0,
        active: !0,
        _registerListener: function(t, n, i, r) {
            var o, s = this._indexOfListener(t, i);
            if (-1 !== s) {
                if (o = this._bindings[s],
                o.isOnce() !== n)
                    throw new Error("You cannot add" + (n ? "" : "Once") + "() then add" + (n ? "Once" : "") + "() the same listener without removing the relationship first.")
            } else
                o = new e(this,t,n,i,r),
                this._addBinding(o);
            return this.memorize && this._prevParams && o.execute(this._prevParams),
            o
        },
        _addBinding: function(t) {
            var e = this._bindings.length;
            do
                --e;
            while (this._bindings[e] && t._priority <= this._bindings[e]._priority);this._bindings.splice(e + 1, 0, t)
        },
        _indexOfListener: function(t, e) {
            for (var n, i = this._bindings.length; i--; )
                if (n = this._bindings[i],
                n._listener === t && n.context === e)
                    return i;
            return -1
        },
        has: function(t, e) {
            return -1 !== this._indexOfListener(t, e)
        },
        add: function(t, e, i) {
            return n(t, "add"),
            this._registerListener(t, !1, e, i)
        },
        addOnce: function(t, e, i) {
            return n(t, "addOnce"),
            this._registerListener(t, !0, e, i)
        },
        remove: function(t, e) {
            n(t, "remove");
            var i = this._indexOfListener(t, e);
            return -1 !== i && (this._bindings[i]._destroy(),
            this._bindings.splice(i, 1)),
            t
        },
        removeAll: function() {
            for (var t = this._bindings.length; t--; )
                this._bindings[t]._destroy();
            this._bindings.length = 0
        },
        getNumListeners: function() {
            return this._bindings.length
        },
        halt: function() {
            this._shouldPropagate = !1
        },
        dispatch: function(t) {
            if (this.active) {
                var e, n = Array.prototype.slice.call(arguments), i = this._bindings.length;
                if (this.memorize && (this._prevParams = n),
                i) {
                    e = this._bindings.slice(),
                    this._shouldPropagate = !0;
                    do
                        i--;
                    while (e[i] && this._shouldPropagate && e[i].execute(n) !== !1)
                }
            }
        },
        forget: function() {
            this._prevParams = null
        },
        dispose: function() {
            this.removeAll(),
            delete this._bindings,
            delete this._prevParams
        },
        toString: function() {
            return "[Signal active:" + this.active + " numListeners:" + this.getNumListeners() + "]"
        }
    };
    var r = i;
    r.Signal = i,
    "function" == typeof define && define.amd ? define("signals", [], function() {
        return r
    }) : "undefined" != typeof module && module.exports ? module.exports = r : t.signals = r
}(this);
var extend = function(t, e) {
    function n() {
        this.constructor = t
    }
    for (var i in e)
        hasProp.call(e, i) && (t[i] = e[i]);
    return n.prototype = e.prototype,
    t.prototype = new n,
    t.__super__ = e.prototype,
    t
}
  , hasProp = {}.hasOwnProperty;
define("views/overlay/overlay", ["jquery", "underscore", "backbone", "views/abstract", "signals", "greensock", "modernizr"], function(t, e, n, i, r) {
    var o;
    return o = function(e) {
        function i() {
            return i.__super__.constructor.apply(this, arguments)
        }
        return extend(i, e),
        i.prototype.id = "overlayView",
        i.prototype.el = "#overlayView",
        i.prototype.events = {},
        i.prototype.signal = new r.Signal,
        i.prototype.initialize = function() {
            return this.render(),
            this.align(),
            t(window).on("resize", t.proxy(this.align, this))
        }
        ,
        i.prototype.unbind = function() {
            return this.$el.off(),
            TweenMax.killTweensOf(this.el),
            TweenMax.killDelayedCallsTo(this.animateInComplete),
            TweenMax.killDelayedCallsTo(this.animateOutComplete),
            t(window).off("resize", t.proxy(this.align, this)),
            this.signal.removeAll()
        }
        ,
        i.prototype.remove = function() {
            return this.$el.empty()
        }
        ,
        i.prototype.align = function() {
            return this.$el.css("height", t(document).height())
        }
        ,
        i.prototype.animateInBG = function() {
            return this.$el.show(),
            Modernizr.csstransitions ? (TweenMax.delayedCall(.05, function() {
                return this.$el.removeClass("animate-out").addClass("animate-in")
            }, null, this),
            this.$el.off(),
            this.$el.on("transitionend webkitTransitionEnd oTransitionEnd otransitionend", t.proxy(this.animateInBGComplete, this))) : TweenMax.to(this.$el.get(0), .55, {
                opacity: 1,
                onComplete: t.proxy(this.animateInBGComplete, this),
                delay: .05
            })
        }
        ,
        i.prototype.animateInBGComplete = function() {
            return this.$el.off("transitionend webkitTransitionEnd oTransitionEnd otransitionend", t.proxy(this.animateInBGComplete, this))
        }
        ,
        i.prototype.animateOutBG = function() {
            return Modernizr.csstransitions ? (this.$el.removeClass("animate-in").addClass("animate-out"),
            this.$el.off(),
            this.$el.on("transitionend webkitTransitionEnd oTransitionEnd otransitionend", t.proxy(this.animateOutBGComplete, this))) : TweenMax.to(this.el, .5, {
                opacity: 0,
                onComplete: t.proxy(this.animateOutBGComplete, this)
            })
        }
        ,
        i.prototype.animateOutBGComplete = function() {
            return this.$el.off("transitionend webkitTransitionEnd oTransitionEnd otransitionend", t.proxy(this.animateOutBGComplete, this)),
            this.$el.hide()
        }
        ,
        i.prototype.animateIn = function(t) {
            var e;
            return n.Notifications.trigger("overlayAnimateIn"),
            TweenMax.killDelayedCallsTo(this.animateInComplete),
            TweenMax.killDelayedCallsTo(this.animateOutComplete),
            e = t || .55,
            TweenMax.delayedCall(e, this.animateInComplete, null, this)
        }
        ,
        i.prototype.animateInComplete = function() {
            return this.align(),
            this.signal.dispatch("animateInComplete"),
            n.Notifications.trigger("overlayAnimateInComplete")
        }
        ,
        i.prototype.animateOut = function(t) {
            return t = t || {},
            t.fade = t.fade || !1,
            TweenMax.killDelayedCallsTo(this.animateInComplete),
            TweenMax.killDelayedCallsTo(this.animateOutComplete),
            this.animateOutBG(),
            n.Notifications.trigger("overlayAnimateOut"),
            t.fade ? TweenMax.delayedCall(.55, this.animateOutComplete, null, this) : this.animateOutComplete()
        }
        ,
        i.prototype.animateOutComplete = function() {
            return this.$el.removeClass("animate-in").removeClass("animate-out").hide(),
            this.signal.dispatch("animateOutComplete"),
            n.Notifications.trigger("overlayAnimateOutComplete")
        }
        ,
        i
    }(i)
}),
define("runtime", [], function() {
    function t(t) {
        return null != t
    }
    var e = {};
    return Array.isArray || (Array.isArray = function(t) {
        return "[object Array]" == Object.prototype.toString.call(t)
    }
    ),
    Object.keys || (Object.keys = function(t) {
        var e = [];
        for (var n in t)
            t.hasOwnProperty(n) && e.push(n);
        return e
    }
    ),
    e.merge = function(e, n) {
        var i = e["class"]
          , r = n["class"];
        (i || r) && (i = i || [],
        r = r || [],
        Array.isArray(i) || (i = [i]),
        Array.isArray(r) || (r = [r]),
        i = i.filter(t),
        r = r.filter(t),
        e["class"] = i.concat(r).join(" "));
        for (var o in n)
            "class" != o && (e[o] = n[o]);
        return e
    }
    ,
    e.attrs = function(t, n) {
        var i = []
          , r = t.terse;
        delete t.terse;
        var o = Object.keys(t)
          , s = o.length;
        if (s) {
            i.push("");
            for (var a = 0; s > a; ++a) {
                var l = o[a]
                  , u = t[l];
                "boolean" == typeof u || null == u ? u && (r ? i.push(l) : i.push(l + '="' + l + '"')) : 0 == l.indexOf("data") && "string" != typeof u ? i.push(l + "='" + JSON.stringify(u) + "'") : "class" == l && Array.isArray(u) ? i.push(l + '="' + e.escape(u.join(" ")) + '"') : n && n[l] ? i.push(l + '="' + e.escape(u) + '"') : i.push(l + '="' + u + '"')
            }
        }
        return i.join(" ")
    }
    ,
    e.escape = function(t) {
        return String(t).replace(/&(?!(\w+|\#\d+);)/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;")
    }
    ,
    e.rethrow = function(t, e, n) {
        if (!e)
            throw t;
        var i = 3
          , r = require("fs").readFileSync(e, "utf8")
          , o = r.split("\n")
          , s = Math.max(n - i, 0)
          , a = Math.min(o.length, n + i)
          , i = o.slice(s, a).map(function(t, e) {
            var i = e + s + 1;
            return (i == n ? "  > " : "    ") + i + "| " + t
        }).join("\n");
        throw t.path = e,
        t.message = (e || "Jade") + ":" + n + "\n" + i + "\n\n" + t.message,
        t
    }
    ,
    e
}),
define("app/templates/project", ["runtime"], function(jade) {
    return function anonymous(locals, attrs, escape, rethrow, merge) {
        attrs = attrs || jade.attrs,
        escape = escape || jade.escape,
        rethrow = rethrow || jade.rethrow,
        merge = merge || jade.merge;
        var buf = [];
        with (locals || {}) {
            var interp;
            buf.push('<div id="projectView" class="overlayContentView"><div class="projectHomeContainer"><div class="projectDescriptionRow"><h1 class="projectDescription"><span class="highlight">');
            var __val__ = project.get("name");
            buf.push(escape(null == __val__ ? "" : __val__)),
            buf.push("</span>&nbsp;&mdash;&nbsp;" + escape(null == (interp = project.get("title")) ? "" : interp) + '</h1><div class="projectSeparator"><div class="projectSeparatorContent"></div></div><h5 class="projectInfo"><span class="highlight">Role:</span>');
            var __val__ = project.get("role");
            buf.push(escape(null == __val__ ? "" : __val__)),
            buf.push('<br/><span class="highlight">Studio:</span>');
            var __val__ = project.get("studio");
            buf.push(escape(null == __val__ ? "" : __val__)),
            buf.push('</h5><span class="projectLoader"></span></div></div></div>')
        }
        return buf.join("")
    }
}),
!function(t, e, n) {
    function i(t, n) {
        var i, r = e.createElement(t || "div");
        for (i in n)
            r[i] = n[i];
        return r
    }
    function r(t) {
        for (var e = 1, n = arguments.length; n > e; e++)
            t.appendChild(arguments[e]);
        return t
    }
    function o(t, e, n, i) {
        var r = ["opacity", e, ~~(100 * t), n, i].join("-")
          , o = .01 + n / i * 100
          , s = Math.max(1 - (1 - t) / e * (100 - o), t)
          , a = c.substring(0, c.indexOf("Animation")).toLowerCase()
          , l = a && "-" + a + "-" || "";
        return p[r] || (f.insertRule("@" + l + "keyframes " + r + "{0%{opacity:" + s + "}" + o + "%{opacity:" + t + "}" + (o + .01) + "%{opacity:1}" + (o + e) % 100 + "%{opacity:" + t + "}100%{opacity:" + s + "}}", f.cssRules.length),
        p[r] = 1),
        r
    }
    function s(t, e) {
        var i, r, o = t.style;
        if (o[e] !== n)
            return e;
        for (e = e.charAt(0).toUpperCase() + e.slice(1),
        r = 0; r < h.length; r++)
            if (i = h[r] + e,
            o[i] !== n)
                return i
    }
    function a(t, e) {
        for (var n in e)
            t.style[s(t, n) || n] = e[n];
        return t
    }
    function l(t) {
        for (var e = 1; e < arguments.length; e++) {
            var i = arguments[e];
            for (var r in i)
                t[r] === n && (t[r] = i[r])
        }
        return t
    }
    function u(t) {
        for (var e = {
            x: t.offsetLeft,
            y: t.offsetTop
        }; t = t.offsetParent; )
            e.x += t.offsetLeft,
            e.y += t.offsetTop;
        return e
    }
    var c, h = ["webkit", "Moz", "ms", "O"], p = {}, f = function() {
        var t = i("style", {
            type: "text/css"
        });
        return r(e.getElementsByTagName("head")[0], t),
        t.sheet || t.styleSheet
    }(), d = {
        lines: 12,
        length: 7,
        width: 5,
        radius: 10,
        rotate: 0,
        corners: 1,
        color: "#000",
        speed: 1,
        trail: 100,
        opacity: .25,
        fps: 20,
        zIndex: 2e9,
        className: "spinner",
        top: "auto",
        left: "auto",
        position: "relative"
    }, m = function g(t) {
        return this.spin ? void (this.opts = l(t || {}, g.defaults, d)) : new g(t)
    };
    m.defaults = {},
    l(m.prototype, {
        spin: function(t) {
            this.stop();
            var e, n, r = this, o = r.opts, s = r.el = a(i(0, {
                className: o.className
            }), {
                position: o.position,
                width: 0,
                zIndex: o.zIndex
            }), l = o.radius + o.length + o.width;
            if (t && (t.insertBefore(s, t.firstChild || null),
            n = u(t),
            e = u(s),
            a(s, {
                left: ("auto" == o.left ? n.x - e.x + (t.offsetWidth >> 1) : parseInt(o.left, 10) + l) + "px",
                top: ("auto" == o.top ? n.y - e.y + (t.offsetHeight >> 1) : parseInt(o.top, 10) + l) + "px"
            })),
            s.setAttribute("aria-role", "progressbar"),
            r.lines(s, r.opts),
            !c) {
                var h = 0
                  , p = o.fps
                  , f = p / o.speed
                  , d = (1 - o.opacity) / (f * o.trail / 100)
                  , m = f / o.lines;
                !function g() {
                    h++;
                    for (var t = o.lines; t; t--) {
                        var e = Math.max(1 - (h + t * m) % f * d, o.opacity);
                        r.opacity(s, o.lines - t, e, o)
                    }
                    r.timeout = r.el && setTimeout(g, ~~(1e3 / p))
                }()
            }
            return r
        },
        stop: function() {
            var t = this.el;
            return t && (clearTimeout(this.timeout),
            t.parentNode && t.parentNode.removeChild(t),
            this.el = n),
            this
        },
        lines: function(t, e) {
            function n(t, n) {
                return a(i(), {
                    position: "absolute",
                    width: e.length + e.width + "px",
                    height: e.width + "px",
                    background: t,
                    boxShadow: n,
                    transformOrigin: "left",
                    transform: "rotate(" + ~~(360 / e.lines * l + e.rotate) + "deg) translate(" + e.radius + "px,0)",
                    borderRadius: (e.corners * e.width >> 1) + "px"
                })
            }
            for (var s, l = 0; l < e.lines; l++)
                s = a(i(), {
                    position: "absolute",
                    top: 1 + ~(e.width / 2) + "px",
                    transform: e.hwaccel ? "translate3d(0,0,0)" : "",
                    opacity: e.opacity,
                    animation: c && o(e.opacity, e.trail, l, e.lines) + " " + 1 / e.speed + "s linear infinite"
                }),
                e.shadow && r(s, a(n("#000", "0 0 4px #000"), {
                    top: "2px"
                })),
                r(t, r(s, n(e.color, "0 0 1px rgba(0,0,0,.1)")));
            return t
        },
        opacity: function(t, e, n) {
            e < t.childNodes.length && (t.childNodes[e].style.opacity = n)
        }
    }),
    function() {
        function t(t, e) {
            return i("<" + t + ' xmlns="urn:schemas-microsoft.com:vml" class="spin-vml">', e)
        }
        var e = a(i("group"), {
            behavior: "url(#default#VML)"
        });
        !s(e, "transform") && e.adj ? (f.addRule(".spin-vml", "behavior:url(#default#VML)"),
        m.prototype.lines = function(e, n) {
            function i() {
                return a(t("group", {
                    coordsize: u + " " + u,
                    coordorigin: -l + " " + -l
                }), {
                    width: u,
                    height: u
                })
            }
            function o(e, o, s) {
                r(h, r(a(i(), {
                    rotation: 360 / n.lines * e + "deg",
                    left: ~~o
                }), r(a(t("roundrect", {
                    arcsize: n.corners
                }), {
                    width: l,
                    height: n.width,
                    left: n.radius,
                    top: -n.width >> 1,
                    filter: s
                }), t("fill", {
                    color: n.color,
                    opacity: n.opacity
                }), t("stroke", {
                    opacity: 0
                }))))
            }
            var s, l = n.length + n.width, u = 2 * l, c = 2 * -(n.width + n.length) + "px", h = a(i(), {
                position: "absolute",
                top: c,
                left: c
            });
            if (n.shadow)
                for (s = 1; s <= n.lines; s++)
                    o(s, -2, "progid:DXImageTransform.Microsoft.Blur(pixelradius=2,makeshadow=1,shadowopacity=.3)");
            for (s = 1; s <= n.lines; s++)
                o(s);
            return r(e, h)
        }
        ,
        m.prototype.opacity = function(t, e, n, i) {
            var r = t.firstChild;
            i = i.shadow && i.lines || 0,
            r && e + i < r.childNodes.length && (r = r.childNodes[e + i],
            r = r && r.firstChild,
            r = r && r.firstChild,
            r && (r.opacity = n))
        }
        ) : c = s(e, "animation")
    }(),
    "function" == typeof define && define.amd ? define("components/loader/spin", [], function() {
        return m
    }) : t.Spinner = m
}(window, document);
var extend = function(t, e) {
    function n() {
        this.constructor = t
    }
    for (var i in e)
        hasProp.call(e, i) && (t[i] = e[i]);
    return n.prototype = e.prototype,
    t.prototype = new n,
    t.__super__ = e.prototype,
    t
}
  , hasProp = {}.hasOwnProperty;
define("views/project/project", ["jquery", "underscore", "backbone", "views/overlay/overlay", "app/templates/project", "components/loader/spin", "config", "greensock"], function(t, e, n, i, r, o, s) {
    var a;
    return a = function(e) {
        function n() {
            return n.__super__.constructor.apply(this, arguments)
        }
        return extend(n, e),
        n.prototype._templateData = null,
        n.prototype._loadStartTime = null,
        n.prototype.$homeContainer = null,
        n.prototype.$projectDescriptionRow = null,
        n.prototype.projectDescription = null,
        n.prototype.$projectDescription = null,
        n.prototype.projectSeparator = null,
        n.prototype.$projectSeparator = null,
        n.prototype.projectSeparatorHeight = 0,
        n.prototype.projectInfo = null,
        n.prototype.$projectInfo = null,
        n.prototype.spinner = null,
        n.prototype.$spinner = null,
        n.prototype.contentEl = null,
        n.prototype.$contentEl = null,
        n.prototype.initialize = function() {
            return n.__super__.initialize.apply(this, arguments)
        }
        ,
        n.prototype.render = function() {
            var e, n;
            return e = r({
                project: this.collection.getSelected(),
                prevProject: this.collection.getPrevious(),
                nextProject: this.collection.getNext()
            }),
            this.$el.html(e),
            n = {
                lines: 17,
                length: 5,
                width: 2,
                radius: 13,
                corners: 1,
                rotate: 5,
                color: "#2bb0a8",
                speed: 1.3,
                trail: 60,
                shadow: !1,
                hwaccel: !1,
                className: "spinner",
                zIndex: 0,
                top: "auto",
                left: "auto"
            },
            this.spinner = new o(n).spin(this.$el.find(".projectLoader").get(0)),
            this.$homeContainer = this.$el.find(".projectHomeContainer"),
            this.$projectDescriptionRow = this.$el.find(".projectDescriptionRow"),
            this.projectDescription = this.$el.find(".projectDescription").get(0),
            this.$projectDescription = t(this.projectDescription),
            this.projectSeparator = this.$el.find(".projectSeparatorContent").get(0),
            this.$projectSeparator = t(this.projectSeparator),
            this.projectInfo = this.$el.find(".projectInfo").get(0),
            this.$projectInfo = t(this.projectInfo),
            this.projectLoader = this.$el.find(".projectLoader").get(0),
            this.$projectLoader = t(this.projectLoader),
            this.contentEL = this.$el.find(".overlayContentView"),
            this.$contentEl = t(this.contentEl),
            i.prototype.render.call(this)
        }
        ,
        n.prototype.unbind = function() {
            return TweenMax.killTweensOf(this.contentEL),
            TweenMax.killDelayedCallsTo(this.align),
            i.prototype.unbind.call(this)
        }
        ,
        n.prototype.align = function() {
            var t, e, i;
            return t = this.$homeContainer.height(),
            e = this.$projectDescriptionRow.height(),
            i = Math.round((t - e) / 2),
            this.$projectDescriptionRow.css("margin-top", i),
            n.__super__.align.apply(this, arguments),
            0 === e ? (this.$homeContainer.css("visibility", "hidden"),
            TweenMax.delayedCall(.1, this.align, [], this)) : this.$homeContainer.css("visibility", "visible")
        }
        ,
        n.prototype.loadCompleted = function() {
            return this.spinner.stop(),
            this.spinner = null,
            this.$el.find(".projectLoader").unbind(),
            this.$el.find(".projectLoader").remove()
        }
        ,
        n.prototype.animateIn = function() {
            return !s.isHandheld() && Modernizr.cssanimations ? (this.$projectDescription.addClass("animate-in"),
            this.$projectSeparator.addClass("animate-in"),
            this.$projectInfo.addClass("animate-in"),
            this.$projectLoader.addClass("animate-in")) : (this.$projectDescription.css("opacity", 1),
            this.$projectSeparator.show(),
            this.$projectInfo.css("opacity", 1),
            this.$projectLoader.css("opacity", 1)),
            n.__super__.animateIn.apply(this, arguments)
        }
        ,
        n.prototype.animateOut = function(t) {
            return n.__super__.animateOut.apply(this, arguments)
        }
        ,
        n.prototype.remove = function() {
            return this.spinner && (this.spinner.stop(),
            this.spinner = null,
            this.$el.find(".projectLoader").unbind(),
            this.$el.find(".projectLoader").remove()),
            this.$contentEl.remove(),
            this.contentEl = null,
            this.$contentEl = null
        }
        ,
        n
    }(i)
}),
define("app/templates/project-navigation", ["runtime"], function(jade) {
    return function anonymous(locals, attrs, escape, rethrow, merge) {
        attrs = attrs || jade.attrs,
        escape = escape || jade.escape,
        rethrow = rethrow || jade.rethrow,
        merge = merge || jade.merge;
        var buf = [];
        with (locals || {}) {
            var arrow_mixin = function(t) {
                this.block,
                this.attributes || {},
                this.escaped || {};
                buf.push("<div"),
                buf.push(attrs({
                    "data-deg": t,
                    "class": "arrowIcon"
                }, {
                    "class": !0,
                    "data-deg": !0
                })),
                buf.push('><?xml version="1.0" encoding="utf-8" ?><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 64 64" style="enable-background:new 0 0 64 64;" xml:space="preserve"><style>.st0{fill:none;stroke:#000000;stroke-width:6;stroke-miterlimit:10;}</style><g'),
                buf.push(attrs({
                    transform: "rotate(" + t + " 32 32)"
                }, {
                    transform: !0
                })),
                buf.push('><polyline points="23,36 32.2,26.8 41.4,36 " class="st0"></polyline></g></svg></div>')
            };
            buf.push('<ul class="projectNavigation"><li><a'),
            buf.push(attrs({
                id: "leftButton",
                href: "work/" + prevProject.get("slug") + "/",
                "class": "projectNavButton"
            }, {
                href: !0,
                "class": !0
            })),
            buf.push('><img src="/assets/images/ui/arrow_circle.png" class="projectNavButtonCircle"></img>'),
            arrow_mixin(-90),
            buf.push('</a></li><li><a id="topButton" href="#" class="projectNavButton"><img src="/assets/images/ui/arrow_circle.png" class="projectNavButtonCircle"></img>'),
            arrow_mixin(0),
            buf.push("</a></li><li><a"),
            buf.push(attrs({
                id: "rightButton",
                href: "work/" + nextProject.get("slug") + "/",
                "class": "projectNavButton"
            }, {
                href: !0,
                "class": !0
            })),
            buf.push('><img src="/assets/images/ui/arrow_circle.png" class="projectNavButtonCircle"></img>'),
            arrow_mixin(90),
            buf.push("</a></li></ul>")
        }
        return buf.join("")
    }
}),
define("components/buttons/ProjectDetailsNavButton", ["jquery", "modernizr", "greensock"], function(t) {
    var e;
    return e = function() {
        return this._el = null,
        this.$el = null,
        this._icon = null,
        this.$icon = null,
        this._deg = 0
    }
    ,
    e.prototype.initialize = function(e) {
        return this._el = e,
        this.$el = t(this._el),
        this._icon = this.$el.find(".arrowIcon").get(0),
        this.$icon = t(this._icon),
        this._deg = this.$icon.data("deg"),
        Modernizr.touch ? this.$el.on("vmousedown", t.proxy(this.onMouseOver, this)) : this.$el.on("mouseenter", t.proxy(this.onMouseOver, this))
    }
    ,
    e.prototype.destroy = function() {
        return Modernizr.touch ? this.$el.off("vmousedown", t.proxy(this.onMouseOver, this)) : this.$el.off("mouseenter", t.proxy(this.onMouseOver, this))
    }
    ,
    e.prototype.onMouseOver = function(t) {
        return this.animateOver()
    }
    ,
    e.prototype.animateOver = function(t) {
        var e, n, i;
        switch (i = {},
        n = {},
        this._deg) {
        case -90:
            i = {
                x: -this.$el.width() / 2
            },
            n = {
                x: this.$el.width() / 2
            };
            break;
        case 90:
            i = {
                x: this.$el.width() / 2
            },
            n = {
                x: -this.$el.width() / 2
            };
            break;
        default:
            i = {
                y: -this.$el.height() / 2
            },
            n = {
                y: this.$el.height() / 2
            }
        }
        return e = new TimelineMax,
        e.add(TweenMax.to(this._icon, .15, {
            css: i,
            ease: Sine.easeIn
        })),
        e.add(TweenMax.fromTo(this._icon, .15, {
            css: n,
            ease: Sine.easeOut
        }, {
            x: 0,
            y: 0,
            ease: Sine.easeOut
        }))
    }
    ,
    e
});
var extend = function(t, e) {
    function n() {
        this.constructor = t
    }
    for (var i in e)
        hasProp.call(e, i) && (t[i] = e[i]);
    return n.prototype = e.prototype,
    t.prototype = new n,
    t.__super__ = e.prototype,
    t
}
  , hasProp = {}.hasOwnProperty;
define("views/project/projectDetails", ["jquery", "underscore", "backbone", "views/abstract", "app/templates/project-navigation", "signals", "config", "components/scroller/MinimalScroller", "components/buttons/ProjectDetailsNavButton", "requestAnimationFrame", "greensock", "modernizr"], function(t, e, n, i, r, o, s, a, l) {
    var u;
    return u = function(i) {
        function u() {
            return u.__super__.constructor.apply(this, arguments)
        }
        return extend(u, i),
        u.prototype.id = "projectDetailsView",
        u.prototype.signal = new o.Signal,
        u.prototype._minLoadWait = 3e3,
        u.prototype._loadStartTime = null,
        u.prototype._loadEndTime = null,
        u.prototype._isLoaded = !1,
        u.prototype._prevMarginTopVal = null,
        u.prototype._viewHeight = 0,
        u.prototype._viewWidth = 0,
        u.prototype._projectNavButtons = null,
        u.prototype._projectDetailsInnerEl = null,
        u.prototype._projectDetailContentEl = null,
        u.prototype._projectDetailWrapperEl = null,
        u.prototype._projectDetailWrapperContentEl = null,
        u.prototype._projectDetailContentMaskEl = null,
        u.prototype._currentTime = 0,
        u.prototype._currentScrollY = 0,
        u.prototype._currentScrollYN = 0,
        u.prototype._scroller = null,
        u.prototype._forceSnap = !1,
        u.prototype._prefixed = Modernizr.prefixed("transform"),
        u.prototype._scrollTop = 0,
        u.prototype._targetMarginTopVal = 0,
        u.prototype._marginTopVal = 0,
        u.prototype._currentContentVal = 0,
        u.prototype._contentMaskVal = 0,
        u.prototype.initialize = function(e) {
            return this.render(),
            t(window).on("resize", t.proxy(this.align, this))
        }
        ,
        u.prototype.events = function() {
            return Modernizr.touch ? {
                "click #topButton": "onClickNavTopButton",
                "click #leftButton": "onClickNavLeftButton",
                "click #rightButton": "onClickNavRightButton",
                "vclick #topButton": "onClickNavTopButton",
                "vclick #leftButton": "onClickNavLeftButton",
                "vclick #rightButton": "onClickNavRightButton"
            } : {
                "click #topButton": "onClickNavTopButton",
                "click #leftButton": "onClickNavLeftButton",
                "click #rightButton": "onClickNavRightButton"
            }
        }
        ,
        u.prototype.unbind = function() {
            var e, n;
            if (this.signal.removeAll(),
            TweenMax.killDelayedCallsTo(this.renderComplete),
            t(window).off("resize", t.proxy(this.align, this)),
            this._scroller && this._scroller.off("update", t.proxy(this.onScrollBarUpdate, this)),
            this._projectNavButtons)
                for (n = 0; n < this._projectNavButtons.length; )
                    e = this._projectNavButtons[n],
                    e.destroy(),
                    e = null,
                    n++;
            return this._projectNavButtons = null
        }
        ,
        u.prototype.remove = function() {
            return this._scroller && (this._scroller.destroy(),
            this._scroller = null),
            this.$el.remove()
        }
        ,
        u.prototype.render = function() {
            var t, n;
            return this._isLoaded ? (n = this,
            this.$el.append('<div id="projectDetailsInner"><div id="projectDetailContentMask"></div></div>'),
            this.$el.find("#projectDetailContentMask").append(e.template(this.template, {
                model: this.model
            })),
            t = r({
                project: this.model,
                prevProject: this.collection.getPrevious(),
                nextProject: this.collection.getNext()
            }),
            this.$el.find(".projectDetailContent").append(t),
            this.$el.find(".projectNavButton").each(function(t, e) {
                var i;
                return i = new l,
                i.initialize(e),
                null === n._projectNavButtons && (n._projectNavButtons = []),
                n._projectNavButtons.push(i)
            }),
            this._projectDetailsInnerEl = this.$el.find("#projectDetailsInner").get(0),
            this._projectDetailContentMaskEl = this.$el.find("#projectDetailContentMask").get(0),
            this._projectDetailContentEl = this.$el.find(".projectDetailContent").get(0),
            this.renderImages(),
            this) : this
        }
        ,
        u.prototype.renderImages = function() {
            var t, e, n;
            return n = this.$el.find(".projectDetailContent img").length,
            t = 0,
            e = this,
            this.$el.find(".projectDetailContent img").bind("load", function() {
                return t++,
                n === t ? e.renderComplete() : void 0
            }),
            this.$el.find(".projectDetailContent img").bind("error", function() {
                e.loadProjectTemplateError()
            })
        }
        ,
        u.prototype.renderComplete = function() {
            var e, n;
            return this._loadEndTime = (new Date).getTime(),
            e = this._loadEndTime - this._loadStartTime,
            e < this._minLoadWait ? (n = this._minLoadWait - e,
            void TweenMax.delayedCall(n / 1e3, this.renderComplete, null, this)) : (this.align(),
            this.animateIn(),
            TweenMax.to(this._scroller, .7, {
                scrollTop: this._viewHeight,
                onComplete: t.proxy(this.animateInComplete, this),
                ease: Quad.easeOut
            }))
        }
        ,
        u.prototype.loadProjectTemplate = function() {
            var e, n;
            return n = "text!" + s.relUri + "data/" + this.model.get("template"),
            e = "css!" + s.relUri + "data/" + this.model.get("css"),
            this._loadStartTime = (new Date).getTime(),
            require([n, e], t.proxy(this.loadProjectTemplateComplete, this), t.proxy(this.loadProjectTemplateError, this))
        }
        ,
        u.prototype.loadProjectTemplateComplete = function(t, e) {
            return this._isLoaded = !0,
            this.template = t,
            this.render()
        }
        ,
        u.prototype.loadProjectTemplateError = function() {
            return this._isLoaded = !1,
            n.Notifications.trigger("projectLoadingError")
        }
        ,
        u.prototype.initScrollBar = function() {
            return this._scroller && (this._scroller.off("update", t.proxy(this.onScrollBarUpdate, this)),
            this._scroller.destroy(),
            this._scroller = null),
            this._scroller = new a(this.$el.find("#projectDetailsInner").get(0),t(this._projectDetailContentEl).outerHeight() + this._viewHeight,t(document).height(),{
                multiplier: Modernizr.touch ? .25 : .35
            }),
            this._scroller.on("update", t.proxy(this.onScrollBarUpdate, this)),
            this.scrollUpdate(0)
        }
        ,
        u.prototype.onScrollBarUpdate = function(t) {
            return this.scrollUpdate(t.scrollTopPercentage)
        }
        ,
        u.prototype.scrollUpdate = function(e) {
            var n, i, r;
            return this._scrollTop = t(this._projectDetailContentEl).outerHeight() * e,
            this._marginTopVal = this._viewHeight - this._scrollTop,
            r = this._viewHeight - this._marginTopVal,
            i = r >= this._viewHeight ? this._marginTopVal : -this._marginTopVal,
            n = r >= this._viewHeight ? 0 : this._marginTopVal,
            i !== this._currentContentVal && (this._currentContentVal = i,
            Modernizr.csstransforms3d ? this._projectDetailContentEl.style[this._prefixed] = "translate3d(0px, " + this._currentContentVal + "px, 0px)" : this._projectDetailContentEl.style[this._prefixed] = "translate(0px, " + this._currentContentVal + "px)"),
            n !== this._contentMaskVal ? (this._contentMaskVal = n,
            Modernizr.csstransforms3d ? this._projectDetailContentMaskEl.style[this._prefixed] = "translate3d(0px, " + this._contentMaskVal + "px, 0px)" : this._projectDetailContentMaskEl.style[this._prefixed] = "translate(0px, " + this._contentMaskVal + "px)") : void 0
        }
        ,
        u.prototype.align = function() {
            var e;
            return this._viewHeight = t(document).height(),
            this._viewWidth = t(document).width(),
            this.$el.css("height", this._viewHeight),
            t(this._projectDetailsInnerEl).css("height", this._viewHeight),
            t(this._projectDetailContentMaskEl).css("height", this._viewHeight),
            e = 0,
            this._viewWidth > parseInt(this.$el.find(".projectDetailContent").css("max-width")) && (e = Math.floor((this._viewWidth - this.$el.find(".projectDetailContent").width()) / 2)),
            this.$el.find(".projectDetailContent").css("margin-left", e),
            this._scroller ? (this._scroller.setContentHeight(t(this._projectDetailContentEl).height()),
            this._scroller.setContainerHeight(this._viewHeight)) : void 0
        }
        ,
        u.prototype.animateIn = function() {
            return this.signal.dispatch("animateIn"),
            this._forceSnap = !0,
            this.initScrollBar(),
            this.$el.css("opacity", 1)
        }
        ,
        u.prototype.animateInComplete = function() {
            return this.signal.dispatch("animateInComplete"),
            this._forceSnap = !1
        }
        ,
        u.prototype.animateOut = function() {
            return this.el.style[Modernizr.prefixed("transition-duration")] = "0.5s",
            TweenMax.set(this.$el.get(0), {
                opacity: 0
            }),
            TweenMax.delayedCall(.5, t.proxy(this.animateOutComplete, this))
        }
        ,
        u.prototype.animateOutComplete = function() {
            return this.signal.dispatch("animateOutComplete")
        }
        ,
        u.prototype.onClickNavTopButton = function(t) {
            var e, n;
            return t.preventDefault(),
            n = this.$el.find(".projectDetailContent").height() / 1e3 * .35,
            TweenMax.isTweening(this._scroller) || (e = this,
            TweenMax.to(this._scroller, n, {
                scrollTop: 0,
                onStart: function() {
                    return e._forceSnap = !0
                },
                onComplete: function() {
                    return e._forceSnap = !1
                },
                ease: Quint.easeInOut
            })),
            !1
        }
        ,
        u.prototype.onClickNavLeftButton = function(e) {
            return e.preventDefault(),
            n.history.navigate(t(e.currentTarget).attr("href"), {
                trigger: !0
            }),
            !1
        }
        ,
        u.prototype.onClickNavRightButton = function(e) {
            return e.preventDefault(),
            n.history.navigate(t(e.currentTarget).attr("href"), {
                trigger: !0
            }),
            !1
        }
        ,
        u
    }(i)
}),
define("app/templates/info", ["runtime"], function(jade) {
    return function anonymous(locals, attrs, escape, rethrow, merge) {
        attrs = attrs || jade.attrs,
        escape = escape || jade.escape,
        rethrow = rethrow || jade.rethrow,
        merge = merge || jade.merge;
        var buf = [];
        with (locals || {}) {
            buf.push('<div id="infoView" class="overlayContentView"><div class="infoTitleRow"><h1 class="infoTitle"><span class="highlight">WeLoveNoise</span>&nbsp;&mdash;&nbsp;Is the creative outlet of Luke Finch,<br/>A Multi-Disciplinary Design Director currently <br/>working in San Francisco, CA.</h1></div><div class="infoSocialRow"><ul><li><a href="http://twitter.com/welovenoise" target="_blank">Twitter</a></li><li class="divider">/</li><li><a href="http://www.linkedin.com/in/welovenoise" target="_blank">LinkedIn</a></li><li class="divider">/</li><li><a href="https://www.facebook.com/WeLoveNoise" target="_blank">Facebook</a></li><li class="divider">/</li><li><a href="http://www.behance.net/WeLoveNoise" target="_blank">Behance</a></li><li class="divider">/</li><li><a href="https://dribbble.com/welovenoise" target="_blank">Dribbble</a></li><li class="block"><a href="mailto:helloluke@welovenoise.com" target="_blank">helloluke@welovenoise.com</a></li></ul></div></div>')
        }
        return buf.join("")
    }
});
var extend = function(t, e) {
    function n() {
        this.constructor = t
    }
    for (var i in e)
        hasProp.call(e, i) && (t[i] = e[i]);
    return n.prototype = e.prototype,
    t.prototype = new n,
    t.__super__ = e.prototype,
    t
}
  , hasProp = {}.hasOwnProperty;
define("views/info/info", ["jquery", "underscore", "backbone", "views/overlay/overlay", "app/templates/info", "config", "greensock"], function(t, e, n, i, r, o) {
    var s;
    return s = function(e) {
        function n() {
            return n.__super__.constructor.apply(this, arguments)
        }
        return extend(n, e),
        n.prototype.contentEl = null,
        n.prototype.$contentEl = null,
        n.prototype.infoTitleRow = null,
        n.prototype.$infoTitleRow = null,
        n.prototype.infoSocialRow = null,
        n.prototype.$infoSocialRow = null,
        n.prototype._animateInTimeline = null,
        n.prototype.events = function() {
            return {}
        }
        ,
        n.prototype.render = function() {
            return this.$el.html(r()),
            this.contentEL = this.$el.find(".overlayContentView").get(0),
            this.$contentEl = t(this.contentEl),
            this.infoTitleRow = this.$el.find(".infoTitleRow").get(0),
            this.$infoTitleRow = t(this.infoTitleRow),
            this.infoSocialRow = this.$el.find(".infoSocialRow").get(0),
            this.$infoSocialRow = t(this.infoSocialRow),
            this
        }
        ,
        n.prototype.remove = function() {
            return this.infoTitleRow = null,
            this.$infoTitleRow = null,
            this.infoSocialRow = null,
            this.$infoSocialRow = null,
            this.$contentEl.remove(),
            this.contentEl = null,
            this.$contentEl = null,
            n.__super__.remove.apply(this, arguments)
        }
        ,
        n.prototype.unbind = function() {
            return this._animateInTimeline && (this._animateInTimeline.kill(),
            this._animateInTimeline = null),
            TweenMax.killTweensOf(this.contentEL),
            i.prototype.unbind.call(this)
        }
        ,
        n.prototype.animateIn = function() {
            return !o.isHandheld() && Modernizr.cssanimations ? (this.$infoTitleRow.addClass("animate-in"),
            this.$infoSocialRow.addClass("animate-in")) : (this.$infoTitleRow.css("opacity", 1),
            this.$infoSocialRow.css("opacity", 1)),
            n.__super__.animateIn.apply(this, arguments)
        }
        ,
        n.prototype.animateOut = function(t) {
            return this.$infoTitleRow.removeClass("animate-in"),
            this.$infoSocialRow.removeClass("animate-in"),
            n.__super__.animateOut.apply(this, arguments)
        }
        ,
        n
    }(i)
}),
define("app/templates/error", ["runtime"], function(jade) {
    return function anonymous(locals, attrs, escape, rethrow, merge) {
        attrs = attrs || jade.attrs,
        escape = escape || jade.escape,
        rethrow = rethrow || jade.rethrow,
        merge = merge || jade.merge;
        var buf = [];
        with (locals || {}) {
            buf.push('<div id="errorView" class="overlayContentView"><div class="errorTitleRow"><h1 class="errorTitle"><span class="highlight">Error</span>&nbsp;&mdash;&nbsp;Move along now,<br/>Nothing more to see here</h1></div></div>')
        }
        return buf.join("")
    }
});
var extend = function(t, e) {
    function n() {
        this.constructor = t
    }
    for (var i in e)
        hasProp.call(e, i) && (t[i] = e[i]);
    return n.prototype = e.prototype,
    t.prototype = new n,
    t.__super__ = e.prototype,
    t
}
  , hasProp = {}.hasOwnProperty;
define("views/error/error", ["jquery", "underscore", "backbone", "views/overlay/overlay", "app/templates/error"], function(t, e, n, i, r) {
    var o;
    return o = function(e) {
        function n() {
            return n.__super__.constructor.apply(this, arguments)
        }
        return extend(n, e),
        n.prototype.events = function() {
            return Modernizr.touch ? {
                touchstart: "onTouchStart",
                touchmove: "onTouchMove",
                touchend: "onTouchEnd"
            } : {}
        }
        ,
        n.prototype.render = function() {
            return this.$el.empty().append(r()),
            this
        }
        ,
        n.prototype.onTouchStart = function(e) {
            return t(e.target).is("a")
        }
        ,
        n.prototype.onTouchMove = function(t) {
            return !0
        }
        ,
        n.prototype.onTouchEnd = function(t) {
            return !0
        }
        ,
        n
    }(i)
});
var extend = function(t, e) {
    function n() {
        this.constructor = t
    }
    for (var i in e)
        hasProp.call(e, i) && (t[i] = e[i]);
    return n.prototype = e.prototype,
    t.prototype = new n,
    t.__super__ = e.prototype,
    t
}
  , hasProp = {}.hasOwnProperty;
define("views/app", ["jquery", "underscore", "backbone", "views/abstract", "views/loader/loader", "views/home/home", "views/home/header", "views/project/project", "views/project/projectDetails", "views/info/info", "views/error/error", "config", "module", "greensock"], function(t, e, n, i, r, o, s, a, l, u, c, h, p) {
    var f;
    return f = function(e) {
        function i() {
            return i.__super__.constructor.apply(this, arguments)
        }
        return extend(i, e),
        i.prototype.id = "content",
        i.prototype.el = "#content",
        i.prototype.currentOverlay = null,
        i.prototype.nextOverlayName = null,
        i.prototype.projectDetailsView = null,
        i.prototype.initialize = function(e) {
            return e.collection && (this.collection = e.collection),
            this.loaderView = new r,
            this.headerView = new s({
                enabled: !1
            }),
            this.homeView = new o({
                collection: this.collection
            }),
            n.Notifications.on("loaderAnimateInComplete", this.onLoaderAnimatedIn, this),
            t(document).on("keydown", t.proxy(this.onKeyDown, this)),
            this.$el.on("touchmove", t.proxy(this.onTouchMove, this)),
            this.animateTitle(),
            i.__super__.initialize.apply(this, arguments)
        }
        ,
        i.prototype.onTouchMove = function(t) {
            return t.preventDefault()
        }
        ,
        i.prototype.onLoaderAnimatedIn = function() {
            return n.Notifications.off("loaderAnimateInComplete"),
            n.Notifications.on("homeLoadStart", this.onHomeLoadStart, this),
            n.Notifications.on("homeLoadComplete", this.onHomeLoadComplete, this)
        }
        ,
        i.prototype.onHomeLoadStart = function() {
            return n.Notifications.off("homeLoadStart")
        }
        ,
        i.prototype.onHomeLoadComplete = function() {
            return this.headerView.enabled(!0),
            t("#timeline").fadeIn("slow"),
            n.Notifications.off("homeLoadComplete")
        }
        ,
        i.prototype.showOverlay = function(e, n) {
            if (n = n || {},
            this.nextOverlayName = e,
            this.currentOverlay)
                return void this.currentOverlay.animateOut({
                    fade: !(null != this.projectDetailsView)
                });
            switch (this.nextOverlayName = null,
            e) {
            case "info":
                this.hideProjectDetails(),
                this.currentOverlay = new u(n),
                this.currentOverlay.signal.add(t.proxy(this.onOverlayAnimateInComplete, this)),
                this.currentOverlay.signal.add(t.proxy(this.onOverlayAnimateOutComplete, this)),
                this.currentOverlay.animateInBG(),
                this.currentOverlay.animateIn();
                break;
            case "error":
                this.hideProjectDetails(),
                this.currentOverlay = new c(n),
                this.currentOverlay.signal.add(t.proxy(this.onOverlayAnimateInComplete, this)),
                this.currentOverlay.signal.add(t.proxy(this.onOverlayAnimateOutComplete, this)),
                this.currentOverlay.animateInBG(),
                this.currentOverlay.animateIn();
                break;
            case "project":
                n.collection = this.collection,
                n.model = this.collection.getSelected(),
                this.currentOverlay = new a(n),
                this.showProjectDetails(n),
                this.currentOverlay.signal.add(t.proxy(this.onOverlayAnimateInComplete, this)),
                this.currentOverlay.signal.add(t.proxy(this.onOverlayAnimateOutComplete, this)),
                this.currentOverlay.animateInBG(),
                this.currentOverlay.animateIn()
            }
            return this.homeView.pause(!0),
            this.headerView.setState("close")
        }
        ,
        i.prototype.showProjectDetails = function(e) {
            return e = e || {},
            this.projectDetailsView ? (this.projectDetailsView.signal.add(t.proxy(this.onProjectDetailsAnimateOutComplete, this)),
            void this.projectDetailsView.animateOut({
                fade: !1
            })) : (e.collection = this.collection,
            e.model = this.collection.getSelected(),
            this.projectDetailsView = new l(e),
            this.projectDetailsView.signal.add(t.proxy(this.onProjectDetailsAnimateInComplete, this)),
            this.$el.find("#" + this.projectDetailsView.id).length > 0 && this.$el.find("#" + this.projectDetailsView.id).remove(),
            this.$el.append(this.projectDetailsView.render().el),
            this.projectDetailsView.loadProjectTemplate())
        }
        ,
        i.prototype.onProjectDetailsAnimateInComplete = function(e) {
            switch (e) {
            case "animateInComplete":
                if (this.projectDetailsView && this.projectDetailsView.signal.remove(t.proxy(this.onProjectDetailsAnimateInComplete, this)),
                this.currentOverlay && this.currentOverlay instanceof a)
                    return this.currentOverlay.loadCompleted()
            }
        }
        ,
        i.prototype.onProjectDetailsAnimateOutComplete = function(e) {
            switch (e) {
            case "animateOutComplete":
                return this.projectDetailsView && (this.projectDetailsView.signal.remove(t.proxy(this.onProjectDetailsAnimateOutComplete, this)),
                this.projectDetailsView.remove(),
                this.projectDetailsView.unbind(),
                this.projectDetailsView = null),
                this.showProjectDetails()
            }
        }
        ,
        i.prototype.onOverlayAnimateInComplete = function(e) {
            switch (e) {
            case "animateInComplete":
                return this.currentOverlay.signal.remove(t.proxy(this.onOverlayAnimateInComplete, this))
            }
        }
        ,
        i.prototype.onOverlayAnimateOutComplete = function(e) {
            switch (e) {
            case "animateOutComplete":
                return this.currentOverlay.signal.remove(t.proxy(this.onOverlayAnimateInComplete, this)),
                this.currentOverlay.signal.remove(t.proxy(this.onOverlayAnimateOutComplete, this)),
                this.currentOverlay.remove(),
                this.currentOverlay.unbind(),
                this.currentOverlay = null,
                this.nextOverlayName ? this.showOverlay(this.nextOverlayName) : this.homeView.pause(!1)
            }
        }
        ,
        i.prototype.hideOverlay = function(t) {
            return this.headerView.setState("info"),
            this.hideProjectDetails(),
            null != this.currentOverlay ? this.currentOverlay.animateOut({
                fade: !0
            }) : this.homeView.pause(!1)
        }
        ,
        i.prototype.hideProjectDetails = function() {
            return this.projectDetailsView ? (this.projectDetailsView.signal.add(t.proxy(this.onProjectDetailsHideComplete, this)),
            this.projectDetailsView.animateOut()) : void 0
        }
        ,
        i.prototype.onProjectDetailsHideComplete = function(e) {
            switch (e) {
            case "animateOutComplete":
                if (this.projectDetailsView)
                    return this.projectDetailsView.signal.remove(t.proxy(this.onProjectDetailsAnimateOutComplete, this)),
                    this.projectDetailsView.remove(),
                    this.projectDetailsView.unbind(),
                    this.projectDetailsView = null
            }
        }
        ,
        i.prototype.onKeyDown = function(t) {
            var e;
            switch (e = t.keyCode || t.which) {
            case 27:
                return n.Notifications.trigger("close");
            case 37:
                return n.Notifications.trigger("prevProject");
            case 39:
                return n.Notifications.trigger("nextProject")
            }
        }
        ,
        i.prototype.animateTitle = function() {
            var t = "Works of WeLoveNoise".split("");
            document.title = "";
            var e = 0
              , n = setInterval(function() {
                e < t.length ? " " == t[e] ? (document.title += " " + t[++e],
                e++) : document.title += t[e++] : clearInterval(n)
            }, 50)
        }
        ,
        i
    }(i)
}),
define("mout/string/WHITE_SPACES", [], function() {
    return [" ", "\n", "\r", "	", "\f", "", " ", " ", "᠎", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", "\u2028", "\u2029", " ", " ", "　"]
}),
define("mout/lang/toString", [], function() {
    function t(t) {
        return null == t ? "" : t.toString()
    }
    return t
}),
define("mout/string/replaceAccents", ["../lang/toString"], function(t) {
    function e(e) {
        return e = t(e),
        e.search(/[\xC0-\xFF]/g) > -1 && (e = e.replace(/[\xC0-\xC5]/g, "A").replace(/[\xC6]/g, "AE").replace(/[\xC7]/g, "C").replace(/[\xC8-\xCB]/g, "E").replace(/[\xCC-\xCF]/g, "I").replace(/[\xD0]/g, "D").replace(/[\xD1]/g, "N").replace(/[\xD2-\xD6\xD8]/g, "O").replace(/[\xD9-\xDC]/g, "U").replace(/[\xDD]/g, "Y").replace(/[\xDE]/g, "P").replace(/[\xE0-\xE5]/g, "a").replace(/[\xE6]/g, "ae").replace(/[\xE7]/g, "c").replace(/[\xE8-\xEB]/g, "e").replace(/[\xEC-\xEF]/g, "i").replace(/[\xF1]/g, "n").replace(/[\xF2-\xF6\xF8]/g, "o").replace(/[\xF9-\xFC]/g, "u").replace(/[\xFE]/g, "p").replace(/[\xFD\xFF]/g, "y")),
        e
    }
    return e
}),
define("mout/string/removeNonWord", ["../lang/toString"], function(t) {
    function e(e) {
        return e = t(e),
        e.replace(n, "")
    }
    var n = /[^\x20\x2D0-9A-Z\x5Fa-z\xC0-\xD6\xD8-\xF6\xF8-\xFF]/g;
    return e
}),
define("mout/string/upperCase", ["../lang/toString"], function(t) {
    function e(e) {
        return e = t(e),
        e.toUpperCase()
    }
    return e
}),
define("mout/string/lowerCase", ["../lang/toString"], function(t) {
    function e(e) {
        return e = t(e),
        e.toLowerCase()
    }
    return e
}),
define("mout/string/camelCase", ["../lang/toString", "./replaceAccents", "./removeNonWord", "./upperCase", "./lowerCase"], function(t, e, n, i, r) {
    function o(o) {
        return o = t(o),
        o = e(o),
        o = n(o).replace(/[\-_]/g, " ").replace(/\s[a-z]/g, i).replace(/\s+/g, "").replace(/^[A-Z]/g, r)
    }
    return o
}),
define("mout/string/contains", ["../lang/toString"], function(t) {
    function e(e, n, i) {
        return e = t(e),
        n = t(n),
        -1 !== e.indexOf(n, i)
    }
    return e
}),
define("mout/string/ltrim", ["../lang/toString", "./WHITE_SPACES"], function(t, e) {
    function n(n, i) {
        n = t(n),
        i = i || e;
        for (var r, o, s = 0, a = n.length, l = i.length, u = !0; u && a > s; )
            for (u = !1,
            r = -1,
            o = n.charAt(s); ++r < l; )
                if (o === i[r]) {
                    u = !0,
                    s++;
                    break
                }
        return s >= a ? "" : n.substr(s, a)
    }
    return n
}),
define("mout/string/rtrim", ["../lang/toString", "./WHITE_SPACES"], function(t, e) {
    function n(n, i) {
        n = t(n),
        i = i || e;
        for (var r, o, s = n.length - 1, a = i.length, l = !0; l && s >= 0; )
            for (l = !1,
            r = -1,
            o = n.charAt(s); ++r < a; )
                if (o === i[r]) {
                    l = !0,
                    s--;
                    break
                }
        return s >= 0 ? n.substring(0, s + 1) : ""
    }
    return n
}),
define("mout/string/trim", ["../lang/toString", "./WHITE_SPACES", "./ltrim", "./rtrim"], function(t, e, n, i) {
    function r(r, o) {
        return r = t(r),
        o = o || e,
        n(i(r, o), o)
    }
    return r
}),
define("mout/string/truncate", ["../lang/toString", "./trim"], function(t, e) {
    function n(n, i, r, o) {
        return n = t(n),
        r = r || "...",
        i = o ? i + 1 : i,
        n = e(n),
        n.length <= i ? n : (n = n.substr(0, i - r.length),
        n = o ? n.substr(0, n.lastIndexOf(" ")) : e(n),
        n + r)
    }
    return n
}),
define("mout/string/crop", ["../lang/toString", "./truncate"], function(t, e) {
    function n(n, i, r) {
        return n = t(n),
        e(n, i, r, !0)
    }
    return n
}),
define("mout/string/endsWith", ["../lang/toString"], function(t) {
    function e(e, n) {
        return e = t(e),
        n = t(n),
        -1 !== e.indexOf(n, e.length - n.length)
    }
    return e
}),
define("mout/string/escapeHtml", ["../lang/toString"], function(t) {
    function e(e) {
        return e = t(e).replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/'/g, "&#39;").replace(/"/g, "&quot;")
    }
    return e
}),
define("mout/string/escapeRegExp", ["../lang/toString"], function(t) {
    function e(e) {
        return t(e).replace(/\W/g, "\\$&")
    }
    return e
}),
define("mout/string/escapeUnicode", ["../lang/toString"], function(t) {
    function e(e, n) {
        return e = t(e),
        e.replace(/[\s\S]/g, function(t) {
            return !n && /[\x20-\x7E]/.test(t) ? t : "\\u" + ("000" + t.charCodeAt(0).toString(16)).slice(-4)
        })
    }
    return e
}),
define("mout/string/slugify", ["../lang/toString", "./replaceAccents", "./removeNonWord", "./trim"], function(t, e, n, i) {
    function r(r, o) {
        return r = t(r),
        null == o && (o = "-"),
        r = e(r),
        r = n(r),
        r = i(r).replace(/ +/g, o).toLowerCase()
    }
    return r
}),
define("mout/string/unCamelCase", ["../lang/toString"], function(t) {
    function e(e, i) {
        function r(t, e, n) {
            return e + i + n
        }
        return null == i && (i = " "),
        e = t(e),
        e = e.replace(n, r),
        e = e.toLowerCase()
    }
    var n = /([a-z\xE0-\xFF])([A-Z\xC0\xDF])/g;
    return e
}),
define("mout/string/hyphenate", ["../lang/toString", "./slugify", "./unCamelCase"], function(t, e, n) {
    function i(i) {
        return i = t(i),
        i = n(i),
        e(i, "-")
    }
    return i
}),
define("mout/string/insert", ["../math/clamp", "../lang/toString"], function(t, e) {
    function n(n, i, r) {
        return n = e(n),
        0 > i && (i = n.length + i),
        i = t(i, 0, n.length),
        n.substr(0, i) + r + n.substr(i)
    }
    return n
}),
define("mout/lang/isPrimitive", [], function() {
    function t(t) {
        switch (typeof t) {
        case "string":
        case "number":
        case "boolean":
            return !0
        }
        return null == t
    }
    return t
}),
define("mout/object/get", ["../lang/isPrimitive"], function(t) {
    function e(t, e) {
        for (var n = e.split("."), i = n.pop(); e = n.shift(); )
            if (t = t[e],
            null == t)
                return;
        return t[i]
    }
    return e
}),
define("mout/string/interpolate", ["../lang/toString", "../object/get"], function(t, e) {
    function n(n, r, o) {
        n = t(n);
        var s = function(n, i) {
            return t(e(r, i))
        };
        return n.replace(o || i, s)
    }
    var i = /\{\{([^\}]+)\}\}/g;
    return n
}),
define("mout/number/toInt", [], function() {
    function t(t) {
        return ~~t
    }
    return t
}),
define("mout/string/repeat", ["../lang/toString", "../number/toInt"], function(t, e) {
    function n(n, i) {
        var r = "";
        if (n = t(n),
        i = e(i),
        1 > i)
            return "";
        for (; i > 0; )
            i % 2 && (r += n),
            i = Math.floor(i / 2),
            n += n;
        return r
    }
    return n
}),
define("mout/string/lpad", ["../lang/toString", "./repeat"], function(t, e) {
    function n(n, i, r) {
        return n = t(n),
        r = r || " ",
        n.length < i ? e(r, i - n.length) + n : n
    }
    return n
}),
define("mout/function/identity", [], function() {
    function t(t) {
        return t
    }
    return t
}),
define("mout/function/prop", [], function() {
    function t(t) {
        return function(e) {
            return e[t]
        }
    }
    return t
}),
define("mout/object/hasOwn", [], function() {
    function t(t, e) {
        return Object.prototype.hasOwnProperty.call(t, e)
    }
    return t
}),
define("mout/object/forIn", ["./hasOwn"], function(t) {
    function e() {
        o = ["toString", "toLocaleString", "valueOf", "hasOwnProperty", "isPrototypeOf", "propertyIsEnumerable", "constructor"],
        r = !0;
        for (var t in {
            toString: null
        })
            r = !1
    }
    function n(n, s, a) {
        var l, u = 0;
        null == r && e();
        for (l in n)
            if (i(s, n, l, a) === !1)
                break;
        if (r)
            for (var c = n.constructor, h = !!c && n === c.prototype; (l = o[u++]) && ("constructor" === l && (h || !t(n, l)) || n[l] === Object.prototype[l] || i(s, n, l, a) !== !1); )
                ;
    }
    function i(t, e, n, i) {
        return t.call(i, e[n], n, e)
    }
    var r, o;
    return n
}),
define("mout/object/forOwn", ["./hasOwn", "./forIn"], function(t, e) {
    function n(n, i, r) {
        e(n, function(e, o) {
            return t(n, o) ? i.call(r, n[o], o, n) : void 0
        })
    }
    return n
}),
define("mout/lang/kindOf", [], function() {
    function t(t) {
        return null === t ? "Null" : t === e ? "Undefined" : n.exec(i.call(t))[1]
    }
    var e, n = /^\[object (.*)\]$/, i = Object.prototype.toString;
    return t
}),
define("mout/lang/isKind", ["./kindOf"], function(t) {
    function e(e, n) {
        return t(e) === n
    }
    return e
}),
define("mout/lang/isArray", ["./isKind"], function(t) {
    var e = Array.isArray || function(e) {
        return t(e, "Array")
    }
    ;
    return e
}),
define("mout/object/deepMatches", ["./forOwn", "../lang/isArray"], function(t, e) {
    function n(t, e) {
        for (var n = -1, i = t.length; ++n < i; )
            if (o(t[n], e))
                return !0;
        return !1
    }
    function i(t, e) {
        for (var i = -1, r = e.length; ++i < r; )
            if (!n(t, e[i]))
                return !1;
        return !0
    }
    function r(e, n) {
        var i = !0;
        return t(n, function(t, n) {
            return o(e[n], t) ? void 0 : i = !1
        }),
        i
    }
    function o(t, n) {
        return t && "object" == typeof t ? e(t) && e(n) ? i(t, n) : r(t, n) : t === n
    }
    return o
}),
define("mout/function/makeIterator_", ["./identity", "./prop", "../object/deepMatches"], function(t, e, n) {
    function i(i, r) {
        if (null == i)
            return t;
        switch (typeof i) {
        case "function":
            return "undefined" != typeof r ? function(t, e, n) {
                return i.call(r, t, e, n)
            }
            : i;
        case "object":
            return function(t) {
                return n(t, i)
            }
            ;
        case "string":
        case "number":
            return e(i)
        }
    }
    return i
}),
define("mout/array/filter", ["../function/makeIterator_"], function(t) {
    function e(e, n, i) {
        n = t(n, i);
        var r = [];
        if (null == e)
            return r;
        for (var o, s = -1, a = e.length; ++s < a; )
            o = e[s],
            n(o, s, e) && r.push(o);
        return r
    }
    return e
}),
define("mout/array/join", ["./filter"], function(t) {
    function e(t) {
        return null != t && "" !== t
    }
    function n(n, i) {
        return i = i || "",
        t(n, e).join(i)
    }
    return n
}),
define("mout/array/slice", [], function() {
    function t(t, e, n) {
        var i = t.length;
        e = null == e ? 0 : 0 > e ? Math.max(i + e, 0) : Math.min(e, i),
        n = null == n ? i : 0 > n ? Math.max(i + n, 0) : Math.min(n, i);
        for (var r = []; n > e; )
            r.push(t[e++]);
        return r
    }
    return t
}),
define("mout/string/makePath", ["../array/join", "../array/slice"], function(t, e) {
    function n(n) {
        var i = t(e(arguments), "/");
        return i.replace(/([^:\/]|^)\/{2,}/g, "$1/")
    }
    return n
}),
define("mout/string/normalizeLineBreaks", ["../lang/toString"], function(t) {
    function e(e, n) {
        return e = t(e),
        n = n || "\n",
        e.replace(/\r\n/g, n).replace(/\r/g, n).replace(/\n/g, n)
    }
    return e
}),
define("mout/string/pascalCase", ["../lang/toString", "./camelCase", "./upperCase"], function(t, e, n) {
    function i(i) {
        return i = t(i),
        e(i).replace(/^[a-z]/, n)
    }
    return i
}),
define("mout/string/properCase", ["../lang/toString", "./lowerCase", "./upperCase"], function(t, e, n) {
    function i(i) {
        return i = t(i),
        e(i).replace(/^\w|\s\w/g, n)
    }
    return i
}),
define("mout/string/removeNonASCII", ["../lang/toString"], function(t) {
    function e(e) {
        return e = t(e),
        e.replace(/[^\x20-\x7E]/g, "")
    }
    return e
}),
define("mout/lang/GLOBAL", [], function() {
    return Function("return this")()
}),
define("mout/lang/toArray", ["./kindOf", "./GLOBAL"], function(t, e) {
    function n(n) {
        var i, r = [], o = t(n);
        if (null != n)
            if (null == n.length || "String" === o || "Function" === o || "RegExp" === o || n === e)
                r[r.length] = n;
            else
                for (i = n.length; i--; )
                    r[i] = n[i];
        return r
    }
    return n
}),
define("mout/string/replace", ["../lang/toString", "../lang/toArray"], function(t, e) {
    function n(n, i, r) {
        n = t(n),
        i = e(i),
        r = e(r);
        var o = i.length
          , s = r.length;
        if (1 !== s && o !== s)
            throw new Error("Unequal number of searches and replacements");
        for (var a = -1; ++a < o; )
            n = n.replace(i[a], r[1 === s ? 0 : a]);
        return n
    }
    return n
}),
define("mout/string/rpad", ["../lang/toString", "./repeat"], function(t, e) {
    function n(n, i, r) {
        return n = t(n),
        r = r || " ",
        n.length < i ? n + e(r, i - n.length) : n
    }
    return n
}),
define("mout/string/sentenceCase", ["../lang/toString", "./lowerCase", "./upperCase"], function(t, e, n) {
    function i(i) {
        return i = t(i),
        e(i).replace(/(^\w)|\.\s+(\w)/gm, n)
    }
    return i
}),
define("mout/string/startsWith", ["../lang/toString"], function(t) {
    function e(e, n) {
        return e = t(e),
        n = t(n),
        0 === e.indexOf(n)
    }
    return e
}),
define("mout/string/stripHtmlTags", ["../lang/toString"], function(t) {
    function e(e) {
        return e = t(e),
        e.replace(/<[^>]*>/g, "")
    }
    return e
}),
define("mout/string/typecast", [], function() {
    function t(t) {
        var n;
        return n = null === t || "null" === t ? null : "true" === t ? !0 : "false" === t ? !1 : t === e || "undefined" === t ? e : "" === t || isNaN(t) ? t : parseFloat(t)
    }
    var e;
    return t
}),
define("mout/string/underscore", ["../lang/toString", "./slugify", "./unCamelCase"], function(t, e, n) {
    function i(i) {
        return i = t(i),
        i = n(i),
        e(i, "_")
    }
    return i
}),
define("mout/string/unescapeHtml", ["../lang/toString"], function(t) {
    function e(e) {
        return e = t(e).replace(/&amp;/g, "&").replace(/&lt;/g, "<").replace(/&gt;/g, ">").replace(/&#0*39;/g, "'").replace(/&quot;/g, '"')
    }
    return e
}),
define("mout/string/unescapeUnicode", ["../lang/toString"], function(t) {
    function e(e) {
        return e = t(e),
        e.replace(/\\u[0-9a-f]{4}/g, function(t) {
            var e = parseInt(t.slice(2), 16);
            return String.fromCharCode(e)
        })
    }
    return e
}),
define("mout/string/unhyphenate", ["../lang/toString"], function(t) {
    function e(e) {
        return e = t(e),
        e.replace(/(\w)(-)(\w)/g, "$1 $3")
    }
    return e
}),
define("mout/string", ["require", "./string/WHITE_SPACES", "./string/camelCase", "./string/contains", "./string/crop", "./string/endsWith", "./string/escapeHtml", "./string/escapeRegExp", "./string/escapeUnicode", "./string/hyphenate", "./string/insert", "./string/interpolate", "./string/lowerCase", "./string/lpad", "./string/ltrim", "./string/makePath", "./string/normalizeLineBreaks", "./string/pascalCase", "./string/properCase", "./string/removeNonASCII", "./string/removeNonWord", "./string/repeat", "./string/replace", "./string/replaceAccents", "./string/rpad", "./string/rtrim", "./string/sentenceCase", "./string/slugify", "./string/startsWith", "./string/stripHtmlTags", "./string/trim", "./string/truncate", "./string/typecast", "./string/unCamelCase", "./string/underscore", "./string/unescapeHtml", "./string/unescapeUnicode", "./string/unhyphenate", "./string/upperCase"], function(t) {
    return {
        WHITE_SPACES: t("./string/WHITE_SPACES"),
        camelCase: t("./string/camelCase"),
        contains: t("./string/contains"),
        crop: t("./string/crop"),
        endsWith: t("./string/endsWith"),
        escapeHtml: t("./string/escapeHtml"),
        escapeRegExp: t("./string/escapeRegExp"),
        escapeUnicode: t("./string/escapeUnicode"),
        hyphenate: t("./string/hyphenate"),
        insert: t("./string/insert"),
        interpolate: t("./string/interpolate"),
        lowerCase: t("./string/lowerCase"),
        lpad: t("./string/lpad"),
        ltrim: t("./string/ltrim"),
        makePath: t("./string/makePath"),
        normalizeLineBreaks: t("./string/normalizeLineBreaks"),
        pascalCase: t("./string/pascalCase"),
        properCase: t("./string/properCase"),
        removeNonASCII: t("./string/removeNonASCII"),
        removeNonWord: t("./string/removeNonWord"),
        repeat: t("./string/repeat"),
        replace: t("./string/replace"),
        replaceAccents: t("./string/replaceAccents"),
        rpad: t("./string/rpad"),
        rtrim: t("./string/rtrim"),
        sentenceCase: t("./string/sentenceCase"),
        slugify: t("./string/slugify"),
        startsWith: t("./string/startsWith"),
        stripHtmlTags: t("./string/stripHtmlTags"),
        trim: t("./string/trim"),
        truncate: t("./string/truncate"),
        typecast: t("./string/typecast"),
        unCamelCase: t("./string/unCamelCase"),
        underscore: t("./string/underscore"),
        unescapeHtml: t("./string/unescapeHtml"),
        unescapeUnicode: t("./string/unescapeUnicode"),
        unhyphenate: t("./string/unhyphenate"),
        upperCase: t("./string/upperCase")
    }
});
var extend = function(t, e) {
    function n() {
        this.constructor = t
    }
    for (var i in e)
        hasProp.call(e, i) && (t[i] = e[i]);
    return n.prototype = e.prototype,
    t.prototype = new n,
    t.__super__ = e.prototype,
    t
}
  , hasProp = {}.hasOwnProperty;
define("app/router", ["jquery", "underscore", "backbone", "collections/projects", "models/project", "views/app", "config", "mout/string"], function(t, e, n, i, r, o, s, a) {
    var l;
    return l = function(e) {
        function r() {
            return r.__super__.constructor.apply(this, arguments)
        }
        return extend(r, e),
        r.prototype.currentRoute = "",
        r.prototype.firstRoute = !0,
        r.prototype.routes = {
            "": "showHome",
            "info/": "getInfo",
            info: "getInfo",
            "404/": "get404",
            404: "get404",
            "error/": "getError",
            error: "getError",
            "work/:slug/": "getProject",
            "work/:slug": "getProject",
            "*actions": "defaultAction"
        },
        r.prototype.initialize = function() {
            return this.collection = new i,
            this.collection.fetch({
                success: t.proxy(this.collectionFetchSuccess, this),
                error: t.proxy(this.collectionFetchError, this)
            }),
            window._gaq = window._gaq || [],
            this.bind("route", this.trackPageview)
        }
        ,
        r.prototype.trackPageview = function() {
            var t;
            return this.firstRoute === !1 && (t = void 0,
            t = n.history.getFragment(),
            window._gaq.push(["_trackPageview", "/" + t])),
            this.firstRoute = !1
        }
        ,
        r.prototype.collectionFetchSuccess = function() {
            return this.start()
        }
        ,
        r.prototype.collectionFetchError = function() {
            return this.error()
        }
        ,
        r.prototype.start = function() {
            return n.Notifications.on("homeLoadComplete", this.onHomeLoadComplete, this),
            n.Notifications.on("loaderAnimateOutComplete", this.onLoaderAnimateOutComplete, this),
            n.Notifications.on("headerLogoClick", this.onHeaderClick, this),
            n.Notifications.on("close", this.onClose, this),
            n.Notifications.on("prevProject", this.onPrevProject, this),
            n.Notifications.on("nextProject", this.onNextProject, this),
            n.Notifications.on("projectLoadingError", this.onProjectLoadError, this),
            this.appView = new o({
                collection: this.collection
            })
        }
        ,
        r.prototype.error = function() {
            return n.Notifications.on("homeLoadComplete", this.onHomeLoadComplete, this),
            n.Notifications.on("loaderAnimateOutComplete", this.onLoaderAnimateOutComplete, this),
            n.Notifications.on("headerLogoClick", this.onHeaderClick, this),
            n.Notifications.on("projectLoadingError", this.onProjectLoadError, this),
            this.appView = new o
        }
        ,
        r.prototype.onHomeLoadComplete = function() {
            return n.Notifications.off("homeLoadComplete")
        }
        ,
        r.prototype.onLoaderAnimateOutComplete = function() {
            return this._bindRoutes(),
            n.history.start({
                pushState: !0,
                root: s.relUri
            })
        }
        ,
        r.prototype.onClose = function() {
            switch (this.currentRoute) {
            case "info":
            case "error":
            case "project":
                return n.history.navigate("", {
                    trigger: !0
                })
            }
        }
        ,
        r.prototype.onPrevProject = function() {
            switch (this.currentRoute) {
            case "project":
                return n.history.navigate("work/" + this.collection.getPrevious().get("slug") + "/", {
                    trigger: !0
                })
            }
        }
        ,
        r.prototype.onNextProject = function() {
            switch (this.currentRoute) {
            case "project":
                return n.history.navigate("work/" + this.collection.getNext().get("slug") + "/", {
                    trigger: !0
                })
            }
        }
        ,
        r.prototype.onHeaderClick = function() {
            switch (this.currentRoute) {
            case "info":
            case "error":
            case "project":
                return n.history.navigate("", {
                    trigger: !0
                });
            default:
                return n.history.navigate("info/", {
                    trigger: !0
                })
            }
        }
        ,
        r.prototype.getInfo = function() {
            return this.currentRoute = "info",
            this.setTitle("Info"),
            this.appView.showOverlay("info")
        }
        ,
        r.prototype.get404 = function() {
            return this.currentRoute = "error",
            this.setTitle("error"),
            this.appView.showOverlay("error", {
                template: "404.jade"
            })
        }
        ,
        r.prototype.getError = function() {
            return this.currentRoute = "error",
            this.setTitle("error"),
            this.appView.showOverlay("error", {
                template: "error.jade"
            })
        }
        ,
        r.prototype.getPageNotFound = function() {
            return this.currentRoute = "pageNotFound",
            this.appView.showOverlay("pageNotFound")
        }
        ,
        r.prototype.getProject = function(t) {
            return this.currentRoute = "project",
            this.collection.getSlug(t) ? (this.collection.setSelected(this.collection.getSlug(t).id),
            this.setTitle(this.collection.getSelected().get("name")),
            this.appView.showOverlay("project")) : void n.history.navigate("404/", {
                trigger: !0,
                replace: !0
            })
        }
        ,
        r.prototype.showHome = function() {
            return this.currentRoute = "",
            this.setTitle(),
            this.appView.hideOverlay()
        }
        ,
        r.prototype.setTitle = function(e) {
            return e ? t("head title").text("WeLoveNoise - " + a.properCase(e)) : t("head title").text("WeLoveNoise - Design & Direction for Digital, Mobile & Brand")
        }
        ,
        r.prototype.onProjectLoadError = function() {
            return TweenMax.delayedCall(.1, this.getError, [], this)
        }
        ,
        r.prototype.defaultAction = function(t) {
            return n.history.navigate("404/", {
                trigger: !0
            })
        }
        ,
        r
    }(n.Router)
}),
define("app/main", ["jquery", "underscore", "backbone", "app/router", "config", "greensock"], function(t, e, n, i, r) {
    var o, s;
    return o = function(t) {
        return TweenMax.delayedCall(.05, s, [t], this)
    }
    ,
    s = function(t) {
        var r;
        return n.Notifications = {},
        e.extend(n.Notifications, n.Events),
        r = new i(t)
    }
    ,
    {
        initialize: o
    }
}),
define("app", ["app/main"], function(t) {
    return t
}),
function() {
    require.config({
        paths: {
            jquery: "lib/jquery/dist/jquery.min",
            underscore: "lib/underscore-amd/underscore",
            backbone: "lib/backbone-amd/backbone",
            collections: "app/collections",
            models: "app/models",
            runtime: "app/templates/runtime",
            views: "app/views",
            components: "app/components",
            helpers: "app/helpers",
            routers: "app/routers",
            config: "app/config",
            modernizr: "modernizr",
            greensock: "lib/greensock-js/src/minified/TweenMax.min",
            signals: "lib/js-signals/dist/signals",
            enquire: "lib/enquire/dist/enquire",
            requestAnimationFrame: "lib/raf.js/raf",
            "coffee-script": "lib/coffee-script/coffee-script",
            cs: "lib/require-cs/cs",
            mout: "lib/mout/src/"
        },
        shim: {
            enquire: {
                deps: ["lib/matchmedia/matchMedia"],
                exports: "enquire"
            }
        },
        map: {
            "*": {
                jquery: "jquery-loader",
                text: "lib/text/text",
                css: "lib/require-css/css"
            },
            "jquery-loader": {
                jquery: "jquery"
            }
        },
        enforceDefine: !1,
        packages: ["app"],
        baseUrl: "js",
        urlArgs: "bust=" + (new Date).getTime()
    }),
    define("jquery-loader", ["jquery"], function(t) {
        return t.noConflict(!0)
    }),
    require(["modernizr", "app", "jquery"], function(t, e, n) {
        e.initialize()
    })
}(),
define("main", function() {}),
define("lib/text/text", ["module"], function(t) {
    "use strict";
    var e, n, i, r, o, s = ["Msxml2.XMLHTTP", "Microsoft.XMLHTTP", "Msxml2.XMLHTTP.4.0"], a = /^\s*<\?xml(\s)+version=[\'\"](\d)*.(\d)*[\'\"](\s)*\?>/im, l = /<body[^>]*>\s*([\s\S]+)\s*<\/body>/im, u = "undefined" != typeof location && location.href, c = u && location.protocol && location.protocol.replace(/\:/, ""), h = u && location.hostname, p = u && (location.port || void 0), f = {}, d = t.config && t.config() || {};
    return e = {
        version: "2.0.14",
        strip: function(t) {
            if (t) {
                t = t.replace(a, "");
                var e = t.match(l);
                e && (t = e[1])
            } else
                t = "";
            return t
        },
        jsEscape: function(t) {
            return t.replace(/(['\\])/g, "\\$1").replace(/[\f]/g, "\\f").replace(/[\b]/g, "\\b").replace(/[\n]/g, "\\n").replace(/[\t]/g, "\\t").replace(/[\r]/g, "\\r").replace(/[\u2028]/g, "\\u2028").replace(/[\u2029]/g, "\\u2029")
        },
        createXhr: d.createXhr || function() {
            var t, e, n;
            if ("undefined" != typeof XMLHttpRequest)
                return new XMLHttpRequest;
            if ("undefined" != typeof ActiveXObject)
                for (e = 0; 3 > e; e += 1) {
                    n = s[e];
                    try {
                        t = new ActiveXObject(n)
                    } catch (i) {}
                    if (t) {
                        s = [n];
                        break
                    }
                }
            return t
        }
        ,
        parseName: function(t) {
            var e, n, i, r = !1, o = t.lastIndexOf("."), s = 0 === t.indexOf("./") || 0 === t.indexOf("../");
            return -1 !== o && (!s || o > 1) ? (e = t.substring(0, o),
            n = t.substring(o + 1)) : e = t,
            i = n || e,
            o = i.indexOf("!"),
            -1 !== o && (r = "strip" === i.substring(o + 1),
            i = i.substring(0, o),
            n ? n = i : e = i),
            {
                moduleName: e,
                ext: n,
                strip: r
            }
        },
        xdRegExp: /^((\w+)\:)?\/\/([^\/\\]+)/,
        useXhr: function(t, n, i, r) {
            var o, s, a, l = e.xdRegExp.exec(t);
            return l ? (o = l[2],
            s = l[3],
            s = s.split(":"),
            a = s[1],
            s = s[0],
            !(o && o !== n || s && s.toLowerCase() !== i.toLowerCase() || (a || s) && a !== r)) : !0
        },
        finishLoad: function(t, n, i, r) {
            i = n ? e.strip(i) : i,
            d.isBuild && (f[t] = i),
            r(i)
        },
        load: function(t, n, i, r) {
            if (r && r.isBuild && !r.inlineText)
                return void i();
            d.isBuild = r && r.isBuild;
            var o = e.parseName(t)
              , s = o.moduleName + (o.ext ? "." + o.ext : "")
              , a = n.toUrl(s)
              , l = d.useXhr || e.useXhr;
            return 0 === a.indexOf("empty:") ? void i() : void (!u || l(a, c, h, p) ? e.get(a, function(n) {
                e.finishLoad(t, o.strip, n, i)
            }, function(t) {
                i.error && i.error(t)
            }) : n([s], function(t) {
                e.finishLoad(o.moduleName + "." + o.ext, o.strip, t, i)
            }))
        },
        write: function(t, n, i, r) {
            if (f.hasOwnProperty(n)) {
                var o = e.jsEscape(f[n]);
                i.asModule(t + "!" + n, "define(function () { return '" + o + "';});\n")
            }
        },
        writeFile: function(t, n, i, r, o) {
            var s = e.parseName(n)
              , a = s.ext ? "." + s.ext : ""
              , l = s.moduleName + a
              , u = i.toUrl(s.moduleName + a) + ".js";
            e.load(l, i, function(n) {
                var i = function(t) {
                    return r(u, t)
                };
                i.asModule = function(t, e) {
                    return r.asModule(t, u, e)
                }
                ,
                e.write(t, l, i, o)
            }, o)
        }
    },
    "node" === d.env || !d.env && "undefined" != typeof process && process.versions && process.versions.node && !process.versions["node-webkit"] && !process.versions["atom-shell"] ? (n = require.nodeRequire("fs"),
    e.get = function(t, e, i) {
        try {
            var r = n.readFileSync(t, "utf8");
            "\ufeff" === r[0] && (r = r.substring(1)),
            e(r)
        } catch (o) {
            i && i(o)
        }
    }
    ) : "xhr" === d.env || !d.env && e.createXhr() ? e.get = function(t, n, i, r) {
        var o, s = e.createXhr();
        if (s.open("GET", t, !0),
        r)
            for (o in r)
                r.hasOwnProperty(o) && s.setRequestHeader(o.toLowerCase(), r[o]);
        d.onXhr && d.onXhr(s, t),
        s.onreadystatechange = function(e) {
            var r, o;
            4 === s.readyState && (r = s.status || 0,
            r > 399 && 600 > r ? (o = new Error(t + " HTTP status: " + r),
            o.xhr = s,
            i && i(o)) : n(s.responseText),
            d.onXhrComplete && d.onXhrComplete(s, t))
        }
        ,
        s.send(null)
    }
    : "rhino" === d.env || !d.env && "undefined" != typeof Packages && "undefined" != typeof java ? e.get = function(t, e) {
        var n, i, r = "utf-8", o = new java.io.File(t), s = java.lang.System.getProperty("line.separator"), a = new java.io.BufferedReader(new java.io.InputStreamReader(new java.io.FileInputStream(o),r)), l = "";
        try {
            for (n = new java.lang.StringBuffer,
            i = a.readLine(),
            i && i.length() && 65279 === i.charAt(0) && (i = i.substring(1)),
            null !== i && n.append(i); null !== (i = a.readLine()); )
                n.append(s),
                n.append(i);
            l = String(n.toString())
        } finally {
            a.close()
        }
        e(l)
    }
    : ("xpconnect" === d.env || !d.env && "undefined" != typeof Components && Components.classes && Components.interfaces) && (i = Components.classes,
    r = Components.interfaces,
    Components.utils["import"]("resource://gre/modules/FileUtils.jsm"),
    o = "@mozilla.org/windows-registry-key;1"in i,
    e.get = function(t, e) {
        var n, s, a, l = {};
        o && (t = t.replace(/\//g, "\\")),
        a = new FileUtils.File(t);
        try {
            n = i["@mozilla.org/network/file-input-stream;1"].createInstance(r.nsIFileInputStream),
            n.init(a, 1, 0, !1),
            s = i["@mozilla.org/intl/converter-input-stream;1"].createInstance(r.nsIConverterInputStream),
            s.init(n, "utf-8", n.available(), r.nsIConverterInputStream.DEFAULT_REPLACEMENT_CHARACTER),
            s.readString(n.available(), l),
            s.close(),
            n.close(),
            e(l.value)
        } catch (u) {
            throw new Error((a && a.path || "") + ": " + u)
        }
    }
    ),
    e
}),
define("lib/require-css/css", [], function() {
    if ("undefined" == typeof window)
        return {
            load: function(t, e, n) {
                n()
            }
        };
    var t = document.getElementsByTagName("head")[0]
      , e = window.navigator.userAgent.match(/Trident\/([^ ;]*)|AppleWebKit\/([^ ;]*)|Opera\/([^ ;]*)|rv\:([^ ;]*)(.*?)Gecko\/([^ ;]*)|MSIE\s([^ ;]*)|AndroidWebKit\/([^ ;]*)/) || 0
      , n = !1
      , i = !0;
    e[1] || e[7] ? n = parseInt(e[1]) < 6 || parseInt(e[7]) <= 9 : e[2] || e[8] ? i = !1 : e[4] && (n = parseInt(e[4]) < 18);
    var r = {};
    r.pluginBuilder = "./css-builder";
    var o, s, a, l = function() {
        o = document.createElement("style"),
        t.appendChild(o),
        s = o.styleSheet || o.sheet
    }, u = 0, c = [], h = function(t) {
        s.addImport(t),
        o.onload = function() {
            p()
        }
        ,
        u++,
        31 == u && (l(),
        u = 0)
    }, p = function() {
        a();
        var t = c.shift();
        return t ? (a = t[1],
        void h(t[0])) : void (a = null)
    }, f = function(t, e) {
        if (s && s.addImport || l(),
        s && s.addImport)
            a ? c.push([t, e]) : (h(t),
            a = e);
        else {
            o.textContent = '@import "' + t + '";';
            var n = setInterval(function() {
                try {
                    o.sheet.cssRules,
                    clearInterval(n),
                    e()
                } catch (t) {}
            }, 10)
        }
    }, d = function(e, n) {
        var r = document.createElement("link");
        if (r.type = "text/css",
        r.rel = "stylesheet",
        i)
            r.onload = function() {
                r.onload = function() {}
                ,
                setTimeout(n, 7)
            }
            ;
        else
            var o = setInterval(function() {
                for (var t = 0; t < document.styleSheets.length; t++) {
                    var e = document.styleSheets[t];
                    if (e.href == r.href)
                        return clearInterval(o),
                        n()
                }
            }, 10);
        r.href = e,
        t.appendChild(r)
    };
    return r.normalize = function(t, e) {
        return ".css" == t.substr(t.length - 4, 4) && (t = t.substr(0, t.length - 4)),
        e(t)
    }
    ,
    r.load = function(t, e, i, r) {
        (n ? f : d)(e.toUrl(t + ".css"), i)
    }
    ,
    r
}),
function(t, e) {
    "object" == typeof module && "object" == typeof module.exports ? module.exports = t.document ? e(t, !0) : function(t) {
        if (!t.document)
            throw new Error("jQuery requires a window with a document");
        return e(t)
    }
    : e(t)
}("undefined" != typeof window ? window : this, function(t, e) {
    function n(t) {
        var e = "length"in t && t.length
          , n = K.type(t);
        return "function" === n || K.isWindow(t) ? !1 : 1 === t.nodeType && e ? !0 : "array" === n || 0 === e || "number" == typeof e && e > 0 && e - 1 in t
    }
    function i(t, e, n) {
        if (K.isFunction(e))
            return K.grep(t, function(t, i) {
                return !!e.call(t, i, t) !== n
            });
        if (e.nodeType)
            return K.grep(t, function(t) {
                return t === e !== n
            });
        if ("string" == typeof e) {
            if (at.test(e))
                return K.filter(e, t, n);
            e = K.filter(e, t)
        }
        return K.grep(t, function(t) {
            return V.call(e, t) >= 0 !== n
        })
    }
    function r(t, e) {
        for (; (t = t[e]) && 1 !== t.nodeType; )
            ;
        return t
    }
    function o(t) {
        var e = dt[t] = {};
        return K.each(t.match(ft) || [], function(t, n) {
            e[n] = !0
        }),
        e
    }
    function s() {
        J.removeEventListener("DOMContentLoaded", s, !1),
        t.removeEventListener("load", s, !1),
        K.ready()
    }
    function a() {
        Object.defineProperty(this.cache = {}, 0, {
            get: function() {
                return {}
            }
        }),
        this.expando = K.expando + a.uid++
    }
    function l(t, e, n) {
        var i;
        if (void 0 === n && 1 === t.nodeType)
            if (i = "data-" + e.replace(xt, "-$1").toLowerCase(),
            n = t.getAttribute(i),
            "string" == typeof n) {
                try {
                    n = "true" === n ? !0 : "false" === n ? !1 : "null" === n ? null : +n + "" === n ? +n : _t.test(n) ? K.parseJSON(n) : n
                } catch (r) {}
                yt.set(t, e, n)
            } else
                n = void 0;
        return n
    }
    function u() {
        return !0
    }
    function c() {
        return !1
    }
    function h() {
        try {
            return J.activeElement
        } catch (t) {}
    }
    function p(t, e) {
        return K.nodeName(t, "table") && K.nodeName(11 !== e.nodeType ? e : e.firstChild, "tr") ? t.getElementsByTagName("tbody")[0] || t.appendChild(t.ownerDocument.createElement("tbody")) : t
    }
    function f(t) {
        return t.type = (null !== t.getAttribute("type")) + "/" + t.type,
        t
    }
    function d(t) {
        var e = Rt.exec(t.type);
        return e ? t.type = e[1] : t.removeAttribute("type"),
        t
    }
    function m(t, e) {
        for (var n = 0, i = t.length; i > n; n++)
            vt.set(t[n], "globalEval", !e || vt.get(e[n], "globalEval"))
    }
    function g(t, e) {
        var n, i, r, o, s, a, l, u;
        if (1 === e.nodeType) {
            if (vt.hasData(t) && (o = vt.access(t),
            s = vt.set(e, o),
            u = o.events)) {
                delete s.handle,
                s.events = {};
                for (r in u)
                    for (n = 0,
                    i = u[r].length; i > n; n++)
                        K.event.add(e, r, u[r][n])
            }
            yt.hasData(t) && (a = yt.access(t),
            l = K.extend({}, a),
            yt.set(e, l))
        }
    }
    function v(t, e) {
        var n = t.getElementsByTagName ? t.getElementsByTagName(e || "*") : t.querySelectorAll ? t.querySelectorAll(e || "*") : [];
        return void 0 === e || e && K.nodeName(t, e) ? K.merge([t], n) : n
    }
    function y(t, e) {
        var n = e.nodeName.toLowerCase();
        "input" === n && Ct.test(t.type) ? e.checked = t.checked : ("input" === n || "textarea" === n) && (e.defaultValue = t.defaultValue)
    }
    function _(e, n) {
        var i, r = K(n.createElement(e)).appendTo(n.body), o = t.getDefaultComputedStyle && (i = t.getDefaultComputedStyle(r[0])) ? i.display : K.css(r[0], "display");
        return r.detach(),
        o
    }
    function x(t) {
        var e = J
          , n = Ht[t];
        return n || (n = _(t, e),
        "none" !== n && n || (qt = (qt || K("<iframe frameborder='0' width='0' height='0'/>")).appendTo(e.documentElement),
        e = qt[0].contentDocument,
        e.write(),
        e.close(),
        n = _(t, e),
        qt.detach()),
        Ht[t] = n),
        n
    }
    function w(t, e, n) {
        var i, r, o, s, a = t.style;
        return n = n || zt(t),
        n && (s = n.getPropertyValue(e) || n[e]),
        n && ("" !== s || K.contains(t.ownerDocument, t) || (s = K.style(t, e)),
        Bt.test(s) && Ft.test(e) && (i = a.width,
        r = a.minWidth,
        o = a.maxWidth,
        a.minWidth = a.maxWidth = a.width = s,
        s = n.width,
        a.width = i,
        a.minWidth = r,
        a.maxWidth = o)),
        void 0 !== s ? s + "" : s
    }
    function b(t, e) {
        return {
            get: function() {
                return t() ? void delete this.get : (this.get = e).apply(this, arguments)
            }
        }
    }
    function T(t, e) {
        if (e in t)
            return e;
        for (var n = e[0].toUpperCase() + e.slice(1), i = e, r = Gt.length; r--; )
            if (e = Gt[r] + n,
            e in t)
                return e;
        return i
    }
    function C(t, e, n) {
        var i = Xt.exec(e);
        return i ? Math.max(0, i[1] - (n || 0)) + (i[2] || "px") : e
    }
    function S(t, e, n, i, r) {
        for (var o = n === (i ? "border" : "content") ? 4 : "width" === e ? 1 : 0, s = 0; 4 > o; o += 2)
            "margin" === n && (s += K.css(t, n + bt[o], !0, r)),
            i ? ("content" === n && (s -= K.css(t, "padding" + bt[o], !0, r)),
            "margin" !== n && (s -= K.css(t, "border" + bt[o] + "Width", !0, r))) : (s += K.css(t, "padding" + bt[o], !0, r),
            "padding" !== n && (s += K.css(t, "border" + bt[o] + "Width", !0, r)));
        return s
    }
    function k(t, e, n) {
        var i = !0
          , r = "width" === e ? t.offsetWidth : t.offsetHeight
          , o = zt(t)
          , s = "border-box" === K.css(t, "boxSizing", !1, o);
        if (0 >= r || null == r) {
            if (r = w(t, e, o),
            (0 > r || null == r) && (r = t.style[e]),
            Bt.test(r))
                return r;
            i = s && (Q.boxSizingReliable() || r === t.style[e]),
            r = parseFloat(r) || 0
        }
        return r + S(t, e, n || (s ? "border" : "content"), i, o) + "px"
    }
    function j(t, e) {
        for (var n, i, r, o = [], s = 0, a = t.length; a > s; s++)
            i = t[s],
            i.style && (o[s] = vt.get(i, "olddisplay"),
            n = i.style.display,
            e ? (o[s] || "none" !== n || (i.style.display = ""),
            "" === i.style.display && Tt(i) && (o[s] = vt.access(i, "olddisplay", x(i.nodeName)))) : (r = Tt(i),
            "none" === n && r || vt.set(i, "olddisplay", r ? n : K.css(i, "display"))));
        for (s = 0; a > s; s++)
            i = t[s],
            i.style && (e && "none" !== i.style.display && "" !== i.style.display || (i.style.display = e ? o[s] || "" : "none"));
        return t
    }
    function P(t, e, n, i, r) {
        return new P.prototype.init(t,e,n,i,r)
    }
    function E() {
        return setTimeout(function() {
            Qt = void 0
        }),
        Qt = K.now()
    }
    function O(t, e) {
        var n, i = 0, r = {
            height: t
        };
        for (e = e ? 1 : 0; 4 > i; i += 2 - e)
            n = bt[i],
            r["margin" + n] = r["padding" + n] = t;
        return e && (r.opacity = r.width = t),
        r
    }
    function M(t, e, n) {
        for (var i, r = (ne[e] || []).concat(ne["*"]), o = 0, s = r.length; s > o; o++)
            if (i = r[o].call(n, e, t))
                return i
    }
    function D(t, e, n) {
        var i, r, o, s, a, l, u, c, h = this, p = {}, f = t.style, d = t.nodeType && Tt(t), m = vt.get(t, "fxshow");
        n.queue || (a = K._queueHooks(t, "fx"),
        null == a.unqueued && (a.unqueued = 0,
        l = a.empty.fire,
        a.empty.fire = function() {
            a.unqueued || l()
        }
        ),
        a.unqueued++,
        h.always(function() {
            h.always(function() {
                a.unqueued--,
                K.queue(t, "fx").length || a.empty.fire()
            })
        })),
        1 === t.nodeType && ("height"in e || "width"in e) && (n.overflow = [f.overflow, f.overflowX, f.overflowY],
        u = K.css(t, "display"),
        c = "none" === u ? vt.get(t, "olddisplay") || x(t.nodeName) : u,
        "inline" === c && "none" === K.css(t, "float") && (f.display = "inline-block")),
        n.overflow && (f.overflow = "hidden",
        h.always(function() {
            f.overflow = n.overflow[0],
            f.overflowX = n.overflow[1],
            f.overflowY = n.overflow[2]
        }));
        for (i in e)
            if (r = e[i],
            Zt.exec(r)) {
                if (delete e[i],
                o = o || "toggle" === r,
                r === (d ? "hide" : "show")) {
                    if ("show" !== r || !m || void 0 === m[i])
                        continue;
                    d = !0
                }
                p[i] = m && m[i] || K.style(t, i)
            } else
                u = void 0;
        if (K.isEmptyObject(p))
            "inline" === ("none" === u ? x(t.nodeName) : u) && (f.display = u);
        else {
            m ? "hidden"in m && (d = m.hidden) : m = vt.access(t, "fxshow", {}),
            o && (m.hidden = !d),
            d ? K(t).show() : h.done(function() {
                K(t).hide()
            }),
            h.done(function() {
                var e;
                vt.remove(t, "fxshow");
                for (e in p)
                    K.style(t, e, p[e])
            });
            for (i in p)
                s = M(d ? m[i] : 0, i, h),
                i in m || (m[i] = s.start,
                d && (s.end = s.start,
                s.start = "width" === i || "height" === i ? 1 : 0))
        }
    }
    function A(t, e) {
        var n, i, r, o, s;
        for (n in t)
            if (i = K.camelCase(n),
            r = e[i],
            o = t[n],
            K.isArray(o) && (r = o[1],
            o = t[n] = o[0]),
            n !== i && (t[i] = o,
            delete t[n]),
            s = K.cssHooks[i],
            s && "expand"in s) {
                o = s.expand(o),
                delete t[i];
                for (n in o)
                    n in t || (t[n] = o[n],
                    e[n] = r)
            } else
                e[i] = r
    }
    function N(t, e, n) {
        var i, r, o = 0, s = ee.length, a = K.Deferred().always(function() {
            delete l.elem
        }), l = function() {
            if (r)
                return !1;
            for (var e = Qt || E(), n = Math.max(0, u.startTime + u.duration - e), i = n / u.duration || 0, o = 1 - i, s = 0, l = u.tweens.length; l > s; s++)
                u.tweens[s].run(o);
            return a.notifyWith(t, [u, o, n]),
            1 > o && l ? n : (a.resolveWith(t, [u]),
            !1)
        }, u = a.promise({
            elem: t,
            props: K.extend({}, e),
            opts: K.extend(!0, {
                specialEasing: {}
            }, n),
            originalProperties: e,
            originalOptions: n,
            startTime: Qt || E(),
            duration: n.duration,
            tweens: [],
            createTween: function(e, n) {
                var i = K.Tween(t, u.opts, e, n, u.opts.specialEasing[e] || u.opts.easing);
                return u.tweens.push(i),
                i
            },
            stop: function(e) {
                var n = 0
                  , i = e ? u.tweens.length : 0;
                if (r)
                    return this;
                for (r = !0; i > n; n++)
                    u.tweens[n].run(1);
                return e ? a.resolveWith(t, [u, e]) : a.rejectWith(t, [u, e]),
                this
            }
        }), c = u.props;
        for (A(c, u.opts.specialEasing); s > o; o++)
            if (i = ee[o].call(u, t, c, u.opts))
                return i;
        return K.map(c, M, u),
        K.isFunction(u.opts.start) && u.opts.start.call(t, u),
        K.fx.timer(K.extend(l, {
            elem: t,
            anim: u,
            queue: u.opts.queue
        })),
        u.progress(u.opts.progress).done(u.opts.done, u.opts.complete).fail(u.opts.fail).always(u.opts.always)
    }
    function L(t) {
        return function(e, n) {
            "string" != typeof e && (n = e,
            e = "*");
            var i, r = 0, o = e.toLowerCase().match(ft) || [];
            if (K.isFunction(n))
                for (; i = o[r++]; )
                    "+" === i[0] ? (i = i.slice(1) || "*",
                    (t[i] = t[i] || []).unshift(n)) : (t[i] = t[i] || []).push(n)
        }
    }
    function R(t, e, n, i) {
        function r(a) {
            var l;
            return o[a] = !0,
            K.each(t[a] || [], function(t, a) {
                var u = a(e, n, i);
                return "string" != typeof u || s || o[u] ? s ? !(l = u) : void 0 : (e.dataTypes.unshift(u),
                r(u),
                !1)
            }),
            l
        }
        var o = {}
          , s = t === _e;
        return r(e.dataTypes[0]) || !o["*"] && r("*")
    }
    function $(t, e) {
        var n, i, r = K.ajaxSettings.flatOptions || {};
        for (n in e)
            void 0 !== e[n] && ((r[n] ? t : i || (i = {}))[n] = e[n]);
        return i && K.extend(!0, t, i),
        t
    }
    function I(t, e, n) {
        for (var i, r, o, s, a = t.contents, l = t.dataTypes; "*" === l[0]; )
            l.shift(),
            void 0 === i && (i = t.mimeType || e.getResponseHeader("Content-Type"));
        if (i)
            for (r in a)
                if (a[r] && a[r].test(i)) {
                    l.unshift(r);
                    break
                }
        if (l[0]in n)
            o = l[0];
        else {
            for (r in n) {
                if (!l[0] || t.converters[r + " " + l[0]]) {
                    o = r;
                    break
                }
                s || (s = r)
            }
            o = o || s
        }
        return o ? (o !== l[0] && l.unshift(o),
        n[o]) : void 0
    }
    function q(t, e, n, i) {
        var r, o, s, a, l, u = {}, c = t.dataTypes.slice();
        if (c[1])
            for (s in t.converters)
                u[s.toLowerCase()] = t.converters[s];
        for (o = c.shift(); o; )
            if (t.responseFields[o] && (n[t.responseFields[o]] = e),
            !l && i && t.dataFilter && (e = t.dataFilter(e, t.dataType)),
            l = o,
            o = c.shift())
                if ("*" === o)
                    o = l;
                else if ("*" !== l && l !== o) {
                    if (s = u[l + " " + o] || u["* " + o],
                    !s)
                        for (r in u)
                            if (a = r.split(" "),
                            a[1] === o && (s = u[l + " " + a[0]] || u["* " + a[0]])) {
                                s === !0 ? s = u[r] : u[r] !== !0 && (o = a[0],
                                c.unshift(a[1]));
                                break
                            }
                    if (s !== !0)
                        if (s && t["throws"])
                            e = s(e);
                        else
                            try {
                                e = s(e)
                            } catch (h) {
                                return {
                                    state: "parsererror",
                                    error: s ? h : "No conversion from " + l + " to " + o
                                }
                            }
                }
        return {
            state: "success",
            data: e
        }
    }
    function H(t, e, n, i) {
        var r;
        if (K.isArray(e))
            K.each(e, function(e, r) {
                n || Ce.test(t) ? i(t, r) : H(t + "[" + ("object" == typeof r ? e : "") + "]", r, n, i)
            });
        else if (n || "object" !== K.type(e))
            i(t, e);
        else
            for (r in e)
                H(t + "[" + r + "]", e[r], n, i);
    }
    function F(t) {
        return K.isWindow(t) ? t : 9 === t.nodeType && t.defaultView
    }
    var B = []
      , z = B.slice
      , W = B.concat
      , X = B.push
      , V = B.indexOf
      , Y = {}
      , U = Y.toString
      , G = Y.hasOwnProperty
      , Q = {}
      , J = t.document
      , Z = "2.1.4"
      , K = function(t, e) {
        return new K.fn.init(t,e)
    }
      , tt = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g
      , et = /^-ms-/
      , nt = /-([\da-z])/gi
      , it = function(t, e) {
        return e.toUpperCase()
    };
    K.fn = K.prototype = {
        jquery: Z,
        constructor: K,
        selector: "",
        length: 0,
        toArray: function() {
            return z.call(this)
        },
        get: function(t) {
            return null != t ? 0 > t ? this[t + this.length] : this[t] : z.call(this)
        },
        pushStack: function(t) {
            var e = K.merge(this.constructor(), t);
            return e.prevObject = this,
            e.context = this.context,
            e
        },
        each: function(t, e) {
            return K.each(this, t, e)
        },
        map: function(t) {
            return this.pushStack(K.map(this, function(e, n) {
                return t.call(e, n, e)
            }))
        },
        slice: function() {
            return this.pushStack(z.apply(this, arguments))
        },
        first: function() {
            return this.eq(0)
        },
        last: function() {
            return this.eq(-1)
        },
        eq: function(t) {
            var e = this.length
              , n = +t + (0 > t ? e : 0);
            return this.pushStack(n >= 0 && e > n ? [this[n]] : [])
        },
        end: function() {
            return this.prevObject || this.constructor(null)
        },
        push: X,
        sort: B.sort,
        splice: B.splice
    },
    K.extend = K.fn.extend = function() {
        var t, e, n, i, r, o, s = arguments[0] || {}, a = 1, l = arguments.length, u = !1;
        for ("boolean" == typeof s && (u = s,
        s = arguments[a] || {},
        a++),
        "object" == typeof s || K.isFunction(s) || (s = {}),
        a === l && (s = this,
        a--); l > a; a++)
            if (null != (t = arguments[a]))
                for (e in t)
                    n = s[e],
                    i = t[e],
                    s !== i && (u && i && (K.isPlainObject(i) || (r = K.isArray(i))) ? (r ? (r = !1,
                    o = n && K.isArray(n) ? n : []) : o = n && K.isPlainObject(n) ? n : {},
                    s[e] = K.extend(u, o, i)) : void 0 !== i && (s[e] = i));
        return s
    }
    ,
    K.extend({
        expando: "jQuery" + (Z + Math.random()).replace(/\D/g, ""),
        isReady: !0,
        error: function(t) {
            throw new Error(t)
        },
        noop: function() {},
        isFunction: function(t) {
            return "function" === K.type(t)
        },
        isArray: Array.isArray,
        isWindow: function(t) {
            return null != t && t === t.window
        },
        isNumeric: function(t) {
            return !K.isArray(t) && t - parseFloat(t) + 1 >= 0
        },
        isPlainObject: function(t) {
            return "object" !== K.type(t) || t.nodeType || K.isWindow(t) ? !1 : t.constructor && !G.call(t.constructor.prototype, "isPrototypeOf") ? !1 : !0
        },
        isEmptyObject: function(t) {
            var e;
            for (e in t)
                return !1;
            return !0
        },
        type: function(t) {
            return null == t ? t + "" : "object" == typeof t || "function" == typeof t ? Y[U.call(t)] || "object" : typeof t
        },
        globalEval: function(t) {
            var e, n = eval;
            t = K.trim(t),
            t && (1 === t.indexOf("use strict") ? (e = J.createElement("script"),
            e.text = t,
            J.head.appendChild(e).parentNode.removeChild(e)) : n(t))
        },
        camelCase: function(t) {
            return t.replace(et, "ms-").replace(nt, it)
        },
        nodeName: function(t, e) {
            return t.nodeName && t.nodeName.toLowerCase() === e.toLowerCase()
        },
        each: function(t, e, i) {
            var r, o = 0, s = t.length, a = n(t);
            if (i) {
                if (a)
                    for (; s > o && (r = e.apply(t[o], i),
                    r !== !1); o++)
                        ;
                else
                    for (o in t)
                        if (r = e.apply(t[o], i),
                        r === !1)
                            break
            } else if (a)
                for (; s > o && (r = e.call(t[o], o, t[o]),
                r !== !1); o++)
                    ;
            else
                for (o in t)
                    if (r = e.call(t[o], o, t[o]),
                    r === !1)
                        break;
            return t
        },
        trim: function(t) {
            return null == t ? "" : (t + "").replace(tt, "")
        },
        makeArray: function(t, e) {
            var i = e || [];
            return null != t && (n(Object(t)) ? K.merge(i, "string" == typeof t ? [t] : t) : X.call(i, t)),
            i
        },
        inArray: function(t, e, n) {
            return null == e ? -1 : V.call(e, t, n)
        },
        merge: function(t, e) {
            for (var n = +e.length, i = 0, r = t.length; n > i; i++)
                t[r++] = e[i];
            return t.length = r,
            t
        },
        grep: function(t, e, n) {
            for (var i, r = [], o = 0, s = t.length, a = !n; s > o; o++)
                i = !e(t[o], o),
                i !== a && r.push(t[o]);
            return r
        },
        map: function(t, e, i) {
            var r, o = 0, s = t.length, a = n(t), l = [];
            if (a)
                for (; s > o; o++)
                    r = e(t[o], o, i),
                    null != r && l.push(r);
            else
                for (o in t)
                    r = e(t[o], o, i),
                    null != r && l.push(r);
            return W.apply([], l)
        },
        guid: 1,
        proxy: function(t, e) {
            var n, i, r;
            return "string" == typeof e && (n = t[e],
            e = t,
            t = n),
            K.isFunction(t) ? (i = z.call(arguments, 2),
            r = function() {
                return t.apply(e || this, i.concat(z.call(arguments)))
            }
            ,
            r.guid = t.guid = t.guid || K.guid++,
            r) : void 0
        },
        now: Date.now,
        support: Q
    }),
    K.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function(t, e) {
        Y["[object " + e + "]"] = e.toLowerCase()
    });
    var rt = function(t) {
        function e(t, e, n, i) {
            var r, o, s, a, l, u, h, f, d, m;
            if ((e ? e.ownerDocument || e : H) !== D && M(e),
            e = e || D,
            n = n || [],
            a = e.nodeType,
            "string" != typeof t || !t || 1 !== a && 9 !== a && 11 !== a)
                return n;
            if (!i && N) {
                if (11 !== a && (r = yt.exec(t)))
                    if (s = r[1]) {
                        if (9 === a) {
                            if (o = e.getElementById(s),
                            !o || !o.parentNode)
                                return n;
                            if (o.id === s)
                                return n.push(o),
                                n
                        } else if (e.ownerDocument && (o = e.ownerDocument.getElementById(s)) && I(e, o) && o.id === s)
                            return n.push(o),
                            n
                    } else {
                        if (r[2])
                            return Z.apply(n, e.getElementsByTagName(t)),
                            n;
                        if ((s = r[3]) && w.getElementsByClassName)
                            return Z.apply(n, e.getElementsByClassName(s)),
                            n
                    }
                if (w.qsa && (!L || !L.test(t))) {
                    if (f = h = q,
                    d = e,
                    m = 1 !== a && t,
                    1 === a && "object" !== e.nodeName.toLowerCase()) {
                        for (u = S(t),
                        (h = e.getAttribute("id")) ? f = h.replace(xt, "\\$&") : e.setAttribute("id", f),
                        f = "[id='" + f + "'] ",
                        l = u.length; l--; )
                            u[l] = f + p(u[l]);
                        d = _t.test(t) && c(e.parentNode) || e,
                        m = u.join(",")
                    }
                    if (m)
                        try {
                            return Z.apply(n, d.querySelectorAll(m)),
                            n
                        } catch (g) {} finally {
                            h || e.removeAttribute("id")
                        }
                }
            }
            return j(t.replace(lt, "$1"), e, n, i)
        }
        function n() {
            function t(n, i) {
                return e.push(n + " ") > b.cacheLength && delete t[e.shift()],
                t[n + " "] = i
            }
            var e = [];
            return t
        }
        function i(t) {
            return t[q] = !0,
            t
        }
        function r(t) {
            var e = D.createElement("div");
            try {
                return !!t(e)
            } catch (n) {
                return !1
            } finally {
                e.parentNode && e.parentNode.removeChild(e),
                e = null
            }
        }
        function o(t, e) {
            for (var n = t.split("|"), i = t.length; i--; )
                b.attrHandle[n[i]] = e
        }
        function s(t, e) {
            var n = e && t
              , i = n && 1 === t.nodeType && 1 === e.nodeType && (~e.sourceIndex || Y) - (~t.sourceIndex || Y);
            if (i)
                return i;
            if (n)
                for (; n = n.nextSibling; )
                    if (n === e)
                        return -1;
            return t ? 1 : -1
        }
        function a(t) {
            return function(e) {
                var n = e.nodeName.toLowerCase();
                return "input" === n && e.type === t
            }
        }
        function l(t) {
            return function(e) {
                var n = e.nodeName.toLowerCase();
                return ("input" === n || "button" === n) && e.type === t
            }
        }
        function u(t) {
            return i(function(e) {
                return e = +e,
                i(function(n, i) {
                    for (var r, o = t([], n.length, e), s = o.length; s--; )
                        n[r = o[s]] && (n[r] = !(i[r] = n[r]))
                })
            })
        }
        function c(t) {
            return t && "undefined" != typeof t.getElementsByTagName && t
        }
        function h() {}
        function p(t) {
            for (var e = 0, n = t.length, i = ""; n > e; e++)
                i += t[e].value;
            return i
        }
        function f(t, e, n) {
            var i = e.dir
              , r = n && "parentNode" === i
              , o = B++;
            return e.first ? function(e, n, o) {
                for (; e = e[i]; )
                    if (1 === e.nodeType || r)
                        return t(e, n, o)
            }
            : function(e, n, s) {
                var a, l, u = [F, o];
                if (s) {
                    for (; e = e[i]; )
                        if ((1 === e.nodeType || r) && t(e, n, s))
                            return !0
                } else
                    for (; e = e[i]; )
                        if (1 === e.nodeType || r) {
                            if (l = e[q] || (e[q] = {}),
                            (a = l[i]) && a[0] === F && a[1] === o)
                                return u[2] = a[2];
                            if (l[i] = u,
                            u[2] = t(e, n, s))
                                return !0
                        }
            }
        }
        function d(t) {
            return t.length > 1 ? function(e, n, i) {
                for (var r = t.length; r--; )
                    if (!t[r](e, n, i))
                        return !1;
                return !0
            }
            : t[0]
        }
        function m(t, n, i) {
            for (var r = 0, o = n.length; o > r; r++)
                e(t, n[r], i);
            return i
        }
        function g(t, e, n, i, r) {
            for (var o, s = [], a = 0, l = t.length, u = null != e; l > a; a++)
                (o = t[a]) && (!n || n(o, i, r)) && (s.push(o),
                u && e.push(a));
            return s
        }
        function v(t, e, n, r, o, s) {
            return r && !r[q] && (r = v(r)),
            o && !o[q] && (o = v(o, s)),
            i(function(i, s, a, l) {
                var u, c, h, p = [], f = [], d = s.length, v = i || m(e || "*", a.nodeType ? [a] : a, []), y = !t || !i && e ? v : g(v, p, t, a, l), _ = n ? o || (i ? t : d || r) ? [] : s : y;
                if (n && n(y, _, a, l),
                r)
                    for (u = g(_, f),
                    r(u, [], a, l),
                    c = u.length; c--; )
                        (h = u[c]) && (_[f[c]] = !(y[f[c]] = h));
                if (i) {
                    if (o || t) {
                        if (o) {
                            for (u = [],
                            c = _.length; c--; )
                                (h = _[c]) && u.push(y[c] = h);
                            o(null, _ = [], u, l)
                        }
                        for (c = _.length; c--; )
                            (h = _[c]) && (u = o ? tt(i, h) : p[c]) > -1 && (i[u] = !(s[u] = h))
                    }
                } else
                    _ = g(_ === s ? _.splice(d, _.length) : _),
                    o ? o(null, s, _, l) : Z.apply(s, _)
            })
        }
        function y(t) {
            for (var e, n, i, r = t.length, o = b.relative[t[0].type], s = o || b.relative[" "], a = o ? 1 : 0, l = f(function(t) {
                return t === e
            }, s, !0), u = f(function(t) {
                return tt(e, t) > -1
            }, s, !0), c = [function(t, n, i) {
                var r = !o && (i || n !== P) || ((e = n).nodeType ? l(t, n, i) : u(t, n, i));
                return e = null,
                r
            }
            ]; r > a; a++)
                if (n = b.relative[t[a].type])
                    c = [f(d(c), n)];
                else {
                    if (n = b.filter[t[a].type].apply(null, t[a].matches),
                    n[q]) {
                        for (i = ++a; r > i && !b.relative[t[i].type]; i++)
                            ;
                        return v(a > 1 && d(c), a > 1 && p(t.slice(0, a - 1).concat({
                            value: " " === t[a - 2].type ? "*" : ""
                        })).replace(lt, "$1"), n, i > a && y(t.slice(a, i)), r > i && y(t = t.slice(i)), r > i && p(t))
                    }
                    c.push(n)
                }
            return d(c)
        }
        function _(t, n) {
            var r = n.length > 0
              , o = t.length > 0
              , s = function(i, s, a, l, u) {
                var c, h, p, f = 0, d = "0", m = i && [], v = [], y = P, _ = i || o && b.find.TAG("*", u), x = F += null == y ? 1 : Math.random() || .1, w = _.length;
                for (u && (P = s !== D && s); d !== w && null != (c = _[d]); d++) {
                    if (o && c) {
                        for (h = 0; p = t[h++]; )
                            if (p(c, s, a)) {
                                l.push(c);
                                break
                            }
                        u && (F = x)
                    }
                    r && ((c = !p && c) && f--,
                    i && m.push(c))
                }
                if (f += d,
                r && d !== f) {
                    for (h = 0; p = n[h++]; )
                        p(m, v, s, a);
                    if (i) {
                        if (f > 0)
                            for (; d--; )
                                m[d] || v[d] || (v[d] = Q.call(l));
                        v = g(v)
                    }
                    Z.apply(l, v),
                    u && !i && v.length > 0 && f + n.length > 1 && e.uniqueSort(l)
                }
                return u && (F = x,
                P = y),
                m
            };
            return r ? i(s) : s
        }
        var x, w, b, T, C, S, k, j, P, E, O, M, D, A, N, L, R, $, I, q = "sizzle" + 1 * new Date, H = t.document, F = 0, B = 0, z = n(), W = n(), X = n(), V = function(t, e) {
            return t === e && (O = !0),
            0
        }, Y = 1 << 31, U = {}.hasOwnProperty, G = [], Q = G.pop, J = G.push, Z = G.push, K = G.slice, tt = function(t, e) {
            for (var n = 0, i = t.length; i > n; n++)
                if (t[n] === e)
                    return n;
            return -1
        }, et = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped", nt = "[\\x20\\t\\r\\n\\f]", it = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+", rt = it.replace("w", "w#"), ot = "\\[" + nt + "*(" + it + ")(?:" + nt + "*([*^$|!~]?=)" + nt + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + rt + "))|)" + nt + "*\\]", st = ":(" + it + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + ot + ")*)|.*)\\)|)", at = new RegExp(nt + "+","g"), lt = new RegExp("^" + nt + "+|((?:^|[^\\\\])(?:\\\\.)*)" + nt + "+$","g"), ut = new RegExp("^" + nt + "*," + nt + "*"), ct = new RegExp("^" + nt + "*([>+~]|" + nt + ")" + nt + "*"), ht = new RegExp("=" + nt + "*([^\\]'\"]*?)" + nt + "*\\]","g"), pt = new RegExp(st), ft = new RegExp("^" + rt + "$"), dt = {
            ID: new RegExp("^#(" + it + ")"),
            CLASS: new RegExp("^\\.(" + it + ")"),
            TAG: new RegExp("^(" + it.replace("w", "w*") + ")"),
            ATTR: new RegExp("^" + ot),
            PSEUDO: new RegExp("^" + st),
            CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + nt + "*(even|odd|(([+-]|)(\\d*)n|)" + nt + "*(?:([+-]|)" + nt + "*(\\d+)|))" + nt + "*\\)|)","i"),
            bool: new RegExp("^(?:" + et + ")$","i"),
            needsContext: new RegExp("^" + nt + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + nt + "*((?:-\\d)?\\d*)" + nt + "*\\)|)(?=[^-]|$)","i")
        }, mt = /^(?:input|select|textarea|button)$/i, gt = /^h\d$/i, vt = /^[^{]+\{\s*\[native \w/, yt = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/, _t = /[+~]/, xt = /'|\\/g, wt = new RegExp("\\\\([\\da-f]{1,6}" + nt + "?|(" + nt + ")|.)","ig"), bt = function(t, e, n) {
            var i = "0x" + e - 65536;
            return i !== i || n ? e : 0 > i ? String.fromCharCode(i + 65536) : String.fromCharCode(i >> 10 | 55296, 1023 & i | 56320)
        }, Tt = function() {
            M()
        };
        try {
            Z.apply(G = K.call(H.childNodes), H.childNodes),
            G[H.childNodes.length].nodeType
        } catch (Ct) {
            Z = {
                apply: G.length ? function(t, e) {
                    J.apply(t, K.call(e))
                }
                : function(t, e) {
                    for (var n = t.length, i = 0; t[n++] = e[i++]; )
                        ;
                    t.length = n - 1
                }
            }
        }
        w = e.support = {},
        C = e.isXML = function(t) {
            var e = t && (t.ownerDocument || t).documentElement;
            return e ? "HTML" !== e.nodeName : !1
        }
        ,
        M = e.setDocument = function(t) {
            var e, n, i = t ? t.ownerDocument || t : H;
            return i !== D && 9 === i.nodeType && i.documentElement ? (D = i,
            A = i.documentElement,
            n = i.defaultView,
            n && n !== n.top && (n.addEventListener ? n.addEventListener("unload", Tt, !1) : n.attachEvent && n.attachEvent("onunload", Tt)),
            N = !C(i),
            w.attributes = r(function(t) {
                return t.className = "i",
                !t.getAttribute("className")
            }),
            w.getElementsByTagName = r(function(t) {
                return t.appendChild(i.createComment("")),
                !t.getElementsByTagName("*").length
            }),
            w.getElementsByClassName = vt.test(i.getElementsByClassName),
            w.getById = r(function(t) {
                return A.appendChild(t).id = q,
                !i.getElementsByName || !i.getElementsByName(q).length
            }),
            w.getById ? (b.find.ID = function(t, e) {
                if ("undefined" != typeof e.getElementById && N) {
                    var n = e.getElementById(t);
                    return n && n.parentNode ? [n] : []
                }
            }
            ,
            b.filter.ID = function(t) {
                var e = t.replace(wt, bt);
                return function(t) {
                    return t.getAttribute("id") === e
                }
            }
            ) : (delete b.find.ID,
            b.filter.ID = function(t) {
                var e = t.replace(wt, bt);
                return function(t) {
                    var n = "undefined" != typeof t.getAttributeNode && t.getAttributeNode("id");
                    return n && n.value === e
                }
            }
            ),
            b.find.TAG = w.getElementsByTagName ? function(t, e) {
                return "undefined" != typeof e.getElementsByTagName ? e.getElementsByTagName(t) : w.qsa ? e.querySelectorAll(t) : void 0
            }
            : function(t, e) {
                var n, i = [], r = 0, o = e.getElementsByTagName(t);
                if ("*" === t) {
                    for (; n = o[r++]; )
                        1 === n.nodeType && i.push(n);
                    return i
                }
                return o
            }
            ,
            b.find.CLASS = w.getElementsByClassName && function(t, e) {
                return N ? e.getElementsByClassName(t) : void 0
            }
            ,
            R = [],
            L = [],
            (w.qsa = vt.test(i.querySelectorAll)) && (r(function(t) {
                A.appendChild(t).innerHTML = "<a id='" + q + "'></a><select id='" + q + "-\f]' msallowcapture=''><option selected=''></option></select>",
                t.querySelectorAll("[msallowcapture^='']").length && L.push("[*^$]=" + nt + "*(?:''|\"\")"),
                t.querySelectorAll("[selected]").length || L.push("\\[" + nt + "*(?:value|" + et + ")"),
                t.querySelectorAll("[id~=" + q + "-]").length || L.push("~="),
                t.querySelectorAll(":checked").length || L.push(":checked"),
                t.querySelectorAll("a#" + q + "+*").length || L.push(".#.+[+~]")
            }),
            r(function(t) {
                var e = i.createElement("input");
                e.setAttribute("type", "hidden"),
                t.appendChild(e).setAttribute("name", "D"),
                t.querySelectorAll("[name=d]").length && L.push("name" + nt + "*[*^$|!~]?="),
                t.querySelectorAll(":enabled").length || L.push(":enabled", ":disabled"),
                t.querySelectorAll("*,:x"),
                L.push(",.*:")
            })),
            (w.matchesSelector = vt.test($ = A.matches || A.webkitMatchesSelector || A.mozMatchesSelector || A.oMatchesSelector || A.msMatchesSelector)) && r(function(t) {
                w.disconnectedMatch = $.call(t, "div"),
                $.call(t, "[s!='']:x"),
                R.push("!=", st)
            }),
            L = L.length && new RegExp(L.join("|")),
            R = R.length && new RegExp(R.join("|")),
            e = vt.test(A.compareDocumentPosition),
            I = e || vt.test(A.contains) ? function(t, e) {
                var n = 9 === t.nodeType ? t.documentElement : t
                  , i = e && e.parentNode;
                return t === i || !(!i || 1 !== i.nodeType || !(n.contains ? n.contains(i) : t.compareDocumentPosition && 16 & t.compareDocumentPosition(i)))
            }
            : function(t, e) {
                if (e)
                    for (; e = e.parentNode; )
                        if (e === t)
                            return !0;
                return !1
            }
            ,
            V = e ? function(t, e) {
                if (t === e)
                    return O = !0,
                    0;
                var n = !t.compareDocumentPosition - !e.compareDocumentPosition;
                return n ? n : (n = (t.ownerDocument || t) === (e.ownerDocument || e) ? t.compareDocumentPosition(e) : 1,
                1 & n || !w.sortDetached && e.compareDocumentPosition(t) === n ? t === i || t.ownerDocument === H && I(H, t) ? -1 : e === i || e.ownerDocument === H && I(H, e) ? 1 : E ? tt(E, t) - tt(E, e) : 0 : 4 & n ? -1 : 1)
            }
            : function(t, e) {
                if (t === e)
                    return O = !0,
                    0;
                var n, r = 0, o = t.parentNode, a = e.parentNode, l = [t], u = [e];
                if (!o || !a)
                    return t === i ? -1 : e === i ? 1 : o ? -1 : a ? 1 : E ? tt(E, t) - tt(E, e) : 0;
                if (o === a)
                    return s(t, e);
                for (n = t; n = n.parentNode; )
                    l.unshift(n);
                for (n = e; n = n.parentNode; )
                    u.unshift(n);
                for (; l[r] === u[r]; )
                    r++;
                return r ? s(l[r], u[r]) : l[r] === H ? -1 : u[r] === H ? 1 : 0
            }
            ,
            i) : D
        }
        ,
        e.matches = function(t, n) {
            return e(t, null, null, n)
        }
        ,
        e.matchesSelector = function(t, n) {
            if ((t.ownerDocument || t) !== D && M(t),
            n = n.replace(ht, "='$1']"),
            !(!w.matchesSelector || !N || R && R.test(n) || L && L.test(n)))
                try {
                    var i = $.call(t, n);
                    if (i || w.disconnectedMatch || t.document && 11 !== t.document.nodeType)
                        return i
                } catch (r) {}
            return e(n, D, null, [t]).length > 0
        }
        ,
        e.contains = function(t, e) {
            return (t.ownerDocument || t) !== D && M(t),
            I(t, e)
        }
        ,
        e.attr = function(t, e) {
            (t.ownerDocument || t) !== D && M(t);
            var n = b.attrHandle[e.toLowerCase()]
              , i = n && U.call(b.attrHandle, e.toLowerCase()) ? n(t, e, !N) : void 0;
            return void 0 !== i ? i : w.attributes || !N ? t.getAttribute(e) : (i = t.getAttributeNode(e)) && i.specified ? i.value : null
        }
        ,
        e.error = function(t) {
            throw new Error("Syntax error, unrecognized expression: " + t)
        }
        ,
        e.uniqueSort = function(t) {
            var e, n = [], i = 0, r = 0;
            if (O = !w.detectDuplicates,
            E = !w.sortStable && t.slice(0),
            t.sort(V),
            O) {
                for (; e = t[r++]; )
                    e === t[r] && (i = n.push(r));
                for (; i--; )
                    t.splice(n[i], 1)
            }
            return E = null,
            t
        }
        ,
        T = e.getText = function(t) {
            var e, n = "", i = 0, r = t.nodeType;
            if (r) {
                if (1 === r || 9 === r || 11 === r) {
                    if ("string" == typeof t.textContent)
                        return t.textContent;
                    for (t = t.firstChild; t; t = t.nextSibling)
                        n += T(t)
                } else if (3 === r || 4 === r)
                    return t.nodeValue
            } else
                for (; e = t[i++]; )
                    n += T(e);
            return n
        }
        ,
        b = e.selectors = {
            cacheLength: 50,
            createPseudo: i,
            match: dt,
            attrHandle: {},
            find: {},
            relative: {
                ">": {
                    dir: "parentNode",
                    first: !0
                },
                " ": {
                    dir: "parentNode"
                },
                "+": {
                    dir: "previousSibling",
                    first: !0
                },
                "~": {
                    dir: "previousSibling"
                }
            },
            preFilter: {
                ATTR: function(t) {
                    return t[1] = t[1].replace(wt, bt),
                    t[3] = (t[3] || t[4] || t[5] || "").replace(wt, bt),
                    "~=" === t[2] && (t[3] = " " + t[3] + " "),
                    t.slice(0, 4)
                },
                CHILD: function(t) {
                    return t[1] = t[1].toLowerCase(),
                    "nth" === t[1].slice(0, 3) ? (t[3] || e.error(t[0]),
                    t[4] = +(t[4] ? t[5] + (t[6] || 1) : 2 * ("even" === t[3] || "odd" === t[3])),
                    t[5] = +(t[7] + t[8] || "odd" === t[3])) : t[3] && e.error(t[0]),
                    t
                },
                PSEUDO: function(t) {
                    var e, n = !t[6] && t[2];
                    return dt.CHILD.test(t[0]) ? null : (t[3] ? t[2] = t[4] || t[5] || "" : n && pt.test(n) && (e = S(n, !0)) && (e = n.indexOf(")", n.length - e) - n.length) && (t[0] = t[0].slice(0, e),
                    t[2] = n.slice(0, e)),
                    t.slice(0, 3))
                }
            },
            filter: {
                TAG: function(t) {
                    var e = t.replace(wt, bt).toLowerCase();
                    return "*" === t ? function() {
                        return !0
                    }
                    : function(t) {
                        return t.nodeName && t.nodeName.toLowerCase() === e
                    }
                },
                CLASS: function(t) {
                    var e = z[t + " "];
                    return e || (e = new RegExp("(^|" + nt + ")" + t + "(" + nt + "|$)")) && z(t, function(t) {
                        return e.test("string" == typeof t.className && t.className || "undefined" != typeof t.getAttribute && t.getAttribute("class") || "")
                    })
                },
                ATTR: function(t, n, i) {
                    return function(r) {
                        var o = e.attr(r, t);
                        return null == o ? "!=" === n : n ? (o += "",
                        "=" === n ? o === i : "!=" === n ? o !== i : "^=" === n ? i && 0 === o.indexOf(i) : "*=" === n ? i && o.indexOf(i) > -1 : "$=" === n ? i && o.slice(-i.length) === i : "~=" === n ? (" " + o.replace(at, " ") + " ").indexOf(i) > -1 : "|=" === n ? o === i || o.slice(0, i.length + 1) === i + "-" : !1) : !0
                    }
                },
                CHILD: function(t, e, n, i, r) {
                    var o = "nth" !== t.slice(0, 3)
                      , s = "last" !== t.slice(-4)
                      , a = "of-type" === e;
                    return 1 === i && 0 === r ? function(t) {
                        return !!t.parentNode
                    }
                    : function(e, n, l) {
                        var u, c, h, p, f, d, m = o !== s ? "nextSibling" : "previousSibling", g = e.parentNode, v = a && e.nodeName.toLowerCase(), y = !l && !a;
                        if (g) {
                            if (o) {
                                for (; m; ) {
                                    for (h = e; h = h[m]; )
                                        if (a ? h.nodeName.toLowerCase() === v : 1 === h.nodeType)
                                            return !1;
                                    d = m = "only" === t && !d && "nextSibling"
                                }
                                return !0
                            }
                            if (d = [s ? g.firstChild : g.lastChild],
                            s && y) {
                                for (c = g[q] || (g[q] = {}),
                                u = c[t] || [],
                                f = u[0] === F && u[1],
                                p = u[0] === F && u[2],
                                h = f && g.childNodes[f]; h = ++f && h && h[m] || (p = f = 0) || d.pop(); )
                                    if (1 === h.nodeType && ++p && h === e) {
                                        c[t] = [F, f, p];
                                        break
                                    }
                            } else if (y && (u = (e[q] || (e[q] = {}))[t]) && u[0] === F)
                                p = u[1];
                            else
                                for (; (h = ++f && h && h[m] || (p = f = 0) || d.pop()) && ((a ? h.nodeName.toLowerCase() !== v : 1 !== h.nodeType) || !++p || (y && ((h[q] || (h[q] = {}))[t] = [F, p]),
                                h !== e)); )
                                    ;
                            return p -= r,
                            p === i || p % i === 0 && p / i >= 0
                        }
                    }
                },
                PSEUDO: function(t, n) {
                    var r, o = b.pseudos[t] || b.setFilters[t.toLowerCase()] || e.error("unsupported pseudo: " + t);
                    return o[q] ? o(n) : o.length > 1 ? (r = [t, t, "", n],
                    b.setFilters.hasOwnProperty(t.toLowerCase()) ? i(function(t, e) {
                        for (var i, r = o(t, n), s = r.length; s--; )
                            i = tt(t, r[s]),
                            t[i] = !(e[i] = r[s])
                    }) : function(t) {
                        return o(t, 0, r)
                    }
                    ) : o
                }
            },
            pseudos: {
                not: i(function(t) {
                    var e = []
                      , n = []
                      , r = k(t.replace(lt, "$1"));
                    return r[q] ? i(function(t, e, n, i) {
                        for (var o, s = r(t, null, i, []), a = t.length; a--; )
                            (o = s[a]) && (t[a] = !(e[a] = o))
                    }) : function(t, i, o) {
                        return e[0] = t,
                        r(e, null, o, n),
                        e[0] = null,
                        !n.pop()
                    }
                }),
                has: i(function(t) {
                    return function(n) {
                        return e(t, n).length > 0
                    }
                }),
                contains: i(function(t) {
                    return t = t.replace(wt, bt),
                    function(e) {
                        return (e.textContent || e.innerText || T(e)).indexOf(t) > -1
                    }
                }),
                lang: i(function(t) {
                    return ft.test(t || "") || e.error("unsupported lang: " + t),
                    t = t.replace(wt, bt).toLowerCase(),
                    function(e) {
                        var n;
                        do
                            if (n = N ? e.lang : e.getAttribute("xml:lang") || e.getAttribute("lang"))
                                return n = n.toLowerCase(),
                                n === t || 0 === n.indexOf(t + "-");
                        while ((e = e.parentNode) && 1 === e.nodeType);return !1
                    }
                }),
                target: function(e) {
                    var n = t.location && t.location.hash;
                    return n && n.slice(1) === e.id
                },
                root: function(t) {
                    return t === A
                },
                focus: function(t) {
                    return t === D.activeElement && (!D.hasFocus || D.hasFocus()) && !!(t.type || t.href || ~t.tabIndex)
                },
                enabled: function(t) {
                    return t.disabled === !1
                },
                disabled: function(t) {
                    return t.disabled === !0
                },
                checked: function(t) {
                    var e = t.nodeName.toLowerCase();
                    return "input" === e && !!t.checked || "option" === e && !!t.selected
                },
                selected: function(t) {
                    return t.parentNode && t.parentNode.selectedIndex,
                    t.selected === !0
                },
                empty: function(t) {
                    for (t = t.firstChild; t; t = t.nextSibling)
                        if (t.nodeType < 6)
                            return !1;
                    return !0
                },
                parent: function(t) {
                    return !b.pseudos.empty(t)
                },
                header: function(t) {
                    return gt.test(t.nodeName)
                },
                input: function(t) {
                    return mt.test(t.nodeName)
                },
                button: function(t) {
                    var e = t.nodeName.toLowerCase();
                    return "input" === e && "button" === t.type || "button" === e
                },
                text: function(t) {
                    var e;
                    return "input" === t.nodeName.toLowerCase() && "text" === t.type && (null == (e = t.getAttribute("type")) || "text" === e.toLowerCase())
                },
                first: u(function() {
                    return [0]
                }),
                last: u(function(t, e) {
                    return [e - 1]
                }),
                eq: u(function(t, e, n) {
                    return [0 > n ? n + e : n]
                }),
                even: u(function(t, e) {
                    for (var n = 0; e > n; n += 2)
                        t.push(n);
                    return t
                }),
                odd: u(function(t, e) {
                    for (var n = 1; e > n; n += 2)
                        t.push(n);
                    return t
                }),
                lt: u(function(t, e, n) {
                    for (var i = 0 > n ? n + e : n; --i >= 0; )
                        t.push(i);
                    return t
                }),
                gt: u(function(t, e, n) {
                    for (var i = 0 > n ? n + e : n; ++i < e; )
                        t.push(i);
                    return t
                })
            }
        },
        b.pseudos.nth = b.pseudos.eq;
        for (x in {
            radio: !0,
            checkbox: !0,
            file: !0,
            password: !0,
            image: !0
        })
            b.pseudos[x] = a(x);
        for (x in {
            submit: !0,
            reset: !0
        })
            b.pseudos[x] = l(x);
        return h.prototype = b.filters = b.pseudos,
        b.setFilters = new h,
        S = e.tokenize = function(t, n) {
            var i, r, o, s, a, l, u, c = W[t + " "];
            if (c)
                return n ? 0 : c.slice(0);
            for (a = t,
            l = [],
            u = b.preFilter; a; ) {
                (!i || (r = ut.exec(a))) && (r && (a = a.slice(r[0].length) || a),
                l.push(o = [])),
                i = !1,
                (r = ct.exec(a)) && (i = r.shift(),
                o.push({
                    value: i,
                    type: r[0].replace(lt, " ")
                }),
                a = a.slice(i.length));
                for (s in b.filter)
                    !(r = dt[s].exec(a)) || u[s] && !(r = u[s](r)) || (i = r.shift(),
                    o.push({
                        value: i,
                        type: s,
                        matches: r
                    }),
                    a = a.slice(i.length));
                if (!i)
                    break
            }
            return n ? a.length : a ? e.error(t) : W(t, l).slice(0)
        }
        ,
        k = e.compile = function(t, e) {
            var n, i = [], r = [], o = X[t + " "];
            if (!o) {
                for (e || (e = S(t)),
                n = e.length; n--; )
                    o = y(e[n]),
                    o[q] ? i.push(o) : r.push(o);
                o = X(t, _(r, i)),
                o.selector = t
            }
            return o
        }
        ,
        j = e.select = function(t, e, n, i) {
            var r, o, s, a, l, u = "function" == typeof t && t, h = !i && S(t = u.selector || t);
            if (n = n || [],
            1 === h.length) {
                if (o = h[0] = h[0].slice(0),
                o.length > 2 && "ID" === (s = o[0]).type && w.getById && 9 === e.nodeType && N && b.relative[o[1].type]) {
                    if (e = (b.find.ID(s.matches[0].replace(wt, bt), e) || [])[0],
                    !e)
                        return n;
                    u && (e = e.parentNode),
                    t = t.slice(o.shift().value.length)
                }
                for (r = dt.needsContext.test(t) ? 0 : o.length; r-- && (s = o[r],
                !b.relative[a = s.type]); )
                    if ((l = b.find[a]) && (i = l(s.matches[0].replace(wt, bt), _t.test(o[0].type) && c(e.parentNode) || e))) {
                        if (o.splice(r, 1),
                        t = i.length && p(o),
                        !t)
                            return Z.apply(n, i),
                            n;
                        break
                    }
            }
            return (u || k(t, h))(i, e, !N, n, _t.test(t) && c(e.parentNode) || e),
            n
        }
        ,
        w.sortStable = q.split("").sort(V).join("") === q,
        w.detectDuplicates = !!O,
        M(),
        w.sortDetached = r(function(t) {
            return 1 & t.compareDocumentPosition(D.createElement("div"))
        }),
        r(function(t) {
            return t.innerHTML = "<a href='#'></a>",
            "#" === t.firstChild.getAttribute("href")
        }) || o("type|href|height|width", function(t, e, n) {
            return n ? void 0 : t.getAttribute(e, "type" === e.toLowerCase() ? 1 : 2)
        }),
        w.attributes && r(function(t) {
            return t.innerHTML = "<input/>",
            t.firstChild.setAttribute("value", ""),
            "" === t.firstChild.getAttribute("value")
        }) || o("value", function(t, e, n) {
            return n || "input" !== t.nodeName.toLowerCase() ? void 0 : t.defaultValue
        }),
        r(function(t) {
            return null == t.getAttribute("disabled")
        }) || o(et, function(t, e, n) {
            var i;
            return n ? void 0 : t[e] === !0 ? e.toLowerCase() : (i = t.getAttributeNode(e)) && i.specified ? i.value : null
        }),
        e
    }(t);
    K.find = rt,
    K.expr = rt.selectors,
    K.expr[":"] = K.expr.pseudos,
    K.unique = rt.uniqueSort,
    K.text = rt.getText,
    K.isXMLDoc = rt.isXML,
    K.contains = rt.contains;
    var ot = K.expr.match.needsContext
      , st = /^<(\w+)\s*\/?>(?:<\/\1>|)$/
      , at = /^.[^:#\[\.,]*$/;
    K.filter = function(t, e, n) {
        var i = e[0];
        return n && (t = ":not(" + t + ")"),
        1 === e.length && 1 === i.nodeType ? K.find.matchesSelector(i, t) ? [i] : [] : K.find.matches(t, K.grep(e, function(t) {
            return 1 === t.nodeType
        }))
    }
    ,
    K.fn.extend({
        find: function(t) {
            var e, n = this.length, i = [], r = this;
            if ("string" != typeof t)
                return this.pushStack(K(t).filter(function() {
                    for (e = 0; n > e; e++)
                        if (K.contains(r[e], this))
                            return !0
                }));
            for (e = 0; n > e; e++)
                K.find(t, r[e], i);
            return i = this.pushStack(n > 1 ? K.unique(i) : i),
            i.selector = this.selector ? this.selector + " " + t : t,
            i
        },
        filter: function(t) {
            return this.pushStack(i(this, t || [], !1))
        },
        not: function(t) {
            return this.pushStack(i(this, t || [], !0))
        },
        is: function(t) {
            return !!i(this, "string" == typeof t && ot.test(t) ? K(t) : t || [], !1).length
        }
    });
    var lt, ut = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/, ct = K.fn.init = function(t, e) {
        var n, i;
        if (!t)
            return this;
        if ("string" == typeof t) {
            if (n = "<" === t[0] && ">" === t[t.length - 1] && t.length >= 3 ? [null, t, null] : ut.exec(t),
            !n || !n[1] && e)
                return !e || e.jquery ? (e || lt).find(t) : this.constructor(e).find(t);
            if (n[1]) {
                if (e = e instanceof K ? e[0] : e,
                K.merge(this, K.parseHTML(n[1], e && e.nodeType ? e.ownerDocument || e : J, !0)),
                st.test(n[1]) && K.isPlainObject(e))
                    for (n in e)
                        K.isFunction(this[n]) ? this[n](e[n]) : this.attr(n, e[n]);
                return this
            }
            return i = J.getElementById(n[2]),
            i && i.parentNode && (this.length = 1,
            this[0] = i),
            this.context = J,
            this.selector = t,
            this
        }
        return t.nodeType ? (this.context = this[0] = t,
        this.length = 1,
        this) : K.isFunction(t) ? "undefined" != typeof lt.ready ? lt.ready(t) : t(K) : (void 0 !== t.selector && (this.selector = t.selector,
        this.context = t.context),
        K.makeArray(t, this))
    }
    ;
    ct.prototype = K.fn,
    lt = K(J);
    var ht = /^(?:parents|prev(?:Until|All))/
      , pt = {
        children: !0,
        contents: !0,
        next: !0,
        prev: !0
    };
    K.extend({
        dir: function(t, e, n) {
            for (var i = [], r = void 0 !== n; (t = t[e]) && 9 !== t.nodeType; )
                if (1 === t.nodeType) {
                    if (r && K(t).is(n))
                        break;
                    i.push(t)
                }
            return i
        },
        sibling: function(t, e) {
            for (var n = []; t; t = t.nextSibling)
                1 === t.nodeType && t !== e && n.push(t);
            return n
        }
    }),
    K.fn.extend({
        has: function(t) {
            var e = K(t, this)
              , n = e.length;
            return this.filter(function() {
                for (var t = 0; n > t; t++)
                    if (K.contains(this, e[t]))
                        return !0
            })
        },
        closest: function(t, e) {
            for (var n, i = 0, r = this.length, o = [], s = ot.test(t) || "string" != typeof t ? K(t, e || this.context) : 0; r > i; i++)
                for (n = this[i]; n && n !== e; n = n.parentNode)
                    if (n.nodeType < 11 && (s ? s.index(n) > -1 : 1 === n.nodeType && K.find.matchesSelector(n, t))) {
                        o.push(n);
                        break
                    }
            return this.pushStack(o.length > 1 ? K.unique(o) : o)
        },
        index: function(t) {
            return t ? "string" == typeof t ? V.call(K(t), this[0]) : V.call(this, t.jquery ? t[0] : t) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
        },
        add: function(t, e) {
            return this.pushStack(K.unique(K.merge(this.get(), K(t, e))))
        },
        addBack: function(t) {
            return this.add(null == t ? this.prevObject : this.prevObject.filter(t))
        }
    }),
    K.each({
        parent: function(t) {
            var e = t.parentNode;
            return e && 11 !== e.nodeType ? e : null
        },
        parents: function(t) {
            return K.dir(t, "parentNode")
        },
        parentsUntil: function(t, e, n) {
            return K.dir(t, "parentNode", n)
        },
        next: function(t) {
            return r(t, "nextSibling")
        },
        prev: function(t) {
            return r(t, "previousSibling")
        },
        nextAll: function(t) {
            return K.dir(t, "nextSibling")
        },
        prevAll: function(t) {
            return K.dir(t, "previousSibling")
        },
        nextUntil: function(t, e, n) {
            return K.dir(t, "nextSibling", n)
        },
        prevUntil: function(t, e, n) {
            return K.dir(t, "previousSibling", n)
        },
        siblings: function(t) {
            return K.sibling((t.parentNode || {}).firstChild, t)
        },
        children: function(t) {
            return K.sibling(t.firstChild)
        },
        contents: function(t) {
            return t.contentDocument || K.merge([], t.childNodes)
        }
    }, function(t, e) {
        K.fn[t] = function(n, i) {
            var r = K.map(this, e, n);
            return "Until" !== t.slice(-5) && (i = n),
            i && "string" == typeof i && (r = K.filter(i, r)),
            this.length > 1 && (pt[t] || K.unique(r),
            ht.test(t) && r.reverse()),
            this.pushStack(r)
        }
    });
    var ft = /\S+/g
      , dt = {};
    K.Callbacks = function(t) {
        t = "string" == typeof t ? dt[t] || o(t) : K.extend({}, t);
        var e, n, i, r, s, a, l = [], u = !t.once && [], c = function(o) {
            for (e = t.memory && o,
            n = !0,
            a = r || 0,
            r = 0,
            s = l.length,
            i = !0; l && s > a; a++)
                if (l[a].apply(o[0], o[1]) === !1 && t.stopOnFalse) {
                    e = !1;
                    break
                }
            i = !1,
            l && (u ? u.length && c(u.shift()) : e ? l = [] : h.disable())
        }, h = {
            add: function() {
                if (l) {
                    var n = l.length;
                    !function o(e) {
                        K.each(e, function(e, n) {
                            var i = K.type(n);
                            "function" === i ? t.unique && h.has(n) || l.push(n) : n && n.length && "string" !== i && o(n)
                        })
                    }(arguments),
                    i ? s = l.length : e && (r = n,
                    c(e))
                }
                return this
            },
            remove: function() {
                return l && K.each(arguments, function(t, e) {
                    for (var n; (n = K.inArray(e, l, n)) > -1; )
                        l.splice(n, 1),
                        i && (s >= n && s--,
                        a >= n && a--)
                }),
                this
            },
            has: function(t) {
                return t ? K.inArray(t, l) > -1 : !(!l || !l.length)
            },
            empty: function() {
                return l = [],
                s = 0,
                this
            },
            disable: function() {
                return l = u = e = void 0,
                this
            },
            disabled: function() {
                return !l
            },
            lock: function() {
                return u = void 0,
                e || h.disable(),
                this
            },
            locked: function() {
                return !u
            },
            fireWith: function(t, e) {
                return !l || n && !u || (e = e || [],
                e = [t, e.slice ? e.slice() : e],
                i ? u.push(e) : c(e)),
                this
            },
            fire: function() {
                return h.fireWith(this, arguments),
                this
            },
            fired: function() {
                return !!n
            }
        };
        return h
    }
    ,
    K.extend({
        Deferred: function(t) {
            var e = [["resolve", "done", K.Callbacks("once memory"), "resolved"], ["reject", "fail", K.Callbacks("once memory"), "rejected"], ["notify", "progress", K.Callbacks("memory")]]
              , n = "pending"
              , i = {
                state: function() {
                    return n
                },
                always: function() {
                    return r.done(arguments).fail(arguments),
                    this
                },
                then: function() {
                    var t = arguments;
                    return K.Deferred(function(n) {
                        K.each(e, function(e, o) {
                            var s = K.isFunction(t[e]) && t[e];
                            r[o[1]](function() {
                                var t = s && s.apply(this, arguments);
                                t && K.isFunction(t.promise) ? t.promise().done(n.resolve).fail(n.reject).progress(n.notify) : n[o[0] + "With"](this === i ? n.promise() : this, s ? [t] : arguments)
                            })
                        }),
                        t = null
                    }).promise()
                },
                promise: function(t) {
                    return null != t ? K.extend(t, i) : i
                }
            }
              , r = {};
            return i.pipe = i.then,
            K.each(e, function(t, o) {
                var s = o[2]
                  , a = o[3];
                i[o[1]] = s.add,
                a && s.add(function() {
                    n = a
                }, e[1 ^ t][2].disable, e[2][2].lock),
                r[o[0]] = function() {
                    return r[o[0] + "With"](this === r ? i : this, arguments),
                    this
                }
                ,
                r[o[0] + "With"] = s.fireWith
            }),
            i.promise(r),
            t && t.call(r, r),
            r
        },
        when: function(t) {
            var e, n, i, r = 0, o = z.call(arguments), s = o.length, a = 1 !== s || t && K.isFunction(t.promise) ? s : 0, l = 1 === a ? t : K.Deferred(), u = function(t, n, i) {
                return function(r) {
                    n[t] = this,
                    i[t] = arguments.length > 1 ? z.call(arguments) : r,
                    i === e ? l.notifyWith(n, i) : --a || l.resolveWith(n, i)
                }
            };
            if (s > 1)
                for (e = new Array(s),
                n = new Array(s),
                i = new Array(s); s > r; r++)
                    o[r] && K.isFunction(o[r].promise) ? o[r].promise().done(u(r, i, o)).fail(l.reject).progress(u(r, n, e)) : --a;
            return a || l.resolveWith(i, o),
            l.promise()
        }
    });
    var mt;
    K.fn.ready = function(t) {
        return K.ready.promise().done(t),
        this
    }
    ,
    K.extend({
        isReady: !1,
        readyWait: 1,
        holdReady: function(t) {
            t ? K.readyWait++ : K.ready(!0)
        },
        ready: function(t) {
            (t === !0 ? --K.readyWait : K.isReady) || (K.isReady = !0,
            t !== !0 && --K.readyWait > 0 || (mt.resolveWith(J, [K]),
            K.fn.triggerHandler && (K(J).triggerHandler("ready"),
            K(J).off("ready"))))
        }
    }),
    K.ready.promise = function(e) {
        return mt || (mt = K.Deferred(),
        "complete" === J.readyState ? setTimeout(K.ready) : (J.addEventListener("DOMContentLoaded", s, !1),
        t.addEventListener("load", s, !1))),
        mt.promise(e)
    }
    ,
    K.ready.promise();
    var gt = K.access = function(t, e, n, i, r, o, s) {
        var a = 0
          , l = t.length
          , u = null == n;
        if ("object" === K.type(n)) {
            r = !0;
            for (a in n)
                K.access(t, e, a, n[a], !0, o, s)
        } else if (void 0 !== i && (r = !0,
        K.isFunction(i) || (s = !0),
        u && (s ? (e.call(t, i),
        e = null) : (u = e,
        e = function(t, e, n) {
            return u.call(K(t), n)
        }
        )),
        e))
            for (; l > a; a++)
                e(t[a], n, s ? i : i.call(t[a], a, e(t[a], n)));
        return r ? t : u ? e.call(t) : l ? e(t[0], n) : o
    }
    ;
    K.acceptData = function(t) {
        return 1 === t.nodeType || 9 === t.nodeType || !+t.nodeType
    }
    ,
    a.uid = 1,
    a.accepts = K.acceptData,
    a.prototype = {
        key: function(t) {
            if (!a.accepts(t))
                return 0;
            var e = {}
              , n = t[this.expando];
            if (!n) {
                n = a.uid++;
                try {
                    e[this.expando] = {
                        value: n
                    },
                    Object.defineProperties(t, e)
                } catch (i) {
                    e[this.expando] = n,
                    K.extend(t, e)
                }
            }
            return this.cache[n] || (this.cache[n] = {}),
            n
        },
        set: function(t, e, n) {
            var i, r = this.key(t), o = this.cache[r];
            if ("string" == typeof e)
                o[e] = n;
            else if (K.isEmptyObject(o))
                K.extend(this.cache[r], e);
            else
                for (i in e)
                    o[i] = e[i];
            return o
        },
        get: function(t, e) {
            var n = this.cache[this.key(t)];
            return void 0 === e ? n : n[e]
        },
        access: function(t, e, n) {
            var i;
            return void 0 === e || e && "string" == typeof e && void 0 === n ? (i = this.get(t, e),
            void 0 !== i ? i : this.get(t, K.camelCase(e))) : (this.set(t, e, n),
            void 0 !== n ? n : e)
        },
        remove: function(t, e) {
            var n, i, r, o = this.key(t), s = this.cache[o];
            if (void 0 === e)
                this.cache[o] = {};
            else {
                K.isArray(e) ? i = e.concat(e.map(K.camelCase)) : (r = K.camelCase(e),
                e in s ? i = [e, r] : (i = r,
                i = i in s ? [i] : i.match(ft) || [])),
                n = i.length;
                for (; n--; )
                    delete s[i[n]]
            }
        },
        hasData: function(t) {
            return !K.isEmptyObject(this.cache[t[this.expando]] || {})
        },
        discard: function(t) {
            t[this.expando] && delete this.cache[t[this.expando]]
        }
    };
    var vt = new a
      , yt = new a
      , _t = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/
      , xt = /([A-Z])/g;
    K.extend({
        hasData: function(t) {
            return yt.hasData(t) || vt.hasData(t)
        },
        data: function(t, e, n) {
            return yt.access(t, e, n)
        },
        removeData: function(t, e) {
            yt.remove(t, e)
        },
        _data: function(t, e, n) {
            return vt.access(t, e, n)
        },
        _removeData: function(t, e) {
            vt.remove(t, e)
        }
    }),
    K.fn.extend({
        data: function(t, e) {
            var n, i, r, o = this[0], s = o && o.attributes;
            if (void 0 === t) {
                if (this.length && (r = yt.get(o),
                1 === o.nodeType && !vt.get(o, "hasDataAttrs"))) {
                    for (n = s.length; n--; )
                        s[n] && (i = s[n].name,
                        0 === i.indexOf("data-") && (i = K.camelCase(i.slice(5)),
                        l(o, i, r[i])));
                    vt.set(o, "hasDataAttrs", !0)
                }
                return r
            }
            return "object" == typeof t ? this.each(function() {
                yt.set(this, t)
            }) : gt(this, function(e) {
                var n, i = K.camelCase(t);
                if (o && void 0 === e) {
                    if (n = yt.get(o, t),
                    void 0 !== n)
                        return n;
                    if (n = yt.get(o, i),
                    void 0 !== n)
                        return n;
                    if (n = l(o, i, void 0),
                    void 0 !== n)
                        return n
                } else
                    this.each(function() {
                        var n = yt.get(this, i);
                        yt.set(this, i, e),
                        -1 !== t.indexOf("-") && void 0 !== n && yt.set(this, t, e)
                    })
            }, null, e, arguments.length > 1, null, !0)
        },
        removeData: function(t) {
            return this.each(function() {
                yt.remove(this, t)
            })
        }
    }),
    K.extend({
        queue: function(t, e, n) {
            var i;
            return t ? (e = (e || "fx") + "queue",
            i = vt.get(t, e),
            n && (!i || K.isArray(n) ? i = vt.access(t, e, K.makeArray(n)) : i.push(n)),
            i || []) : void 0
        },
        dequeue: function(t, e) {
            e = e || "fx";
            var n = K.queue(t, e)
              , i = n.length
              , r = n.shift()
              , o = K._queueHooks(t, e)
              , s = function() {
                K.dequeue(t, e)
            };
            "inprogress" === r && (r = n.shift(),
            i--),
            r && ("fx" === e && n.unshift("inprogress"),
            delete o.stop,
            r.call(t, s, o)),
            !i && o && o.empty.fire()
        },
        _queueHooks: function(t, e) {
            var n = e + "queueHooks";
            return vt.get(t, n) || vt.access(t, n, {
                empty: K.Callbacks("once memory").add(function() {
                    vt.remove(t, [e + "queue", n])
                })
            })
        }
    }),
    K.fn.extend({
        queue: function(t, e) {
            var n = 2;
            return "string" != typeof t && (e = t,
            t = "fx",
            n--),
            arguments.length < n ? K.queue(this[0], t) : void 0 === e ? this : this.each(function() {
                var n = K.queue(this, t, e);
                K._queueHooks(this, t),
                "fx" === t && "inprogress" !== n[0] && K.dequeue(this, t)
            })
        },
        dequeue: function(t) {
            return this.each(function() {
                K.dequeue(this, t)
            })
        },
        clearQueue: function(t) {
            return this.queue(t || "fx", [])
        },
        promise: function(t, e) {
            var n, i = 1, r = K.Deferred(), o = this, s = this.length, a = function() {
                --i || r.resolveWith(o, [o])
            };
            for ("string" != typeof t && (e = t,
            t = void 0),
            t = t || "fx"; s--; )
                n = vt.get(o[s], t + "queueHooks"),
                n && n.empty && (i++,
                n.empty.add(a));
            return a(),
            r.promise(e)
        }
    });
    var wt = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source
      , bt = ["Top", "Right", "Bottom", "Left"]
      , Tt = function(t, e) {
        return t = e || t,
        "none" === K.css(t, "display") || !K.contains(t.ownerDocument, t)
    }
      , Ct = /^(?:checkbox|radio)$/i;
    !function() {
        var t = J.createDocumentFragment()
          , e = t.appendChild(J.createElement("div"))
          , n = J.createElement("input");
        n.setAttribute("type", "radio"),
        n.setAttribute("checked", "checked"),
        n.setAttribute("name", "t"),
        e.appendChild(n),
        Q.checkClone = e.cloneNode(!0).cloneNode(!0).lastChild.checked,
        e.innerHTML = "<textarea>x</textarea>",
        Q.noCloneChecked = !!e.cloneNode(!0).lastChild.defaultValue
    }();
    var St = "undefined";
    Q.focusinBubbles = "onfocusin"in t;
    var kt = /^key/
      , jt = /^(?:mouse|pointer|contextmenu)|click/
      , Pt = /^(?:focusinfocus|focusoutblur)$/
      , Et = /^([^.]*)(?:\.(.+)|)$/;
    K.event = {
        global: {},
        add: function(t, e, n, i, r) {
            var o, s, a, l, u, c, h, p, f, d, m, g = vt.get(t);
            if (g)
                for (n.handler && (o = n,
                n = o.handler,
                r = o.selector),
                n.guid || (n.guid = K.guid++),
                (l = g.events) || (l = g.events = {}),
                (s = g.handle) || (s = g.handle = function(e) {
                    return typeof K !== St && K.event.triggered !== e.type ? K.event.dispatch.apply(t, arguments) : void 0
                }
                ),
                e = (e || "").match(ft) || [""],
                u = e.length; u--; )
                    a = Et.exec(e[u]) || [],
                    f = m = a[1],
                    d = (a[2] || "").split(".").sort(),
                    f && (h = K.event.special[f] || {},
                    f = (r ? h.delegateType : h.bindType) || f,
                    h = K.event.special[f] || {},
                    c = K.extend({
                        type: f,
                        origType: m,
                        data: i,
                        handler: n,
                        guid: n.guid,
                        selector: r,
                        needsContext: r && K.expr.match.needsContext.test(r),
                        namespace: d.join(".")
                    }, o),
                    (p = l[f]) || (p = l[f] = [],
                    p.delegateCount = 0,
                    h.setup && h.setup.call(t, i, d, s) !== !1 || t.addEventListener && t.addEventListener(f, s, !1)),
                    h.add && (h.add.call(t, c),
                    c.handler.guid || (c.handler.guid = n.guid)),
                    r ? p.splice(p.delegateCount++, 0, c) : p.push(c),
                    K.event.global[f] = !0)
        },
        remove: function(t, e, n, i, r) {
            var o, s, a, l, u, c, h, p, f, d, m, g = vt.hasData(t) && vt.get(t);
            if (g && (l = g.events)) {
                for (e = (e || "").match(ft) || [""],
                u = e.length; u--; )
                    if (a = Et.exec(e[u]) || [],
                    f = m = a[1],
                    d = (a[2] || "").split(".").sort(),
                    f) {
                        for (h = K.event.special[f] || {},
                        f = (i ? h.delegateType : h.bindType) || f,
                        p = l[f] || [],
                        a = a[2] && new RegExp("(^|\\.)" + d.join("\\.(?:.*\\.|)") + "(\\.|$)"),
                        s = o = p.length; o--; )
                            c = p[o],
                            !r && m !== c.origType || n && n.guid !== c.guid || a && !a.test(c.namespace) || i && i !== c.selector && ("**" !== i || !c.selector) || (p.splice(o, 1),
                            c.selector && p.delegateCount--,
                            h.remove && h.remove.call(t, c));
                        s && !p.length && (h.teardown && h.teardown.call(t, d, g.handle) !== !1 || K.removeEvent(t, f, g.handle),
                        delete l[f])
                    } else
                        for (f in l)
                            K.event.remove(t, f + e[u], n, i, !0);
                K.isEmptyObject(l) && (delete g.handle,
                vt.remove(t, "events"))
            }
        },
        trigger: function(e, n, i, r) {
            var o, s, a, l, u, c, h, p = [i || J], f = G.call(e, "type") ? e.type : e, d = G.call(e, "namespace") ? e.namespace.split(".") : [];
            if (s = a = i = i || J,
            3 !== i.nodeType && 8 !== i.nodeType && !Pt.test(f + K.event.triggered) && (f.indexOf(".") >= 0 && (d = f.split("."),
            f = d.shift(),
            d.sort()),
            u = f.indexOf(":") < 0 && "on" + f,
            e = e[K.expando] ? e : new K.Event(f,"object" == typeof e && e),
            e.isTrigger = r ? 2 : 3,
            e.namespace = d.join("."),
            e.namespace_re = e.namespace ? new RegExp("(^|\\.)" + d.join("\\.(?:.*\\.|)") + "(\\.|$)") : null,
            e.result = void 0,
            e.target || (e.target = i),
            n = null == n ? [e] : K.makeArray(n, [e]),
            h = K.event.special[f] || {},
            r || !h.trigger || h.trigger.apply(i, n) !== !1)) {
                if (!r && !h.noBubble && !K.isWindow(i)) {
                    for (l = h.delegateType || f,
                    Pt.test(l + f) || (s = s.parentNode); s; s = s.parentNode)
                        p.push(s),
                        a = s;
                    a === (i.ownerDocument || J) && p.push(a.defaultView || a.parentWindow || t)
                }
                for (o = 0; (s = p[o++]) && !e.isPropagationStopped(); )
                    e.type = o > 1 ? l : h.bindType || f,
                    c = (vt.get(s, "events") || {})[e.type] && vt.get(s, "handle"),
                    c && c.apply(s, n),
                    c = u && s[u],
                    c && c.apply && K.acceptData(s) && (e.result = c.apply(s, n),
                    e.result === !1 && e.preventDefault());
                return e.type = f,
                r || e.isDefaultPrevented() || h._default && h._default.apply(p.pop(), n) !== !1 || !K.acceptData(i) || u && K.isFunction(i[f]) && !K.isWindow(i) && (a = i[u],
                a && (i[u] = null),
                K.event.triggered = f,
                i[f](),
                K.event.triggered = void 0,
                a && (i[u] = a)),
                e.result
            }
        },
        dispatch: function(t) {
            t = K.event.fix(t);
            var e, n, i, r, o, s = [], a = z.call(arguments), l = (vt.get(this, "events") || {})[t.type] || [], u = K.event.special[t.type] || {};
            if (a[0] = t,
            t.delegateTarget = this,
            !u.preDispatch || u.preDispatch.call(this, t) !== !1) {
                for (s = K.event.handlers.call(this, t, l),
                e = 0; (r = s[e++]) && !t.isPropagationStopped(); )
                    for (t.currentTarget = r.elem,
                    n = 0; (o = r.handlers[n++]) && !t.isImmediatePropagationStopped(); )
                        (!t.namespace_re || t.namespace_re.test(o.namespace)) && (t.handleObj = o,
                        t.data = o.data,
                        i = ((K.event.special[o.origType] || {}).handle || o.handler).apply(r.elem, a),
                        void 0 !== i && (t.result = i) === !1 && (t.preventDefault(),
                        t.stopPropagation()));
                return u.postDispatch && u.postDispatch.call(this, t),
                t.result
            }
        },
        handlers: function(t, e) {
            var n, i, r, o, s = [], a = e.delegateCount, l = t.target;
            if (a && l.nodeType && (!t.button || "click" !== t.type))
                for (; l !== this; l = l.parentNode || this)
                    if (l.disabled !== !0 || "click" !== t.type) {
                        for (i = [],
                        n = 0; a > n; n++)
                            o = e[n],
                            r = o.selector + " ",
                            void 0 === i[r] && (i[r] = o.needsContext ? K(r, this).index(l) >= 0 : K.find(r, this, null, [l]).length),
                            i[r] && i.push(o);
                        i.length && s.push({
                            elem: l,
                            handlers: i
                        })
                    }
            return a < e.length && s.push({
                elem: this,
                handlers: e.slice(a)
            }),
            s
        },
        props: "altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),
        fixHooks: {},
        keyHooks: {
            props: "char charCode key keyCode".split(" "),
            filter: function(t, e) {
                return null == t.which && (t.which = null != e.charCode ? e.charCode : e.keyCode),
                t
            }
        },
        mouseHooks: {
            props: "button buttons clientX clientY offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
            filter: function(t, e) {
                var n, i, r, o = e.button;
                return null == t.pageX && null != e.clientX && (n = t.target.ownerDocument || J,
                i = n.documentElement,
                r = n.body,
                t.pageX = e.clientX + (i && i.scrollLeft || r && r.scrollLeft || 0) - (i && i.clientLeft || r && r.clientLeft || 0),
                t.pageY = e.clientY + (i && i.scrollTop || r && r.scrollTop || 0) - (i && i.clientTop || r && r.clientTop || 0)),
                t.which || void 0 === o || (t.which = 1 & o ? 1 : 2 & o ? 3 : 4 & o ? 2 : 0),
                t
            }
        },
        fix: function(t) {
            if (t[K.expando])
                return t;
            var e, n, i, r = t.type, o = t, s = this.fixHooks[r];
            for (s || (this.fixHooks[r] = s = jt.test(r) ? this.mouseHooks : kt.test(r) ? this.keyHooks : {}),
            i = s.props ? this.props.concat(s.props) : this.props,
            t = new K.Event(o),
            e = i.length; e--; )
                n = i[e],
                t[n] = o[n];
            return t.target || (t.target = J),
            3 === t.target.nodeType && (t.target = t.target.parentNode),
            s.filter ? s.filter(t, o) : t
        },
        special: {
            load: {
                noBubble: !0
            },
            focus: {
                trigger: function() {
                    return this !== h() && this.focus ? (this.focus(),
                    !1) : void 0
                },
                delegateType: "focusin"
            },
            blur: {
                trigger: function() {
                    return this === h() && this.blur ? (this.blur(),
                    !1) : void 0
                },
                delegateType: "focusout"
            },
            click: {
                trigger: function() {
                    return "checkbox" === this.type && this.click && K.nodeName(this, "input") ? (this.click(),
                    !1) : void 0
                },
                _default: function(t) {
                    return K.nodeName(t.target, "a")
                }
            },
            beforeunload: {
                postDispatch: function(t) {
                    void 0 !== t.result && t.originalEvent && (t.originalEvent.returnValue = t.result)
                }
            }
        },
        simulate: function(t, e, n, i) {
            var r = K.extend(new K.Event, n, {
                type: t,
                isSimulated: !0,
                originalEvent: {}
            });
            i ? K.event.trigger(r, null, e) : K.event.dispatch.call(e, r),
            r.isDefaultPrevented() && n.preventDefault()
        }
    },
    K.removeEvent = function(t, e, n) {
        t.removeEventListener && t.removeEventListener(e, n, !1)
    }
    ,
    K.Event = function(t, e) {
        return this instanceof K.Event ? (t && t.type ? (this.originalEvent = t,
        this.type = t.type,
        this.isDefaultPrevented = t.defaultPrevented || void 0 === t.defaultPrevented && t.returnValue === !1 ? u : c) : this.type = t,
        e && K.extend(this, e),
        this.timeStamp = t && t.timeStamp || K.now(),
        void (this[K.expando] = !0)) : new K.Event(t,e)
    }
    ,
    K.Event.prototype = {
        isDefaultPrevented: c,
        isPropagationStopped: c,
        isImmediatePropagationStopped: c,
        preventDefault: function() {
            var t = this.originalEvent;
            this.isDefaultPrevented = u,
            t && t.preventDefault && t.preventDefault()
        },
        stopPropagation: function() {
            var t = this.originalEvent;
            this.isPropagationStopped = u,
            t && t.stopPropagation && t.stopPropagation()
        },
        stopImmediatePropagation: function() {
            var t = this.originalEvent;
            this.isImmediatePropagationStopped = u,
            t && t.stopImmediatePropagation && t.stopImmediatePropagation(),
            this.stopPropagation()
        }
    },
    K.each({
        mouseenter: "mouseover",
        mouseleave: "mouseout",
        pointerenter: "pointerover",
        pointerleave: "pointerout"
    }, function(t, e) {
        K.event.special[t] = {
            delegateType: e,
            bindType: e,
            handle: function(t) {
                var n, i = this, r = t.relatedTarget, o = t.handleObj;
                return (!r || r !== i && !K.contains(i, r)) && (t.type = o.origType,
                n = o.handler.apply(this, arguments),
                t.type = e),
                n
            }
        }
    }),
    Q.focusinBubbles || K.each({
        focus: "focusin",
        blur: "focusout"
    }, function(t, e) {
        var n = function(t) {
            K.event.simulate(e, t.target, K.event.fix(t), !0)
        };
        K.event.special[e] = {
            setup: function() {
                var i = this.ownerDocument || this
                  , r = vt.access(i, e);
                r || i.addEventListener(t, n, !0),
                vt.access(i, e, (r || 0) + 1)
            },
            teardown: function() {
                var i = this.ownerDocument || this
                  , r = vt.access(i, e) - 1;
                r ? vt.access(i, e, r) : (i.removeEventListener(t, n, !0),
                vt.remove(i, e))
            }
        }
    }),
    K.fn.extend({
        on: function(t, e, n, i, r) {
            var o, s;
            if ("object" == typeof t) {
                "string" != typeof e && (n = n || e,
                e = void 0);
                for (s in t)
                    this.on(s, e, n, t[s], r);
                return this
            }
            if (null == n && null == i ? (i = e,
            n = e = void 0) : null == i && ("string" == typeof e ? (i = n,
            n = void 0) : (i = n,
            n = e,
            e = void 0)),
            i === !1)
                i = c;
            else if (!i)
                return this;
            return 1 === r && (o = i,
            i = function(t) {
                return K().off(t),
                o.apply(this, arguments)
            }
            ,
            i.guid = o.guid || (o.guid = K.guid++)),
            this.each(function() {
                K.event.add(this, t, i, n, e)
            })
        },
        one: function(t, e, n, i) {
            return this.on(t, e, n, i, 1)
        },
        off: function(t, e, n) {
            var i, r;
            if (t && t.preventDefault && t.handleObj)
                return i = t.handleObj,
                K(t.delegateTarget).off(i.namespace ? i.origType + "." + i.namespace : i.origType, i.selector, i.handler),
                this;
            if ("object" == typeof t) {
                for (r in t)
                    this.off(r, e, t[r]);
                return this
            }
            return (e === !1 || "function" == typeof e) && (n = e,
            e = void 0),
            n === !1 && (n = c),
            this.each(function() {
                K.event.remove(this, t, n, e)
            })
        },
        trigger: function(t, e) {
            return this.each(function() {
                K.event.trigger(t, e, this)
            })
        },
        triggerHandler: function(t, e) {
            var n = this[0];
            return n ? K.event.trigger(t, e, n, !0) : void 0
        }
    });
    var Ot = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi
      , Mt = /<([\w:]+)/
      , Dt = /<|&#?\w+;/
      , At = /<(?:script|style|link)/i
      , Nt = /checked\s*(?:[^=]|=\s*.checked.)/i
      , Lt = /^$|\/(?:java|ecma)script/i
      , Rt = /^true\/(.*)/
      , $t = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g
      , It = {
        option: [1, "<select multiple='multiple'>", "</select>"],
        thead: [1, "<table>", "</table>"],
        col: [2, "<table><colgroup>", "</colgroup></table>"],
        tr: [2, "<table><tbody>", "</tbody></table>"],
        td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
        _default: [0, "", ""]
    };
    It.optgroup = It.option,
    It.tbody = It.tfoot = It.colgroup = It.caption = It.thead,
    It.th = It.td,
    K.extend({
        clone: function(t, e, n) {
            var i, r, o, s, a = t.cloneNode(!0), l = K.contains(t.ownerDocument, t);
            if (!(Q.noCloneChecked || 1 !== t.nodeType && 11 !== t.nodeType || K.isXMLDoc(t)))
                for (s = v(a),
                o = v(t),
                i = 0,
                r = o.length; r > i; i++)
                    y(o[i], s[i]);
            if (e)
                if (n)
                    for (o = o || v(t),
                    s = s || v(a),
                    i = 0,
                    r = o.length; r > i; i++)
                        g(o[i], s[i]);
                else
                    g(t, a);
            return s = v(a, "script"),
            s.length > 0 && m(s, !l && v(t, "script")),
            a
        },
        buildFragment: function(t, e, n, i) {
            for (var r, o, s, a, l, u, c = e.createDocumentFragment(), h = [], p = 0, f = t.length; f > p; p++)
                if (r = t[p],
                r || 0 === r)
                    if ("object" === K.type(r))
                        K.merge(h, r.nodeType ? [r] : r);
                    else if (Dt.test(r)) {
                        for (o = o || c.appendChild(e.createElement("div")),
                        s = (Mt.exec(r) || ["", ""])[1].toLowerCase(),
                        a = It[s] || It._default,
                        o.innerHTML = a[1] + r.replace(Ot, "<$1></$2>") + a[2],
                        u = a[0]; u--; )
                            o = o.lastChild;
                        K.merge(h, o.childNodes),
                        o = c.firstChild,
                        o.textContent = ""
                    } else
                        h.push(e.createTextNode(r));
            for (c.textContent = "",
            p = 0; r = h[p++]; )
                if ((!i || -1 === K.inArray(r, i)) && (l = K.contains(r.ownerDocument, r),
                o = v(c.appendChild(r), "script"),
                l && m(o),
                n))
                    for (u = 0; r = o[u++]; )
                        Lt.test(r.type || "") && n.push(r);
            return c
        },
        cleanData: function(t) {
            for (var e, n, i, r, o = K.event.special, s = 0; void 0 !== (n = t[s]); s++) {
                if (K.acceptData(n) && (r = n[vt.expando],
                r && (e = vt.cache[r]))) {
                    if (e.events)
                        for (i in e.events)
                            o[i] ? K.event.remove(n, i) : K.removeEvent(n, i, e.handle);
                    vt.cache[r] && delete vt.cache[r]
                }
                delete yt.cache[n[yt.expando]]
            }
        }
    }),
    K.fn.extend({
        text: function(t) {
            return gt(this, function(t) {
                return void 0 === t ? K.text(this) : this.empty().each(function() {
                    (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) && (this.textContent = t)
                })
            }, null, t, arguments.length)
        },
        append: function() {
            return this.domManip(arguments, function(t) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var e = p(this, t);
                    e.appendChild(t)
                }
            })
        },
        prepend: function() {
            return this.domManip(arguments, function(t) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var e = p(this, t);
                    e.insertBefore(t, e.firstChild)
                }
            })
        },
        before: function() {
            return this.domManip(arguments, function(t) {
                this.parentNode && this.parentNode.insertBefore(t, this)
            })
        },
        after: function() {
            return this.domManip(arguments, function(t) {
                this.parentNode && this.parentNode.insertBefore(t, this.nextSibling)
            })
        },
        remove: function(t, e) {
            for (var n, i = t ? K.filter(t, this) : this, r = 0; null != (n = i[r]); r++)
                e || 1 !== n.nodeType || K.cleanData(v(n)),
                n.parentNode && (e && K.contains(n.ownerDocument, n) && m(v(n, "script")),
                n.parentNode.removeChild(n));
            return this
        },
        empty: function() {
            for (var t, e = 0; null != (t = this[e]); e++)
                1 === t.nodeType && (K.cleanData(v(t, !1)),
                t.textContent = "");
            return this
        },
        clone: function(t, e) {
            return t = null == t ? !1 : t,
            e = null == e ? t : e,
            this.map(function() {
                return K.clone(this, t, e)
            })
        },
        html: function(t) {
            return gt(this, function(t) {
                var e = this[0] || {}
                  , n = 0
                  , i = this.length;
                if (void 0 === t && 1 === e.nodeType)
                    return e.innerHTML;
                if ("string" == typeof t && !At.test(t) && !It[(Mt.exec(t) || ["", ""])[1].toLowerCase()]) {
                    t = t.replace(Ot, "<$1></$2>");
                    try {
                        for (; i > n; n++)
                            e = this[n] || {},
                            1 === e.nodeType && (K.cleanData(v(e, !1)),
                            e.innerHTML = t);
                        e = 0
                    } catch (r) {}
                }
                e && this.empty().append(t)
            }, null, t, arguments.length)
        },
        replaceWith: function() {
            var t = arguments[0];
            return this.domManip(arguments, function(e) {
                t = this.parentNode,
                K.cleanData(v(this)),
                t && t.replaceChild(e, this)
            }),
            t && (t.length || t.nodeType) ? this : this.remove()
        },
        detach: function(t) {
            return this.remove(t, !0)
        },
        domManip: function(t, e) {
            t = W.apply([], t);
            var n, i, r, o, s, a, l = 0, u = this.length, c = this, h = u - 1, p = t[0], m = K.isFunction(p);
            if (m || u > 1 && "string" == typeof p && !Q.checkClone && Nt.test(p))
                return this.each(function(n) {
                    var i = c.eq(n);
                    m && (t[0] = p.call(this, n, i.html())),
                    i.domManip(t, e)
                });
            if (u && (n = K.buildFragment(t, this[0].ownerDocument, !1, this),
            i = n.firstChild,
            1 === n.childNodes.length && (n = i),
            i)) {
                for (r = K.map(v(n, "script"), f),
                o = r.length; u > l; l++)
                    s = n,
                    l !== h && (s = K.clone(s, !0, !0),
                    o && K.merge(r, v(s, "script"))),
                    e.call(this[l], s, l);
                if (o)
                    for (a = r[r.length - 1].ownerDocument,
                    K.map(r, d),
                    l = 0; o > l; l++)
                        s = r[l],
                        Lt.test(s.type || "") && !vt.access(s, "globalEval") && K.contains(a, s) && (s.src ? K._evalUrl && K._evalUrl(s.src) : K.globalEval(s.textContent.replace($t, "")))
            }
            return this
        }
    }),
    K.each({
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, function(t, e) {
        K.fn[t] = function(t) {
            for (var n, i = [], r = K(t), o = r.length - 1, s = 0; o >= s; s++)
                n = s === o ? this : this.clone(!0),
                K(r[s])[e](n),
                X.apply(i, n.get());
            return this.pushStack(i)
        }
    });
    var qt, Ht = {}, Ft = /^margin/, Bt = new RegExp("^(" + wt + ")(?!px)[a-z%]+$","i"), zt = function(e) {
        return e.ownerDocument.defaultView.opener ? e.ownerDocument.defaultView.getComputedStyle(e, null) : t.getComputedStyle(e, null)
    };
    !function() {
        function e() {
            s.style.cssText = "-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;display:block;margin-top:1%;top:1%;border:1px;padding:1px;width:4px;position:absolute",
            s.innerHTML = "",
            r.appendChild(o);
            var e = t.getComputedStyle(s, null);
            n = "1%" !== e.top,
            i = "4px" === e.width,
            r.removeChild(o)
        }
        var n, i, r = J.documentElement, o = J.createElement("div"), s = J.createElement("div");
        s.style && (s.style.backgroundClip = "content-box",
        s.cloneNode(!0).style.backgroundClip = "",
        Q.clearCloneStyle = "content-box" === s.style.backgroundClip,
        o.style.cssText = "border:0;width:0;height:0;top:0;left:-9999px;margin-top:1px;position:absolute",
        o.appendChild(s),
        t.getComputedStyle && K.extend(Q, {
            pixelPosition: function() {
                return e(),
                n
            },
            boxSizingReliable: function() {
                return null == i && e(),
                i
            },
            reliableMarginRight: function() {
                var e, n = s.appendChild(J.createElement("div"));
                return n.style.cssText = s.style.cssText = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0",
                n.style.marginRight = n.style.width = "0",
                s.style.width = "1px",
                r.appendChild(o),
                e = !parseFloat(t.getComputedStyle(n, null).marginRight),
                r.removeChild(o),
                s.removeChild(n),
                e
            }
        }))
    }(),
    K.swap = function(t, e, n, i) {
        var r, o, s = {};
        for (o in e)
            s[o] = t.style[o],
            t.style[o] = e[o];
        r = n.apply(t, i || []);
        for (o in e)
            t.style[o] = s[o];
        return r
    }
    ;
    var Wt = /^(none|table(?!-c[ea]).+)/
      , Xt = new RegExp("^(" + wt + ")(.*)$","i")
      , Vt = new RegExp("^([+-])=(" + wt + ")","i")
      , Yt = {
        position: "absolute",
        visibility: "hidden",
        display: "block"
    }
      , Ut = {
        letterSpacing: "0",
        fontWeight: "400"
    }
      , Gt = ["Webkit", "O", "Moz", "ms"];
    K.extend({
        cssHooks: {
            opacity: {
                get: function(t, e) {
                    if (e) {
                        var n = w(t, "opacity");
                        return "" === n ? "1" : n
                    }
                }
            }
        },
        cssNumber: {
            columnCount: !0,
            fillOpacity: !0,
            flexGrow: !0,
            flexShrink: !0,
            fontWeight: !0,
            lineHeight: !0,
            opacity: !0,
            order: !0,
            orphans: !0,
            widows: !0,
            zIndex: !0,
            zoom: !0
        },
        cssProps: {
            "float": "cssFloat"
        },
        style: function(t, e, n, i) {
            if (t && 3 !== t.nodeType && 8 !== t.nodeType && t.style) {
                var r, o, s, a = K.camelCase(e), l = t.style;
                return e = K.cssProps[a] || (K.cssProps[a] = T(l, a)),
                s = K.cssHooks[e] || K.cssHooks[a],
                void 0 === n ? s && "get"in s && void 0 !== (r = s.get(t, !1, i)) ? r : l[e] : (o = typeof n,
                "string" === o && (r = Vt.exec(n)) && (n = (r[1] + 1) * r[2] + parseFloat(K.css(t, e)),
                o = "number"),
                null != n && n === n && ("number" !== o || K.cssNumber[a] || (n += "px"),
                Q.clearCloneStyle || "" !== n || 0 !== e.indexOf("background") || (l[e] = "inherit"),
                s && "set"in s && void 0 === (n = s.set(t, n, i)) || (l[e] = n)),
                void 0)
            }
        },
        css: function(t, e, n, i) {
            var r, o, s, a = K.camelCase(e);
            return e = K.cssProps[a] || (K.cssProps[a] = T(t.style, a)),
            s = K.cssHooks[e] || K.cssHooks[a],
            s && "get"in s && (r = s.get(t, !0, n)),
            void 0 === r && (r = w(t, e, i)),
            "normal" === r && e in Ut && (r = Ut[e]),
            "" === n || n ? (o = parseFloat(r),
            n === !0 || K.isNumeric(o) ? o || 0 : r) : r
        }
    }),
    K.each(["height", "width"], function(t, e) {
        K.cssHooks[e] = {
            get: function(t, n, i) {
                return n ? Wt.test(K.css(t, "display")) && 0 === t.offsetWidth ? K.swap(t, Yt, function() {
                    return k(t, e, i)
                }) : k(t, e, i) : void 0
            },
            set: function(t, n, i) {
                var r = i && zt(t);
                return C(t, n, i ? S(t, e, i, "border-box" === K.css(t, "boxSizing", !1, r), r) : 0)
            }
        }
    }),
    K.cssHooks.marginRight = b(Q.reliableMarginRight, function(t, e) {
        return e ? K.swap(t, {
            display: "inline-block"
        }, w, [t, "marginRight"]) : void 0
    }),
    K.each({
        margin: "",
        padding: "",
        border: "Width"
    }, function(t, e) {
        K.cssHooks[t + e] = {
            expand: function(n) {
                for (var i = 0, r = {}, o = "string" == typeof n ? n.split(" ") : [n]; 4 > i; i++)
                    r[t + bt[i] + e] = o[i] || o[i - 2] || o[0];
                return r
            }
        },
        Ft.test(t) || (K.cssHooks[t + e].set = C)
    }),
    K.fn.extend({
        css: function(t, e) {
            return gt(this, function(t, e, n) {
                var i, r, o = {}, s = 0;
                if (K.isArray(e)) {
                    for (i = zt(t),
                    r = e.length; r > s; s++)
                        o[e[s]] = K.css(t, e[s], !1, i);
                    return o
                }
                return void 0 !== n ? K.style(t, e, n) : K.css(t, e)
            }, t, e, arguments.length > 1)
        },
        show: function() {
            return j(this, !0)
        },
        hide: function() {
            return j(this)
        },
        toggle: function(t) {
            return "boolean" == typeof t ? t ? this.show() : this.hide() : this.each(function() {
                Tt(this) ? K(this).show() : K(this).hide()
            })
        }
    }),
    K.Tween = P,
    P.prototype = {
        constructor: P,
        init: function(t, e, n, i, r, o) {
            this.elem = t,
            this.prop = n,
            this.easing = r || "swing",
            this.options = e,
            this.start = this.now = this.cur(),
            this.end = i,
            this.unit = o || (K.cssNumber[n] ? "" : "px")
        },
        cur: function() {
            var t = P.propHooks[this.prop];
            return t && t.get ? t.get(this) : P.propHooks._default.get(this)
        },
        run: function(t) {
            var e, n = P.propHooks[this.prop];
            return this.options.duration ? this.pos = e = K.easing[this.easing](t, this.options.duration * t, 0, 1, this.options.duration) : this.pos = e = t,
            this.now = (this.end - this.start) * e + this.start,
            this.options.step && this.options.step.call(this.elem, this.now, this),
            n && n.set ? n.set(this) : P.propHooks._default.set(this),
            this
        }
    },
    P.prototype.init.prototype = P.prototype,
    P.propHooks = {
        _default: {
            get: function(t) {
                var e;
                return null == t.elem[t.prop] || t.elem.style && null != t.elem.style[t.prop] ? (e = K.css(t.elem, t.prop, ""),
                e && "auto" !== e ? e : 0) : t.elem[t.prop]
            },
            set: function(t) {
                K.fx.step[t.prop] ? K.fx.step[t.prop](t) : t.elem.style && (null != t.elem.style[K.cssProps[t.prop]] || K.cssHooks[t.prop]) ? K.style(t.elem, t.prop, t.now + t.unit) : t.elem[t.prop] = t.now
            }
        }
    },
    P.propHooks.scrollTop = P.propHooks.scrollLeft = {
        set: function(t) {
            t.elem.nodeType && t.elem.parentNode && (t.elem[t.prop] = t.now)
        }
    },
    K.easing = {
        linear: function(t) {
            return t
        },
        swing: function(t) {
            return .5 - Math.cos(t * Math.PI) / 2
        }
    },
    K.fx = P.prototype.init,
    K.fx.step = {};
    var Qt, Jt, Zt = /^(?:toggle|show|hide)$/, Kt = new RegExp("^(?:([+-])=|)(" + wt + ")([a-z%]*)$","i"), te = /queueHooks$/, ee = [D], ne = {
        "*": [function(t, e) {
            var n = this.createTween(t, e)
              , i = n.cur()
              , r = Kt.exec(e)
              , o = r && r[3] || (K.cssNumber[t] ? "" : "px")
              , s = (K.cssNumber[t] || "px" !== o && +i) && Kt.exec(K.css(n.elem, t))
              , a = 1
              , l = 20;
            if (s && s[3] !== o) {
                o = o || s[3],
                r = r || [],
                s = +i || 1;
                do
                    a = a || ".5",
                    s /= a,
                    K.style(n.elem, t, s + o);
                while (a !== (a = n.cur() / i) && 1 !== a && --l)
            }
            return r && (s = n.start = +s || +i || 0,
            n.unit = o,
            n.end = r[1] ? s + (r[1] + 1) * r[2] : +r[2]),
            n
        }
        ]
    };
    K.Animation = K.extend(N, {
        tweener: function(t, e) {
            K.isFunction(t) ? (e = t,
            t = ["*"]) : t = t.split(" ");
            for (var n, i = 0, r = t.length; r > i; i++)
                n = t[i],
                ne[n] = ne[n] || [],
                ne[n].unshift(e)
        },
        prefilter: function(t, e) {
            e ? ee.unshift(t) : ee.push(t)
        }
    }),
    K.speed = function(t, e, n) {
        var i = t && "object" == typeof t ? K.extend({}, t) : {
            complete: n || !n && e || K.isFunction(t) && t,
            duration: t,
            easing: n && e || e && !K.isFunction(e) && e
        };
        return i.duration = K.fx.off ? 0 : "number" == typeof i.duration ? i.duration : i.duration in K.fx.speeds ? K.fx.speeds[i.duration] : K.fx.speeds._default,
        (null == i.queue || i.queue === !0) && (i.queue = "fx"),
        i.old = i.complete,
        i.complete = function() {
            K.isFunction(i.old) && i.old.call(this),
            i.queue && K.dequeue(this, i.queue)
        }
        ,
        i
    }
    ,
    K.fn.extend({
        fadeTo: function(t, e, n, i) {
            return this.filter(Tt).css("opacity", 0).show().end().animate({
                opacity: e
            }, t, n, i)
        },
        animate: function(t, e, n, i) {
            var r = K.isEmptyObject(t)
              , o = K.speed(e, n, i)
              , s = function() {
                var e = N(this, K.extend({}, t), o);
                (r || vt.get(this, "finish")) && e.stop(!0)
            };
            return s.finish = s,
            r || o.queue === !1 ? this.each(s) : this.queue(o.queue, s)
        },
        stop: function(t, e, n) {
            var i = function(t) {
                var e = t.stop;
                delete t.stop,
                e(n)
            };
            return "string" != typeof t && (n = e,
            e = t,
            t = void 0),
            e && t !== !1 && this.queue(t || "fx", []),
            this.each(function() {
                var e = !0
                  , r = null != t && t + "queueHooks"
                  , o = K.timers
                  , s = vt.get(this);
                if (r)
                    s[r] && s[r].stop && i(s[r]);
                else
                    for (r in s)
                        s[r] && s[r].stop && te.test(r) && i(s[r]);
                for (r = o.length; r--; )
                    o[r].elem !== this || null != t && o[r].queue !== t || (o[r].anim.stop(n),
                    e = !1,
                    o.splice(r, 1));
                (e || !n) && K.dequeue(this, t)
            })
        },
        finish: function(t) {
            return t !== !1 && (t = t || "fx"),
            this.each(function() {
                var e, n = vt.get(this), i = n[t + "queue"], r = n[t + "queueHooks"], o = K.timers, s = i ? i.length : 0;
                for (n.finish = !0,
                K.queue(this, t, []),
                r && r.stop && r.stop.call(this, !0),
                e = o.length; e--; )
                    o[e].elem === this && o[e].queue === t && (o[e].anim.stop(!0),
                    o.splice(e, 1));
                for (e = 0; s > e; e++)
                    i[e] && i[e].finish && i[e].finish.call(this);
                delete n.finish
            })
        }
    }),
    K.each(["toggle", "show", "hide"], function(t, e) {
        var n = K.fn[e];
        K.fn[e] = function(t, i, r) {
            return null == t || "boolean" == typeof t ? n.apply(this, arguments) : this.animate(O(e, !0), t, i, r)
        }
    }),
    K.each({
        slideDown: O("show"),
        slideUp: O("hide"),
        slideToggle: O("toggle"),
        fadeIn: {
            opacity: "show"
        },
        fadeOut: {
            opacity: "hide"
        },
        fadeToggle: {
            opacity: "toggle"
        }
    }, function(t, e) {
        K.fn[t] = function(t, n, i) {
            return this.animate(e, t, n, i)
        }
    }),
    K.timers = [],
    K.fx.tick = function() {
        var t, e = 0, n = K.timers;
        for (Qt = K.now(); e < n.length; e++)
            t = n[e],
            t() || n[e] !== t || n.splice(e--, 1);
        n.length || K.fx.stop(),
        Qt = void 0
    }
    ,
    K.fx.timer = function(t) {
        K.timers.push(t),
        t() ? K.fx.start() : K.timers.pop()
    }
    ,
    K.fx.interval = 13,
    K.fx.start = function() {
        Jt || (Jt = setInterval(K.fx.tick, K.fx.interval))
    }
    ,
    K.fx.stop = function() {
        clearInterval(Jt),
        Jt = null
    }
    ,
    K.fx.speeds = {
        slow: 600,
        fast: 200,
        _default: 400
    },
    K.fn.delay = function(t, e) {
        return t = K.fx ? K.fx.speeds[t] || t : t,
        e = e || "fx",
        this.queue(e, function(e, n) {
            var i = setTimeout(e, t);
            n.stop = function() {
                clearTimeout(i)
            }
        })
    }
    ,
    function() {
        var t = J.createElement("input")
          , e = J.createElement("select")
          , n = e.appendChild(J.createElement("option"));
        t.type = "checkbox",
        Q.checkOn = "" !== t.value,
        Q.optSelected = n.selected,
        e.disabled = !0,
        Q.optDisabled = !n.disabled,
        t = J.createElement("input"),
        t.value = "t",
        t.type = "radio",
        Q.radioValue = "t" === t.value
    }();
    var ie, re, oe = K.expr.attrHandle;
    K.fn.extend({
        attr: function(t, e) {
            return gt(this, K.attr, t, e, arguments.length > 1)
        },
        removeAttr: function(t) {
            return this.each(function() {
                K.removeAttr(this, t)
            })
        }
    }),
    K.extend({
        attr: function(t, e, n) {
            var i, r, o = t.nodeType;
            if (t && 3 !== o && 8 !== o && 2 !== o)
                return typeof t.getAttribute === St ? K.prop(t, e, n) : (1 === o && K.isXMLDoc(t) || (e = e.toLowerCase(),
                i = K.attrHooks[e] || (K.expr.match.bool.test(e) ? re : ie)),
                void 0 === n ? i && "get"in i && null !== (r = i.get(t, e)) ? r : (r = K.find.attr(t, e),
                null == r ? void 0 : r) : null !== n ? i && "set"in i && void 0 !== (r = i.set(t, n, e)) ? r : (t.setAttribute(e, n + ""),
                n) : void K.removeAttr(t, e))
        },
        removeAttr: function(t, e) {
            var n, i, r = 0, o = e && e.match(ft);
            if (o && 1 === t.nodeType)
                for (; n = o[r++]; )
                    i = K.propFix[n] || n,
                    K.expr.match.bool.test(n) && (t[i] = !1),
                    t.removeAttribute(n)
        },
        attrHooks: {
            type: {
                set: function(t, e) {
                    if (!Q.radioValue && "radio" === e && K.nodeName(t, "input")) {
                        var n = t.value;
                        return t.setAttribute("type", e),
                        n && (t.value = n),
                        e
                    }
                }
            }
        }
    }),
    re = {
        set: function(t, e, n) {
            return e === !1 ? K.removeAttr(t, n) : t.setAttribute(n, n),
            n
        }
    },
    K.each(K.expr.match.bool.source.match(/\w+/g), function(t, e) {
        var n = oe[e] || K.find.attr;
        oe[e] = function(t, e, i) {
            var r, o;
            return i || (o = oe[e],
            oe[e] = r,
            r = null != n(t, e, i) ? e.toLowerCase() : null,
            oe[e] = o),
            r
        }
    });
    var se = /^(?:input|select|textarea|button)$/i;
    K.fn.extend({
        prop: function(t, e) {
            return gt(this, K.prop, t, e, arguments.length > 1)
        },
        removeProp: function(t) {
            return this.each(function() {
                delete this[K.propFix[t] || t]
            })
        }
    }),
    K.extend({
        propFix: {
            "for": "htmlFor",
            "class": "className"
        },
        prop: function(t, e, n) {
            var i, r, o, s = t.nodeType;
            if (t && 3 !== s && 8 !== s && 2 !== s)
                return o = 1 !== s || !K.isXMLDoc(t),
                o && (e = K.propFix[e] || e,
                r = K.propHooks[e]),
                void 0 !== n ? r && "set"in r && void 0 !== (i = r.set(t, n, e)) ? i : t[e] = n : r && "get"in r && null !== (i = r.get(t, e)) ? i : t[e]
        },
        propHooks: {
            tabIndex: {
                get: function(t) {
                    return t.hasAttribute("tabindex") || se.test(t.nodeName) || t.href ? t.tabIndex : -1
                }
            }
        }
    }),
    Q.optSelected || (K.propHooks.selected = {
        get: function(t) {
            var e = t.parentNode;
            return e && e.parentNode && e.parentNode.selectedIndex,
            null
        }
    }),
    K.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function() {
        K.propFix[this.toLowerCase()] = this
    });
    var ae = /[\t\r\n\f]/g;
    K.fn.extend({
        addClass: function(t) {
            var e, n, i, r, o, s, a = "string" == typeof t && t, l = 0, u = this.length;
            if (K.isFunction(t))
                return this.each(function(e) {
                    K(this).addClass(t.call(this, e, this.className))
                });
            if (a)
                for (e = (t || "").match(ft) || []; u > l; l++)
                    if (n = this[l],
                    i = 1 === n.nodeType && (n.className ? (" " + n.className + " ").replace(ae, " ") : " ")) {
                        for (o = 0; r = e[o++]; )
                            i.indexOf(" " + r + " ") < 0 && (i += r + " ");
                        s = K.trim(i),
                        n.className !== s && (n.className = s)
                    }
            return this
        },
        removeClass: function(t) {
            var e, n, i, r, o, s, a = 0 === arguments.length || "string" == typeof t && t, l = 0, u = this.length;
            if (K.isFunction(t))
                return this.each(function(e) {
                    K(this).removeClass(t.call(this, e, this.className))
                });
            if (a)
                for (e = (t || "").match(ft) || []; u > l; l++)
                    if (n = this[l],
                    i = 1 === n.nodeType && (n.className ? (" " + n.className + " ").replace(ae, " ") : "")) {
                        for (o = 0; r = e[o++]; )
                            for (; i.indexOf(" " + r + " ") >= 0; )
                                i = i.replace(" " + r + " ", " ");
                        s = t ? K.trim(i) : "",
                        n.className !== s && (n.className = s)
                    }
            return this
        },
        toggleClass: function(t, e) {
            var n = typeof t;
            return "boolean" == typeof e && "string" === n ? e ? this.addClass(t) : this.removeClass(t) : K.isFunction(t) ? this.each(function(n) {
                K(this).toggleClass(t.call(this, n, this.className, e), e)
            }) : this.each(function() {
                if ("string" === n)
                    for (var e, i = 0, r = K(this), o = t.match(ft) || []; e = o[i++]; )
                        r.hasClass(e) ? r.removeClass(e) : r.addClass(e);
                else
                    (n === St || "boolean" === n) && (this.className && vt.set(this, "__className__", this.className),
                    this.className = this.className || t === !1 ? "" : vt.get(this, "__className__") || "")
            })
        },
        hasClass: function(t) {
            for (var e = " " + t + " ", n = 0, i = this.length; i > n; n++)
                if (1 === this[n].nodeType && (" " + this[n].className + " ").replace(ae, " ").indexOf(e) >= 0)
                    return !0;
            return !1
        }
    });
    var le = /\r/g;
    K.fn.extend({
        val: function(t) {
            var e, n, i, r = this[0];
            {
                if (arguments.length)
                    return i = K.isFunction(t),
                    this.each(function(n) {
                        var r;
                        1 === this.nodeType && (r = i ? t.call(this, n, K(this).val()) : t,
                        null == r ? r = "" : "number" == typeof r ? r += "" : K.isArray(r) && (r = K.map(r, function(t) {
                            return null == t ? "" : t + ""
                        })),
                        e = K.valHooks[this.type] || K.valHooks[this.nodeName.toLowerCase()],
                        e && "set"in e && void 0 !== e.set(this, r, "value") || (this.value = r))
                    });
                if (r)
                    return e = K.valHooks[r.type] || K.valHooks[r.nodeName.toLowerCase()],
                    e && "get"in e && void 0 !== (n = e.get(r, "value")) ? n : (n = r.value,
                    "string" == typeof n ? n.replace(le, "") : null == n ? "" : n)
            }
        }
    }),
    K.extend({
        valHooks: {
            option: {
                get: function(t) {
                    var e = K.find.attr(t, "value");
                    return null != e ? e : K.trim(K.text(t))
                }
            },
            select: {
                get: function(t) {
                    for (var e, n, i = t.options, r = t.selectedIndex, o = "select-one" === t.type || 0 > r, s = o ? null : [], a = o ? r + 1 : i.length, l = 0 > r ? a : o ? r : 0; a > l; l++)
                        if (n = i[l],
                        !(!n.selected && l !== r || (Q.optDisabled ? n.disabled : null !== n.getAttribute("disabled")) || n.parentNode.disabled && K.nodeName(n.parentNode, "optgroup"))) {
                            if (e = K(n).val(),
                            o)
                                return e;
                            s.push(e)
                        }
                    return s
                },
                set: function(t, e) {
                    for (var n, i, r = t.options, o = K.makeArray(e), s = r.length; s--; )
                        i = r[s],
                        (i.selected = K.inArray(i.value, o) >= 0) && (n = !0);
                    return n || (t.selectedIndex = -1),
                    o
                }
            }
        }
    }),
    K.each(["radio", "checkbox"], function() {
        K.valHooks[this] = {
            set: function(t, e) {
                return K.isArray(e) ? t.checked = K.inArray(K(t).val(), e) >= 0 : void 0
            }
        },
        Q.checkOn || (K.valHooks[this].get = function(t) {
            return null === t.getAttribute("value") ? "on" : t.value
        }
        )
    }),
    K.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "), function(t, e) {
        K.fn[e] = function(t, n) {
            return arguments.length > 0 ? this.on(e, null, t, n) : this.trigger(e)
        }
    }),
    K.fn.extend({
        hover: function(t, e) {
            return this.mouseenter(t).mouseleave(e || t)
        },
        bind: function(t, e, n) {
            return this.on(t, null, e, n)
        },
        unbind: function(t, e) {
            return this.off(t, null, e)
        },
        delegate: function(t, e, n, i) {
            return this.on(e, t, n, i)
        },
        undelegate: function(t, e, n) {
            return 1 === arguments.length ? this.off(t, "**") : this.off(e, t || "**", n)
        }
    });
    var ue = K.now()
      , ce = /\?/;
    K.parseJSON = function(t) {
        return JSON.parse(t + "")
    }
    ,
    K.parseXML = function(t) {
        var e, n;
        if (!t || "string" != typeof t)
            return null;
        try {
            n = new DOMParser,
            e = n.parseFromString(t, "text/xml")
        } catch (i) {
            e = void 0
        }
        return (!e || e.getElementsByTagName("parsererror").length) && K.error("Invalid XML: " + t),
        e
    }
    ;
    var he = /#.*$/
      , pe = /([?&])_=[^&]*/
      , fe = /^(.*?):[ \t]*([^\r\n]*)$/gm
      , de = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/
      , me = /^(?:GET|HEAD)$/
      , ge = /^\/\//
      , ve = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/
      , ye = {}
      , _e = {}
      , xe = "*/".concat("*")
      , we = t.location.href
      , be = ve.exec(we.toLowerCase()) || [];
    K.extend({
        active: 0,
        lastModified: {},
        etag: {},
        ajaxSettings: {
            url: we,
            type: "GET",
            isLocal: de.test(be[1]),
            global: !0,
            processData: !0,
            async: !0,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            accepts: {
                "*": xe,
                text: "text/plain",
                html: "text/html",
                xml: "application/xml, text/xml",
                json: "application/json, text/javascript"
            },
            contents: {
                xml: /xml/,
                html: /html/,
                json: /json/
            },
            responseFields: {
                xml: "responseXML",
                text: "responseText",
                json: "responseJSON"
            },
            converters: {
                "* text": String,
                "text html": !0,
                "text json": K.parseJSON,
                "text xml": K.parseXML
            },
            flatOptions: {
                url: !0,
                context: !0
            }
        },
        ajaxSetup: function(t, e) {
            return e ? $($(t, K.ajaxSettings), e) : $(K.ajaxSettings, t)
        },
        ajaxPrefilter: L(ye),
        ajaxTransport: L(_e),
        ajax: function(t, e) {
            function n(t, e, n, s) {
                var l, c, v, y, x, b = e;
                2 !== _ && (_ = 2,
                a && clearTimeout(a),
                i = void 0,
                o = s || "",
                w.readyState = t > 0 ? 4 : 0,
                l = t >= 200 && 300 > t || 304 === t,
                n && (y = I(h, w, n)),
                y = q(h, y, w, l),
                l ? (h.ifModified && (x = w.getResponseHeader("Last-Modified"),
                x && (K.lastModified[r] = x),
                x = w.getResponseHeader("etag"),
                x && (K.etag[r] = x)),
                204 === t || "HEAD" === h.type ? b = "nocontent" : 304 === t ? b = "notmodified" : (b = y.state,
                c = y.data,
                v = y.error,
                l = !v)) : (v = b,
                (t || !b) && (b = "error",
                0 > t && (t = 0))),
                w.status = t,
                w.statusText = (e || b) + "",
                l ? d.resolveWith(p, [c, b, w]) : d.rejectWith(p, [w, b, v]),
                w.statusCode(g),
                g = void 0,
                u && f.trigger(l ? "ajaxSuccess" : "ajaxError", [w, h, l ? c : v]),
                m.fireWith(p, [w, b]),
                u && (f.trigger("ajaxComplete", [w, h]),
                --K.active || K.event.trigger("ajaxStop")))
            }
            "object" == typeof t && (e = t,
            t = void 0),
            e = e || {};
            var i, r, o, s, a, l, u, c, h = K.ajaxSetup({}, e), p = h.context || h, f = h.context && (p.nodeType || p.jquery) ? K(p) : K.event, d = K.Deferred(), m = K.Callbacks("once memory"), g = h.statusCode || {}, v = {}, y = {}, _ = 0, x = "canceled", w = {
                readyState: 0,
                getResponseHeader: function(t) {
                    var e;
                    if (2 === _) {
                        if (!s)
                            for (s = {}; e = fe.exec(o); )
                                s[e[1].toLowerCase()] = e[2];
                        e = s[t.toLowerCase()]
                    }
                    return null == e ? null : e
                },
                getAllResponseHeaders: function() {
                    return 2 === _ ? o : null
                },
                setRequestHeader: function(t, e) {
                    var n = t.toLowerCase();
                    return _ || (t = y[n] = y[n] || t,
                    v[t] = e),
                    this
                },
                overrideMimeType: function(t) {
                    return _ || (h.mimeType = t),
                    this
                },
                statusCode: function(t) {
                    var e;
                    if (t)
                        if (2 > _)
                            for (e in t)
                                g[e] = [g[e], t[e]];
                        else
                            w.always(t[w.status]);
                    return this
                },
                abort: function(t) {
                    var e = t || x;
                    return i && i.abort(e),
                    n(0, e),
                    this
                }
            };
            if (d.promise(w).complete = m.add,
            w.success = w.done,
            w.error = w.fail,
            h.url = ((t || h.url || we) + "").replace(he, "").replace(ge, be[1] + "//"),
            h.type = e.method || e.type || h.method || h.type,
            h.dataTypes = K.trim(h.dataType || "*").toLowerCase().match(ft) || [""],
            null == h.crossDomain && (l = ve.exec(h.url.toLowerCase()),
            h.crossDomain = !(!l || l[1] === be[1] && l[2] === be[2] && (l[3] || ("http:" === l[1] ? "80" : "443")) === (be[3] || ("http:" === be[1] ? "80" : "443")))),
            h.data && h.processData && "string" != typeof h.data && (h.data = K.param(h.data, h.traditional)),
            R(ye, h, e, w),
            2 === _)
                return w;
            u = K.event && h.global,
            u && 0 === K.active++ && K.event.trigger("ajaxStart"),
            h.type = h.type.toUpperCase(),
            h.hasContent = !me.test(h.type),
            r = h.url,
            h.hasContent || (h.data && (r = h.url += (ce.test(r) ? "&" : "?") + h.data,
            delete h.data),
            h.cache === !1 && (h.url = pe.test(r) ? r.replace(pe, "$1_=" + ue++) : r + (ce.test(r) ? "&" : "?") + "_=" + ue++)),
            h.ifModified && (K.lastModified[r] && w.setRequestHeader("If-Modified-Since", K.lastModified[r]),
            K.etag[r] && w.setRequestHeader("If-None-Match", K.etag[r])),
            (h.data && h.hasContent && h.contentType !== !1 || e.contentType) && w.setRequestHeader("Content-Type", h.contentType),
            w.setRequestHeader("Accept", h.dataTypes[0] && h.accepts[h.dataTypes[0]] ? h.accepts[h.dataTypes[0]] + ("*" !== h.dataTypes[0] ? ", " + xe + "; q=0.01" : "") : h.accepts["*"]);
            for (c in h.headers)
                w.setRequestHeader(c, h.headers[c]);
            if (h.beforeSend && (h.beforeSend.call(p, w, h) === !1 || 2 === _))
                return w.abort();
            x = "abort";
            for (c in {
                success: 1,
                error: 1,
                complete: 1
            })
                w[c](h[c]);
            if (i = R(_e, h, e, w)) {
                w.readyState = 1,
                u && f.trigger("ajaxSend", [w, h]),
                h.async && h.timeout > 0 && (a = setTimeout(function() {
                    w.abort("timeout")
                }, h.timeout));
                try {
                    _ = 1,
                    i.send(v, n)
                } catch (b) {
                    if (!(2 > _))
                        throw b;
                    n(-1, b)
                }
            } else
                n(-1, "No Transport");
            return w
        },
        getJSON: function(t, e, n) {
            return K.get(t, e, n, "json")
        },
        getScript: function(t, e) {
            return K.get(t, void 0, e, "script")
        }
    }),
    K.each(["get", "post"], function(t, e) {
        K[e] = function(t, n, i, r) {
            return K.isFunction(n) && (r = r || i,
            i = n,
            n = void 0),
            K.ajax({
                url: t,
                type: e,
                dataType: r,
                data: n,
                success: i
            })
        }
    }),
    K._evalUrl = function(t) {
        return K.ajax({
            url: t,
            type: "GET",
            dataType: "script",
            async: !1,
            global: !1,
            "throws": !0
        })
    }
    ,
    K.fn.extend({
        wrapAll: function(t) {
            var e;
            return K.isFunction(t) ? this.each(function(e) {
                K(this).wrapAll(t.call(this, e))
            }) : (this[0] && (e = K(t, this[0].ownerDocument).eq(0).clone(!0),
            this[0].parentNode && e.insertBefore(this[0]),
            e.map(function() {
                for (var t = this; t.firstElementChild; )
                    t = t.firstElementChild;
                return t
            }).append(this)),
            this)
        },
        wrapInner: function(t) {
            return K.isFunction(t) ? this.each(function(e) {
                K(this).wrapInner(t.call(this, e))
            }) : this.each(function() {
                var e = K(this)
                  , n = e.contents();
                n.length ? n.wrapAll(t) : e.append(t)
            })
        },
        wrap: function(t) {
            var e = K.isFunction(t);
            return this.each(function(n) {
                K(this).wrapAll(e ? t.call(this, n) : t)
            })
        },
        unwrap: function() {
            return this.parent().each(function() {
                K.nodeName(this, "body") || K(this).replaceWith(this.childNodes)
            }).end()
        }
    }),
    K.expr.filters.hidden = function(t) {
        return t.offsetWidth <= 0 && t.offsetHeight <= 0
    }
    ,
    K.expr.filters.visible = function(t) {
        return !K.expr.filters.hidden(t)
    }
    ;
    var Te = /%20/g
      , Ce = /\[\]$/
      , Se = /\r?\n/g
      , ke = /^(?:submit|button|image|reset|file)$/i
      , je = /^(?:input|select|textarea|keygen)/i;
    K.param = function(t, e) {
        var n, i = [], r = function(t, e) {
            e = K.isFunction(e) ? e() : null == e ? "" : e,
            i[i.length] = encodeURIComponent(t) + "=" + encodeURIComponent(e)
        };
        if (void 0 === e && (e = K.ajaxSettings && K.ajaxSettings.traditional),
        K.isArray(t) || t.jquery && !K.isPlainObject(t))
            K.each(t, function() {
                r(this.name, this.value)
            });
        else
            for (n in t)
                H(n, t[n], e, r);
        return i.join("&").replace(Te, "+")
    }
    ,
    K.fn.extend({
        serialize: function() {
            return K.param(this.serializeArray())
        },
        serializeArray: function() {
            return this.map(function() {
                var t = K.prop(this, "elements");
                return t ? K.makeArray(t) : this
            }).filter(function() {
                var t = this.type;
                return this.name && !K(this).is(":disabled") && je.test(this.nodeName) && !ke.test(t) && (this.checked || !Ct.test(t))
            }).map(function(t, e) {
                var n = K(this).val();
                return null == n ? null : K.isArray(n) ? K.map(n, function(t) {
                    return {
                        name: e.name,
                        value: t.replace(Se, "\r\n")
                    }
                }) : {
                    name: e.name,
                    value: n.replace(Se, "\r\n")
                }
            }).get()
        }
    }),
    K.ajaxSettings.xhr = function() {
        try {
            return new XMLHttpRequest
        } catch (t) {}
    }
    ;
    var Pe = 0
      , Ee = {}
      , Oe = {
        0: 200,
        1223: 204
    }
      , Me = K.ajaxSettings.xhr();
    t.attachEvent && t.attachEvent("onunload", function() {
        for (var t in Ee)
            Ee[t]()
    }),
    Q.cors = !!Me && "withCredentials"in Me,
    Q.ajax = Me = !!Me,
    K.ajaxTransport(function(t) {
        var e;
        return Q.cors || Me && !t.crossDomain ? {
            send: function(n, i) {
                var r, o = t.xhr(), s = ++Pe;
                if (o.open(t.type, t.url, t.async, t.username, t.password),
                t.xhrFields)
                    for (r in t.xhrFields)
                        o[r] = t.xhrFields[r];
                t.mimeType && o.overrideMimeType && o.overrideMimeType(t.mimeType),
                t.crossDomain || n["X-Requested-With"] || (n["X-Requested-With"] = "XMLHttpRequest");
                for (r in n)
                    o.setRequestHeader(r, n[r]);
                e = function(t) {
                    return function() {
                        e && (delete Ee[s],
                        e = o.onload = o.onerror = null,
                        "abort" === t ? o.abort() : "error" === t ? i(o.status, o.statusText) : i(Oe[o.status] || o.status, o.statusText, "string" == typeof o.responseText ? {
                            text: o.responseText
                        } : void 0, o.getAllResponseHeaders()))
                    }
                }
                ,
                o.onload = e(),
                o.onerror = e("error"),
                e = Ee[s] = e("abort");
                try {
                    o.send(t.hasContent && t.data || null)
                } catch (a) {
                    if (e)
                        throw a
                }
            },
            abort: function() {
                e && e()
            }
        } : void 0
    }),
    K.ajaxSetup({
        accepts: {
            script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
        },
        contents: {
            script: /(?:java|ecma)script/
        },
        converters: {
            "text script": function(t) {
                return K.globalEval(t),
                t
            }
        }
    }),
    K.ajaxPrefilter("script", function(t) {
        void 0 === t.cache && (t.cache = !1),
        t.crossDomain && (t.type = "GET")
    }),
    K.ajaxTransport("script", function(t) {
        if (t.crossDomain) {
            var e, n;
            return {
                send: function(i, r) {
                    e = K("<script>").prop({
                        async: !0,
                        charset: t.scriptCharset,
                        src: t.url
                    }).on("load error", n = function(t) {
                        e.remove(),
                        n = null,
                        t && r("error" === t.type ? 404 : 200, t.type)
                    }
                    ),
                    J.head.appendChild(e[0])
                },
                abort: function() {
                    n && n()
                }
            }
        }
    });
    var De = []
      , Ae = /(=)\?(?=&|$)|\?\?/;
    K.ajaxSetup({
        jsonp: "callback",
        jsonpCallback: function() {
            var t = De.pop() || K.expando + "_" + ue++;
            return this[t] = !0,
            t
        }
    }),
    K.ajaxPrefilter("json jsonp", function(e, n, i) {
        var r, o, s, a = e.jsonp !== !1 && (Ae.test(e.url) ? "url" : "string" == typeof e.data && !(e.contentType || "").indexOf("application/x-www-form-urlencoded") && Ae.test(e.data) && "data");
        return a || "jsonp" === e.dataTypes[0] ? (r = e.jsonpCallback = K.isFunction(e.jsonpCallback) ? e.jsonpCallback() : e.jsonpCallback,
        a ? e[a] = e[a].replace(Ae, "$1" + r) : e.jsonp !== !1 && (e.url += (ce.test(e.url) ? "&" : "?") + e.jsonp + "=" + r),
        e.converters["script json"] = function() {
            return s || K.error(r + " was not called"),
            s[0]
        }
        ,
        e.dataTypes[0] = "json",
        o = t[r],
        t[r] = function() {
            s = arguments
        }
        ,
        i.always(function() {
            t[r] = o,
            e[r] && (e.jsonpCallback = n.jsonpCallback,
            De.push(r)),
            s && K.isFunction(o) && o(s[0]),
            s = o = void 0
        }),
        "script") : void 0
    }),
    K.parseHTML = function(t, e, n) {
        if (!t || "string" != typeof t)
            return null;
        "boolean" == typeof e && (n = e,
        e = !1),
        e = e || J;
        var i = st.exec(t)
          , r = !n && [];
        return i ? [e.createElement(i[1])] : (i = K.buildFragment([t], e, r),
        r && r.length && K(r).remove(),
        K.merge([], i.childNodes))
    }
    ;
    var Ne = K.fn.load;
    K.fn.load = function(t, e, n) {
        if ("string" != typeof t && Ne)
            return Ne.apply(this, arguments);
        var i, r, o, s = this, a = t.indexOf(" ");
        return a >= 0 && (i = K.trim(t.slice(a)),
        t = t.slice(0, a)),
        K.isFunction(e) ? (n = e,
        e = void 0) : e && "object" == typeof e && (r = "POST"),
        s.length > 0 && K.ajax({
            url: t,
            type: r,
            dataType: "html",
            data: e
        }).done(function(t) {
            o = arguments,
            s.html(i ? K("<div>").append(K.parseHTML(t)).find(i) : t)
        }).complete(n && function(t, e) {
            s.each(n, o || [t.responseText, e, t])
        }
        ),
        this
    }
    ,
    K.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function(t, e) {
        K.fn[e] = function(t) {
            return this.on(e, t)
        }
    }),
    K.expr.filters.animated = function(t) {
        return K.grep(K.timers, function(e) {
            return t === e.elem
        }).length
    }
    ;
    var Le = t.document.documentElement;
    K.offset = {
        setOffset: function(t, e, n) {
            var i, r, o, s, a, l, u, c = K.css(t, "position"), h = K(t), p = {};
            "static" === c && (t.style.position = "relative"),
            a = h.offset(),
            o = K.css(t, "top"),
            l = K.css(t, "left"),
            u = ("absolute" === c || "fixed" === c) && (o + l).indexOf("auto") > -1,
            u ? (i = h.position(),
            s = i.top,
            r = i.left) : (s = parseFloat(o) || 0,
            r = parseFloat(l) || 0),
            K.isFunction(e) && (e = e.call(t, n, a)),
            null != e.top && (p.top = e.top - a.top + s),
            null != e.left && (p.left = e.left - a.left + r),
            "using"in e ? e.using.call(t, p) : h.css(p)
        }
    },
    K.fn.extend({
        offset: function(t) {
            if (arguments.length)
                return void 0 === t ? this : this.each(function(e) {
                    K.offset.setOffset(this, t, e)
                });
            var e, n, i = this[0], r = {
                top: 0,
                left: 0
            }, o = i && i.ownerDocument;
            if (o)
                return e = o.documentElement,
                K.contains(e, i) ? (typeof i.getBoundingClientRect !== St && (r = i.getBoundingClientRect()),
                n = F(o),
                {
                    top: r.top + n.pageYOffset - e.clientTop,
                    left: r.left + n.pageXOffset - e.clientLeft
                }) : r
        },
        position: function() {
            if (this[0]) {
                var t, e, n = this[0], i = {
                    top: 0,
                    left: 0
                };
                return "fixed" === K.css(n, "position") ? e = n.getBoundingClientRect() : (t = this.offsetParent(),
                e = this.offset(),
                K.nodeName(t[0], "html") || (i = t.offset()),
                i.top += K.css(t[0], "borderTopWidth", !0),
                i.left += K.css(t[0], "borderLeftWidth", !0)),
                {
                    top: e.top - i.top - K.css(n, "marginTop", !0),
                    left: e.left - i.left - K.css(n, "marginLeft", !0)
                }
            }
        },
        offsetParent: function() {
            return this.map(function() {
                for (var t = this.offsetParent || Le; t && !K.nodeName(t, "html") && "static" === K.css(t, "position"); )
                    t = t.offsetParent;
                return t || Le
            })
        }
    }),
    K.each({
        scrollLeft: "pageXOffset",
        scrollTop: "pageYOffset"
    }, function(e, n) {
        var i = "pageYOffset" === n;
        K.fn[e] = function(r) {
            return gt(this, function(e, r, o) {
                var s = F(e);
                return void 0 === o ? s ? s[n] : e[r] : void (s ? s.scrollTo(i ? t.pageXOffset : o, i ? o : t.pageYOffset) : e[r] = o)
            }, e, r, arguments.length, null)
        }
    }),
    K.each(["top", "left"], function(t, e) {
        K.cssHooks[e] = b(Q.pixelPosition, function(t, n) {
            return n ? (n = w(t, e),
            Bt.test(n) ? K(t).position()[e] + "px" : n) : void 0
        })
    }),
    K.each({
        Height: "height",
        Width: "width"
    }, function(t, e) {
        K.each({
            padding: "inner" + t,
            content: e,
            "": "outer" + t
        }, function(n, i) {
            K.fn[i] = function(i, r) {
                var o = arguments.length && (n || "boolean" != typeof i)
                  , s = n || (i === !0 || r === !0 ? "margin" : "border");
                return gt(this, function(e, n, i) {
                    var r;
                    return K.isWindow(e) ? e.document.documentElement["client" + t] : 9 === e.nodeType ? (r = e.documentElement,
                    Math.max(e.body["scroll" + t], r["scroll" + t], e.body["offset" + t], r["offset" + t], r["client" + t])) : void 0 === i ? K.css(e, n, s) : K.style(e, n, i, s)
                }, e, o ? i : void 0, o, null)
            }
        })
    }),
    K.fn.size = function() {
        return this.length
    }
    ,
    K.fn.andSelf = K.fn.addBack,
    "function" == typeof define && define.amd && define("jquery", [], function() {
        return K
    });
    var Re = t.jQuery
      , $e = t.$;
    return K.noConflict = function(e) {
        return t.$ === K && (t.$ = $e),
        e && t.jQuery === K && (t.jQuery = Re),
        K
    }
    ,
    typeof e === St && (t.jQuery = t.$ = K),
    K
}),
define("lib/jquery/dist/jquery", function() {});
