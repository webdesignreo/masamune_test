
/* ==========================================================================
   
   scrollUp

   ========================================================================== */
$(document).ready(function(){
    $("#scrollUp").hide();
    $(function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 200) {
                $('#scrollUp').fadeIn();
            } else {
                $('#scrollUp').stop().fadeOut();
            }
        });
        $('#scrollUp').click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
    });


/* ==========================================================================
   
   Loading

   ========================================================================== */

  $('.spinner').delay(500).fadeOut('slow'); 
  $('#preloader').delay(600).fadeOut('slow'); 
  $('body').delay(500).css({'overflow':'visible'});


/*===================================================================================*/

/*  linkBox  */

/*===================================================================================*/


     $(".linkBox").click(function(){
         if($(this).find("a").attr("target")=="_blank"){
             window.open($(this).find("a").attr("href"), '_blank');
         }else{
             window.location=$(this).find("a").attr("href");
         }
     return false;
     });
})