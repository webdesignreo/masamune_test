'use strict';

var ride = ride || {
  common: {
    data: {},
    init: {},
    initEvent: {},
    defaultEvent: {}
  },
  top: {
    data: {},
    init: {}
  },
  archive: {
    init: {}
  },
  single: {
    init: {}
  },
  works: {
    init: {}
  },
  service: {
    init: {}
  },
  company: {
    data: {},
    init: {}
  },
  about: {
    init: {}
  },
  contact: {
    init: {}
  },
  en: {
    data: {},
    init: {}
  },
  other: {
    init: {}
  }
};




ride.common.data = function () {
  $body = $('body'), $window = $(window), windowW = 0, windowH = 0, loadJudge = 'first', $topMainSlide, scrollTop = 0, footerOffset, sideHeight, sideOffset, saveOffset, diff, downSet, upSet, startPos = 0, fixFlag = false;
};

ride.common.init = function () {
  return {
    action: function action(eachPageJs) {
      ride.common.initEvent.sessionJudge();
      ride.common.initEvent.hashJudge();
      ride.common.initEvent.setBrowser();
      //var svgPoint = ride.common.initEvent.svgDefine();
      //ride.common.initEvent.imgLoad(eachPageJs, svgPoint);
      ride.common.defaultEvent.setSize();
      ride.works.init.setSLider();
      //ride.works.init.action.bind();
      console.log(ride, '-------common.js-------');
      console.log("action return");
    }
  };
}();

ride.common.initEvent = function () {
  return {
    sessionJudge: function sessionJudge() {
      if (window.sessionStorage) {
        var str = window.sessionStorage.getItem("load_key");
        if (str == 'second') {
          $('body').addClass('secondLoad');
        } else {
          window.sessionStorage.setItem("load_key", 'second');
        }
      }
    },
    hashJudge: function hashJudge() {
      if (location.hash != '' && $('body').attr('data-page') != 'en') {
        $(window).on('load', function () {
          var offset = $(window).scrollTop() - 60;
          TweenLite.to(window, 0, { scrollTo: { y: offset } });
        });
      }
    },
    setBrowser: function setBrowser() {
      var judgeLotate, _this;
      _this = window;
      _this.ua = function () {
        return {
          Safari: typeof window.chrome == 'undefined' && 'WebkitAppearance' in document.documentElement.style,
          Firefox: 'MozAppearance' in document.documentElement.style,
          IE: window.navigator.userAgent.toLowerCase().indexOf('msie') !== -1 || window.navigator.userAgent.toLowerCase().indexOf('trident') !== -1,
          IE9: window.navigator.appVersion.toLowerCase().indexOf('msie 9.') !== -1,
          ltIE9: typeof window.addEventListener === 'undefined' && typeof document.getElementsByClassName === 'undefined',
          Touch: typeof document.ontouchstart !== "undefined",
          Pointer: window.navigator.pointerEnabled,
          MSPoniter: window.navigator.msPointerEnabled,
          Windows: window.navigator.userAgent.toLowerCase().indexOf('win') !== -1,
          Mac: window.navigator.userAgent.toLowerCase().indexOf('mac') !== -1
        };
      }();
      if (window.ua.Safari) {
        $('html').addClass('safari');
      }
      if (window.ua.Firefox) {
        $('html').addClass('firefox');
      }
      if (window.ua.IE) {
        $('html').addClass('ie');
      }
      if (window.ua.IE9) {
        $('html').addClass('ie9');
      }
      if (window.ua.ltIE9) {
        $('html').addClass('ie8');
      }
      if (window.ua.Touch) {
        $('html').addClass('tablet');
      }
    },
    imgLoad: function imgLoad(eachPageJs, pointResult) {
      TweenLite.to('.commonLoader_svg .i1', 0.3, { 'ease': Power4.easeOut, 'width': '100%', onComplete: function onComplete() {
          var $img = $('img');
          var total = $img.length;
          if (total > 0) {
            var count = 0;
            $img.each(function () {
              var myImg = new Image();
              if (ua.ltIE9) {
                myImg.src = $(this).attr('src') + "?" + new Date().getTime();
              } else {
                myImg.src = $(this).attr('src');
              }
              myImg.onerror = function () {
                ride.common.initEvent.callback(eachPageJs, pointResult);
              };
              myImg.onload = function () {
                count++;
                if (count == total) {
                  ride.common.initEvent.callback(eachPageJs, pointResult);
                }
              };
            });
          } else {
            ride.common.initEvent.callback(eachPageJs, pointResult);
          }
        } });
    },
    callback: function callback(eachPageJs, pointResult) {
      var fLastPoint = pointResult[0];
      var setPosition = pointResult[1];
      var drawingPath = pointResult[2];
      eachPageJs();


      if ($('body').is('.secondLoad') || $('body').attr('data-page') != 'top' && $('body').attr('data-page') != 'en') {
        TweenLite.to('.commonLoader_svg .i2', 0.3, { 'ease': Power3.easeOut, 'height': '100%', onComplete: function onComplete() {
            TweenLite.to('.commonLoader_svg .i3', 0.3, { 'ease': Power3.easeOut, 'width': '100%', onComplete: function onComplete() {
                TweenLite.to('.commonLoader_svg .i4', 0.3, { 'ease': Power3.easeOut, 'height': '100%', onComplete: function onComplete() {
                    if ($('body').attr('data-page') == 'top' || $('body').attr('data-page') == 'en') {
                      $('.commonLoader').addClass('bgHide');
                      setTimeout(function () {
                        $('.commonLoader').css({
                          'width': 140,
                          'height': 140,
                          'position': 'absolute'
                        });
                      }, 1000);
                    } else {
                      $('.commonLoader').stop(true, true).fadeOut(800);
                    }
                  } });
              } });
          } });
      } else {
        Snap.animate(0, fLastPoint, setPosition, 2000, mina.easeOut, function () {
          drawingPath.attr();
          $('.commonLoader').addClass('bgOut');
          setTimeout(function () {
            $('.commonLoader').css({
              'transition': 'none'
            });
            if ($('body').attr('data-page') == 'top' || $('body').attr('data-page') == 'en') {
              $('.commonLoader').css({
                'width': 140,
                'height': 140,
                'position': 'absolute'
              });
            } else {
              $('.commonLoader').stop(true, true).fadeOut(300);
            }
            if ($('.mainVisual_list')[0]) {
              $('.mainVisual_list').slick('slickPlay');
            }
          }, 3000);
        });
      }

      $(window).resize(function () {
        ride.common.defaultEvent.resize();
      });
      $(window).scroll(function () {
        ride.common.defaultEvent.scroll();
      });
      ride.common.defaultEvent.resize();
      ride.common.defaultEvent.scroll();
      ride.common.defaultEvent.anchor();
      if ($('.header_lang')[0]) {
        ride.common.defaultEvent.langChange();
      }
      if ($('body').attr('data-page') == 'top') {
        ride.common.defaultEvent.sidebar();
        $(window).scroll(function () {
          ride.common.defaultEvent.sidebar();
        });
      }
      ride.common.defaultEvent.setevents();
    }
  };
}();

ride.common.defaultEvent = function () {
  return {
    resize: function resize() {
      ride.common.defaultEvent.setSize();

      if ($('body').attr('data-page') == 'top' || $('body').attr('data-page') == 'en') {
        $('.wrapper').css('margin-top', ride.common.data.windowH);
        $('#header').css('margin-top', ride.common.data.windowH - 60);
      }

      ride.common.data.footerOffset = $('.footer').offset().top - ride.common.data.windowH;
      if ($('.column_right_inner')[0]) {
        ride.common.data.sideHeight = $('.column_right_inner').height();
      }
    },
    setSize: function setSize() {
      ride.common.data.windowW = $(window).width();
      ride.common.data.windowH = $(window).height();
    },
    scroll: function scroll() {
      ride.common.data.scrollTop = $(window).scrollTop();
      if ($('body').attr('data-page') == 'top' || $('body').attr('data-page') == 'en') {
        if (ride.common.data.scrollTop > ride.common.data.windowH - 60) {
          $('body').addClass('fixed');
        } else {
          $('body').removeClass('fixed');
        }
      };
    },
    sidebar: function sidebar() {
      var _this = this;
      ride.common.defaultEvent.resize();
      var $sidebar = $('.column_right_inner');

      // ウィンドウの高さよりサイドバーが小さいかどうかの判定
      if (window.ua.Touch) {
        $sidebar.css({
          'position': 'absolute',
          'top': 0,
          'bottom': 'auto'
        });
      } else if (ride.common.data.sideHeight < ride.common.data.windowH) {
        /* footerが出現したら */
        if (ride.common.data.scrollTop > ride.common.data.footerOffset) {
          $sidebar.css({
            'position': 'absolute',
            'top': 'auto',
            'bottom': -30
          });
          // スクロール値がウィンドウの高さを超えたら
        } else if (ride.common.data.scrollTop > ride.common.data.windowH - 60) {
          $sidebar.css({
            'position': 'fixed',
            'top': 80,
            'bottom': 'auto'
          });
        } else {
          $sidebar.css({
            'position': 'absolute',
            'top': 0,
            'bottom': 'auto'
          });
        }
      } else {
        // scroll top
        if (ride.common.data.saveOffset - ride.common.data.scrollTop > 0) {
          ride.common.data.downSet = false;
          ride.common.data.sideOffset = $('.column_right_inner').offset().top;

          /* footerが出現したら */
          if (ride.common.data.scrollTop > ride.common.data.footerOffset) {
            ride.common.data.upSet = true;
            $sidebar.css({
              'position': 'absolute',
              'top': 'auto',
              'bottom': -30
            });
            /* サイドバーの底がウィンドウ下についたら */
          } else if (ride.common.data.scrollTop > ride.common.data.windowH) {
            if (ride.common.data.upSet) {
              ride.common.data.diff = ride.common.data.scrollTop;
              $sidebar.css({
                'position': 'absolute',
                'top': ride.common.data.sideOffset - ride.common.data.windowH,
                'bottom': 'auto'
              });
            } else {
              // ポジションを指定してからのスクロールの差異がサイドバーのあまりを超えたとき固定
              if (ride.common.data.scrollTop + 30 - ride.common.data.diff < ride.common.data.windowH - ride.common.data.sideHeight) {
                $sidebar.css({
                  'position': 'fixed',
                  'top': 80,
                  'bottom': 'auto'
                });
              }
            }
            ride.common.data.upSet = false;
            /* デフォルト */
          } else if (ride.common.data.scrollTop < ride.common.data.windowH) {
            ride.common.data.upSet = true;
            $sidebar.css({
              'position': 'absolute',
              'top': 30,
              'bottom': 'auto'
            });
          }
        } else {
          // scroll down
          ride.common.data.upSet = true;
          /* footerが出現したら */
          if (ride.common.data.scrollTop > ride.common.data.footerOffset) {
            $sidebar.css({
              'position': 'absolute',
              'top': 'auto',
              'bottom': -30
            });
            /* サイドバーの底がウィンドウ下についたら */
          } else if (ride.common.data.scrollTop > ride.common.data.sideHeight) {
            if (ride.common.data.downSet) {
              // if(ride.common.data.diff-(ride.common.data.scrollTop-60) < ride.common.data.windowH-ride.common.data.sideHeight){
              $sidebar.css({
                'position': 'fixed',
                'top': 'auto',
                'bottom': 0
              });
              // }
            } else {
              ride.common.data.diff = ride.common.data.scrollTop;
              $sidebar.css({
                'position': 'absolute',
                'top': ride.common.data.sideOffset - ride.common.data.windowH,
                'bottom': 'auto'
              });
              ride.common.data.downSet = true;
            }

            /* デフォルト */
          } else if (ride.common.data.scrollTop < ride.common.data.sideHeight) {
            $sidebar.css({
              'position': 'absolute',
              'top': 30,
              'bottom': 'auto'
            });
          }
        }
      }

      ride.common.data.saveOffset = ride.common.data.scrollTop;
    },
    anchor: function anchor() {
      $(document).on('click', '.anchorLink', function (e) {
        console.log('click');
        var targetLink = $(this).attr('href');
        if (targetLink === '' || targetLink === '#') {
          var offset = 0;
        } else {
          var offset = $(targetLink).offset();
          offset = offset.top - 60;
        };
        var speed = 800;
        TweenLite.to(window, 1, { scrollTo: { y: offset }, ease: Power4.easeOut });
        return false;
      });
    },
    langChange: function langChange() {
      var lastPos = 0,
          pos,
          langFlag = 'jp',
          canvasX = 48,
          canvasY = 24;

      var s = Snap(canvasX, canvasY).attr({ viewBox: [0, 0, canvasX, canvasY], id: "circleObj" });
      var langChange_svg = document.querySelector('.header_lang');
      s.prependTo(langChange_svg);
      var textStyle = {
        fill: "#999",
        stroke: "none",
        textAnchor: "middle",
        dominantBaseline: "middle"
      };

      var langText = s.text(canvasX / 2 - 1, canvasY / 2 + 1, ['JP', '　', 'EN']).attr(textStyle);
      langText.clone();

      s.rect(0, 0, canvasX, canvasY).attr({ fill: "none", stroke: "#999", strokeWidth: 1 });

      var knobGroup = s.g();
      var knob = knobGroup.rect(0, 0, canvasY, canvasY).attr({ fill: "#999" });
      var knobText = knobGroup.rect(0, 0, canvasY, canvasY).attr({ fill: "#fff", clipPath: langText });

      knobGroup.drag(function (dx, dy, x, y) {
        pos = x - (ride.common.data.windowW - (canvasX + 10)) - canvasX / 4;
        var attrPos = {
          x: pos
        };
        knob.attr(attrPos);
        knobText.attr(attrPos);
      }, function (x, y, e) {
        pos = x - (ride.common.data.windowW - (canvasX + 10)) - canvasX / 4;
        var attrPos = {
          x: pos
        };
        knob.attr(attrPos);
        knobText.attr(attrPos);
        knobGroup.stop();
      }, function () {
        var lastPos = pos;
        lastPos = lastPos < 0 ? 0 : lastPos > 24 ? 24 : lastPos;
        var lastPos = Snap.snapTo(canvasX / 2, lastPos, canvasX / 4);
        langChosen(lastPos);
      });

      s.click(function (e) {
        pos = langFlag == 'jp' ? 0 : canvasX / 2;
        lastPos = e.screenX - (ride.common.data.windowW - (canvasX + 10));
        lastPos = lastPos > canvasX / 2 ? canvasX / 2 : 0;
        langChosen(lastPos);
      });

      function langChosen(lastPos) {
        Snap.animate(pos, lastPos, function (movePos) {
          var attrPos = {
            x: movePos
          };
          knob.attr(attrPos);
          knobText.attr(attrPos);
        }, 500, mina.bounce, function () {
          pos = lastPos;
          if (lastPos == 0) {
            // jp
            langFlag = 'jp';
          } else {
            // en
            langFlag = 'en';
          }
          // location.href = '';
        });
      }
    },
    setevents: function setevents() {
      var $headerNav = $('.header_openNav');
      $('.header_menu').on('click', function () {
        if ($('body').is('.navOpen')) {
          $('body').removeClass('navOpen');
          TweenLite.to($headerNav, 0.3, { right: '-94%', alpha: 0 });
        } else {
          $('body').addClass('navOpen');
          TweenLite.to($headerNav, 0.3, { right: 0, alpha: 1, onComplete: function onComplete() {} });
        }
        return false;
      });

      var $topicsSlide = $('.picktopics_list');
      var arrow = {
        left: '/images/common/i_arrow_left.png',
        right: '/images/common/i_arrow_right.png'
      };
      if ($topicsSlide) {
        $topicsSlide.slick({
          speed: 800,
          autoplay: true,
          nextArrow: '<div class="slick-next"></div>',
          prevArrow: '<div class="slick-prev"></div>',
          dots: true,
          pauseOnHover: false
        });
        $topicsSlide.on('setPosition', function (event, slick, currentSlide, nextSlide) {});
      }
    },

    titleAnimationEvent: function titleAnimationEvent() {
      var scrollTop;
      var $window = $(window);
      var winH = $window.height();
      var winW = $window.width();

      function Inview(target) {
        this.target = target;
        this.inviewInit();
        this.inviewHandler();
      }

      Inview.prototype.inviewInit = function () {
        this.titlePos = this.target.offset().top;
      };

      Inview.prototype.inviewEvent = function () {
        if (scrollTop + winH / 2 > this.titlePos) {
          this.target.addClass('inview');
        }
      };

      Inview.prototype.inviewHandler = function () {
        var _this = this;
        scrollTop = $(document).scrollTop();
        $(window).on('resize', function () {
          scrollTop = $(document).scrollTop();
          _this.inviewEvent();
        });
        $(window).on("load", function () {
          _this.inviewInit();
        });
        $(window).on('scroll', function () {
          scrollTop = $(document).scrollTop();
          _this.inviewEvent();
        });
      };

      $('.borderAnimation').each(function () {
        var instance = new Inview($(this));
      });
    }
  };
}();
